package jejusmarttour.parser;

import android.util.Log;

import java.util.ArrayList;

import jejusmarttour.vo.ScheduleVO;

public class ScheduleParser
{
	// 날짜별로 스케쥴 삭제
	public static ArrayList < ScheduleVO > deleteSchedule ( ArrayList < ScheduleVO > scheduleVOArrayList , int position )
	{
		ArrayList < ScheduleVO > deletedSchedule = new ArrayList < ScheduleVO > ( );

		for ( int i = 0; i < scheduleVOArrayList.size ( ); i++ )
		{
			int day = Integer.valueOf ( scheduleVOArrayList.get ( i ).getDay ( ) );

			if ( day == position )
			{
				deletedSchedule.add ( scheduleVOArrayList.get ( i ) );
			}
		}

		scheduleVOArrayList.removeAll ( deletedSchedule );

		int count = scheduleVOArrayList.size ( );

		for ( int i = 0; i < count; i++ )
		{
			if ( count == 0 )
				return scheduleVOArrayList;

			if ( position == 1 )
			{
				scheduleVOArrayList.get ( i ).setDays ( ( Integer.valueOf ( scheduleVOArrayList.get ( i ).getDays ( ) ) - 1 ) + "" );
				scheduleVOArrayList.get ( i ).setDay ( ( Integer.valueOf ( scheduleVOArrayList.get ( i ).getDay ( ) ) - 1 ) + "" );
			}
			else if ( position > 1 )
			{
				scheduleVOArrayList.get ( i ).setDays ( ( Integer.valueOf ( scheduleVOArrayList.get ( i ).getDays ( ) ) - 1 ) + "" );

				if ( Integer.valueOf ( scheduleVOArrayList.get ( i ).getDay ( ) ) > position )
				{
					scheduleVOArrayList.get ( i ).setDay ( ( Integer.valueOf ( scheduleVOArrayList.get ( i ).getDay ( ) ) - 1 ) + "" );
				}
			}
		}

		return scheduleVOArrayList;
	}

	// 날짜별로 스케쥴 가져오기
	public static ArrayList < ScheduleVO > getSchedule ( ArrayList < ScheduleVO > scheduleVOArrayList , int day ){
		Log.e("ScheduleParser", "getSchedule start" );
		if ( day == 0 )
			return scheduleVOArrayList;

		ArrayList < ScheduleVO > daySchedule = new ArrayList < ScheduleVO > ( );

		for ( int i = 0; i < scheduleVOArrayList.size ( ); i++ ){
			if ( Integer.valueOf ( scheduleVOArrayList.get ( i ).getDay ( ) ) == day ){
				daySchedule.add ( scheduleVOArrayList.get ( i ) );
			}
		}

		return daySchedule;
	}
}
