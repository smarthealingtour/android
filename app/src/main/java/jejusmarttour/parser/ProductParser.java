package jejusmarttour.parser;

import java.util.ArrayList;

import jejusmarttour.vo.DistanceVO;
import jejusmarttour.vo.MemoVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import jejusmarttour.vo.UserTourProductVO;


public class ProductParser
{
	public static SmartTourProductsVO scheduleDataParse ( TourProductVO tourProductVO , ArrayList <ScheduleVO> scheduleArray )
	{
		if ( tourProductVO == null && scheduleArray == null )
			return null;

		SmartTourProductsVO smartTourProductsVO = new SmartTourProductsVO ( );
		ArrayList < ScheduleVO > scheduleArrayList = new ArrayList < ScheduleVO > ( );

		for ( int i = 0 ; i < scheduleArray.size ( ) ; i++ )
		{
			scheduleArrayList.add ( scheduleArray.get ( i ) );
		}

		smartTourProductsVO.setTourProductVO(tourProductVO);
		smartTourProductsVO.setScheduleArrayList(scheduleArrayList);

		return smartTourProductsVO;
	}
}
