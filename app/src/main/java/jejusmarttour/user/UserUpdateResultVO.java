package jejusmarttour.user;

public class UserUpdateResultVO
{
	private String nickname;
	// 이메일 필드
	private String email;
	// success , duplicate
	private String result;
	// email exist , nickname exist
	private String description;

	public String getNickname ( )
	{
		return nickname;
	}

	public void setNickname ( String nickname )
	{
		this.nickname = nickname;
	}

	public String getEmail ( )
	{
		return email;
	}

	public void setEmail ( String email )
	{
		this.email = email;
	}

	public String getResult ( )
	{
		return result;
	}

	public void setResult ( String result )
	{
		this.result = result;
	}

	public String getDescription ( )
	{
		return description;
	}

	public void setDescription ( String description )
	{
		this.description = description;
	}

}
