package jejusmarttour.user;

import jejusmarttour.util.SmartTourUtils;

public class UserVO{
	// 검색결과
	private String count = null; // 0:없음 1:있음
	private String save_htour = null;
	private String save_hcnt = null;
	private String nickname = null;
	private String email = null;
	private String imageAddress = null;
	private String hcd = null;
	private String scd = null;

	public boolean hasKeywords ( ){
		if ( getHcdArray ( ) == null && getScdArray ( ) == null ) {
			return false;
		}
		else {
			return true;
		}
	}

	public String getCount ( ){
		return count;
	}

	public void setCount ( String count ){
		this.count = count;
	}

	public String getSave_htour ( ){
		return save_htour;
	}

	public void setSave_htour ( String save_htour ){
		this.save_htour = save_htour;
	}

	public String getSave_hcnt ( ){
		return save_hcnt;
	}

	public void setSave_hcnt ( String save_hcnt ){
		this.save_hcnt = save_hcnt;
	}

	public String getNickname ( ){
		return nickname;
	}

	public void setNickname ( String nickname )
	{
		this.nickname = nickname;
	}

	public String getEmail ( ){
		return email;
	}

	public void setEmail ( String email ){
		this.email = email;
	}

	public String [ ] getSave_htourArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getSave_htour ( ) );
	}

	public String [ ] getSave_hcntArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getSave_hcnt ( ) );
	}

	public String getImageAddress ( ){
		return imageAddress;
	}

	public void setImageAddress ( String imageAddress ){
		this.imageAddress = imageAddress;
	}

	public String getHcd ( ){
		return hcd;
	}

	public void setHcd ( String hcd ){
		this.hcd = hcd;
	}

	public String getScd ( ){
		return scd;
	}

	public void setScd ( String scd ){
		this.scd = scd;
	}

	public String [ ] getScdArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getScd ( ) );
	}

	public String [ ] getHcdArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getHcd ( ) );
	}

}
