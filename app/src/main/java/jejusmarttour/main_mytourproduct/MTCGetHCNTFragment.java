package jejusmarttour.main_mytourproduct;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.MTCHealingContentsTask;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import syl.com.jejusmarttour.R;

public class MTCGetHCNTFragment extends Fragment implements JSONArrayResult , OnScrollListener
{
	private static final String TAG = MTCGetHCNTFragment.class.toString ( );

	private String searchStr;

	private MTCHealingContentsTask mtcHealingContentsTask;
	private SearchProductRequestVO vo;

	private MTCGetHCNTSListViewAdapter listViewAdapter;
	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private ArrayList < HealingContentsVO > listData;
	private TextView countView;

	public static MTCGetHCNTFragment newInstance ( )
	{
		MTCGetHCNTFragment fragment = new MTCGetHCNTFragment ( );
		return fragment;
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState )
	{
		View view = null;

		try
		{
			view = inflater.inflate ( R.layout.fragment_mtc_get_cnt_view , container , false );
		}
		catch ( InflateException e )
		{
			e.printStackTrace ( );
		}

		listData = new ArrayList < HealingContentsVO > ( );
		lockListView = true;

		listViewAdapter = new MTCGetHCNTSListViewAdapter ( getActivity ( ) , listData , onCheckedChangeListener );
		ListView listView = ( ListView ) view.findViewById ( R.id.list_view);
		listView.setChoiceMode ( ListView.CHOICE_MODE_MULTIPLE );
		listView.setOnScrollListener ( this );
		listView.setAdapter ( listViewAdapter );

		// ListView 아이템 터치 시 이벤트 추가
		listView.setOnItemClickListener ( onClickListItem );

		executeTask ( );

		return view;
	}

	// 검색 실행
	private void executeTask ( )
	{
		mtcHealingContentsTask = new MTCHealingContentsTask ( getActivity ( ) , this );
		mtcHealingContentsTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( ) );
	}

	// 요청할 파라미터
	private String [ ] getParam ( )
	{
		String [ ] param = { "hcnt" , String.valueOf ( requestPageNumber ) };
		return param;
	}

	@Override
	public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount )
	{
		int lastInScreen = firstVisibleItem + visibleItemCount;

		if ( ( lastInScreen == totalItemCount ) && lockListView == false )
		{
			currentPageNumber += 1;
			requestPageNumber += 1;

			executeTask ( );

			lockListView = true;
		}
	}

	@Override
	public void onScrollStateChanged ( AbsListView arg0 , int arg1 )
	{

	}

	private void InsertItemToListView ( ArrayList < HealingContentsVO > resultList )
	{
		for ( int i = 0; i < resultList.size ( ); i++ )
		{
			listViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		listViewAdapter.notifyDataSetChanged ( );
	}

	// 아이템 터치 이벤트
	private OnItemClickListener onClickListItem = new OnItemClickListener ( )
	{
		@Override
		public void onItemClick ( AdapterView < ? > arg0 , View view , int position , long arg3 )
		{
			ArrayList < ScheduleVO > scheduleVOArrayList = ((CommonData)getActivity().getApplication()).getMakeScheduleArrayList();

			ScheduleVO scheduleVO = new ScheduleVO ( );
			scheduleVO.setHealingContentsVO( listData.get ( position ) );

			if ( ( ( CheckBox ) ( ( LinearLayout ) view ).getChildAt ( 1 ) ).isChecked ( ) == true )
			{
				( ( CheckBox ) ( ( LinearLayout ) view ).getChildAt ( 1 ) ).setChecked ( false );

				for ( int i = 0; i < scheduleVOArrayList.size ( ); i++ )
				{
					if ( scheduleVOArrayList.get ( i ).getHealingContentsVO( ) == scheduleVO.getHealingContentsVO( ) )
					{
						scheduleVOArrayList.remove ( i );
					}
				}
			}
			else
			{
				( ( CheckBox ) ( ( LinearLayout ) view ).getChildAt ( 1 ) ).setChecked ( true );
				scheduleVOArrayList.add ( scheduleVO );
			}

			((CommonData)getActivity().getApplication()).setMakeScheduleArrayList ( scheduleVOArrayList );

			// CommonData.setScheduleVO ( scheduleVO );

			// Intent intent = new Intent ( getActivity ( ) ,
			// DetailInfoActivity.class );
			// startActivity ( intent );
		}
	};

	@Override
	public void setJSONArrayResult ( ArrayList resultList )
	{
		lockListView = true;
		mtcHealingContentsTask.cancel ( true );

		if ( resultList == null )
		{
			return;
		}
		else
		{
			InsertItemToListView ( resultList );
		}

		lockListView = false;
	}

	CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener ( )
	{
		@Override
		public void onCheckedChanged ( CompoundButton buttonView , boolean isChecked )
		{
			if ( isChecked == true )
			{

			}
			else
			{

			}
		}
	};

}
