package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import jejusmarttour.util.code_data.SpotSpinnerTree;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

public class MTCAddSiteActivity extends AppCompatActivity implements AddFromMyList{
	private static final String TAG = MTCAddSiteActivity.class.toString ( );

	private AddFromAllSpotListFragment tourSpotFragment;

	private Spinner spinner1;
	private Spinner spinner2;
	private Spinner spinner3;

	private String code = "";
	//스피너 전체가 선택될때 태스크 한번더 실행되니까 캐치해내려구 만듬
	private String excutedCode = "";

	private GetCheckedDataFromFragment gotCheckedData;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_sy_healing_tour_product );

		initSpinner();
		initFragment();
		initActionBar();
	}

	private void initSpinner(){
		spinner1 = (Spinner)findViewById(R.id.spinner1);
		spinner2 = (Spinner)findViewById(R.id.spinner2);
		spinner3 = (Spinner)findViewById(R.id.spinner3);

		final Resources.Theme theme = getResources().newTheme();
		theme.applyStyle(R.style.SpinnerDropDownStyle, true);

		SpotSpinnerTree spotSpinnerTree = SpotSpinnerTree.getInstance();

		ArrayAdapter<ArrayList<String>> adapter1 = new ArrayAdapter<ArrayList<String>>(this, R.layout.spinner_layout,
				spotSpinnerTree.getList(-1,-1)
			/*convertNodesToList.getNameArray( rootNode.getChildList())*/
			/*spotSpinnerTree.getChildNameList(rootNode)*/);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		ArrayAdapter< ArrayList<String>> adapter2 = new ArrayAdapter<ArrayList<String>>(this, R.layout.spinner_layout, new ArrayList());
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		ArrayAdapter< ArrayList<String>> adapter3 = new ArrayAdapter<ArrayList<String>>(this, R.layout.spinner_layout, new ArrayList());
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
		spinner3.setAdapter(adapter3);

		spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
				adapter2.clear();
				adapter3.clear();
				spinner2.setSelection(0);
				spinner3.setSelection(0);
				code = spotSpinnerTree.getCode( position,-1,-1);

				switch ( position ){
					case 0:
						setCode(code);
						adapter2.addAll( spotSpinnerTree.getList( position, -1) );
						break;
					case 1:
						setCode(code);
						adapter2.addAll( spotSpinnerTree.getList( position, -1) );
						break;
					case 2:
						setCode(code);
						adapter2.addAll( spotSpinnerTree.getList( position, -1) );
						break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
			}
		});

		spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				spinner3.setBackgroundResource(R.drawable.bg_spinner);
				adapter3.clear();
				spinner3.setSelection(0);

				if (position == 0){
					spinner3.setBackgroundResource(R.drawable.bg_inactive_spinner);
				}

				if ( code.substring(0,1).equals("1")){
					code = spotSpinnerTree.getCode( 0, position, -1);
					setCode(code);

					if (position == 1){
						adapter3.addAll( spotSpinnerTree.getList( 0, position));
					}
					else if (position == 3){
						adapter3.addAll( spotSpinnerTree.getList( 0, position));
					}
					else if (position == 8){
						adapter3.addAll( spotSpinnerTree.getList( 0, position));
					}
					else {
						spinner3.setBackgroundResource(R.drawable.bg_inactive_spinner);
					}
				}
				else if ( code.substring(0,1).equals("2")){
					code = spotSpinnerTree.getCode( 1, position, -1);
					setCode(code);

					adapter3.addAll( spotSpinnerTree.getList( 1, position));
				}
				else if ( code.substring(0,1).equals("3")){
					code = spotSpinnerTree.getCode( 2, position, -1);

					setCode(code);
					adapter3.addAll( spotSpinnerTree.getList( 2, position));
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				code = spotSpinnerTree.getCode(code, position);
				setCode(code);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
	}

	private void setCode(String code){
		//실행됬었던 코드가 실행하려는 코드와 같으면 테스크 실행 x
		if ( !excutedCode.equals(code) ){
			if ( !code.substring(0,1).equals("3") ){
				tourSpotFragment.setScdAndTaskStart( code);
			}
			else {
				tourSpotFragment.setHcdAndTaskStart( code);
			}
			tourSpotFragment.allCheckOff();
		}
		excutedCode = code;
	}

	private void initFragment(){
		tourSpotFragment = new AddFromAllSpotListFragment( );

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace( R.id.viewlist, tourSpotFragment);
		fragmentTransaction.commit();
	}

	// 액션바 설정
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_make_my_tour_course , null );
		ImageView cancelBtn = customView.findViewById ( R.id.cancel_btn );
		ImageView saveBtn = customView.findViewById ( R.id.save_btn );
		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( getResources ( ).getString ( R.string.title_activity_mtcadd_site ) );

		cancelBtn.setOnClickListener (view -> finish ( ));
		saveBtn.setOnClickListener(view -> {
			ArrayList<ScheduleVO> saveData = new ArrayList<>();

			saveData.addAll(gotCheckedData.getCheckedData());

			Log.e(TAG, "saveData.size(): " + saveData.size());

			Intent intent = getIntent ( );

			((CommonData)getApplication()).setMakeScheduleArrayList( saveData );
			setResult ( RESULT_OK , intent );
			finishActivity ( ScheduleManegeFragment.REQUEST_CODE_SITE );
			finish ( );
        });

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	@Override
	public void checkedDataSendToActivity(GetCheckedDataFromFragment getCheckedDataFromFragment) {
		gotCheckedData = getCheckedDataFromFragment;
	}

	/*private class PageListener implements ViewPager.OnPageChangeListener{

		@Override
		public void onPageScrolled ( int position , float positionOffset , int positionOffsetPixels ){
		}

		@Override
		public void onPageScrollStateChanged ( int state ){
		}

		@Override
		public void onPageSelected ( int position ){
			setChangeTabItem ( position );
		}

	}*/

	/*private ToggleButton [ ] getToggleButtons ( ){
		ToggleButton [ ] buttons = { ( ToggleButton ) radioGroupTab.getChildAt ( 0 ) , ( ToggleButton ) radioGroupTab.getChildAt ( 1 ) };
		return buttons;
	}

	private void setChangeTabItem ( int position ){
		getToggleButtons ( ) [ position ].setChecked ( true );
		validateToggleButton( position );
	}

	private void validateToggleButton(int position ){
		for ( int i = 0 ; i < getToggleButtons ( ).length ; i++ ){
			if ( i != position )
				getToggleButtons ( ) [ i ].setChecked ( false );
		}
	}*/

	/*// 뷰페이지 어뎁터
	class ViewPagerAdapter extends FragmentPagerAdapter{
		private int position = 0;

		public ViewPagerAdapter ( FragmentManager fm ){
			super ( fm );
		}

		@Override
		public Fragment getItem ( int position ){
			if ( position < 0 || MAX_PAGE <= position )
				return null;

			this.position = position;

			switch ( position ){
				case 0 :
					cur_fragment = new MTCGetAllHCNTFragment ( ).newInstance ( );
					break;
				case 1 :
					cur_fragment = new MTCGetAllHCNTFragment( ).newInstance ( );
					break;
				case 2 :
					cur_fragment = new MTCGetAllHCNTFragment( ).newInstance ( );
					break;
			}
			return cur_fragment;
		}

		public int getCurrentPosition ( ){
			return position;
		}

		@Override
		public int getCount ( ){
			return MAX_PAGE;
		}

		@Override
		public CharSequence getPageTitle ( int position ){
			return getResources ( ).getStringArray ( R.array.htp_main_titles ) [ position ];
		}
	}*/
}
