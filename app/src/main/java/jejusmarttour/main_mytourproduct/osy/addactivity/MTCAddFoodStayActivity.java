package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

public class MTCAddFoodStayActivity extends AppCompatActivity implements AddFromMyList {
	private static final String TAG = MTCAddFoodStayActivity.class.toString ( );

	private String fragmentType;

	private GetCheckedDataFromFragment gotCheckedData;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_sy_healing_tour_product );

		fragmentType = getIntent().getStringExtra("type");

		LinearLayout spinners = (LinearLayout) findViewById(R.id.spinners);
		spinners.setVisibility(View.GONE);

		findViewById(R.id.spinner_below).setVisibility(View.GONE);

		initFragment();
		initActionBar();
	}

	private void initFragment(){
		Fragment fragment;
		if ( fragmentType.equals("food") )
			fragment = new AddFoodFragment();
		else
			fragment = new AddStayFragment();

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace( R.id.viewlist, fragment);
		fragmentTransaction.commit();
	}

	// 액션바 설정
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_make_my_tour_course , null );
		ImageView cancelBtn = customView.findViewById ( R.id.cancel_btn );
		ImageView saveBtn = customView.findViewById ( R.id.save_btn );
		TextView titleTextView = customView.findViewById ( R.id.title_text );

		if(fragmentType.equals("food")) 	titleTextView.setText ( "음식점 추가" );
		else 								titleTextView.setText ( "숙소 추가" );


		cancelBtn.setOnClickListener (view -> finish ( ));
		saveBtn.setOnClickListener(view -> {
			ArrayList<ScheduleVO> saveData = new ArrayList<>();

			saveData.addAll(gotCheckedData.getCheckedData());

			Log.e(TAG, "saveData.size(): " + saveData.size());

			Intent intent = getIntent ( );

			((CommonData)getApplication()).setMakeScheduleArrayList( saveData );
			setResult ( RESULT_OK , intent );
			finishActivity ( ScheduleManegeFragment.REQUEST_CODE_SITE );
			finish ( );
        });

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	@Override
	public void checkedDataSendToActivity(GetCheckedDataFromFragment getCheckedDataFromFragment) {
		gotCheckedData = getCheckedDataFromFragment;
	}

	/*private class PageListener implements ViewPager.OnPageChangeListener{

		@Override
		public void onPageScrolled ( int position , float positionOffset , int positionOffsetPixels ){
		}

		@Override
		public void onPageScrollStateChanged ( int state ){
		}

		@Override
		public void onPageSelected ( int position ){
			setChangeTabItem ( position );
		}

	}*/

	/*private ToggleButton [ ] getToggleButtons ( ){
		ToggleButton [ ] buttons = { ( ToggleButton ) radioGroupTab.getChildAt ( 0 ) , ( ToggleButton ) radioGroupTab.getChildAt ( 1 ) };
		return buttons;
	}

	private void setChangeTabItem ( int position ){
		getToggleButtons ( ) [ position ].setChecked ( true );
		validateToggleButton( position );
	}

	private void validateToggleButton(int position ){
		for ( int i = 0 ; i < getToggleButtons ( ).length ; i++ ){
			if ( i != position )
				getToggleButtons ( ) [ i ].setChecked ( false );
		}
	}*/

	/*// 뷰페이지 어뎁터
	class ViewPagerAdapter extends FragmentPagerAdapter{
		private int position = 0;

		public ViewPagerAdapter ( FragmentManager fm ){
			super ( fm );
		}

		@Override
		public Fragment getItem ( int position ){
			if ( position < 0 || MAX_PAGE <= position )
				return null;

			this.position = position;

			switch ( position ){
				case 0 :
					cur_fragment = new MTCGetAllHCNTFragment ( ).newInstance ( );
					break;
				case 1 :
					cur_fragment = new MTCGetAllHCNTFragment( ).newInstance ( );
					break;
				case 2 :
					cur_fragment = new MTCGetAllHCNTFragment( ).newInstance ( );
					break;
			}
			return cur_fragment;
		}

		public int getCurrentPosition ( ){
			return position;
		}

		@Override
		public int getCount ( ){
			return MAX_PAGE;
		}

		@Override
		public CharSequence getPageTitle ( int position ){
			return getResources ( ).getStringArray ( R.array.htp_main_titles ) [ position ];
		}
	}*/
}
