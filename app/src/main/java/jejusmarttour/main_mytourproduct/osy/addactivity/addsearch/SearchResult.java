package jejusmarttour.main_mytourproduct.osy.addactivity.addsearch;

/**
 * Created by Osy on 2018-03-23.
 */

public interface SearchResult {
    void setSearchText(String searchText);
}
