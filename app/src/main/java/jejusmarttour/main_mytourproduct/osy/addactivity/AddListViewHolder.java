package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AddListViewHolder {
	CheckBox checkBox;
	ImageView imageView;
	ImageView watchImageView;
	TextView titleText;
	TextView timeText;
	TextView subTitleText;
	LinearLayout group_linearLayout;

}
