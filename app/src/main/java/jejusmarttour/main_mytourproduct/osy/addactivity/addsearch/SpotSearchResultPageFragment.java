package jejusmarttour.main_mytourproduct.osy.addactivity.addsearch;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.HealingContentsTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.user.UserVO;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.SearchProductRequestVO;
import syl.com.jejusmarttour.R;

public class SpotSearchResultPageFragment extends Fragment
		implements JSONArrayResult<HealingContentsVO> , SearchResult{
	private static final String TAG = SpotSearchResultPageFragment.class.toString ( );

	private String searchStr;

	private HealingContentsTask searchProductTask;

	private AddSpotSearchRecyclerViewAdapter mRecyclerViewAdapter;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private ArrayList < HealingContentsVO > listData;

	private TextView countView;
	private boolean fromMap = false;
	private boolean fromRoute = false;
	private boolean doingEndScroll = false;
	private boolean allDataGet = false;	//설정한 카테고리(스피너)의 모든 데이터를 가져온 경우

	private TextView resultText;
	private TextView searchView;

	public static SpotSearchResultPageFragment newInstance ( boolean fromMap , boolean fromRoute ) {
		SpotSearchResultPageFragment fragment = new SpotSearchResultPageFragment( );
		fragment.fromMap = fromMap;
		fragment.fromRoute = fromRoute;
		return fragment;
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ) {
		View view = null;

		try {
			view = inflater.inflate ( R.layout.fragment_search_recycler , container , false );
		}
		catch ( InflateException e ) {
			e.printStackTrace ( );
		}

		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		listData = new ArrayList <> ( );

		resultText = view.findViewById ( R.id.no_result_text );
		searchView = view.findViewById ( R.id.search_text );

		initRecyclerView(view);

		if (!TextUtils.isEmpty(searchStr)) {
			searchView.setText ( searchStr );
		}
		else {
			searchView.setText ( "" );
		}
	}

	private void initRecyclerView(View view){
		mRecyclerViewAdapter = new AddSpotSearchRecyclerViewAdapter ( getActivity ( ) );
		RecyclerView mRecyclerView = view.findViewById ( R.id.list_view);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
		mRecyclerView.setAdapter(mRecyclerViewAdapter);
		mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);

				int visibleLastPosition = ( (LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition() ;

				if ( !doingEndScroll && !allDataGet && recyclerView.getAdapter().getItemCount() == visibleLastPosition+1 ){
					doingEndScroll = true;
					requestPageNumber += 1;

					Log.e("스크롤마지막", "onScroll: " );

					executeTask ( );
				}
			}
		});
	}

	// 검색 실행
	private void executeTask ( ) {
		searchProductTask = new HealingContentsTask ( getActivity ( ) , this );
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) , searchStr ) );
	}

	// 요청할 파라미터
	private SearchProductRequestVO  getParam ( String page , String name ) {
		SearchProductRequestVO vo = new SearchProductRequestVO();
		vo.setHt_name ( name );
		vo.setType ( "0" );
		vo.setPage ( page );
		vo.setSort ( ( ( CommonData ) getActivity ( ).getApplication ( ) ).getSortType ( ) );

		UserVO userVO;
		userVO = CommonData.getUserVO ( );

		if ( userVO.getHcdArray ( ) != null )
			vo.setHcd ( userVO.getHcd ( ) );

		if ( userVO.getScdArray ( ) != null )
			vo.setScd ( userVO.getScd ( ) );

		return vo;
	}

	private void InsertItemToListView ( ArrayList < HealingContentsVO > resultList ) {
		for ( int i = 0 ; i < resultList.size ( ) ; i++ ) {
			mRecyclerViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		mRecyclerViewAdapter.notifyDataSetChanged ( );
	}

	@Override
	public void setJSONArrayResult ( ArrayList<HealingContentsVO> resultList ) {
		searchProductTask.cancel ( true );

		if ( resultList == null ) {
			// countView.setText ( "총 0건" );
			allDataGet = true;
			if ( mRecyclerViewAdapter.getItemCount ( ) == 0 )
				resultText.setVisibility ( View.VISIBLE );
			return;
		}
		else {
			resultText.setVisibility ( View.GONE );

			InsertItemToListView ( resultList );
		}

		doingEndScroll = false;
	}

	@Override
	public void setSearchText(String searchText) {
		requestPageNumber = 1;
		resultText.setVisibility ( View.GONE );

		searchStr = searchText;
		searchView.setText ( searchStr );

		mRecyclerViewAdapter.removeAll();
		allDataGet = false;

		executeTask();
	}
}
