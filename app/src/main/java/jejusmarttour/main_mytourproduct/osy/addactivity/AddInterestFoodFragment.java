package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.MTCSightSeeingTask;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

public class AddInterestFoodFragment extends Fragment implements JSONArrayResult<SightSeeingContentsVO>{
    private AddRecyclerViewAdapter mRecyclerViewAdapter;
    private MTCSightSeeingTask task;

    private boolean taskLock = true;    //onScroll()메서드가 먼저 onCreate()보다 먼저 시작되어 막아음
    private boolean allDataGet = false; //설정한 카테고리(스피너)의 모든 데이터를 가져오면 태스크실행 x

    private int requestPageNumber = 1;
    private ArrayList<ScheduleVO> listData;

    public static AddInterestFoodFragment newInstance ( ){
        AddInterestFoodFragment fragment = new AddInterestFoodFragment( );
        return fragment;
    }

    @Override
    public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
        View view = null;

        try {
            view = inflater.inflate ( R.layout.fragment_listview, container , false );
        }
        catch ( InflateException e ){
            e.printStackTrace ( );
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listData = new ArrayList<>();
        taskLock = false;

        executeTask ();

        initRecyclerView(view);
    }

    private void initRecyclerView(View view){
        mRecyclerViewAdapter = new AddRecyclerViewAdapter( getActivity() , listData , AddRecyclerViewAdapter.TYPE_OF_FOOD_STAY);

        RecyclerView mRecyclerView = view.findViewById(R.id.list_view);
        TextView noDataText = view.findViewById(R.id.no_data_text);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleLastPosition = ( (LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition() ;

                if ( !allDataGet && recyclerView.getAdapter().getItemCount() == visibleLastPosition+1 ){

                    requestPageNumber += 1;

                    Log.e("스크롤마지막", "onScroll: " );

                    executeTask ( );
                }
            }
        });
    }

    private void InsertItemToListView ( ArrayList<ScheduleVO> resultList ){
        for ( int i = 0; i < resultList.size ( ); i++ ){
            mRecyclerViewAdapter.add ( resultList.get ( i ) );
        }
        // 리스트뷰 리로딩
        mRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void setJSONArrayResult ( ArrayList<SightSeeingContentsVO> resultList ){
        task.cancel ( true );

        if (resultList == null) {
            allDataGet = true;
            return;
        }

        Log.e("FoodStayResult", "resultList.size: " + resultList.size() );

        ArrayList<ScheduleVO> vos = new ArrayList<>();

        for (SightSeeingContentsVO sscVo :
                resultList) {
            if (sscVo.getCcnt_code().equals("1")){
                ScheduleVO scheduleVO = new ScheduleVO();
                scheduleVO.setSightSeeingContentsVO(sscVo);
                vos.add(scheduleVO);
            }
        }

        InsertItemToListView(vos);

        taskLock = false;
    }

    private void executeTask (){
        if ( !taskLock){
            taskLock = true;

            task = new MTCSightSeeingTask(getContext(), this);
            task.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , "ccnt", String.valueOf( requestPageNumber ));
        }
    }
}