package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.MTCHealingContentsTask;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

public class AddFromMySpotListActivity extends AppCompatActivity
		implements JSONArrayResult<HealingContentsVO>, AddFromMyList{
	private static final String TAG = AddFromMySpotListActivity.class.toString ( );
	private AddRecyclerViewAdapter mRecyclerViewAdapter;
	private MTCHealingContentsTask task;

	private boolean taskLock = true;    //onScroll()메서드가 먼저 onCreate()보다 먼저 시작되어 막아음
	private int requestPageNumber = 1;

	private ArrayList<GetCheckedDataFromFragment> gotCheckedData;
	private ArrayList<ScheduleVO> listData;

	private boolean allDataGet = false;	//설정한 카테고리(스피너)의 모든 데이터를 가져온 경우

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_addfrommyspotlist );

		gotCheckedData = new ArrayList<>();
		listData = new ArrayList<>();

		taskLock = false;
		executeTask ();

		initRecyclerView();
		initActionBar ( );
	}

	private void initRecyclerView(){
		mRecyclerViewAdapter = new AddRecyclerViewAdapter( this , listData, AddRecyclerViewAdapter.TYPE_OF_SPOT);

		RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_view);
		TextView noDataText = (TextView) findViewById(R.id.no_data_text);

		mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
		mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
		mRecyclerView.setAdapter(mRecyclerViewAdapter);
		mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);

				int visibleLastPosition = ( (LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition() ;

				if ( !allDataGet && recyclerView.getAdapter().getItemCount() == visibleLastPosition+1 ){

					requestPageNumber += 1;

					Log.e("스크롤마지막", "onScroll: " );

					executeTask ( );
				}
			}
		});
	}
	// 액션바 설정
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_make_my_tour_course , null );
		ImageView cancelBtn = customView.findViewById ( R.id.cancel_btn );
		ImageView saveBtn = customView.findViewById ( R.id.save_btn );
		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( getResources ( ).getString ( R.string.title_activity_mtcadd_site ) );

		cancelBtn.setOnClickListener ( view -> finish ( ) );
		saveBtn.setOnClickListener( view -> {
			ArrayList<ScheduleVO> saveData = new ArrayList<>();

			Log.e(TAG, "gotCheckedData.size(): " + gotCheckedData.size() );
			for ( GetCheckedDataFromFragment gotData :
					gotCheckedData ) {
				saveData.addAll(gotData.getCheckedData());
			}

			Log.e(TAG, "saveData.size(): " + saveData.size());

			Intent intent = getIntent ( );
//			intent.putExtra ( "data" , saveData );

			((CommonData)getApplication()).setMakeScheduleArrayList( saveData );

			setResult ( RESULT_OK , intent );
			finishActivity ( ScheduleManegeFragment.REQUEST_CODE_SITE );
			finish ( );
        });

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	@Override
	public void checkedDataSendToActivity(GetCheckedDataFromFragment getCheckedDataFromFragment){
		gotCheckedData.add( getCheckedDataFromFragment );
	}

	// 요청할 파라미터
	private String[] getParam ( ){
		String taskType = "hcnt";
		String [ ] param = new String[] { taskType, String.valueOf( requestPageNumber ) };

		return param;
	}

	private void executeTask (){
		if ( !taskLock){
			taskLock = true;

			task = new MTCHealingContentsTask(this, this);
			task.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( ) );
		}
	}

	private void InsertItemToListView ( ArrayList<ScheduleVO> resultList ){
		for ( int i = 0; i < resultList.size ( ); i++ ){
			Log.e(this.toString(), "resultList.size(): " + resultList.size() );
			mRecyclerViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		mRecyclerViewAdapter.notifyDataSetChanged();
	}

	@Override
	public void setJSONArrayResult ( ArrayList<HealingContentsVO> resultList ){
		task.cancel ( true );

		if (resultList == null){
			allDataGet = true;
			return;
		}

		Log.e("SpotResult", "resultList.size: " + resultList.size() );

		ArrayList<ScheduleVO> vos = new ArrayList<>();

		for (HealingContentsVO hcVo :
				resultList) {
			ScheduleVO scheduleVO = new ScheduleVO();
			scheduleVO.setHealingContentsVO(hcVo);
			vos.add(scheduleVO);
		}

		InsertItemToListView(vos);

		taskLock = false;
	}
}
