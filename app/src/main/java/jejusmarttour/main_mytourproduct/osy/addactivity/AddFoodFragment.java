package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import jejusmarttour.task.FoodStayTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.util.code_data.FoodSpinnerTree;
import jejusmarttour.util.code_data.MyNode;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import syl.com.jejusmarttour.R;

import static jejusmarttour.util.code_data.FoodSpinnerTree.SPINNER_NOT_SELECT;

public class AddFoodFragment extends Fragment implements JSONArrayResult{
	private AddRecyclerViewAdapter mRecyclerViewAdapter;
	private FoodStayTask searchProductTask;

	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private Spinner spinner1;
	private Spinner spinner2;
	private Spinner spinner3;
	private String code = "";
	private MyNode.BranchNode rootNode;

	private String ccd = "";
	private boolean listViewClear = false;

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
		View view = null;

		try{
			view = inflater.inflate ( R.layout.fragment_food_stay, container , false );
		}
		catch ( InflateException e ){
			e.printStackTrace ( );
		}

		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		lockListView = true;

		initRecyclerView( view );
		initSpinner( view );
	}

	private void initRecyclerView(View v){
		mRecyclerViewAdapter = new AddRecyclerViewAdapter( getActivity ( ) , new ArrayList<>(), AddRecyclerViewAdapter.TYPE_OF_FOOD_STAY);
		RecyclerView mRecyclerView = v.findViewById(R.id.list_view);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
		mRecyclerView.setAdapter(mRecyclerViewAdapter);
		mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);

				int visibleLastPosition = ( (LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition() ;

				if ( recyclerView.getAdapter().getItemCount() == visibleLastPosition+1 ){

					requestPageNumber += 1;

					Log.e("스크롤마지막", "onScroll: " );

					executeTask ( );

				}
			}
		});
	}

	private void initSpinner (View v){
		spinner1 = v.findViewById(R.id.spinner1);
		spinner2 = v.findViewById(R.id.spinner2);
		spinner3 = v.findViewById(R.id.spinner3);

		final Resources.Theme theme = getResources().newTheme();
		theme.applyStyle(R.style.SpinnerDropDownStyle, true);

		FoodSpinnerTree foodSpinnerTree = FoodSpinnerTree.getInstance();
		ArrayAdapter< ArrayList<String> > adapter1 = new ArrayAdapter<ArrayList<String>>(getActivity(), R.layout.spinner_layout, foodSpinnerTree.getList(SPINNER_NOT_SELECT,SPINNER_NOT_SELECT));
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		ArrayAdapter< ArrayList<String>> adapter2 = new ArrayAdapter<>(getActivity(), R.layout.spinner_layout, new ArrayList<>());
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		ArrayAdapter< ArrayList<String>> adapter3 = new ArrayAdapter<>(getActivity(), R.layout.spinner_layout, new ArrayList<>());
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
		spinner3.setAdapter(adapter3);

		spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
				adapter2.clear();
				adapter3.clear();
				spinner2.setSelection(0);
				spinner3.setSelection(0);
				code = foodSpinnerTree.getCode( position, SPINNER_NOT_SELECT, SPINNER_NOT_SELECT);

				setCcd( code);
				adapter2.addAll( foodSpinnerTree.getList( position, SPINNER_NOT_SELECT) );
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				spinner3.setBackgroundResource(R.drawable.bg_spinner);
				adapter3.clear();
				spinner3.setSelection(0);

				code = foodSpinnerTree.getCode( Integer.parseInt(code.substring(1,2)) -1, position, SPINNER_NOT_SELECT);

				setCcd( code);

				if (foodSpinnerTree.getList( Integer.parseInt(code.substring(1,2)) -1, position).size() != 0){
					adapter3.addAll( foodSpinnerTree.getList( Integer.parseInt(code.substring(1,2)) -1, position) );
				}
				else {
					spinner3.setBackgroundResource(R.drawable.bg_inactive_spinner);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				code = foodSpinnerTree.getCode(code, position);
				setCcd( code);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	public void setCcd(String ccd) {
		currentPageNumber = 0;
		requestPageNumber = 1;

		this.ccd = ccd;

		Log.e( this.toString(), "setCcdAndTaskStart: 실행" + ccd);
		mRecyclerViewAdapter.removeAll();

		executeTask();
	}

	// 요청할 파라미터
	private SearchProductRequestVO getParam ( String page ){
		SearchProductRequestVO vo = new SearchProductRequestVO();
		vo.setPage(page);
		vo.setType ("1"); //관광장소

		return vo;
	}

	public Integer getCurrentPage ( ){
		return currentPageNumber;
	}

/*	// 아이템 터치 이벤트
	private OnItemClickListener onClickListItem = new OnItemClickListener ( ){
		@Override
		public void onItemClick ( AdapterView < ? > arg0 , View arg1 , int position , long arg3 ){
			( (FoodStaySpotActivity) getActivity ( ) ).onProItemClick ( mRecyclerViewAdapter.getItem ( position ));
		}
	};*/

	private void InsertItemToListView ( ArrayList < ScheduleVO > resultList ){
		for ( int i = 0 ; i < resultList.size ( ) ; i++ ){
			//이미지 data들어오면 삭제
			//resultList.get ( i ).setHt_img ( CommonData.getImageAddress ( "/hcnt/expert/가마오름지하요새/" + ( i + 1 ) + ".JPG" ) );

			//코드가 1이면 음식점
			if (resultList.get(i).getSightSeeingContentsVO().getCcnt_code().equals("1")){
				mRecyclerViewAdapter.add ( resultList.get ( i ) );
			}
		}
		// 리스트뷰 리로딩
		mRecyclerViewAdapter.notifyDataSetChanged ( );
	}


	private void executeTask(){
		searchProductTask = new FoodStayTask( getContext ( ) , this, ccd );
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) ) );
	}

	@Override
	public void setJSONArrayResult ( ArrayList resultList ){
		lockListView = true;
		searchProductTask.cancel ( true );

		if ( resultList == null ){
			return;
		}
		else{
			InsertItemToListView ( resultList );
		}

		lockListView = false;
	}
}
