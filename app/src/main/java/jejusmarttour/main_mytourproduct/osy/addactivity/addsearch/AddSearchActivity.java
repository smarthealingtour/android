package jejusmarttour.main_mytourproduct.osy.addactivity.addsearch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import java.util.ArrayList;

import jejusmarttour.FontAppCompatActivity;
import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.osy.addactivity.AddFromMyList;
import jejusmarttour.main_mytourproduct.osy.addactivity.GetCheckedDataFromFragment;
import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2018-03-23.
 */

public class AddSearchActivity extends FontAppCompatActivity
        implements View.OnClickListener , AddFromMyList {
    private MenuItem mSearchItem;
    private MenuItem search_result_delete;

    private String searchStr;
    private boolean fromMap = false;

    private final String TYPE_OF_FOOD = "B";
    private final String TYPE_OF_STAY = "C";

    /// 토글 버튼 및 뷰페이저
    private final int MAX_PAGE = 3;
    private Fragment cur_fragment = new Fragment();
//    private Fragment productFragment;
    private Fragment spotFragment;
    private Fragment foodFragment;
    private Fragment stayFragment;

    private ToggleButton tab1;
    private ToggleButton tab2;
    private ToggleButton tab3;

    private ViewPager viewPager;
    private final PageListener pageListener = new PageListener();
    private ArrayList<GetCheckedDataFromFragment> gotCheckedDatas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);

        gotCheckedDatas = new ArrayList<>();

        initTabs();
        initFragments();
        initViewPager();
    }

    // 액션바 메뉴
    @Override
    public boolean onCreateOptionsMenu ( Menu menu ){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);

        // 액션바 그림자 지우기
        actionBar.setElevation(0);

        LayoutInflater mInflater = LayoutInflater.from ( this );
        View customView = mInflater.inflate(R.layout.actionbar_search, null);

        EditText searchEdit = customView.findViewById(R.id.search_edit_text);
        searchEdit.setOnEditorActionListener((v, actionId, event) -> {
            switch (actionId) {
                case EditorInfo.IME_ACTION_SEARCH:
                    searchStart(searchEdit);
                    break;
                default:
                    // 기본 엔터키 동작
                    return false;
            }
            return true;
        });
        searchEdit.setOnFocusChangeListener((v, hasFocus) -> {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (hasFocus){
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                else {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
            }
        });
        searchEdit.requestFocus();

        LinearLayout searchBtn = customView.findViewById(R.id.search_btn);
        searchBtn.setOnClickListener(v -> {
            searchStart(searchEdit);
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        });

        customView.findViewById(R.id.search_text_layout).setOnClickListener(v -> {
            searchEdit.requestFocus();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        });

        customView.findViewById(R.id.complete).setOnClickListener(v -> {
            ArrayList<ScheduleVO> saveData = new ArrayList<>();

            for (GetCheckedDataFromFragment data :
                    gotCheckedDatas) {
                saveData.addAll(data.getCheckedData());
            }

            Log.e(this.toString(), "saveData.size(): " + saveData.size());

            Intent intent = getIntent ( );

            ((CommonData)getApplication()).setMakeScheduleArrayList( saveData );
            setResult ( RESULT_OK , intent );
            finishActivity ( ScheduleManegeFragment.REQUEST_SEARCH );
            finish ( );
        });

        customView.findViewById(R.id.back_btn).setOnClickListener(v -> finish());

        actionBar.setCustomView(customView);
        actionBar.setDisplayShowCustomEnabled(true);

        return super.onCreateOptionsMenu(menu);
    }

    private void searchStart(EditText searchEdit) {
        searchStr = searchEdit.getText().toString();
//        ((SearchResult)productFragment).setSearchText(searchStr);
        ((SearchResult)spotFragment).setSearchText(searchStr);
        ((SearchResult)foodFragment).setSearchText(searchStr);
        ((SearchResult)stayFragment).setSearchText(searchStr);
    }

    private void initTabs(){
        tab1 = ( ToggleButton ) findViewById ( R.id.tab2 );
        tab2 = ( ToggleButton ) findViewById ( R.id.tab3 );
        tab3 = ( ToggleButton ) findViewById ( R.id.tab4 );
        findViewById(R.id.tab1).setVisibility(View.GONE);

        tab1.setOnClickListener ( this );
        tab2.setOnClickListener ( this );
        tab3.setOnClickListener ( this );

        tab1.setChecked ( true );
    }

    private void initFragments(){
//        productFragment = ProductSearchResultPageFragment.newInstance(false);
        spotFragment = SpotSearchResultPageFragment.newInstance ( fromMap, false );
        foodFragment = FoodStaySearchResultFragment.newInstance ( fromMap, false , TYPE_OF_FOOD);
        stayFragment = FoodStaySearchResultFragment.newInstance ( fromMap, false , TYPE_OF_STAY);
    }

    private void initViewPager ( ){
        viewPager = ( ViewPager ) findViewById ( R.id.viewpager );
        viewPager.setAdapter ( new ViewPagerAdapter( getSupportFragmentManager ( ) ) );
        viewPager.setOffscreenPageLimit ( MAX_PAGE - 1 );
        viewPager.setOnPageChangeListener ( pageListener );
    }

    private ToggleButton [ ] getToggleButtons ( ){
        return new ToggleButton[]{ tab1 , tab2 , tab3 /*, tab4*/ };
    }

    @Override
    public void onClick ( View v ){
        if ( v.getId ( ) == tab1.getId ( ) ) {
            setTabItem ( 0 );
        }
        else if ( v.getId ( ) == tab2.getId ( ) ) {
            setTabItem ( 1 );
        }
        else if ( v.getId ( ) == tab3.getId ( ) ) {
            setTabItem ( 2 );
        }
//        else if ( v.getId ( ) == tab4.getId ( ) ) {
//            setTabItem ( 3 );
//        }
    }

    private void setTabItem ( int position ){
        if (getToggleButtons()[position].isChecked()) {
            setCurrentItem ( position );
            validateToggleButton( position );
        }
        else {
            getToggleButtons ( ) [ position ].setChecked ( true );
        }
    }

    private void setChangeTabItem ( int position ){
        getToggleButtons ( ) [ position ].setChecked ( true );
        validateToggleButton( position );
    }

    private void validateToggleButton(int position ){
        for ( int i = 0; i < getToggleButtons ( ).length; i++ ){
            if ( i != position )
                getToggleButtons ( ) [ i ].setChecked ( false );
        }
    }

    private void setCurrentItem ( int position ) {
        viewPager.setCurrentItem ( position );
    }

    @Override
    public void checkedDataSendToActivity(GetCheckedDataFromFragment getCheckedDataFromFragment) {
        gotCheckedDatas.add(getCheckedDataFromFragment) ;
    }

    private class PageListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled ( int position , float positionOffset , int positionOffsetPixels ) {}

        @Override
        public void onPageScrollStateChanged ( int state ) {}

        @Override
        public void onPageSelected ( int position )
        {
            setChangeTabItem ( position );
        }

    }

    // 뷰페이지 어뎁터
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private int position = 0;

        public ViewPagerAdapter ( FragmentManager fm )
        {
            super ( fm );
        }

        @Override
        public Fragment getItem ( int position ){
            if ( position < 0 || MAX_PAGE <= position )
                return null;

            this.position = position;

            switch ( position ){
                case 0 :
                    cur_fragment = spotFragment;
                    break;
                case 1 :
                    cur_fragment = foodFragment;
                    break;
                case 2 :
                    cur_fragment = stayFragment;
                    break;
            }
            return cur_fragment;
        }

        public int getCurrentPosition ( )
        {
            return position;
        }

        @Override
        public int getCount ( )
        {
            return MAX_PAGE;
        }

        @Override
        public CharSequence getPageTitle ( int position ){
            return getResources ( ).getStringArray ( R.array.htp_main_titles ) [ position ];
        }
    }
}
