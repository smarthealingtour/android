package jejusmarttour.main_mytourproduct.osy.addactivity.addsearch;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.FoodStayTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.user.UserVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

public class FoodStaySearchResultFragment extends Fragment
		implements JSONArrayResult<ScheduleVO> , SearchResult {
	private String searchStr;
	private FoodStayTask searchProductTask;

	private AddFoodStaySearchRecyclerViewAdapter mRecyclerViewAdapter;
	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private ArrayList < SightSeeingContentsVO > listData;

	private TextView countView;
	private boolean fromMap = false;
	private boolean fromRoute = false;
	private boolean doingEndScroll = false;
	private boolean allDataGet = false;	//설정한 카테고리(스피너)의 모든 데이터를 가져온 경우

	private TextView resultText;
	private String type;
	private TextView searchView;

	public static FoodStaySearchResultFragment newInstance (boolean fromMap , boolean fromRoute , String type){
		FoodStaySearchResultFragment fragment = new FoodStaySearchResultFragment( );

		fragment.fromMap = fromMap;
		fragment.fromRoute = fromRoute;
		fragment.type = type;
		return fragment;
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
		View view = null;

		try{
			view = inflater.inflate ( R.layout.fragment_search_recycler , container , false );
		}
		catch ( InflateException e ){
			e.printStackTrace ( );
		}

		resultText = view.findViewById ( R.id.no_result_text );
		resultText.setVisibility ( View.GONE );

		listData = new ArrayList <> ( );
		lockListView = true;

		searchView = view.findViewById ( R.id.search_text );
		// countView = ( TextView ) view.findViewById ( R.id.count_text );

		if ( !TextUtils.isEmpty(searchStr )){
			searchView.setText ( searchStr );
		}
		else{
			searchView.setText ( "" );
		}

		mRecyclerViewAdapter = new AddFoodStaySearchRecyclerViewAdapter( getActivity ( ) );
		RecyclerView mRecyclerView =  view.findViewById ( R.id.list_view);

		mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
		mRecyclerView.setAdapter(mRecyclerViewAdapter);
		mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);

				int visibleLastPosition = ( (LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition() ;

				if ( !doingEndScroll && !allDataGet && recyclerView.getAdapter().getItemCount() == visibleLastPosition+1 ){
					doingEndScroll = true;
					requestPageNumber += 1;

					Log.e("스크롤마지막", "onScroll: " );

					executeTask ( );
				}
			}
		});
//		listView.setOnScrollListener ( this );
//		listView.setAdapter ( mRecyclerViewAdapter );
//
//		// ListView 아이템 터치 시 이벤트 추가
//		listView.setOnItemClickListener ( onClickListItem );

		return view;
	}

	// 검색 실행
	private void executeTask ( ){
		searchProductTask = new FoodStayTask( getActivity ( ) , this ,type);
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) , searchStr ) );
	}

	// 요청할 파라미터
	private SearchProductRequestVO getParam ( String page , String name ){
		SearchProductRequestVO vo = new SearchProductRequestVO();
		vo.setHt_name ( name );
		vo.setType ( "1" );
		vo.setPage ( page );
		vo.setSort ( ( ( CommonData ) getActivity ( ).getApplication ( ) ).getSortType ( ) );

		UserVO userVO = CommonData.getUserVO ( );

		if ( userVO.getHcdArray ( ) != null )
			vo.setHcd ( userVO.getHcd ( ) );

		if ( userVO.getScdArray ( ) != null )
			vo.setScd ( userVO.getScd ( ) );

		return vo;
	}

	private void InsertItemToListView ( ArrayList < SightSeeingContentsVO > resultList ){
		for ( int i = 0 ; i < resultList.size ( ) ; i++ ){
			mRecyclerViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		mRecyclerViewAdapter.notifyDataSetChanged ( );
	}

	@Override
	public void setJSONArrayResult ( ArrayList<ScheduleVO> resultList ){
		lockListView = true;
		searchProductTask.cancel ( true );

		if ( resultList == null ){
			// countView.setText ( "총 0건" );
			allDataGet = true;
			if ( mRecyclerViewAdapter.getItemCount ( ) == 0 )
				resultText.setVisibility ( View.VISIBLE );
			return;
		}
		else{
			Log.e(this.toString(), "resultList.size(): "+resultList.size() );
			resultText.setVisibility ( View.GONE );

			ArrayList<SightSeeingContentsVO> ss = new ArrayList<>();
			for (ScheduleVO vo:
				 resultList) {
				ss.add(vo.getSightSeeingContentsVO());
			}
			InsertItemToListView ( ss );
		}

		lockListView = false;
		doingEndScroll = false;
	}

	@Override
	public void setSearchText(String searchText) {
		requestPageNumber = 1;
		resultText.setVisibility ( View.GONE );

		searchStr = searchText;
		searchView.setText ( searchStr );
		mRecyclerViewAdapter.removeAll();
		allDataGet = false;

		executeTask();
	}
}