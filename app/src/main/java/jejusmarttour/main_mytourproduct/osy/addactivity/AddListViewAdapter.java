package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.MyInfoConnectResult;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

/**
 * Created by A on 2015-10-25.
 */
public class AddListViewAdapter extends BaseAdapter implements MyInfoConnectResult{
    private static final String TAG = AddListViewAdapter.class.toString ( );

    private ArrayList<ScheduleVO> checkedData;
    private ArrayList<String> checkedDataName;
    private ArrayList<ScheduleVO> listData;
    private Context context;
    private LayoutInflater inflater;

    private ScheduleVO selectRemoveData;
    private ArrayList<CheckBox> checkBoxes;

    private boolean allCheckedOff = false;

    AddListViewAdapter(Context context, ArrayList<ScheduleVO> listData){
        this.context = context;
        this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
        this.listData = listData;

        checkedData = new ArrayList<>();
        checkedDataName = new ArrayList<>();
        checkBoxes = new ArrayList<>();

        ( (AddFromMyList) context).checkedDataSendToActivity(() -> checkedData);
    }

    // 현재 아이템의 수를 리턴
    @Override
    public int getCount ( ){
        return listData.size ( );
    }

    @Override
    public ScheduleVO getItem ( int position ){
        ScheduleVO item = listData.get(position);
        ScheduleVO returnItem = new ScheduleVO();

        if(item != null){
            if(item.isHealingContents()){
                HealingContentsVO vo = item.getHealingContentsVO();

                if(null == vo.getHcnt_type()){
                    returnItem = null;
                }
                else{
                    returnItem.setHealingContentsVO(vo);
                }
            }else {
                SightSeeingContentsVO vo = item.getSightSeeingContentsVO();
                returnItem.setSightSeeingContentsVO(vo);
            }
        }
        return returnItem;
    }

    // 아이템 position의 ID 값 리턴
    @Override
    public long getItemId ( int position ){
        return position;
    }

    // 출력 될 아이템 관리
    @Override
    public View getView ( int position , View convertView , ViewGroup parent ){
        AddListViewHolder viewHolder;

        // 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 convertView가 null인 상태로 들어 옴
        if ( convertView == null ){
            // view가 null일 경우 커스텀 레이아웃을 얻어 옴
            convertView = inflater.inflate ( R.layout.item_addlistview , parent , false );

            viewHolder = new AddListViewHolder( );

//            viewHolder.keywordTitle = convertView.findViewById(R.id.keywordTitle);
//            viewHolder.linearLayout = convertView.findViewById ( R.id.mtc_group_layout );
//            viewHolder.linearLayout.setVisibility(View.GONE);

            viewHolder.checkBox = convertView.findViewById ( R.id.checkbox );
            if ( checkedDataName.contains(listData.get(position).getContentName())){
                viewHolder.checkBox.setChecked(true);
            }
            else {
                viewHolder.checkBox.setChecked(false);
            }
//            viewHolder.checkBox.setClickable(false);
//            viewHolder.checkBox.setFocusable(false);
            viewHolder.checkBox.setOnClickListener(v -> {
                boolean checked = false;
                int removeTarget = 0;

                for (String name :
                        checkedDataName) {
                    if (name.equals(listData.get(position).getContentName()) ){
                        checked = true;

                        viewHolder.checkBox.setChecked(false);

                        break;
                    }
                    removeTarget++;
                }

                if (!checked){
                    checkedData.add( listData.get(position) );
                    checkedDataName.add( listData.get(position).getContentName());
                }
                else {
                    checkedData.remove(removeTarget);
                    checkedDataName.remove(removeTarget);
                }

                Log.e(TAG, "onCheckedChanged, checkedData.size() : " + checkedData.size() );
            });

            /*viewHolder.checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked){
                     checkedData.add( listData.get(position) );
                     checkBoxes.add( viewHolder.checkBox);
                }
                else {
                    if (allCheckedOff) {
                        checkedData.clear();
                    }
                    else  {
                        checkedData.remove( listData.get(position) );
                        checkBoxes.remove( viewHolder.checkBox);
                    }
                }

                boolean checked = false;
                int removeTarget = 0;

                for (String name :
                        checkedDataName) {
                    if (name.equals(listData.get(position).getContentName()) ){
                        checked = true;

                        viewHolder.checkBox.setChecked(false);

                        break;
                    }
                    removeTarget++;
                }

                if (!checked){
                    checkedData.add( listData.get(position) );
                    checkedDataName.add( listData.get(position).getContentName());
                }
                else {
                    checkedData.remove(removeTarget);
                    checkedDataName.remove(removeTarget);
                }

                Log.e(TAG, "onCheckedChanged, checkedData.size() : " + checkedData.size() );
            });*/
            viewHolder.imageView = convertView.findViewById ( R.id.search_item_icon );
            viewHolder.watchImageView = convertView.findViewById(R.id.watch_icon);
            viewHolder.titleText = convertView.findViewById ( R.id.mtc_title_text );
            viewHolder.timeText = convertView.findViewById ( R.id.mtc_session_time_text );
            viewHolder.subTitleText = convertView.findViewById ( R.id.htp_subtitle );
            viewHolder.group_linearLayout =  convertView.findViewById(R.id.group_linearLayout);
//            viewHolder.groupText = convertView.findViewById ( R.id.mtc_group_text );

            convertView.setTag ( viewHolder );
        }
        else{
            viewHolder = ( AddListViewHolder ) convertView.getTag ( );
        }

        ScheduleVO itemData = listData.get(position);
        if(itemData != null){
            if(itemData.isHealingContents()){
                HealingContentsVO itemVo = itemData.getHealingContentsVO();

                convertView.setBackgroundResource(R.color.white);
                viewHolder.group_linearLayout.setVisibility(View.VISIBLE);
                viewHolder.watchImageView.setVisibility ( View.VISIBLE );
                Glide.with(context).load ( CommonData.getLocationIconId(itemVo.getHcnt_type()) ).thumbnail ( 0.1f ).into ( viewHolder.imageView );
                viewHolder.titleText.setText ( itemVo.getHcnt_name() == null ? "이름없음" : itemVo.getHcnt_name() );
                viewHolder.timeText.setText ( itemVo.getHcnt_duration() == null ? "- 분" : SmartTourUtils.getDuration(itemVo.getHcnt_duration()) );
                viewHolder.subTitleText.setText ( itemVo.getHcnt_addr ( ) == null ? "주소없음" : itemVo.getHcnt_addr ( ) );
            }
            else{
                SightSeeingContentsVO itemVo = itemData.getSightSeeingContentsVO();
                convertView.setBackgroundResource(R.color.white);
                viewHolder.group_linearLayout.setVisibility(View.VISIBLE);
                viewHolder.imageView.setImageResource(R.drawable.location_icon);
                viewHolder.titleText.setText ( itemVo.getCcnt_name() == null ? "이름없음" : itemVo.getCcnt_name() );
                viewHolder.watchImageView.setVisibility ( View.GONE );
                viewHolder.timeText.setText ( "" );
                viewHolder.subTitleText.setText ( itemVo.getCcnt_addr() == null ? "주소없음" : itemVo.getCcnt_addr ( ) );
            }
        }

        return convertView;
    }

    @Override
    public void notifyDataSetChanged ( ){
        super.notifyDataSetChanged ( );
    }

    // 외부에서 아이템 추가 요청 시 사용
    public void add ( ScheduleVO vo ){
        listData.add(vo);
    }

    // 외부에서 아이템 삭제 요청 시 사용
    public void remove ( int position ){
        listData.remove(position);
    }

    public void remove ( ScheduleVO removeData ){
        listData.remove ( removeData );
    }

    public void removeAll ( ){
        listData.clear();
    }

    public void allCheckOff(){
        allCheckedOff = true;
        for (CheckBox box :
                checkBoxes) {
            box.setChecked(false);
        }
        checkBoxes.clear();
        allCheckedOff = false;
    }

    @Override
    public void connectResultData(int code, String result) {
        Log.d("Tag", "connectResultData:" + result);
        try {
            JSONObject json = new JSONObject(result);
            if("success".equals(json.getString("result"))) {
                remove( selectRemoveData );

                notifyDataSetChanged();
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
