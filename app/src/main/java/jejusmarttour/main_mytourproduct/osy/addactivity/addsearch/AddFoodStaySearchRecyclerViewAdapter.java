package jejusmarttour.main_mytourproduct.osy.addactivity.addsearch;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.osy.addactivity.AddFromMyList;
import jejusmarttour.main_spot.detailinfo.FoodStayDetailInfoActivity;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

public class AddFoodStaySearchRecyclerViewAdapter extends RecyclerView.Adapter<AddFoodStaySearchRecyclerViewAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<SightSeeingContentsVO> listData;

    private ArrayList<SightSeeingContentsVO> checkedData;
    private ArrayList<String> checkedDataName;

    AddFoodStaySearchRecyclerViewAdapter(Context context){
        this.context = context;

        listData = new ArrayList<>();
        checkedData = new ArrayList<>();
        checkedDataName = new ArrayList<>();

        ( (AddFromMyList) context).checkedDataSendToActivity(this::convertToScheduleVOs);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext());

        return new MyViewHolder(inflater.inflate(R.layout.item_addlistview, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SightSeeingContentsVO itemData = listData.get(position);

        holder.itemView.setBackgroundResource(R.color.white);
        holder.group_linearLayout.setVisibility(View.VISIBLE);
        holder.imageView.setImageResource(R.drawable.location_icon);
        holder.titleText.setText ( itemData.getCcnt_name() == null ? "이름없음" : itemData.getCcnt_name() );
        holder.watchImageView.setVisibility ( View.GONE );
        holder.timeText.setText ( "" );
        holder.subTitleText.setText ( itemData.getCcnt_addr() == null ? "주소없음" : itemData.getCcnt_addr ( ) );


        if ( checkedDataName.contains(listData.get(position).getCcnt_name())){
            holder.checkBox.setChecked(true);
        }
        else {
            holder.checkBox.setChecked(false);
        }

        holder.checkBox.setOnClickListener(v -> {
            boolean checked = false;
            int removeTarget = 0;

            for (String name :
                    checkedDataName) {
                if (name.equals(listData.get(position).getCcnt_name()) ){
                    checked = true;

                    holder.checkBox.setChecked(false);

                    break;
                }
                removeTarget++;
            }

            if (!checked){
                checkedData.add( listData.get(position) );
                checkedDataName.add( listData.get(position).getCcnt_name());
            }
            else {
                checkedData.remove(removeTarget);
                checkedDataName.remove(removeTarget);
            }

            Log.e(this.toString(), "onCheckedChanged, checkedData.size() : " + checkedData.size() );
        });
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, FoodStayDetailInfoActivity.class);

            ScheduleVO scheduleVO = new ScheduleVO();
            scheduleVO.setSightSeeingContentsVO(listData.get(position));

            CommonData.setScheduleVO(scheduleVO);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    // 외부에서 아이템 추가 요청 시 사용
    public void add ( SightSeeingContentsVO vo ){
        listData.add(vo);
    }

    // 외부에서 아이템 삭제 요청 시 사용
    public void remove ( int position ){
        listData.remove(position);
    }

    public void remove ( ScheduleVO removeData ){
        listData.remove ( removeData );
    }

    public void removeAll ( ){
        listData.clear();
    }

    private ArrayList<ScheduleVO> convertToScheduleVOs(){
        ArrayList<ScheduleVO> arrayList = new ArrayList<>();

        for (SightSeeingContentsVO vo:
                checkedData) {
            ScheduleVO scheduleVO = new ScheduleVO();
            scheduleVO.setSightSeeingContentsVO(vo);

            arrayList.add(scheduleVO);
        }

        return arrayList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        View itemView;
        CheckBox checkBox;
        ImageView imageView;
        ImageView watchImageView;
        TextView titleText;
        TextView timeText;
        TextView subTitleText;
        LinearLayout group_linearLayout;

        MyViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            checkBox = itemView.findViewById ( R.id.checkbox );
            imageView = itemView.findViewById ( R.id.search_item_icon );
            watchImageView = itemView.findViewById(R.id.watch_icon);
            titleText = itemView.findViewById ( R.id.mtc_title_text );
            timeText = itemView.findViewById ( R.id.mtc_session_time_text );
            subTitleText = itemView.findViewById ( R.id.htp_subtitle );
            group_linearLayout =  itemView.findViewById(R.id.group_linearLayout);
       }
    }
}
