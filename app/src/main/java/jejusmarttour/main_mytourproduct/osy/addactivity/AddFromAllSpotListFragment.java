package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jejusmarttour.task.GetContentsTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.TourSpotTask;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import syl.com.jejusmarttour.R;

public class AddFromAllSpotListFragment extends Fragment implements JSONArrayResult{
	private AddRecyclerViewAdapter mRecyclerViewAdapter;
	private TourSpotTask searchProductTask;
	private SearchProductRequestVO vo;

	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private String scd = "";
	private String hcd = "";
	private boolean listViewClear = false;
	private boolean allDataGet = false;

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
		View view = null;
		try	{
			view = inflater.inflate ( R.layout.fragment_listview, container , false );
		}
		catch ( InflateException e ) {
			e.printStackTrace ( );
		}

		return view;
	}

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

		ArrayList<ScheduleVO> listData = new ArrayList<>();
        lockListView = true;

        mRecyclerViewAdapter = new AddRecyclerViewAdapter( getActivity ( ) , listData, AddRecyclerViewAdapter.TYPE_OF_SPOT);
		RecyclerView mRecyclerView = view.findViewById(R.id.list_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
		mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);

				int visibleLastPosition = ( (LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition() ;

				if ( !allDataGet && recyclerView.getAdapter().getItemCount() == visibleLastPosition+1 ){

					requestPageNumber += 1;

					Log.e("스크롤마지막", "onScroll: " );

					executeTask ( );

				}
			}
		});
    }

    // 요청할 파라미터
	private SearchProductRequestVO getParam ( String page ){

		vo = new SearchProductRequestVO ( );
		vo.setPage ( page );
		vo.setType ( "0" );

		return vo;
	}

	private void insertItemToListView(ArrayList < ScheduleVO > resultList ){
		for ( int i = 0; i < resultList.size ( ); i++ ){
			mRecyclerViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		mRecyclerViewAdapter.notifyDataSetChanged ( );
	}

	private void clearItemToListView (){
		mRecyclerViewAdapter.removeAll();
		mRecyclerViewAdapter.notifyDataSetChanged ( );
	}

	public void setScdAndTaskStart(String scd) {
		listViewClear = true;
		allDataGet = false;
		currentPageNumber = 0;
		requestPageNumber = 1;

		this.hcd = "";
		this.scd = scd;

		Log.e( this.toString(), "setScdAndTaskStart: 실행" + scd );
		mRecyclerViewAdapter.removeAll();

		executeTask();
	}
	public void setHcdAndTaskStart(String hcd) {
		listViewClear = true;
		allDataGet = false;
		currentPageNumber = 0;
		requestPageNumber = 1;

		this.scd = "";
		this.hcd = hcd;

		Log.e( this.toString(), "setScdAndTaskStart: 실행" + hcd );
		mRecyclerViewAdapter.removeAll();

		executeTask();
	}

	@Override
	public void setJSONArrayResult ( ArrayList resultList ){
		lockListView = true;
		searchProductTask.cancel ( true );

		if ( resultList == null ){
			if (listViewClear){
				clearItemToListView();
			}
			else {
				allDataGet = true;
				return;
			}
		}
		else{
			insertItemToListView( resultList );
		}
		listViewClear = false;
		lockListView = false;
	}

	private void executeTask ( ){
		searchProductTask = new TourSpotTask( getContext ( ) , this , scd, hcd);
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) ) );
	}

	private void executeTask2 ( ){
		GetContentsTask getContentsTask = new GetContentsTask(getActivity(), this);
		searchProductTask = new TourSpotTask( getContext ( ) , this , scd, hcd);
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) ) );
	}

	public void allCheckOff(){
		mRecyclerViewAdapter.allCheckOff();
	}
}
