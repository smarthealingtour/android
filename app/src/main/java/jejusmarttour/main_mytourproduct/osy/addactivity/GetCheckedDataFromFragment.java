package jejusmarttour.main_mytourproduct.osy.addactivity;

import java.util.ArrayList;

import jejusmarttour.vo.ScheduleVO;

/**
 * Created by Osy on 2018-02-13.
 */

public interface GetCheckedDataFromFragment {
    ArrayList<ScheduleVO> getCheckedData();
}
