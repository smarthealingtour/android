package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

public class AddFromMyFoodStayListActivity extends AppCompatActivity implements AddFromMyList{
	private static final String TAG = AddFromMyFoodStayListActivity.class.toString ( );
	private Fragment cur_fragment = new Fragment ( );
	private PagerSlidingTabStrip tab;

	private ArrayList<GetCheckedDataFromFragment> gotCheckedData;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_my_interest_spot );

		gotCheckedData = new ArrayList<>();

		initActionBar ( );
		initViewPager ( );
	}

	private void initViewPager ( ){
		ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
		viewPager.setAdapter ( new ViewPagerAdapter ( getSupportFragmentManager() ) );
		viewPager.setCurrentItem(0);

		tab = ( PagerSlidingTabStrip ) findViewById ( R.id.htp_tabs );
		tab.setShouldExpand(true);
		setTabStyle();
		tab.setViewPager(viewPager);
	}

	@SuppressWarnings ( "deprecation" )
	private void setTabStyle ( ){
		tab.setTextSize ( getResources ( ).getDimensionPixelSize ( R.dimen.font_size_03 ) );
		tab.setDividerColor( getResources().getColor(R.color.white) );
		tab.setBackgroundColor( getResources().getColor(R.color.white) );
		tab.setIndicatorHeight( 5 );
		tab.setIndicatorColor( getResources().getColor(R.color.statusbar) );
		tab.setTextColor( getResources().getColor(R.color.font_color) );
	}

	// 액션바 설정
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_make_my_tour_course , null );
		ImageView cancelBtn = customView.findViewById ( R.id.cancel_btn );
		ImageView saveBtn = customView.findViewById ( R.id.save_btn );
		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( "숙식장소 추가" );

		cancelBtn.setOnClickListener ( view -> finish ( ) );
		saveBtn.setOnClickListener( view -> {
			ArrayList<ScheduleVO> saveData = new ArrayList<>();

			Log.e(TAG, "gotCheckedData.size(): " + gotCheckedData.size() );
			for ( GetCheckedDataFromFragment gotData :
					gotCheckedData ) {
				saveData.addAll(gotData.getCheckedData());
			}

			Log.e(TAG, "saveData.size(): " + saveData.size());

			Intent intent = getIntent ( );
//			intent.putExtra ( "data" , saveData );

			((CommonData)getApplication()).setMakeScheduleArrayList( saveData );

			setResult ( RESULT_OK , intent );
			finishActivity ( ScheduleManegeFragment.REQUEST_CODE_SITE );
			finish ( );
        });

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	@Override
	public void checkedDataSendToActivity(GetCheckedDataFromFragment getCheckedDataFromFragment){
		gotCheckedData.add( getCheckedDataFromFragment );
	}

	// 뷰페이지 어뎁터
	class ViewPagerAdapter extends FragmentPagerAdapter{
		private int position = 0;

		public ViewPagerAdapter ( FragmentManager fm ){
			super ( fm );
		}

		@Override
		public Fragment getItem ( int position ){
			this.position = position;

			switch ( position ){
				case 0 :
					cur_fragment = AddInterestFoodFragment.newInstance ( );
					break;
				case 1 :
					cur_fragment = AddInterestStayFragment.newInstance ( );
					break;
			}
			return cur_fragment;
		}

		public int getCurrentPosition ( ){
			return position;
		}

		@Override
		public int getCount ( ){
			return getResources ( ).getStringArray ( R.array.interest_spot_titles ).length;
		}

		@Override
		public CharSequence getPageTitle ( int position ){
			return getResources ( ).getStringArray ( R.array.interest_spot_titles ) [ position ];
		}
	}
}
