package jejusmarttour.main_mytourproduct.osy.addactivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.main_spot.detailinfo.FoodStayDetailInfoActivity;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

public class AddRecyclerViewAdapter extends RecyclerView.Adapter<AddRecyclerViewAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ScheduleVO> listData;

    private ArrayList<ScheduleVO> checkedData;
    private ArrayList<String> checkedDataName;
    private ArrayList<CheckBox> checkBoxes;

    static final int TYPE_OF_SPOT = 0;
    static final int TYPE_OF_FOOD_STAY = 1;
    private int type;   //


    AddRecyclerViewAdapter(Context context, List<ScheduleVO> listData, int type){
        this.context = context;
        this.listData = new ArrayList<>(listData);
        this.type = type;

        checkedData = new ArrayList<>();
        checkedDataName = new ArrayList<>();
        checkBoxes = new ArrayList<>();

        ( (AddFromMyList) context).checkedDataSendToActivity(() -> checkedData);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext());

        return new MyViewHolder(inflater.inflate(R.layout.item_addlistview, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ScheduleVO itemData = listData.get(position);

        if(itemData != null){
            if(itemData.isHealingContents()){
                HealingContentsVO itemVo = itemData.getHealingContentsVO();

                holder.itemView.setBackgroundResource(R.color.white);
                holder.group_linearLayout.setVisibility(View.VISIBLE);
                holder.watchImageView.setVisibility ( View.VISIBLE );
                Glide.with(context).load ( CommonData.getLocationIconId(itemVo.getHcnt_type()) ).thumbnail ( 0.1f ).into ( holder.imageView );
                holder.titleText.setText ( itemVo.getHcnt_name() == null ? "이름없음" : itemVo.getHcnt_name() );
                holder.timeText.setText ( itemVo.getHcnt_duration() == null ? "- 분" : SmartTourUtils.getDuration(itemVo.getHcnt_duration()) );
                holder.subTitleText.setText ( itemVo.getHcnt_addr ( ) == null ? "주소없음" : itemVo.getHcnt_addr ( ) );
            }
            else{
                SightSeeingContentsVO itemVo = itemData.getSightSeeingContentsVO();
                holder.itemView.setBackgroundResource(R.color.white);
                holder.group_linearLayout.setVisibility(View.VISIBLE);
                holder.imageView.setImageResource(R.drawable.location_icon);
                holder.titleText.setText ( itemVo.getCcnt_name() == null ? "이름없음" : itemVo.getCcnt_name() );
                holder.watchImageView.setVisibility ( View.GONE );
                holder.timeText.setText ( "" );
                holder.subTitleText.setText ( itemVo.getCcnt_addr() == null ? "주소없음" : itemVo.getCcnt_addr ( ) );
            }

            if ( checkedDataName.contains(listData.get(position).getContentName())){
                holder.checkBox.setChecked(true);
            }
            else {
                holder.checkBox.setChecked(false);
            }

            holder.checkBox.setOnClickListener(v -> {
                boolean checked = false;
                int removeTarget = 0;

                for (String name :
                        checkedDataName) {
                    if (name.equals(listData.get(position).getContentName()) ){
                        checked = true;

                        holder.checkBox.setChecked(false);

                        break;
                    }
                    removeTarget++;
                }

                if (!checked){
                    checkedData.add( listData.get(position) );
                    checkedDataName.add( listData.get(position).getContentName());
                }
                else {
                    checkedData.remove(removeTarget);
                    checkedDataName.remove(removeTarget);
                }

                Log.e(this.toString(), "onCheckedChanged, checkedData.size() : " + checkedData.size() );
            });
            holder.itemView.setOnClickListener(v -> {
                if (type == TYPE_OF_SPOT){
                    Intent intent = new Intent(context, DetailInfoActivity.class);
                    CommonData.setScheduleVO(listData.get(position));
                    context.startActivity(intent);
                }
                else {
                    Intent intent = new Intent(context, FoodStayDetailInfoActivity.class);
                    CommonData.setScheduleVO(listData.get(position));
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    // 외부에서 아이템 추가 요청 시 사용
    public void add ( ScheduleVO vo ){
        listData.add(vo);
    }

    // 외부에서 아이템 삭제 요청 시 사용
    public void remove ( int position ){
        listData.remove(position);
    }

    public void remove ( ScheduleVO removeData ){
        listData.remove ( removeData );
    }

    public void removeAll ( ){
        listData.clear();
    }

    public void allCheckOff(){
        boolean allCheckedOff = true;
        for (CheckBox box :
                checkBoxes) {
            box.setChecked(false);
        }
        checkBoxes.clear();
        allCheckedOff = false;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        View itemView;
        CheckBox checkBox;
        ImageView imageView;
        ImageView watchImageView;
        TextView titleText;
        TextView timeText;
        TextView subTitleText;
        LinearLayout group_linearLayout;

        MyViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            checkBox = itemView.findViewById ( R.id.checkbox );
            imageView = itemView.findViewById ( R.id.search_item_icon );
            watchImageView = itemView.findViewById(R.id.watch_icon);
            titleText = itemView.findViewById ( R.id.mtc_title_text );
            timeText = itemView.findViewById ( R.id.mtc_session_time_text );
            subTitleText = itemView.findViewById ( R.id.htp_subtitle );
            group_linearLayout =  itemView.findViewById(R.id.group_linearLayout);
       }
    }
}
