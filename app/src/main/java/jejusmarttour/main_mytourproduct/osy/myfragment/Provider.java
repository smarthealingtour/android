package jejusmarttour.main_mytourproduct.osy.myfragment;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import jejusmarttour.common.KeywordData;
import jejusmarttour.task.GetRouteTask;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.util.TimeUtil;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;

/**
 * Created by Osy on 2017-07-27.
 */

//이 클래스는 건들게 별로 없음.
public class Provider extends AbstractExpandableDataProvider
            implements JSONObjectResult< RouteVO > {

    private List<Pair<GroupData, List<ChildData>>> mData;
    private ArrayList< ScheduleVO > scheduleVOs;

    private Pair<GroupData, List<ChildData>> mLastRemovedGroup;

    private int mLastRemovedGroupPosition = -1;

    private Context context;
    private JSONObjectResult fragmentJsonResult;
    private GetRouteTask getRouteTask;
    private ArrayList<RouteVO> routeVOs;

    // for undo child item
    private ChildData mLastRemovedChild;
    private long mLastRemovedChildParentGroupId = -1;
    private int mLastRemovedChildPosition = -1;

    public Provider(ArrayList< ScheduleVO > scheduleVOs, Context context, JSONObjectResult fragmentJsonResult) {
        mData = new LinkedList<>();
        this.scheduleVOs = scheduleVOs;
        this.context = context;
        this.fragmentJsonResult = fragmentJsonResult;
        this.routeVOs = new ArrayList<>();

        int sectionCount = 1;

        for (int i = 0; i < scheduleVOs.size(); i++) {
            //noinspection UnnecessaryLocalVariable
            ScheduleVO scheduleVO = scheduleVOs.get(i);
            ConcreteGroupData group = newConcreteGroupData(scheduleVO, i);

            List<ChildData> children = new ArrayList<>();

            // add child items
            if (scheduleVOs.get(i).getMemo() != null){
                for (int j = 0; j < scheduleVO.getMemo().size(); j++) {
                    children.add(newConcreteChildData(scheduleVO, group, j));
                }
            }

            mData.add(new Pair<>( group, children));
        }
        distanceDataRefresh();
    }

    public void distanceDataRefresh(){
        for ( int i = 1 ; i < mData.size() ; i++ ){
            String preTitle = mData.get(i-1).first.getTitleText();
            String curTitle = mData.get(i).first.getTitleText();

            getRouteTask = new GetRouteTask ( context );
            getRouteTask.execute ( preTitle , curTitle , String.valueOf ( i ) , curTitle );
        }
    }

    @Override
    public void setJSONObjectResult ( RouteVO result )
    {
        if (result == null){
            Log.e(toString(), "setJSONObjectResult: 쩝..."  );
        }
        else {
            if (result.getDistance() != null){
                mData.get( Integer.valueOf( result.getPosition())).
                        first.setTravelDistance( Integer.valueOf( result.getDistance()));
            }
            else {
                mData.get( Integer.valueOf( result.getPosition())).
                        first.setTravelDistance( 9999 );
            }

            if (result.getLead_time() != null){
                mData.get( Integer.valueOf( result.getPosition())).
                        first.setFootTime( Integer.valueOf( result.getLead_time()) );
            }
            else{
                mData.get( Integer.valueOf( result.getPosition())).
                        first.setFootTime( 33 );
            }

            onEndRouteTaskListener.onEndRouteTask();
        }


    }

    public OnEndRouteTaskListener onEndRouteTaskListener;

    public void setOnEndRouteTaskListener(OnEndRouteTaskListener onEndRouteTaskListener){
        this.onEndRouteTaskListener = onEndRouteTaskListener;
    }

    public ConcreteGroupData newConcreteGroupData(ScheduleVO scheduleVO , int id){
        final long groupId = id;
        final String contentID;
        final String titleText;
        final String keywordText;
        final String addressText;
        final String timeText;
        final String telText;

        final int durationTime;

        if ( scheduleVO.getHealingContentsVO( ) != null ) // 힐링명소
        {
            contentID = scheduleVO.getHealingContentsVO( ).getHcnt_id ( );
            titleText = scheduleVO.getHealingContentsVO( ).getHcnt_name ( ) == null ? "제목없음" : scheduleVO.getHealingContentsVO( ).getHcnt_name();
            keywordText = ( scheduleVO == null ? "키워드없음" : KeywordData.getKeyword ( scheduleVO ) );
            addressText = ( scheduleVO.getHealingContentsVO( ).getHcnt_addr ( ) == null ? "주소없음" : scheduleVO.getHealingContentsVO( ).getHcnt_addr ( ) );
            telText = ( scheduleVO.getHealingContentsVO( ).getHcnt_tel ( ) == null ? "전화번호없음" : scheduleVO.getHealingContentsVO( ).getHcnt_tel ( ) );
            timeText = ( scheduleVO.getHealingContentsVO( ).getHcnt_duration ( ) == null ? "- 분" : SmartTourUtils.getDuration ( scheduleVO.getHealingContentsVO( ).getHcnt_duration ( ) ) );

            if ( scheduleVO.getHealingContentsVO( ).getHcnt_duration ( ) != null){
                durationTime = Integer.valueOf(scheduleVO.getHealingContentsVO( ).getHcnt_duration ( )) ;
            }
            else {
                durationTime = 0;
            }
        }
        else  //  관광장소
        {
            contentID = scheduleVO.getSightSeeingContentsVO( ).getCcnt_id ( );
            titleText = scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( ) == null ? "제목없음" : scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( );
            keywordText = ( scheduleVO == null ? "키워드없음" : KeywordData.getKeyword ( scheduleVO ) );
            addressText = ( "-" );
            telText = ( "-" );
            timeText = ( "- 분" );
            durationTime = 0 ;
            Log.e(this.toString(), "newConcreteGroupData: time: " + durationTime  );
        }
        return new ConcreteGroupData(groupId, contentID, titleText, keywordText, addressText, timeText, telText, durationTime);
    }

    public ConcreteChildData newConcreteChildData(ScheduleVO scheduleVO, ConcreteGroupData group, int memoId){
        long childId = group.generateNewChildId();
        String memo = scheduleVO.getMemo().get(memoId);
        String memoWriter;
        try{
            memoWriter = scheduleVO.getMemoWriter().get(memoId);
        }catch (IndexOutOfBoundsException e){
            memoWriter ="나";
        }

        return new ConcreteChildData(childId, memo, memoWriter);
    }

    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public int getChildCount(int groupPosition) {

        return mData.get(groupPosition).second.size();
    }

    //장소추가로 그룹이 더 생기는 경우.
    @Override
    public void addGroupItem(ArrayList<ScheduleVO> scheduleVOs){
        for (int i = 0 ; i < scheduleVOs.size(); i++){
            ScheduleVO scheduleVO = scheduleVOs.get(i);
            ConcreteGroupData groupData = newConcreteGroupData(scheduleVO, mData.size() + i);

            Log.e(this.toString(), "addGroupItem:  " + i +" "+scheduleVOs.size() );

            List<ChildData> childDatas = new ArrayList<>();
            for (int j = 0 ; j < scheduleVOs.get(i).getMemo().size() ; j++ ){
                ConcreteChildData childData = newConcreteChildData(scheduleVO, groupData, j);
                childDatas.add(childData);
            }

            Pair pair = new Pair<GroupData, List<ChildData>>(groupData, childDatas);
            mData.add(pair);

            this.scheduleVOs.add(scheduleVO);
        }

        distanceDataRefresh();
    }

    //메모추가로 child가 더 생기는 경우.
    @Override
    public void addChildItem(int groupPosition , String memo, String memoWriter){
        Log.e(this.toString(), "groupPosition: " + groupPosition +" mData크기는 : " +mData.size(),null );

        Pair p = mData.get(groupPosition);
        final long newId = ((ConcreteGroupData) p.first).generateNewChildId();
        ( (List<ChildData>)p.second ).add(new ConcreteChildData(newId, memo, memoWriter));

    }

    @Override
    public GroupData getGroupItem(int groupPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            throw new IndexOutOfBoundsException("groupPosition = " + groupPosition);
        }
        return mData.get(groupPosition).first;
    }

    @Override
    public ChildData getChildItem(int groupPosition, int childPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            throw new IndexOutOfBoundsException("groupPosition = " + groupPosition);
        }

//        final List<ChildData> children = mData.get(groupPosition).second;
        final List<ChildData> children = mData.get(groupPosition).second;

        if (childPosition < 0 || childPosition >= children.size()) {
            throw new IndexOutOfBoundsException("childPosition = " + childPosition + " children.size() "+ children.size());
        }

        return children.get(childPosition);
    }

    @Override
    public void moveGroupItem(int fromGroupPosition, int toGroupPosition) {
        if (fromGroupPosition == toGroupPosition) {
            return;
        }
        final Pair<GroupData, List<ChildData>> item = mData.remove(fromGroupPosition);

        mData.add(toGroupPosition, item);

        distanceDataRefresh();
    }

    @Override
    public void moveChildItem(int fromGroupPosition, int fromChildPosition, int toGroupPosition, int toChildPosition) {
        if ((fromGroupPosition == toGroupPosition) && (fromChildPosition == toChildPosition)) {
            return;
        }

        final Pair<GroupData, List<ChildData>> fromGroup = mData.get(fromGroupPosition);
        final Pair<GroupData, List<ChildData>> toGroup = mData.get(toGroupPosition);

        if (fromGroup.first.isSectionDistance()) {
            throw new IllegalStateException("Source group is a section header!");
        }
        if (toGroup.first.isSectionDistance()) {
            throw new IllegalStateException("Destination group is a section header!");
        }

        final ConcreteChildData item = (ConcreteChildData) fromGroup.second.remove(fromChildPosition);

        if (toGroupPosition != fromGroupPosition) {
            // assign a new ID
            final long newId = ((ConcreteGroupData) toGroup.first).generateNewChildId();
            item.setChildId(newId);
        }

        toGroup.second.add(toChildPosition, item);
    }

    @Override
    public void removeGroupItem(int groupPosition) {
        mLastRemovedGroup = mData.remove(groupPosition);
        mLastRemovedGroupPosition = groupPosition;

        mLastRemovedChild = null;
        mLastRemovedChildParentGroupId = -1;
        mLastRemovedChildPosition = -1;

        distanceDataRefresh();
    }

    @Override
    public void removeChildItem(int groupPosition, int childPosition) {
        mLastRemovedChild = mData.get(groupPosition).second.remove(childPosition);
        mLastRemovedChildParentGroupId = mData.get(groupPosition).first.getGroupId();
        mLastRemovedChildPosition = childPosition;

        mLastRemovedGroup = null;
        mLastRemovedGroupPosition = -1;
    }

    //지운거 실행취소
    @Override
    public long undoLastRemoval() {
        if (mLastRemovedGroup != null) {
            return undoGroupRemoval();
        } else if (mLastRemovedChild != null) {
            return undoChildRemoval();
        } else {
            return RecyclerViewExpandableItemManager.NO_EXPANDABLE_POSITION;
        }
    }
    private long undoGroupRemoval() {
        int insertedPosition;
        if (mLastRemovedGroupPosition >= 0 && mLastRemovedGroupPosition < mData.size()) {
            insertedPosition = mLastRemovedGroupPosition;
        } else {
            insertedPosition = mData.size();
        }

        mData.add(insertedPosition, mLastRemovedGroup);

        mLastRemovedGroup = null;
        mLastRemovedGroupPosition = -1;

        distanceDataRefresh();

        return RecyclerViewExpandableItemManager.getPackedPositionForGroup(insertedPosition);
    }
    private long undoChildRemoval() {
        Pair<GroupData, List<ChildData>> group = null;

        int groupPosition = -1;

        // find the group
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).first.getGroupId() == mLastRemovedChildParentGroupId) {
                group = mData.get(i);
                groupPosition = i;
                break;
            }
        }

        if (group == null) {
            return RecyclerViewExpandableItemManager.NO_EXPANDABLE_POSITION;
        }

        int insertedPosition;
        if (mLastRemovedChildPosition >= 0 && mLastRemovedChildPosition < group.second.size()) {
            insertedPosition = mLastRemovedChildPosition;
        } else {
            insertedPosition = group.second.size();
        }

        group.second.add(insertedPosition, mLastRemovedChild);

        mLastRemovedChildParentGroupId = -1;
        mLastRemovedChildPosition = -1;
        mLastRemovedChild = null;

        return RecyclerViewExpandableItemManager.getPackedPositionForChild(groupPosition, insertedPosition);
    }

    public static final class ConcreteGroupData extends GroupData {
        private final long mId;
        private boolean mPinned;
        private long mNextChildId;

        private final String contentID;
        private final String titleText;
        private final String keywordText;
        private final String addressText;
        private final String timeText;
        private final String telText;

        private final int durationTime;

        private int travelDistance;
        private TimeUtil endTime ;
        private int footTime;

        ConcreteGroupData(long id,String contentID, String titleText, String keywordText,
                          String addressText ,String timeText ,String telText, int durationTime) {
            this.contentID = contentID;
            this.titleText = titleText;
            this.keywordText = keywordText;
            this.addressText = addressText;
            this.timeText = timeText;
            this.telText = telText;
            this.durationTime = durationTime;

            mId = id;
            mNextChildId = 0;
        }

        @Override
        public long getGroupId() {
            return mId;
        }

        @Override
        public String getContentID() {
            return contentID;
        }

        @Override
        public int getDurationTime() {
            return durationTime;
        }

        @Override
        public void setTravelDistance(int distance) {
            this.travelDistance = distance;
        }

        @Override
        public int getTravelDistance() {
            return travelDistance;
        }

        @Override
        public void setFootTime( int minute) {
            footTime = minute;
        }

        @Override
        public int getFootTime() {
            return footTime;
        }

        @Override
        public void setEndTime(int hour, int minute) {
            endTime = new TimeUtil();
            endTime.setTime(hour, minute);
        }

        @Override
        public TimeUtil getEndTime() {
            return endTime;
        }

        @Override
        public String getTitleText() {
            return titleText;
        }

        @Override
        public String getKeywordText() {
            return keywordText;
        }

        @Override
        public String getAddressText() {
            return addressText;
        }

        @Override
        public String getTimeText() {
            return timeText;
        }

        @Override
        public String getTelText() {
            return telText;
        }

        @Override
        public boolean isSectionDistance() {
            return false;
        }

        @Override
        public void setPinned(boolean pinnedToSwipeLeft) {
            mPinned = pinnedToSwipeLeft;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        public long generateNewChildId() {
            final long id = mNextChildId;
            Log.e("provider", "generateNewChildId: "+id ,null );
            mNextChildId += 1;
            return id;
        }
    }

    public static final class ConcreteChildData extends ChildData {
        private long mId;
        private final String Memo;
        private final String memoWriter;
        private boolean mPinned;

        ConcreteChildData(long id, String memo, String MemoWriter) {
            mId = id;
            Memo = memo;
            memoWriter = MemoWriter;
        }

        @Override
        public String getMemo() {
            return Memo;
        }

        @Override
        public String getMemoWriter() {
            return memoWriter;
        }

        @Override
        public long getChildId() {
            return mId;
        }

        @Override
        public void setPinned(boolean pinnedToSwipeLeft) {
            mPinned = pinnedToSwipeLeft;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        public void setChildId(long id) {
            this.mId = id;
        }
    }

}