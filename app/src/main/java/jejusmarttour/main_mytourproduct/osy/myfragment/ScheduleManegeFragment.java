package jejusmarttour.main_mytourproduct.osy.myfragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.animator.DraggableItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.kakao.kakaotalk.KakaoTalkService;
import com.kakao.kakaotalk.callback.TalkResponseCallback;
import com.kakao.kakaotalk.response.KakaoTalkProfile;
import com.kakao.network.ErrorResult;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.MTCAddHTPActivity;
import jejusmarttour.main_mytourproduct.MTCMakeScheduleActivity;
import jejusmarttour.main_mytourproduct.osy.addactivity.AddFromMyFoodStayListActivity;
import jejusmarttour.main_mytourproduct.osy.addactivity.AddFromMySpotListActivity;
import jejusmarttour.main_mytourproduct.osy.addactivity.addsearch.AddSearchActivity;
import jejusmarttour.main_mytourproduct.osy.addactivity.MTCAddFoodStayActivity;
import jejusmarttour.main_mytourproduct.osy.addactivity.MTCAddSiteActivity;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.util.CustomViewPager;
import jejusmarttour.util.ImageLoader;
import jejusmarttour.vo.MTCScheduleVO;
import jejusmarttour.vo.MTCSmartTourProductsVO;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2017-07-26.
 */

public class ScheduleManegeFragment extends Fragment
        implements RecyclerViewExpandableItemManager.OnGroupCollapseListener,
        RecyclerViewExpandableItemManager.OnGroupExpandListener, JSONObjectResult< RouteVO > {

    private static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";
    private static final boolean ALLOW_ITEMS_MOVE_ACROSS_SECTIONS = false; // Set this flag "true" to change draggable item range

    // 검색 추가 결과값 요청코드
    public static final int REQUEST_SEARCH = 0;
    // 장소 추가 결과값 요청코드
    public static final int REQUEST_CODE_SITE = 1;
    // 여행 상품 중 장소추가 결과값 요청코드
    public static final int REQUEST_CODE_SELECT_COURSE = 2;

    private int MAX_DISTANCE_COUNT = 0;
    private final int MIN_MEMO_TEXT_LENTH = 10;
    private String currentWriteMemoTAG = null;
    private ProgressDialog progressDialog;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;
    private RecyclerViewDragDropManager mRecyclerViewDragDropManager;

    // 일정 수정인지 아닌지 구분
    private boolean isModifySchedule = false;
    // 일정 리스트
    private ArrayList < ScheduleVO > makeScheduleArrayList;

    private MTCSmartTourProductsVO mtcSmartTourProductsVO;

    // 서버 JSON필드값으로 데이터 저장
    private ArrayList < MTCScheduleVO > changedScheduleArrayList;

    private MyAdapter myItemAdapter;

    // 현재 생성/수정 중인 일정 날짜
    private int newDateNum = 1;

    private LinearLayout buttonsLayout, memoLayout;
    private EditText memoText;
    private CustomViewPager viewPager;

    public static ScheduleManegeFragment newInstance(MTCSmartTourProductsVO mtcSmartTourProductsVO, boolean isModifySchedule, int newDateNum, CustomViewPager viewPager){
        ScheduleManegeFragment fragment = new ScheduleManegeFragment( );
        fragment.mtcSmartTourProductsVO = mtcSmartTourProductsVO;
        fragment.isModifySchedule = isModifySchedule;
        fragment.newDateNum = newDateNum;
        fragment.viewPager = viewPager;

        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule_manager, container, false);

        if ( mtcSmartTourProductsVO == null )
            mtcSmartTourProductsVO = new MTCSmartTourProductsVO ( );

        changedScheduleArrayList = new ArrayList<>();
        makeScheduleArrayList = new ArrayList<>();

        ( (MTCMakeScheduleActivity) getActivity ( ) ).swipeOnOff ( false );

        //final Intent siteIntent = new Intent ( getActivity ( ) , MTCAddSiteActivity.class );
        initAddButtons(view);
        initMemoButtons(view);

        //이게 true면 일정수정을 하는경우 이므로 스케줄을 얻어옴.
        if (isModifySchedule){
            makeScheduleArrayList = mtcSmartTourProductsVO.getScheduleArrayList ( );
        }

        addContents();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //noinspection ConstantConditions
        mRecyclerView = getView().findViewById(R.id.list_view);
        mLayoutManager = new LinearLayoutManager(getContext());

        final Parcelable eimSavedState = (savedInstanceState != null) ?
                savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;
        mRecyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);
        mRecyclerViewExpandableItemManager.setOnGroupExpandListener(this);
        mRecyclerViewExpandableItemManager.setOnGroupCollapseListener(this);

        //어댑터에서 사용할 리스너. 요기 프래그먼트가 관련되어 리스너 여기서 생성하고 보내줌.

        //adapter
        myItemAdapter = new MyAdapter( getDataProvider(), new GroupClickListener() , new ChildClickListener() );

        //요 어댑터들에 펼치고, 접고, 옮기는 등의 메서드 있음
        mWrappedAdapter = mRecyclerViewExpandableItemManager.createWrappedAdapter(myItemAdapter);       // 어댑터에 expand 장착

        // drag & drop manager
        mRecyclerViewDragDropManager = new RecyclerViewDragDropManager( mRecyclerViewExpandableItemManager );
        mRecyclerViewDragDropManager.setOnItemDragEventListener(new RecyclerViewDragDropManager.OnItemDragEventListener() {

            @Override
            public void onItemDragStarted(int position) {
                //드래그 시작하면 펼쳐진 메모들 접힘.
                //myItemAdapter.goneDistanceLayouts();
                if (!mRecyclerViewExpandableItemManager.isAllGroupsCollapsed()){
                    mRecyclerViewExpandableItemManager.collapseAll();

                    //mRecyclerViewExpandableItemManager
                    /*mRecyclerViewDragDropManager.checkConditionAndStartDragging();*/
                }
            }

            @Override
            public void onItemDragPositionChanged(int fromPosition, int toPosition) {
            }

            @Override
            public void onItemDragFinished(int fromPosition, int toPosition, boolean result) {
                Log.e("드래그완료", "onItemDragFinished: " + fromPosition+ " 에서 " + toPosition,null );
                //드래그 끝나면 데이터의 위치 조정.
                ScheduleVO tempVO = makeScheduleArrayList.get(fromPosition);
                makeScheduleArrayList.remove(fromPosition);
                makeScheduleArrayList.add(toPosition, tempVO);

                //MTCScheduleVO의 seq 조정.
                MTCScheduleVO tempMtcVO = changedScheduleArrayList.get(fromPosition);
                changedScheduleArrayList.remove(fromPosition);
                changedScheduleArrayList.add(toPosition, tempMtcVO);

                for(int i = 0 ; i<changedScheduleArrayList.size() ; i++ ){
                    changedScheduleArrayList.get(i).setSeq( String.valueOf(i+1) );
                }
                //myItemAdapter.setNullDistanceLayouts();
            }

            @Override
            public void onItemDragMoveDistanceUpdated(int offsetX, int offsetY) {

            }
        });

        //요기 어댑터들에 펼치고, 접고, 옮기는 등의 메서드 있음
        mWrappedAdapter = mRecyclerViewDragDropManager.createWrappedAdapter( mWrappedAdapter );           // dragging 장착

        mRecyclerViewDragDropManager.setDraggingItemShadowDrawable(
                (NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.material_shadow_z3));

        mRecyclerViewDragDropManager.setCheckCanDropEnabled(ALLOW_ITEMS_MOVE_ACROSS_SECTIONS);
        //myItemAdapter.setAllowItemsMoveAcrossSections(ALLOW_ITEMS_MOVE_ACROSS_SECTIONS);

        final GeneralItemAnimator animator = new DraggableItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Need to disable them when using animation indicator.
        animator.setSupportsChangeAnimations(false);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        mRecyclerView.setItemAnimator(animator);
        mRecyclerView.setHasFixedSize(false);

        // additional decorations
        //noinspection StatementWithEmptyBody
        /*if (supportsViewElevation()) {
            // Lollipop or later has native drop shadow feature. ItemShadowDecorator is not required.
        } else {
            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.material_shadow_z1)));
        }
        mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R.drawable.list_divider_h), true));*/

        // NOTE:
        // The initialization order is very important! This order determines the priority of touch event handling.
        // priority: TouchActionGuard > Swipe > DragAndDrop > ExpandableItem

//        mRecyclerViewTouchActionGuardManager.attachRecyclerView(mRecyclerView);
        mRecyclerViewDragDropManager.attachRecyclerView(mRecyclerView);
        mRecyclerViewExpandableItemManager.attachRecyclerView(mRecyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initAddButtons(View view){
        buttonsLayout = view.findViewById(R.id.mtc_button_layout);

        Button addSpotBtn = view.findViewById ( R.id.mtc_add_site_btn );
        Button addRoomBoardBtn = view.findViewById ( R.id.mtc_add_food_stay_btn);
        ImageButton addSearchBtn = view.findViewById ( R.id.add_search_btn);
        addSpotBtn.setOnClickListener (v -> addSpotPopUp());
        addRoomBoardBtn.setOnClickListener(v -> addFoodStayPopUp());
        addSearchBtn.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AddSearchActivity.class);
            startActivityForResult(intent, REQUEST_SEARCH);
        });
    }

    private void initMemoButtons(View view){
        memoLayout = view.findViewById(R.id.mtc_memo_layout);
        memoText = view.findViewById(R.id.memo_edit_text);

        //메모 취소
        final Button memoCancelBtn = view.findViewById ( R.id.mtc_memo_cancel_btn );
        memoCancelBtn.setOnClickListener (v -> {
            memoText.setText ( "" );
            memoText.clearFocus ( );

            buttonsLayout.setVisibility ( View.VISIBLE );
            memoLayout.setVisibility ( View.GONE );

            InputMethodManager imm = ( InputMethodManager ) getActivity ( ).getSystemService ( Context.INPUT_METHOD_SERVICE );
            imm.hideSoftInputFromWindow(memoText.getWindowToken(), 0);
        });

        memoText = view.findViewById ( R.id.memo_edit_text );
        final Button memoWriteBtn = view.findViewById ( R.id.mtc_memo_write_btn );

        //메모 쓰기
        memoWriteBtn.setOnClickListener ( v -> {
            if ( validateMemoText ( ) ){
                String viewTag = currentWriteMemoTAG;

                for ( int groupPosition = 0 ; groupPosition < makeScheduleArrayList.size ( ) ; groupPosition++ ){
                    if ( viewTag.equals ( makeScheduleArrayList.get ( groupPosition ).getContentName ( ) ) ){
                        if ( makeScheduleArrayList.get ( groupPosition ).getMemo ( ) == null ){
                            makeScheduleArrayList.get ( groupPosition ).setMemo ( new ArrayList<> ( ) );
                            changedScheduleArrayList.get ( groupPosition ).setMemo (new ArrayList<> () );
                        }

                        makeScheduleArrayList.get ( groupPosition ).getMemo ( ).add ( String.valueOf ( memoText.getText ( ) ) );
                        changedScheduleArrayList.get ( groupPosition ).getMemo ( ).add ( String.valueOf ( memoText.getText ( ) ) );

                        myItemAdapter.addChildItem(groupPosition, String.valueOf ( memoText.getText ( ) ), "나");
                    }
                }

                removeEditText();
                removePreferences();
                buttonsLayout.setVisibility ( View.VISIBLE );
                memoLayout.setVisibility(View.GONE);

                InputMethodManager imm = ( InputMethodManager ) getActivity ( ).getSystemService ( Context.INPUT_METHOD_SERVICE );
                imm.hideSoftInputFromWindow(memoText.getWindowToken(), 0);

                updateScheduleData();
            }
            else if ( memoText.getText ( ).length ( ) == 0 ){
                buttonsLayout.setVisibility ( View.VISIBLE );
                memoLayout.setVisibility ( View.GONE );
            }
        });
    }

    // 현재 데이터 저장
    private void updateScheduleData ( ) {
        Log.e( this.toString() , "updateScheduleData");
        Log.e("ScheduleManegeFragment", "makeScheduleArrayList: " + makeScheduleArrayList.get(0).getContentName() );
        Log.e("ScheduleManegeFragment", "changedScheduleArrayList: " + changedScheduleArrayList.get(0).getHcnt_id() );

        mtcSmartTourProductsVO.setScheduleArrayList ( makeScheduleArrayList );
        mtcSmartTourProductsVO.setMtcScheduleArrayList (changedScheduleArrayList);
        ( (MTCMakeScheduleActivity) getActivity ( ) ).setScheduleData( mtcSmartTourProductsVO );
    }

    private void removeEditText ( )
    {
        memoText.setText ( null );
    }
    // 값(Key Data) 삭제하기

    private boolean validateMemoText ( ){
        if ( memoText.getText ( ) != null ){
            if ( memoText.getText ( ).toString ( ).length ( ) < MIN_MEMO_TEXT_LENTH ){
                Toast.makeText ( getActivity ( ) , MIN_MEMO_TEXT_LENTH + "자 이상 적어주세요." , Toast.LENGTH_SHORT ).show ( );
                return false;
            }
        }
        return true;
    }

    private void addSpotPopUp(){
        LinearLayout layout = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.popup_add_products , null );

        Button addByMyListBtn = layout.findViewById ( R.id.add_by_mylist_btn );
        Button addHTPBtn = layout.findViewById ( R.id.add_htp_btn );
        Button addMTPBtn = layout.findViewById ( R.id.add_mtp_btn );

        addByMyListBtn.setText("  나의 관심장소에서 추가");
        addHTPBtn.setText("  나의 여행상품에서 추가");
        addMTPBtn.setText("  전체 관광지에서 추가");

        AlertDialog.Builder alertBox = new AlertDialog.Builder ( getActivity ( ) );
        alertBox.setView ( layout );
//        alertbox.setView(R.layout.popup_add_products);
        alertBox.create ( );

        final DialogInterface popup = alertBox.show ( );

        addByMyListBtn.setOnClickListener(v -> {
            //나의 관심장소에서 추가
            Intent intent = new Intent ( getActivity ( ) , AddFromMySpotListActivity.class );
            ( ( CommonData ) getActivity ( ).getApplication ( ) ).setMakeScheduleArrayList ( new ArrayList < ScheduleVO > ( ) );
            startActivityForResult ( intent , REQUEST_CODE_SITE );
            popup.dismiss ( );
        });

        addHTPBtn.setOnClickListener (v -> {
            //나의 여행 상품에서 추가
            Intent intent = new Intent ( getActivity ( ) , MTCAddHTPActivity.class );
            ( (CommonData) getActivity ( ).getApplication ( ) ).setMakeScheduleArrayList ( new ArrayList < ScheduleVO > ( ) );
            startActivityForResult ( intent , REQUEST_CODE_SELECT_COURSE );
            popup.dismiss ( );
        });

        addMTPBtn.setOnClickListener (v -> {
            //전체 관광지에서 추가
            Intent intent = new Intent ( getActivity ( ) , MTCAddSiteActivity.class );
            ( ( CommonData ) getActivity ( ).getApplication ( ) ).setMakeScheduleArrayList ( new ArrayList < ScheduleVO > ( ) );
            startActivityForResult ( intent , REQUEST_CODE_SITE );
            popup.dismiss ( );
        });
    }

    private void addFoodStayPopUp(){
        LinearLayout layout = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.popup_add_products , null );

        Button addByMyListBtn = layout.findViewById ( R.id.add_by_mylist_btn );
        Button addHTPBtn = layout.findViewById ( R.id.add_htp_btn );
        Button addMTPBtn = layout.findViewById ( R.id.add_mtp_btn );

        addByMyListBtn.setText("  나의 관심장소에서 추가");
        addHTPBtn.setText("  전체 음식점에서 추가");
        addMTPBtn.setText("  전체 숙소에서 추가");

        AlertDialog.Builder alertBox = new AlertDialog.Builder ( getActivity ( ) );
        alertBox.setView ( layout );
//        alertBox.setView(R.layout.popup_add_products);
        alertBox.create ( );

        final DialogInterface popup = alertBox.show ( );

        addByMyListBtn.setOnClickListener(v -> {
            //나의 관심여행상품
            Intent intent = new Intent(getActivity(), AddFromMyFoodStayListActivity.class);
            ( ( CommonData ) getActivity ( ).getApplication ( ) ).setMakeScheduleArrayList (new ArrayList<>() );
            startActivityForResult ( intent , REQUEST_CODE_SITE );
            popup.dismiss ( );

        });

        addHTPBtn.setOnClickListener (v -> {
            //음식점
            Intent intent = new Intent ( getActivity ( ) , MTCAddFoodStayActivity.class );
            intent.putExtra("type","food");
            ( ( CommonData ) getActivity ( ).getApplication ( ) ).setMakeScheduleArrayList ( new ArrayList<> ( ) );
            startActivityForResult ( intent , REQUEST_CODE_SELECT_COURSE );
            popup.dismiss ( );
        });

        addMTPBtn.setOnClickListener (v -> {
            //숙박
            Intent intent = new Intent ( getActivity ( ) , MTCAddFoodStayActivity.class );
            intent.putExtra("type","stay");
            ( ( CommonData ) getActivity ( ).getApplication ( ) ).setMakeScheduleArrayList ( new ArrayList<> ( ) );
            startActivityForResult ( intent , REQUEST_CODE_SITE );
            popup.dismiss ( );
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save current state to support screen rotation, etc...
        if (mRecyclerViewExpandableItemManager != null) {
            outState.putParcelable(
                    SAVED_STATE_EXPANDABLE_ITEM_MANAGER,
                    mRecyclerViewExpandableItemManager.getSavedState());
        }
    }

    @Override
    public void onDestroyView() {
        if (mRecyclerViewDragDropManager != null) {
            mRecyclerViewDragDropManager.release();
            mRecyclerViewDragDropManager = null;
        }

        if (mRecyclerViewExpandableItemManager != null) {
            mRecyclerViewExpandableItemManager.release();
            mRecyclerViewExpandableItemManager = null;
        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }
        mLayoutManager = null;

        super.onDestroyView();
    }

    //그룹이 접힐때
    @Override
    public void onGroupCollapse(int groupPosition, boolean fromUser, Object payload) { }

    //그룹이 펼쳐졌을때
    @Override
    public void onGroupExpand(int groupPosition, boolean fromUser, Object payload) {
        if (fromUser) {
            adjustScrollPositionOnGroupExpanded(groupPosition);
        }
    }

    private void adjustScrollPositionOnGroupExpanded(int groupPosition) {
        int childItemHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.list_item_height);
        int topMargin = (int) (getActivity().getResources().getDisplayMetrics().density * 16); // top-spacing: 16dp
        int bottomMargin = topMargin; // bottom-spacing: 16dp

        mRecyclerViewExpandableItemManager.scrollToGroup(groupPosition, childItemHeight, topMargin, bottomMargin);
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    // 카카오 이미지 불러오기
    private void addKakaoImageInMemo (final ImageView imageView ){
        String imgResource = "userimg_icon";
        final ImageLoader iLoader = new ImageLoader ( getActivity ( ) ,
                getResources ( ).getIdentifier ( imgResource , "drawable" , getActivity ( ).getPackageName ( ) ) );
        String photoStr = CommonData.getUserProfileImage ( getActivity ( ) );

        if ( photoStr.equals ( "" ) ){
            KakaoTalkService.requestProfile (new TalkResponseCallback<KakaoTalkProfile>(){
                @Override
                public void onNotKakaoTalkUser() {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onSessionClosed (ErrorResult errorResult) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onNotSignedUp () {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onSuccess (KakaoTalkProfile result) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onSuccessForUiThread(KakaoTalkProfile resultObj) {
                    // TODO Auto-generated method stub
					/*Log.d(TAG, "UserProfile : " + resultObj.getThumbnailURL());
					CommonData.setUserProfileImage(MyPageActivity.this, resultObj.getThumbnailURL());
					iLoader.DisplayCornerImage(resultObj.getThumbnailURL(), kakao_user_photo, 1, 1, 0);*/
                }
            } );
        }
        else{
            iLoader.DisplayCornerImage ( photoStr , imageView , 1 , 1 , 0 );
        }
    }

    // 일정 데이타 재정리
    private void invalidateSchedule ( ){
        if ( makeScheduleArrayList.size ( ) == 0 )
            return;
        // 처음에 뿌려줄때...
        MAX_DISTANCE_COUNT = makeScheduleArrayList.size ( ) - 1;
        //scheduleContainer.removeAllViewsInLayout();

        changedScheduleArrayList = new ArrayList<> ( );

        /*for ( int i = 0 ; i < makeScheduleArrayList.size ( ) ; i++ ){
            //getDistance ( i , makeScheduleArrayList.get ( i ).getContentID ( ) ); // 거리추가
        }

        if(makeScheduleArrayList.size ( ) == 1) {
            //addContents ( );
        }
        else{
            //progressDialog = ProgressDialog.show ( getActivity ( ) , "" , "Loading..." , true , false );
        }*/
        addContents ( );
        updateScheduleData ( );
    }

    //컨텐츠 ArrayList add하기
    private void addContents ( ){
        //makeSA내용 changedSA로..
        int count = makeScheduleArrayList.size ( );

        for ( int i = 0 ; i < count ; i++ ) {

            changedScheduleArrayList.add(new MTCScheduleVO());

            String contentID;
            if ( makeScheduleArrayList.get ( i ).getHealingContentsVO( ) != null ){   // 힐링명소
                contentID = makeScheduleArrayList.get(i).getHealingContentsVO().getHcnt_id();
            }
            else{
                contentID = makeScheduleArrayList.get ( i ).getSightSeeingContentsVO( ).getCcnt_id ( );
            }
            changedScheduleArrayList.get(i).setSeq(i + 1 + "");
            changedScheduleArrayList.get(i).setHcnt_id(contentID);

            if (makeScheduleArrayList.get(i).getMemo() != null){           // 메모추가
                int memoCount = makeScheduleArrayList.get(i).getMemo().size();
                ArrayList<String> memoArr = new ArrayList<String>();
                for (int k = 0; k < memoCount; k++) {
                    memoArr.add(makeScheduleArrayList.get(i).getMemo().get(k));
                }
                changedScheduleArrayList.get(i).setMemo(memoArr);
            }
        }
        //insertDistanceLayout();
    }

    @Override
    public void onStop(){
        super.onStop ( );
        savePreferences(memoText.getText().toString());
    }

    @Override
    public void onResume ( ){
        super.onResume ( );
        if ( !getPreferences().equals("") ){
            memoText.setText ( getPreferences ( ) );
        }
    }

    //이 preferences들은 메모들의 작성중 액티비티 변경 시 메모를 저장하고 다시 돌아올시 값을 전달해줌.
    // 값(Key Data) 삭제하기
    private void removePreferences ( ){
        SharedPreferences pref = getActivity ( ).getSharedPreferences ( "pref" , getActivity ( ).MODE_PRIVATE );
        SharedPreferences.Editor editor = pref.edit ( );
        editor.remove("memo");
        editor.commit();
    }

    // 작성하던 메모 값 불러오기
    private String getPreferences ( ){
        SharedPreferences pref = getActivity ( ).getSharedPreferences ( "pref" , getActivity ( ).MODE_PRIVATE );
        return pref.getString ( "memo" , "" );
    }

    // 작성하던 메모 값 저장하기
    private void savePreferences ( String memo ){
        SharedPreferences pref = getActivity ( ).getSharedPreferences ( "pref" , getActivity ( ).MODE_PRIVATE );
        SharedPreferences.Editor editor = pref.edit ( );
        editor.putString("memo", memo);
        editor.commit ( );
    }

    @Override       // 장소추가버튼 클릭 후 돌아오는 결과.
    public void onActivityResult ( int requestCode , int resultCode , Intent data ){

        Log.e("실행되기는 하는가?", "onActivityResult : " );

        addParseSchedule ( ( ( CommonData ) getActivity ( ).getApplication ( ) ).getMakeScheduleArrayList ( ) );
        invalidateSchedule ( );

    }

    /*// distance TASK 실행  (두개 장소 거리 계산)
    private void getDistance ( int position , String contentID )
    {
        int count = makeScheduleArrayList.size ( );

        String start_x = null;
        String start_y = null;
        String end_x = null;
        String end_y = null;

        int nextContentsNum = position + 1;

        if ( nextContentsNum < count )
        {
            ScheduleVO currentVO = makeScheduleArrayList.get ( position );
            ScheduleVO nextVO = makeScheduleArrayList.get ( nextContentsNum );

            Log.d ( TAG , "start_x : " + start_x + "  start_y :" + start_y + "  end_x : " + end_x + "  end_y" + end_y );
            getRouteTask = new GetRouteTask( getActivity ( ) , this );
            // executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR ,
            // execute (
            getRouteTask.execute ( currentVO.getContentID ( ) , nextVO.getContentID ( ) , String.valueOf ( position ) , contentID );
        }
    }*/

    // getRouteTask에서 태스크 종료후 실행되는 메서드.
    @Override
    public void setJSONObjectResult ( RouteVO result ){
        /*RouteVO routeVO = result;

        MTCScheduleVO mtcScheduleVO = new MTCScheduleVO ( );
        mtcScheduleVO.setHealingContentsVO ( routeVO.getContentID ( ) );
        mtcScheduleVO.setLead_time ( routeVO.getLead_time ( ) );
        mtcScheduleVO.setDistance ( routeVO.getDistance ( ) );
        mtcScheduleVO.setGeomery ( routeVO.getGeometry ( ) );

        Log.d("제이슨리절트", "setJSONObjectResult: id" + routeVO.getContentID ( ) +
                " lead_time : "+ routeVO.getLead_time ( ) +
                " distance : "+ routeVO.getDistance ( ) +
                " getGeometry : " + routeVO.getGeometry ( ), null);
        changedScheduleArrayList.add ( mtcScheduleVO );

        //addDistance(routeVO);*/
    }

    // 기존 VO를 업데이용 VO 로 Parse
    private void addParseSchedule ( ArrayList < ScheduleVO > scheduleVOs ){
        if ( scheduleVOs.size ( ) == 0 )
            return;

        int first_count = scheduleVOs.size ( );
        Log.e("myFragment 822", "first_count : " + first_count, null);

        //곂치는 스케줄 딜리트어레이에 올려놓음
        ArrayList < ScheduleVO > deleteArr = new ArrayList < ScheduleVO > ( );
        for ( int i = 0 ; i < makeScheduleArrayList.size ( ) ; i++ ){
            for ( int k = 0 ; k < scheduleVOs.size ( ) ; k++ )
            {
                if (makeScheduleArrayList.get(i).getContentID().equals(scheduleVOs.get(k).getContentID())){
                    deleteArr.add ( scheduleVOs.get ( k ) );
                }
            }
        }

        if ( deleteArr.size ( ) > 0 ){
            for ( int i = 0 ; i < deleteArr.size ( ) ; i++ )
                scheduleVOs.remove ( deleteArr.get ( i ) );
        }

        //추가한것들이 기존의것들에 모두 있다면.
        int last_count = scheduleVOs.size ( );

        Log.e("myFragment 846", "last_count : " + last_count, null);
        if ( last_count == 0 ){
            Toast.makeText ( getActivity ( ) , "중복된 장소입니다." , Toast.LENGTH_SHORT ).show ( );
            return;
        }

        //추가한것들중 기존에것에 있는것들만 삭제후 나머지 추가.
        else if ( last_count > 0 && ( first_count != last_count ) )
            Toast.makeText ( getActivity ( ) , "중복된 장소는 삭제되었습니다." , Toast.LENGTH_SHORT ).show ( );

        MTCScheduleVO mtcScheduleVO = null;

        for ( int i = 0 ; i < scheduleVOs.size ( ) ; i++ ){
            mtcScheduleVO = new MTCScheduleVO ( );
            mtcScheduleVO.setSeq ( changedScheduleArrayList.size ( ) + 1 + "" );
            String contentID = scheduleVOs.get ( i ).getContentID ( );
            Log.e("contentID", "contentID: " + contentID, null );
            mtcScheduleVO.setHcnt_id ( contentID );

            scheduleVOs.get ( i ).setDay ( newDateNum + "" );

            if ( scheduleVOs.get ( i ).getMemo ( ) != null ){ // 메모추가

                int memoCount = scheduleVOs.get ( i ).getMemo ( ).size ( );

                ArrayList < String > memeArr = new ArrayList < String > ( );
                for ( int k = 0 ; k < memoCount ; k++ ){
                    memeArr.add ( scheduleVOs.get ( i ).getMemo ( ).get ( k ) );
                }

                mtcScheduleVO.setMemo ( memeArr );
            }
        }
        makeScheduleArrayList.addAll ( scheduleVOs );
        changedScheduleArrayList.add ( mtcScheduleVO );

        myItemAdapter.addGroupItem(scheduleVOs);

        Intent i = new Intent("mtc");
        i.putExtra("changedScheduleArrayList", changedScheduleArrayList);
        i.putExtra("makeScheduleArrayList", makeScheduleArrayList);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(i);
    }

    //그룹리스트 클릭 시
    class GroupClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                //왼쪽 드래그할수 있는 부분 누를 경우 메모가 펼치고,접고
                case R.id.drag_handle:
                    for (int i = 0; i < makeScheduleArrayList.size(); i++) {
                        if (makeScheduleArrayList.get(i).getContentName().equals(v.getTag().toString())) {
                            if(mRecyclerViewExpandableItemManager.isGroupExpanded(i)){
                                mRecyclerViewExpandableItemManager.collapseGroup(i);
                            }
                            else{
                                mRecyclerViewExpandableItemManager.expandGroup(i);
                            }
                            break;
                        }
                    }
                    break;

                case R.id.set_course_btn :
                    viewPager.setCurrentItem(1);
                    break;

                //리스트 클릭시 상세화면으로 이동
                case R.id.content_box:
                    ScheduleVO scheduleVO = new ScheduleVO ( );
                    for (int i = 0; i < makeScheduleArrayList.size(); i++) {
                        if (makeScheduleArrayList.get(i).getContentName().equals(v.getTag().toString())) {
                            scheduleVO = makeScheduleArrayList.get(i);
                            break;
                        }
                    }
                    CommonData.setScheduleVO ( scheduleVO );

                    Intent intent = new Intent ( getActivity ( ) , DetailInfoActivity.class );
                    startActivity ( intent );
                    break;

                //메모추가를 누르면 화면 최하단에 나타나는 레이아웃의 메모추가 버튼.
                case R.id.mtc_add_memo_btn:
                    //메모추가버튼 눌러도 메모가 펼쳐지게
                    for (int i = 0; i < makeScheduleArrayList.size(); i++) {
                        if (makeScheduleArrayList.get(i).getContentName().equals(v.getTag().toString())) {
                            mRecyclerViewExpandableItemManager.expandGroup(i);
                        }
                    }

                    buttonsLayout.setVisibility(View.GONE);
                    memoLayout.setVisibility(View.VISIBLE);
                    currentWriteMemoTAG = v.getTag ( ).toString ( );
                    // 포커스
                    memoText.requestFocus();
                    // 키보드 보이기
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(memoText, InputMethodManager.SHOW_FORCED);
                    break;

                //그룹리스트 삭제버튼.
                case R.id.delete_btn:
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("정말로 삭제하시겠습니까?");
                    builder.setPositiveButton("확인", (dialog, which) -> {
                        int k;
                        for (k = 0; k < makeScheduleArrayList.size(); k++) {
                            if (makeScheduleArrayList.get(k).getContentName().equals(v.getTag().toString())) {
                                makeScheduleArrayList.remove(k);
                                changedScheduleArrayList.remove(k);

                                myItemAdapter.removeGroupItem(k);
                                break;
                            }
                        }

                        invalidateSchedule();

                        Intent i = new Intent("mtc");
                        i.putExtra("changedScheduleArrayList", changedScheduleArrayList);
                        i.putExtra("makeScheduleArrayList", makeScheduleArrayList);
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(i);
                    });

                    builder.setNegativeButton("취소", (dialog, which) -> dialog.dismiss());
                    builder.show();
                    break;
            }
        }
    }

    //메모리스트의 x(삭제버튼) 클릭시
    class ChildClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.delete_btn:
                    final AlertDialog.Builder builder = new AlertDialog.Builder ( getActivity ( ) );
                    builder.setMessage ( "정말로 삭제하시겠습니까?" );
                    builder.setPositiveButton ( "확인" , (dialog, which) -> {
                        for ( int groupPosition = 0 ; groupPosition < makeScheduleArrayList.size ( ) ; groupPosition++ ){
                            if ( makeScheduleArrayList.get (groupPosition).getContentName().equals( ((Pair< String, String>)v.getTag()).first ) ){
                                String memo = ((Pair< String, String>)v.getTag()).second ;

                                for (int childPosition = 0 ; childPosition < makeScheduleArrayList.get (groupPosition).getMemo().size(); childPosition++){
                                    if(makeScheduleArrayList.get (groupPosition).getMemo().get(childPosition).equals( memo) ){
                                        makeScheduleArrayList.get (groupPosition).getMemo().remove ( childPosition );
                                        changedScheduleArrayList.get (groupPosition).getMemo().remove ( childPosition );
                                        myItemAdapter.removeChildItem( groupPosition , childPosition );
                                        break;
                                    }
                                }
                                break;
                            }
                        }

                        //invalidateSchedule ( );
                    });

                    builder.setNegativeButton ( "취소" , (dialog, which) -> dialog.dismiss ( ));
                    builder.show ( );
            }
        }
    }

    //provider의 시작점
    public AbstractExpandableDataProvider getDataProvider() {
        //초기 데이터 프로바이더에 제공. clone메서드로 복사해서 갖다줌.
        Provider mDataProvider = new Provider((ArrayList<ScheduleVO>)makeScheduleArrayList.clone(), this.getContext(), this);
        return mDataProvider;
    }
}