package jejusmarttour.main_mytourproduct.osy.myfragment;

import android.app.TimePickerDialog;
import android.content.Context;

/**
 * Created by Osy on 2017-09-25.
 */

public class MyTimePickerDialog extends TimePickerDialog {

    public MyTimePickerDialog(Context context, int themeResId, OnTimeSetListener listener, int hourOfDay, int minute, boolean is24HourView) {
        super(context, themeResId, listener, hourOfDay, minute, is24HourView);
    }
    public MyTimePickerDialog(Context context, OnTimeSetListener listener, int hourOfDay, int minute, boolean is24HourView) {
        super(context, listener, hourOfDay, minute, is24HourView);
    }
}
