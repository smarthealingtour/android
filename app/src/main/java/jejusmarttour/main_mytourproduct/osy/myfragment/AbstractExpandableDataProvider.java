/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package jejusmarttour.main_mytourproduct.osy.myfragment;

import java.util.ArrayList;

import jejusmarttour.util.TimeUtil;
import jejusmarttour.vo.ScheduleVO;

public abstract class AbstractExpandableDataProvider {
    public static abstract class BaseData {
        public abstract void setPinned(boolean pinned);

        public abstract boolean isPinned();
    }

    public interface OnEndRouteTaskListener{
        void onEndRouteTask();
    }

    public abstract void setOnEndRouteTaskListener(OnEndRouteTaskListener onEndRouteTaskListener);

    public static abstract class GroupData extends BaseData {
        public abstract String getTitleText();
        public abstract String getKeywordText();
        public abstract String getAddressText();
        public abstract String getTimeText();
        public abstract String getTelText();
        public abstract boolean isSectionDistance();
        public abstract long getGroupId();
        public abstract String getContentID();
        public abstract int getDurationTime();

        protected abstract void setTravelDistance(int distance);
        public abstract int getTravelDistance();

        protected abstract void setFootTime( int minute);
        public abstract int getFootTime();

        public abstract void setEndTime(int hour, int minute);
        public abstract TimeUtil getEndTime();
    }

//    public static abstract class DistanceData extends BaseData{
//        public abstract String getDistanceText();
//        public abstract String getTimeText();
//    }

    public static abstract class ChildData extends BaseData {
        public abstract long getChildId();
        public abstract String getMemo();
        public abstract String getMemoWriter();
    }

    public abstract int getGroupCount();
    public abstract int getChildCount(int groupPosition);

    public abstract GroupData getGroupItem(int groupPosition);
    public abstract ChildData getChildItem(int groupPosition, int childPosition);
//    public abstract DistanceData getDistanceItem(int groupPosition);

    //drag에서 사용
    public abstract void moveGroupItem(int fromGroupPosition, int toGroupPosition);
    public abstract void moveChildItem(int fromGroupPosition, int fromChildPosition, int toGroupPosition, int toChildPosition);

    //요거는 추가할때 사용
    public abstract void addGroupItem(ArrayList<ScheduleVO> scheduleVOs);
    public abstract void addChildItem(int groupPosition, String memo, String memoWriter);

    public abstract void removeGroupItem(int groupPosition);
    public abstract void removeChildItem(int groupPosition, int childPosition);
    public abstract long undoLastRemoval();
}
