package jejusmarttour.main_mytourproduct.osy.myfragment;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableDraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.expandable.GroupPositionItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;

import java.util.ArrayList;

import jejusmarttour.main_mytourproduct.osy.util.DrawableUtil;
import jejusmarttour.main_mytourproduct.osy.util.ViewUtil;
import jejusmarttour.util.TimeUtil;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2017-07-26.
 */

public class MyAdapter extends AbstractExpandableItemAdapter<MyAdapter.MyGroupViewHolder, MyAdapter.MyChildViewHolder>
        implements ExpandableDraggableItemAdapter<MyAdapter.MyGroupViewHolder, MyAdapter.MyChildViewHolder> {

    private static final int GROUP_ITEM_VIEW_TYPE_SECTION_DISTANCE = 0;
    private static final int GROUP_ITEM_VIEW_TYPE_SECTION_ITEM = 1;

    private boolean mAllowItemsMoveAcrossSections;

    private ArrayList<LinearLayout> distanceLayouts;

    private View.OnClickListener groupClickListener;
    private View.OnClickListener childClickListener;

    // NOTE: Make accessible with short name
    private interface Expandable extends ExpandableItemConstants {
    }

    private interface Draggable extends DraggableItemConstants {
    }

    private AbstractExpandableDataProvider mProvider;

    public MyAdapter(AbstractExpandableDataProvider dataProvider, View.OnClickListener groupClickListener, View.OnClickListener childClickListener) {
        mProvider = dataProvider;
        this.groupClickListener = groupClickListener;
        this.childClickListener = childClickListener;

        distanceLayouts = new ArrayList<>();

        // ExpandableItemAdapter, ExpandableDraggableItemAdapter require stable ID,
        // and also have to implement the getGroupItemId()/getChildItemId() methods appropriately.
        setHasStableIds(true);
    }

    public static class MyGroupViewHolder extends AbstractDraggableItemViewHolder implements ExpandableItemViewHolder {
        private View view;
        private LinearLayout distanceLayout;
        private ImageView iconImage;
        private LinearLayout contentBox;
        private TextView titleText;
        private TextView keywordText;
        private TextView addressText;
        private TextView telText;
        private TextView timeText;
//        private Button timeButton1;
//        private Button timeButton2;

        private LinearLayout mContainer;
        private View mDragHandle;

        private Button deleteButton;
        private Button addMemoBtn;

        private int mExpandStateFlags;

        private Button mapView;
        private TextView distanceText;
        private TextView footText;
        private TextView carText;

        public MyGroupViewHolder(View view, View.OnClickListener groupListener) {
            super(view);
            this.view = view;
            distanceLayout = view.findViewById(R.id.distance_view);
            distanceText = view.findViewById(R.id.distance_text);
            footText = view.findViewById(R.id.foot_text);
            carText = view.findViewById(R.id.car_text);

//            timeButton1 = view.findViewById(R.id.time_button1);
//            timeButton2 = view.findViewById(R.id.time_button2);
            iconImage = view.findViewById(R.id.location_icon);
            contentBox = view.findViewById(R.id.content_box);
            titleText = view.findViewById(R.id.title_text);
            keywordText = view.findViewById(R.id.category_text);
            addressText = view.findViewById(R.id.address_text);
            telText = view.findViewById(R.id.telephone_text);
            timeText = view.findViewById(R.id.average_time_text);

            addMemoBtn = view.findViewById(R.id.mtc_add_memo_btn);
            deleteButton = view.findViewById(R.id.delete_btn);
            mapView = view.findViewById(R.id.set_course_btn);

            //mIndicator = (ExpandableItemIndicator) view.findViewById(R.id.indicator);

            mContainer = view.findViewById(R.id.container);
            mDragHandle = view.findViewById(R.id.drag_handle);

//            timeButton1.setOnClickListener(timePickerListener);
//            timeButton2.setOnClickListener(timePickerListener);
            mapView.setOnClickListener(groupListener);
            mDragHandle.setOnClickListener(groupListener);
            contentBox.setOnClickListener(groupListener);
            addMemoBtn.setOnClickListener(groupListener);
            deleteButton.setOnClickListener(groupListener);

        }
//        public View.OnClickListener timePickerListener = v -> {
//            switch (v.getId()){
//                case R.id.time_button1:
//                    Context context = view.getContext();
//                    context.setTheme(R.style.TimePickerTheme);
//
//                    TimePickerDialog timePickerDialog = new TimePickerDialog(context,
//                            (view1, hourOfDay, minute) -> {
//
//                                TimeUtil tu1 = (TimeUtil)timeButton1.getTag();
//                                TimeUtil tu2 = (TimeUtil)timeButton2.getTag();
//
//                                int subHour = hourOfDay - tu1.getHour();
//                                int subMinute = minute - tu1.getMinute();
//
//                                tu1.setTime(hourOfDay, minute);
//                                tu2.addTime(subHour, subMinute);
//
//                                if (tu2.isOutDay()){
//                                    Toast.makeText(context, "하루일정을 넘어버렸어요!", Toast.LENGTH_SHORT).show();
//                                }
//
//                                timeButton2.setText( tu2.getTimeString() );
//                                timeButton1.setText( tu1.getTimeString() );
//
//                            },  ((TimeUtil)timeButton1.getTag()).getHour(), ((TimeUtil)timeButton1.getTag()).getMinute(), true);
//                    timePickerDialog.show();
//                    break;
//
//                case R.id.time_button2:
//                    Context context2 = view.getContext();
//                    context2.setTheme(R.style.TimePickerTheme);
//
//                    TimePickerDialog timePickerDialog2 = new TimePickerDialog(context2,
//                            (view1, hourOfDay, minute) -> {
//
//                                TimeUtil tu3 = ((TimeUtil)timeButton2.getTag());
//                                tu3.setTime(hourOfDay, minute);
//                                timeButton2.setText(tu3.getTimeString());
//
//                            }, ((TimeUtil)timeButton2.getTag()).getHour(), ((TimeUtil)timeButton2.getTag()).getMinute(), true);
//                    timePickerDialog2.show();
//                    break;
//            }
//        };

        @Override
        public int getExpandStateFlags() {
            return mExpandStateFlags;
        }

        @Override
        public void setExpandStateFlags(int flag) {
            mExpandStateFlags = flag;
        }
    }

    public static class MyChildViewHolder extends AbstractDraggableItemViewHolder implements ExpandableItemViewHolder {
        private TextView memoText;
        private TextView memoWriter;
        private Button memoDelete;
        private int mExpandStateFlags;

        private LinearLayout mContainer;

        //public View mDragHandle;

        public MyChildViewHolder(View v, View.OnClickListener childClickListener) {
            super(v);
            memoText = v.findViewById(R.id.memo_text);

            mContainer = v.findViewById(R.id.memo_container);
            //mDragHandle = (ImageView)v.findViewById(R.id.memo_drag_handle);
            memoDelete = v.findViewById(R.id.delete_btn);

            memoDelete.setOnClickListener(childClickListener);
        }

        @Override
        public int getExpandStateFlags() {
            return mExpandStateFlags;
        }

        @Override
        public void setExpandStateFlags(int flag) {
            mExpandStateFlags = flag;
        }
    }

    @Override
    public int getGroupCount() {
        return mProvider.getGroupCount();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return mProvider.getChildCount(groupPosition);
    }

    @Override
    public long getGroupId( int groupPosition) {
        return mProvider.getGroupItem( groupPosition).getGroupId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mProvider.getChildItem( groupPosition, childPosition).getChildId();
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        final AbstractExpandableDataProvider.GroupData item = mProvider.getGroupItem( groupPosition);

        /*if (item.isSectionDistance()) {
            return GROUP_ITEM_VIEW_TYPE_SECTION_DISTANCE;
        } else {*/
            return GROUP_ITEM_VIEW_TYPE_SECTION_ITEM;

    }

    @Override
    public int getChildItemViewType( int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public MyGroupViewHolder onCreateGroupViewHolder( ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from( parent.getContext());

        final View v;
        /*switch (viewType) {
            case GROUP_ITEM_VIEW_TYPE_SECTION_DISTANCE:
                v = inflater.inflate(R.layout.layout_mtc_schedule_distance, parent, false);
                break;
            case GROUP_ITEM_VIEW_TYPE_SECTION_ITEM:
                v = inflater.inflate(R.layout.layout_mtc_schedule_contents, parent, false);
                break;
            default:
                throw new IllegalStateException("Unexpected viewType (= " + viewType + ")");
        }*/
        v = inflater.inflate( R.layout.layout_mtc_schedule_contents, parent, false);

        return new MyGroupViewHolder( v, groupClickListener);
    }

    @Override
    public MyChildViewHolder onCreateChildViewHolder( ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from( parent.getContext());
        final View v = inflater.inflate( R.layout.layout_mtc_schedule_memo, parent, false);

        return new MyChildViewHolder(v, childClickListener);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindGroupViewHolder( MyGroupViewHolder holder, int groupPosition, int viewType) {
        // group item
        final AbstractExpandableDataProvider.GroupData item = mProvider.getGroupItem(groupPosition);

        TimeUtil tu1 = new TimeUtil();
        TimeUtil tu2 = new TimeUtil();
        TimeUtil durationTime = new TimeUtil();

        if(groupPosition == 0){
            holder.distanceLayout.setVisibility( View.GONE);

            durationTime.setTime( 9, 0 );
            tu1.setTime(durationTime);

            durationTime.addTime( 0, item.getDurationTime());
            tu2.setTime(durationTime);
        }

        else {
            holder.distanceLayout.setVisibility( View.VISIBLE );

            durationTime.setTime( mProvider.getGroupItem( groupPosition-1).getEndTime());

            durationTime.addTime( 0, item.getFootTime());
            tu1.setTime( durationTime);

            durationTime.addTime( 0, item.getDurationTime());
            tu2.setTime( durationTime);
        }
        item.setEndTime( tu2.getHour(), tu2.getMinute());

        String  i = String.format( "%.1f", ((float)item.getTravelDistance())/1000);

        holder.distanceText.setText( i + " km");
        holder.footText.setText( item.getFootTime() + " 분");
        distanceLayouts.add( holder.distanceLayout );

//        holder.timeButton1.setTag( tu1);
//        holder.timeButton2.setTag( tu2);

        Provider.OnEndRouteTaskListener listener = () -> {
            /*if(groupPosition == 0){
                holder.distanceLayout.setVisibility( View.GONE);

                durationTime.setTime( 9, 0 );
                tu1.setTime(durationTime);

                durationTime.addTime( 0, item.getDurationTime());
                tu2.setTime(durationTime);
            }

            else {
                holder.distanceLayout.setVisibility( View.VISIBLE);

                durationTime.setTime( mProvider.getGroupItem( groupPosition-1).getEndTime());

                durationTime.addTime( 0, item.getFootTime());
                tu1.setTime( durationTime);

                durationTime.addTime( 0, item.getDurationTime());
                tu2.setTime( durationTime);
            }
            item.setEndTime( tu2.getHour(), tu2.getMinute());

            String  ii = String.format( "%.1f", ((float)item.getTravelDistance())/1000);

            holder.distanceText.setText( ii + " km");
            holder.footText.setText( item.getFootTime() + " 분");
            distanceLayouts.add( holder.distanceLayout );

            Log.e(this.toString(), "onBindGroupViewHolder: fucking" );*/
        };

        mProvider.setOnEndRouteTaskListener(listener);

        holder.contentBox.setTag( item.getTitleText() );
        holder.deleteButton.setTag( item.getTitleText() );
        holder.addMemoBtn.setTag( item.getTitleText() );
        holder.mDragHandle.setTag( item.getTitleText() );

//        holder.timeButton1.setText( tu1.getTimeString());
//        holder.timeButton2.setText( tu2.getTimeString());
        holder.titleText.setText( item.getTitleText() );
        holder.keywordText.setText( item.getKeywordText() );
        holder.addressText.setText( item.getAddressText() );
        holder.telText.setText( item.getTelText() );
        holder.timeText.setText( item.getTimeText() );

        // set background resource (target view ID: container)
        final int dragState = holder.getDragStateFlags();
        final int expandState = holder.getExpandStateFlags();

        if (((dragState & Draggable.STATE_FLAG_IS_UPDATED) != 0) ||
                ((expandState & Expandable.STATE_FLAG_IS_UPDATED) != 0)) {
            int bgResId;
            boolean isExpanded;
            boolean animateIndicator = ((expandState & Expandable.STATE_FLAG_HAS_EXPANDED_STATE_CHANGED) != 0);

            if ((dragState & Draggable.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = R.drawable.bg_group_item_dragging_active_state;

                // need to clear drawable state here to get correct appearance of the dragging item.
                DrawableUtil.clearState(holder.mContainer.getForeground());
            } else if (((dragState & Draggable.STATE_FLAG_DRAGGING) != 0) &&
                    ((dragState & Draggable.STATE_FLAG_IS_IN_RANGE) != 0)) {
                bgResId = R.drawable.bg_group_item_dragging_state;
            } else if ((expandState & Expandable.STATE_FLAG_IS_EXPANDED) != 0) {
                bgResId = R.drawable.bg_group_item_expanded_state;
            } else {
                bgResId = R.drawable.bg_group_item_normal_state;
            }

            if ((expandState & Expandable.STATE_FLAG_IS_EXPANDED) != 0) {
                isExpanded = true;
            } else {
                isExpanded = false;
            }

            holder.mContainer.setBackgroundResource(bgResId);
            holder.distanceLayout.setBackgroundResource(bgResId);
            //holder.mIndicator.setExpandedState(isExpanded, animateIndicator);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindChildViewHolder(MyChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
        // child item
        final AbstractExpandableDataProvider.ChildData item = mProvider.getChildItem(groupPosition, childPosition);

        String titleText = mProvider.getGroupItem(groupPosition).getTitleText();
        Pair<String, String> p = new Pair<>( titleText, item.getMemo() );
        // set text
        holder.memoText.setText(item.getMemo());
        holder.memoDelete.setTag(p);

        final int dragState = holder.getDragStateFlags();

        if (((dragState & Draggable.STATE_FLAG_IS_UPDATED) != 0)) {
            int bgResId;

            if ((dragState & Draggable.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = R.drawable.bg_item_dragging_active_state;

                // need to clear drawable state here to get correct appearance of the dragging item.
                DrawableUtil.clearState(holder.mContainer.getForeground());
            } else if (((dragState & Draggable.STATE_FLAG_DRAGGING) != 0) &&
                    ((dragState & Draggable.STATE_FLAG_IS_IN_RANGE) != 0)) {
                bgResId = R.drawable.bg_item_dragging_state;
            } else {
                bgResId = R.drawable.bg_item_normal_state;
            }

            holder.mContainer.setBackgroundResource(bgResId);
        }
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        // check is normal item
        if (!isSectionDistance(holder)) {
            return false;
        }

        // check is enabled
        if (!(holder.itemView.isEnabled())) {
            return false;
        }

        final View containerView = holder.mContainer;
        final View dragHandleView = holder.mDragHandle;

        final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
        final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);

        return !ViewUtil.hitTest(dragHandleView, x - offsetX, y - offsetY);
    }

    @Override
    public boolean onCheckGroupCanStartDrag(MyGroupViewHolder holder, int groupPosition, int x, int y) {
        // check is normal item
        if (!isSectionDistance(holder)) {
            return false;
        }

        // x, y --- relative from the itemView's top-left
        final View containerView = holder.mContainer;
        final View dragHandleView = holder.mDragHandle;

        final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
        final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);

        return ViewUtil.hitTest(dragHandleView, x - offsetX, y - offsetY);
    }

    @Override
    public boolean onCheckChildCanStartDrag(MyChildViewHolder holder, int groupPosition, int childPosition, int x, int y) {
        /*// x, y --- relative from the itemView's top-left
        final View containerView = holder.mContainer;
        final View dragHandleView = holder.mDragHandle;

        final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
        final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);

        return ViewUtil.hitTest(dragHandleView, x - offsetX, y - offsetY);*/
        return false;
    }

    @Override
    public ItemDraggableRange onGetGroupItemDraggableRange(MyGroupViewHolder holder, int groupPosition) {
        if (mAllowItemsMoveAcrossSections) {
            return null;
        } else {
            // sort within the same section
            final int start = findFirstSectionItem(groupPosition);
            final int end = findLastSectionItem(groupPosition);

            return new GroupPositionItemDraggableRange(start, end);
        }
    }

    @Override
    public ItemDraggableRange onGetChildItemDraggableRange(MyChildViewHolder holder, int groupPosition, int childPosition) {
        if (mAllowItemsMoveAcrossSections) {
            return null;
        } else {
            // sort within the same group
            return new GroupPositionItemDraggableRange(groupPosition, groupPosition);

//            // sort within the same section
//            final int start = findFirstSectionItem(groupPosition);
//            final int end = findLastSectionItem(groupPosition);
//
//            return new GroupPositionItemDraggableRange(start, end);
//
//            // sort within the specified child range
//            final int start = 0;
//            final int end = 2;
//
//            return new GroupPositionItemDraggableRange(start, end);
        }
    }

    @Override
    public boolean onCheckGroupCanDrop(int draggingGroupPosition, int dropGroupPosition) {
        return true;
    }

    @Override
    public boolean onCheckChildCanDrop(int draggingGroupPosition, int draggingChildPosition, int dropGroupPosition, int dropChildPosition) {
        final AbstractExpandableDataProvider.GroupData item = mProvider.getGroupItem(dropGroupPosition);

        if (item.isSectionDistance()) {
            // If the group item is a section header, skip it.
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onMoveGroupItem(int fromGroupPosition, int toGroupPosition) {
        mProvider.moveGroupItem(fromGroupPosition, toGroupPosition);
    }

    @Override
    public void onMoveChildItem(int fromGroupPosition, int fromChildPosition, int toGroupPosition, int toChildPosition) {
        mProvider.moveChildItem(fromGroupPosition, fromChildPosition, toGroupPosition, toChildPosition);
    }

    private int findFirstSectionItem(int position) {
        AbstractExpandableDataProvider.GroupData item = mProvider.getGroupItem(position);

        if (item.isSectionDistance()) {
            throw new IllegalStateException("section item is expected");
        }

        while (position > 0) {
            AbstractExpandableDataProvider.GroupData prevItem = mProvider.getGroupItem(position - 1);

            if (prevItem.isSectionDistance()) {
                break;
            }
            position -= 1;
        }

        return position;
    }

    private int findLastSectionItem(int position) {
        AbstractExpandableDataProvider.GroupData item = mProvider.getGroupItem(position);

        if (item.isSectionDistance()) {
            throw new IllegalStateException("section item is expected");
        }

        final int lastIndex = getGroupCount() - 1;

        while (position < lastIndex) {
            AbstractExpandableDataProvider.GroupData nextItem = mProvider.getGroupItem(position + 1);

            if (nextItem.isSectionDistance()) {
                break;
            }

            position += 1;
        }

        return position;
    }

    public void setAllowItemsMoveAcrossSections(boolean allowed) {
        mAllowItemsMoveAcrossSections = allowed;
    }


    public void addGroupItem(ArrayList<ScheduleVO> scheduleVOs){
        mProvider.addGroupItem( scheduleVOs );
        notifyDataSetChanged();
    }

    //메모추가메서드.
    public void addChildItem(int groupPosition, String memo, String memoWriter){
        mProvider.addChildItem(groupPosition, memo, memoWriter);
        notifyDataSetChanged();
    }

    public void removeGroupItem(int groupPosition){
        mProvider.removeGroupItem( groupPosition );
        notifyDataSetChanged();
    }

    public void removeChildItem(int groupPosition,int childPosition){
        mProvider.removeChildItem( groupPosition, childPosition );
        notifyDataSetChanged();
    }

    /*//드래그 시작하면 거리레이아웃 안보이게함
    public void goneDistanceLayouts(){
        for (LinearLayout distanceLayout:
            distanceLayouts) {
            distanceLayout.setVisibility(View.GONE);
            Log.e(this.toString(), "goneDistanceLayouts: 뭔데 이건" );
        }
    }

    //드래그 끝나면 distanceLayout들 null처리
    public void setNullDistanceLayouts(){
        distanceLayouts.clear();
        Log.e(this.toString(), "goneDistanceLayouts: 뭔데 이건2" );
    }*/

    private static boolean isSectionDistance(MyGroupViewHolder holder) {
        final int groupViewType = RecyclerViewExpandableItemManager.getGroupViewType(holder.getItemViewType());
        return (groupViewType != GROUP_ITEM_VIEW_TYPE_SECTION_DISTANCE);
    }
}