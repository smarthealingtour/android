package jejusmarttour.main_mytourproduct.osy.myfragment;

import java.util.ArrayList;

/**
 * Created by Osy on 2017-07-26.
 */


public class MyData {
    //public int number_img;
    private String titleText;
    private String keywordText;
    private String addressText;
    private String timeText;
    private String telText;
    private ArrayList<String> memoList = new ArrayList<>();

    private String distance;

    public MyData(String titleText, String keywordText, String addressText, String timeText, String telText) {
        this(titleText, keywordText, addressText, timeText, telText, null);
    }


    public MyData(String titleText, String keywordText, String addressText,
                  String timeText, String telText, ArrayList<String> memoList) {
        //매개변수를 vo로 받고 밑에 값을 넣어주면 됨!

        this.titleText = titleText;
        this.keywordText = keywordText;
        this.addressText = addressText;
        this.timeText = timeText;
        this.telText = telText;
        this.memoList = memoList;
    }

    public String getTitleText() {
        return titleText;
    }

    public String getKeywordText() {
        return keywordText;
    }

    public String getAddressText() {
        return addressText;
    }

    public String getTimeText() {
        return timeText;
    }

    public String getTelText() {
        return telText;
    }

    public ArrayList<String> getMemoList() {
        return memoList;
    }

    public String getDistanceText(){
        return distance;
    }
}

