package jejusmarttour.main_mytourproduct.osy.interest_activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

import jejusmarttour.main_mytourproduct.MTCAddSiteActivity;
import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import syl.com.jejusmarttour.R;

/**
 * Created by A on 2015-10-25.
 */
public class InterestActivity extends AppCompatActivity {
    private static final String TAG = MTCAddSiteActivity.class.toString();
    private Fragment cur_fragment = new Fragment();
    private PagerSlidingTabStrip tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_interest_spot);

        initViewPager();
        initActionBar();
    }

    private void initViewPager(){
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.setCurrentItem(0);

        tab = ( PagerSlidingTabStrip ) findViewById ( R.id.htp_tabs );
        tab.setShouldExpand(true);
        setTabStyle();
        tab.setViewPager(viewPager);
    }

    @SuppressWarnings ( "deprecation" )
    private void setTabStyle ( ){
        tab.setTextSize ( getResources ( ).getDimensionPixelSize ( R.dimen.font_size_03 ) );
        tab.setDividerColor(getResources().getColor(R.color.white));
        tab.setBackgroundColor(getResources().getColor(R.color.white));
        tab.setIndicatorHeight(5);
        tab.setIndicatorColor(getResources().getColor(R.color.statusbar));
        tab.setTextColor(getResources().getColor(R.color.font_color));
    }

    // 액션바 설정
    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);

        // 액션바 그림자 지우기
        actionBar.setElevation(0);

        LayoutInflater mInflater = LayoutInflater.from(this);

        View customView = mInflater.inflate(R.layout.actionbar_listview, null);
        TextView titleTextView = customView.findViewById ( R.id.title_text );
        titleTextView.setText ( R.string.title_activity_my_interest );

        LinearLayout backBtn = customView.findViewById ( R.id.back_btn );

        backBtn.setOnClickListener (view -> finish ( ));

        actionBar.setCustomView(customView);
        actionBar.setDisplayShowCustomEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_close) {
            Intent intent = getIntent();
            intent.putExtra("data", "value");
            setResult(RESULT_OK, intent);
            finishActivity(ScheduleManegeFragment.REQUEST_CODE_SITE);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // 뷰페이지 어뎁터
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private int position = 0;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            this.position = position;

            switch (position) {
                case 0:
                    cur_fragment = InterestSpotFragment.newInstance();
                    break;
                case 1:
                    cur_fragment = InterestFoodStayFragment.newInstance();
                    break;
            }
            return cur_fragment;
        }

        public int getCurrentPosition() {
            return position;
        }

        @Override
        public int getCount() {
            return getResources().getStringArray(R.array.interest_spot_titles2).length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getResources().getStringArray(R.array.interest_spot_titles2)[position];
        }
    }
}

