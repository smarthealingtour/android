package jejusmarttour.main_mytourproduct.osy.interest_activity;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.MyInfoConnectResult;
import jejusmarttour.task.MyInfoConnectTask;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

/**
 * Created by A on 2015-10-25.
 */
public class InterestViewAdapter<E> extends BaseAdapter implements MyInfoConnectResult{
    private static final String TAG = InterestViewAdapter.class.toString ( );

    private ArrayList<E> listData;//HealingContentsVO, SightSeeingContentsVO
    private Context context;
    private LayoutInflater inflater;

    private E selectRemoveData;

    InterestViewAdapter(Context context, ArrayList<E> listData){
        this.context = context;
        this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
        this.listData = listData;
    }

    // 현재 아이템의 수를 리턴
    @Override
    public int getCount ( ){
        return listData.size ( );
    }

    @Override
    public ScheduleVO getItem ( int position ){
        E item = listData.get(position);
        ScheduleVO returnItem = new ScheduleVO();
        if(item != null){
            if(item instanceof HealingContentsVO){
                HealingContentsVO vo = (HealingContentsVO) item;
                if(null == vo.getHcnt_type()){
                    returnItem = null;
                }else{
                    returnItem.setHealingContentsVO(vo);
                }
            }else {
                SightSeeingContentsVO vo = (SightSeeingContentsVO) item;
                returnItem.setSightSeeingContentsVO(vo);
            }
        }
        return returnItem;
    }

    // 아이템 position의 ID 값 리턴
    @Override
    public long getItemId ( int position ){
        return position;
    }

    // 출력 될 아이템 관리
    @Override
    public View getView ( int position , View convertView , ViewGroup parent ){
        InterestViewHolder viewHolder;

        // 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
        if ( convertView == null ){
            // view가 null일 경우 커스텀 레이아웃을 얻어 옴
            convertView = inflater.inflate ( R.layout.adapter_myfavorite_listview , parent , false );

            viewHolder = new InterestViewHolder ( );

//            viewHolder.keywordTitle = convertView.findViewById(R.id.keywordTitle);
//            viewHolder.checkBox = convertView.findViewById ( R.id.checkbox );
//            viewHolder.checkBox.setVisibility(View.GONE);
//            viewHolder.linearLayout = convertView.findViewById ( R.id.mtc_group_layout );
//            viewHolder.linearLayout.setVisibility(View.GONE);
//            viewHolder.groupText = convertView.findViewById ( R.id.mtc_group_text );

            viewHolder.imageView = convertView.findViewById ( R.id.search_item_icon );
            viewHolder.watchImageView = convertView.findViewById( R.id.watch_icon );
            viewHolder.titleText = convertView.findViewById ( R.id.mtc_title_text );
            viewHolder.timeText = convertView.findViewById ( R.id.mtc_session_time_text );
            viewHolder.subTitleText = convertView.findViewById ( R.id.htp_subtitle );
            viewHolder.group_linearLayout =  convertView.findViewById( R.id.group_linearLayout );
            viewHolder.deleteBtn = convertView.findViewById ( R.id.mtc_delete_btn );
            viewHolder.deleteBtn.setFocusable(false);
            viewHolder.deleteBtn.setClickable(true);
            viewHolder.deleteBtn.setOnClickListener(delBtnClick);

            convertView.setTag ( viewHolder );
        }
        else{
            viewHolder = ( InterestViewHolder ) convertView.getTag ( );
        }

        E itemData = listData.get(position);
        if(itemData != null){
            viewHolder.deleteBtn.setTag(itemData);
            if(itemData instanceof HealingContentsVO){
                HealingContentsVO itemVo = (HealingContentsVO) itemData;

                if( null == itemVo.getHcnt_type() ){
                    convertView.setBackground(null);
//                    viewHolder.keywordTitle.setVisibility(View.VISIBLE);
                    viewHolder.group_linearLayout.setVisibility(View.GONE);
                    viewHolder.deleteBtn.setVisibility(View.GONE);
                    viewHolder.keywordTitle.setText(itemVo.getHcnt_name());
                }
                else{
                    convertView.setBackgroundResource(R.color.white);
//                    viewHolder.keywordTitle.setVisibility(View.GONE);
                    viewHolder.group_linearLayout.setVisibility(View.VISIBLE);
                    viewHolder.deleteBtn.setVisibility(View.VISIBLE);
                    viewHolder.watchImageView.setVisibility ( View.VISIBLE );
                    Glide.with(context).load ( CommonData.getLocationIconId(itemVo.getHcnt_type()) ).thumbnail ( 0.1f ).into ( viewHolder.imageView );
                    viewHolder.titleText.setText ( itemVo.getHcnt_name() == null ? "이름없음" : itemVo.getHcnt_name() );
                    viewHolder.timeText.setText ( itemVo.getHcnt_duration() == null ? "- 분" : SmartTourUtils.getDuration(itemVo.getHcnt_duration()) );
                    viewHolder.subTitleText.setText ( itemVo.getHcnt_addr ( ) == null ? "주소없음" : itemVo.getHcnt_addr ( ) );
                }
            }
            else{
                SightSeeingContentsVO itemVo = (SightSeeingContentsVO) itemData;
                convertView.setBackgroundResource(R.color.white);
//                viewHolder.keywordTitle.setVisibility(View.GONE);
                viewHolder.group_linearLayout.setVisibility(View.VISIBLE);
                viewHolder.deleteBtn.setVisibility(View.VISIBLE);
                viewHolder.imageView.setImageResource(R.drawable.location_icon);
                viewHolder.titleText.setText ( itemVo.getCcnt_name() == null ? "이름없음" : itemVo.getCcnt_name() );
                viewHolder.watchImageView.setVisibility ( View.GONE );
                viewHolder.timeText.setText ( "" );
                viewHolder.subTitleText.setText ( itemVo.getCcnt_addr() == null ? "주소없음" : itemVo.getCcnt_addr ( ) );
            }
        }

        return convertView;
    }

    private View.OnClickListener delBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            selectRemoveData = (E) v.getTag();

            AlertDialog.Builder alert_confirm = new AlertDialog.Builder(context);
            alert_confirm.setMessage("삭제하시겠습니까?").setCancelable(false).setPositiveButton("확인",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            MyInfoConnectTask task = new MyInfoConnectTask(context, InterestViewAdapter.this);
                            Map<String, String> param = new HashMap<String, String>();
                            param.put("type", "delete");
                            param.put("email", CommonData.getUserVO().getEmail());
                            if(selectRemoveData instanceof HealingContentsVO){
                                param.put("url", "cart_hcnt");
                                param.put("hcnt_id",((HealingContentsVO) selectRemoveData).getHcnt_id() );
                                task.setConnectCode(0x1);
                            }
                            else{
                                param.put("url", "cart_ccnt");
                                param.put("ccnt_id", ((SightSeeingContentsVO) selectRemoveData).getCcnt_id());
                                task.setConnectCode(0x2);
                            }
                            task.execute(param);

                            // 'YES'
                        }
                    }).setNegativeButton("취소",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // 'No'
                            return;
                        }
                    });
            AlertDialog alert = alert_confirm.create();
            alert.show();

        }
    };

    @Override
    public void notifyDataSetChanged ( ){
        super.notifyDataSetChanged ( );
    }

    // 외부에서 아이템 추가 요청 시 사용
    public void add ( E vo ){
        listData.add(vo);
    }

    // 외부에서 아이템 삭제 요청 시 사용
    public void remove ( int position ){
        listData.remove(position);
    }

    public void remove ( E removeData ){
        listData.remove ( removeData );
    }

    public void removeAll ( ){
        listData.clear();
    }

    @Override
    public void connectResultData(int code, String result) {
        Log.d("Tag", "connectResultData:" + result);
        try {
            JSONObject json = new JSONObject(result);
            if("success".equals(json.getString("result"))) {
                remove( selectRemoveData );

                notifyDataSetChanged();
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
