package jejusmarttour.main_mytourproduct.osy.interest_activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_spot.detailinfo.FoodStayDetailInfoActivity;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.MTCSightSeeingTask;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

public class InterestFoodStayFragment extends Fragment implements JSONArrayResult<SightSeeingContentsVO> , AbsListView.OnScrollListener{
    private InterestViewAdapter<SightSeeingContentsVO> listViewAdapter;
    private MTCSightSeeingTask task;

    private boolean taskLock = true;    //onScroll()메서드가 먼저 onCreate()보다 먼저 시작되어 막아음
    private int requestPageNumber = 1;

    public static InterestFoodStayFragment newInstance ( ){
        InterestFoodStayFragment fragment = new InterestFoodStayFragment( );
        return fragment;
    }

    @Override
    public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
        View view = null;

        try {
            view = inflater.inflate ( R.layout.fragment_listview2, container , false );
        }
        catch ( InflateException e ){
            e.printStackTrace ( );
        }

        ArrayList listData = new ArrayList();

        listViewAdapter = new InterestViewAdapter<SightSeeingContentsVO>( getContext ( ) , listData);

        ListView listView = view.findViewById(R.id.list_view);
        TextView noDataText = view.findViewById(R.id.no_data_text);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) listView.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, 0);
        listView.setDividerHeight(3);
        listView.setAdapter(listViewAdapter);

        // ListView 아이템 터치 시 이벤트 추가
        listView.setOnItemClickListener ( onClickListItem );
        listView.setOnScrollListener(this);

        taskLock = false;
        executeTask ();

        return view;
    }

    // 아이템 터치 이벤트
    private AdapterView.OnItemClickListener onClickListItem = new AdapterView.OnItemClickListener( ){
        @Override
        public void onItemClick ( AdapterView < ? > arg0 , View arg1 , int position , long arg3 ){
            ScheduleVO item = listViewAdapter.getItem(position);
            if(item != null){
                CommonData.setScheduleVO(item);
                Intent intent = new Intent (getContext() , FoodStayDetailInfoActivity.class );
                startActivity(intent);
            }
        }
    };

    private void InsertItemToListView ( ArrayList<SightSeeingContentsVO> resultList ){
        for ( int i = 0; i < resultList.size ( ); i++ ){
            listViewAdapter.add ( resultList.get ( i ) );
        }
        // 리스트뷰 리로딩
        listViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void setJSONArrayResult ( ArrayList<SightSeeingContentsVO> resultList ){
        task.cancel ( true );

        if(resultList != null){
            InsertItemToListView(resultList);

            taskLock = false;
        }
        else {
            taskLock = true;
        }
    }

    private void executeTask (){
        if ( !taskLock){
            taskLock = true;

            task = new MTCSightSeeingTask(getContext(), this);
            task.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , "ccnt", String.valueOf( requestPageNumber ));
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount ){
        int lastInScreen = firstVisibleItem + visibleItemCount;

        Log.e("scroll 마지막", "onScroll: " );

        if ( ( lastInScreen == totalItemCount ) && !taskLock ){
            Log.e("scroll 다음 실행", "onScroll: " );

            requestPageNumber += 1;
            executeTask ( );
        }
    }
}