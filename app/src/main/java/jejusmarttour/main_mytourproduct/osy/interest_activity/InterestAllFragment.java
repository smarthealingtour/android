/*
package jejusmarttour.main_mytourproduct.osy.interest_activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.MTCHealingContentsTask;
import jejusmarttour.task.MTCSightSeeingTask;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

*/
/**
 * Created by A on 2015-10-25.
 *//*

public class InterestAllFragment extends Fragment implements JSONArrayResult, AbsListView.OnScrollListener{
    private ListView listView;
    private InterestViewAdapter listViewAdapter;
    private MTCHealingContentsTask task;
    private MTCSightSeeingTask task1;
    private TextView noDataText;

    private String taskType;

    private ArrayList listData;

    private boolean lockListView = true;
    private int requestPageNumber = 1;

    public static InterestAllFragment newInstance ( ){
        InterestAllFragment fragment = new InterestAllFragment( );
        return fragment;
    }

    @Override
    public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
        View view = null;

        try{
            view = inflater.inflate ( R.layout.fragment_listview, container , false );
        }
        catch ( InflateException e ){
            e.printStackTrace ( );
        }

        listData = new ArrayList ( );

        listViewAdapter = new InterestViewAdapter( getContext ( ) , listData );
        listView = ( ListView ) view.findViewById ( R.id.recycler_view);
        noDataText = (TextView) view.findViewById ( R.id.no_data_text);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) listView.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, 0);
        listView.setDividerHeight(3);
        listView.setAdapter(listViewAdapter);

        // ListView 아이템 터치 시 이벤트 추가
        listView.setOnItemClickListener ( onClickListItem );
        listView.setOnScrollListener(this);

        taskType = "hcnt";
        executeTask ();

        return view;
    }

    // 요청할 파라미터
    private String[] getParam ( ){
        String [ ] param = new String[] {taskType , String.valueOf( requestPageNumber ) };

        return param;
    }

    // 아이템 터치 이벤트
    private AdapterView.OnItemClickListener onClickListItem = new AdapterView.OnItemClickListener( ){
        @Override
        public void onItemClick ( AdapterView < ? > arg0 , View arg1 , int position , long arg3 ){
            Object item = listViewAdapter.getItem(position);
            if(item != null){
                CommonData.setScheduleVO((ScheduleVO) item);
                Intent intent = new Intent (getContext() , DetailInfoActivity.class );
                startActivity(intent);
            }
        }
    };

    private void InsertItemToListView ( ArrayList resultList ){
        for ( int i = 0; i < resultList.size ( ); i++ ){
            if(resultList.get(i) instanceof HealingContentsVO){
                listViewAdapter.add ((HealingContentsVO) resultList.get ( i ) );
            }
            else{
                listViewAdapter.add ((SightSeeingContentsVO) resultList.get ( i ) );
            }
        }
        // 리스트뷰 리로딩
        listViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void setJSONArrayResult ( ArrayList resultList ){
        if("spot".equals(taskType)){
            task.cancel ( true );
            if(resultList != null){
                InsertItemToListView(resultList);
            }

            taskType = "ccnt";
            executeTask();
        }
        else{
            task1.cancel(true);

            noDataText.setVisibility(View.GONE);

            if ( resultList == null ){
                if(listViewAdapter.getCount() == 0){

                    noDataText.setVisibility(View.VISIBLE);
                    //Toast.makeText(getContext(), "데이터가 없습니다.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            else {
                InsertItemToListView ( resultList );
            }
        }
        lockListView = false;
    }

    private void executeTask (){
        if("spot".equals(taskType)){
            task = new MTCHealingContentsTask(getContext(), this);
            task.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( ) );
        }
        else{
            task1 = new MTCSightSeeingTask ( getContext ( ) , this );
            task1.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( ) );
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount ){
        int lastInScreen = firstVisibleItem + visibleItemCount;

        Log.e("scroll 마지막", "onScroll: " );

        if ( ( lastInScreen == totalItemCount ) && !lockListView){
            Log.e("scroll 다음 실행", "onScroll: " );

            requestPageNumber += 1;

            executeTask ( );

            lockListView = true;
        }
    }
}*/
