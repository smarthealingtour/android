package jejusmarttour.main_mytourproduct;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.recycler_adapter.HorizontalRecyclerViewAdapter;
import jejusmarttour.map.util;
import jejusmarttour.parser.ScheduleParser;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.task.ScheduleTask;
import jejusmarttour.task.MyScheduleUpdateTask;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.MTCScheduleVO;
import jejusmarttour.vo.MTCSmartTourProductsVO;
import jejusmarttour.vo.MyScheduleUpdateRequestVO;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class MakeMyProductActivity extends AppCompatActivity
		implements OnClickListener , JSONArrayResult < ScheduleVO > , JSONObjectResult{
	private static final String TAG = MakeMyProductActivity.class.toString ( );
	public static final int REQUEST_CODE_SCHEDULE = 1;	// 일정 만들기 결과 요청 코드

	private ArrayList < MTCSmartTourProductsVO > mtcSmartTourProductsVOArrayList;
	private ArrayList<String> selectImages;
	private MTCSmartTourProductsVO mtcSmartTourProductsVO;

	private TourProductVO tourProductVO;
	private LinearLayout scheduleContainer;
	private LinearLayout addScheduleBtn;
	private TextView imageCountText;
	private TextView tourDaysText;
	private EditText courseNameText;
	private EditText courseDescriptionText;
	private HorizontalRecyclerViewAdapter horizontalImageListAdapter;

	// 코스수정으로 들어온건지 새로 만드는 걸로 페이지 들어온건지 구분
	private boolean isModifyCourse = false;

	private int newDateNum = 1;			// 일정 만들기 생성일자

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_mtcmake_course );

		initVOs();
		initViews();
		initActionBar ( );

		selectImages = new ArrayList<>();

		if (isModifyCourse){
			loadTourData ( );
		}
		else{
			initTourData ( );
		}
	}

	private void initVOs(){
		Intent intent = getIntent ( );
		tourProductVO = ( TourProductVO ) intent.getSerializableExtra ( "data" );
		isModifyCourse = intent.getBooleanExtra ( "isModify" , false );

		Log.e(TAG, "tourProductVO.toString(): " + tourProductVO.getHt_img() );
		Log.e(TAG, "isModifyCourse: " + isModifyCourse );

		mtcSmartTourProductsVOArrayList = new ArrayList<> ( );
		mtcSmartTourProductsVO = new MTCSmartTourProductsVO ( );
	}

	private void initViews(){
		imageCountText = ( TextView ) findViewById ( R.id.image_count_text );
		tourDaysText = ( TextView ) findViewById ( R.id.tour_days_text );

		courseNameText = ( EditText ) findViewById ( R.id.course_name_text );
		courseDescriptionText = ( EditText ) findViewById ( R.id.course_descript_text );

		scheduleContainer = ( LinearLayout ) findViewById ( R.id.schedule_list_layout );

		addScheduleBtn = ( LinearLayout ) View.inflate ( this , R.layout.layout_add_schedule_btn , null );
		addScheduleBtn.setOnClickListener ( this );
	}

	// 액션바 설정
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_make_my_tour_course , null );
		ImageView cancelBtn = customView.findViewById ( R.id.cancel_btn );
		ImageView saveBtn = customView.findViewById ( R.id.save_btn );
		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( getResources ( ).getString ( R.string.title_activity_mtcmake_course ) );

		cancelBtn.setOnClickListener (view -> finish ( ));
		saveBtn.setOnClickListener(view -> {
            if ( horizontalImageListAdapter.getItemCount() == 0 ){
                Toast.makeText ( this , "이미지는 최소 1개 이상 등록하셔야합니다." , Toast.LENGTH_SHORT ).show ( );
            }
            else if ( courseNameText.getText ( ).length ( ) < 2 ){
                Toast.makeText ( this , "상품명은 2자 이상 등록하셔야합니다." , Toast.LENGTH_SHORT ).show ( );
            }
            else if ( courseDescriptionText.getText ( ).length ( ) < 10 ){
                Toast.makeText ( this , "설명은 10자 이상 등록하셔야합니다." , Toast.LENGTH_SHORT ).show ( );
			}
            else if ( scheduleContainer.getChildCount ( ) == 0 ){
                Toast.makeText ( this , "일정은 1일 이상 등록하셔야합니다." , Toast.LENGTH_SHORT ).show ( );
            }
            else {
				executeUpdateTask( );
			}
        });

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	// 데이터 초기화
	private void initTourData ( ){
		imageCountText.setText(String.valueOf(selectImages.size()));
		courseNameText.setText( null );
		tourDaysText.setText ( null );
		courseDescriptionText.setText ( null );

		scheduleContainer.removeAllViewsInLayout ( );
		scheduleContainer.invalidate();

		initProductImages();
		addScheduleButton();
	}

	// 수정 일경우 데이터 로드
	private void loadTourData ( ){
		//			imageArray [ i ] = CommonData.getImageAddress ( tourProductVO.getHt_imgArray ( ) [ i ] );
		selectImages.addAll(Arrays.asList(tourProductVO.getHt_imgArray()));
		Log.e(TAG, "selectImages.size(): " + selectImages.size() );

		initProductImages();

		imageCountText.setText(String.valueOf(selectImages.size()));
		courseNameText.setText(tourProductVO.getHt_name());

		courseDescriptionText.setText(tourProductVO.getHt_intro());
		executeTask();
	}

	// Schedule TASK 실행
	private void executeTask ( ){
		ScheduleTask scheduleTask = new ScheduleTask(this);
		scheduleTask.execute(tourProductVO.getHt_id());
	}

	// 일정리스트 레이아웃
	private LinearLayout getScheduleLayout ( final int day ){
		final LinearLayout linearLayout = ( LinearLayout ) View.inflate ( this , R.layout.layout_add_schedule , null );
		linearLayout.setTag ( day + 1 );
		TextView daysText = linearLayout.findViewById ( R.id.day_text);
		TextView siteText = linearLayout.findViewById ( R.id.mtc_site_text );
		TextView distanceText = linearLayout.findViewById ( R.id.mtc_distance_text );
		ImageView modifyBtn = linearLayout.findViewById ( R.id.schedule_modify_btn );
		ImageView deleteBtn = linearLayout.findViewById ( R.id.schedule_delete_btn );

		daysText.setText((day + 1) + "일차");

		if ( mtcSmartTourProductsVOArrayList.size ( ) != 0 ){
			int count = mtcSmartTourProductsVOArrayList.get ( day ).getScheduleArrayList().size ( );
			Log.e("MTCMakeCourseAc 301", "getScheduleLayout: " + mtcSmartTourProductsVOArrayList.get ( day ).getScheduleArrayList ( ).get(0).getContentName() );
			Log.e("MTCMakeCourseAc 302", "count : " + count + " day : " + day);

			siteText.setText ( count + "" );
			// 총 거리 계산
			if ( tourProductVO.getDistance ( ) != null ) {
				distanceText.setText(tourProductVO.getDistance() + " km");
			}
			else{
				int distance = 0;

				if ( mtcSmartTourProductsVOArrayList.get ( day ).getMtcScheduleArrayList ( ).get ( 0 ).getDistance ( ) != null ){
					for ( int i = 0 ; i < count - 1 ; i++ )	{
                        String value = mtcSmartTourProductsVOArrayList.get(day).getMtcScheduleArrayList( ).get( i ).getDistance( );
                        if(util.isValueAble(value)) {
                            Double temp = Double.parseDouble(value);
                            distance += temp.intValue();
                        }
					}
				}
				else{
					for ( int i = 0 ; i < count ; i++ )	{
						if ( i != 0 ) {
                            RouteVO vo = mtcSmartTourProductsVOArrayList.get(day).getScheduleArrayList().get(i).getRoute();
                            if(vo != null) {
                                String value = vo.getDistance();
                                if(util.isValueAble(value)) {
                                    Double temp = Double.parseDouble(value);
                                    distance += temp.intValue();
                                }
                            }
                        }
					}
				}
				distanceText.setText ( SmartTourUtils.getDistance ( String.valueOf ( distance ) ) );
			}
		}

		modifyBtn.setOnClickListener (v -> {
            // 일정수정
            final Intent intent = new Intent ( MakeMyProductActivity.this , MTCMakeScheduleActivity.class );
            intent.putExtra( "data" , tourProductVO );
            intent.putExtra("isModifyCourse", true);
            intent.putExtra("day", Integer.valueOf(linearLayout.getTag().toString()));

            ( ( CommonData ) getApplication ( ) ).setMtcSmartTourProductsVO(mtcSmartTourProductsVOArrayList.get(day));

            Log.e(TAG, "일정수정 클릭 ! : " + day );
            startActivityForResult ( intent , REQUEST_CODE_SCHEDULE );
        });
		// 일정삭제
		deleteBtn.setOnClickListener (v -> {
            final AlertDialog.Builder builder = new AlertDialog.Builder ( MakeMyProductActivity.this  );
            builder.setMessage("삭제하시겠습니까?");
            builder.setPositiveButton ( "확인" , (dialog, which) -> {
                Log.e("qtam", "day=========> ");
                scheduleChanged = true;

                mtcSmartTourProductsVOArrayList.remove(day);
                refreshScheduleContainer();
            });

            builder.setNegativeButton("취소", (dialog, which) -> dialog.dismiss());
            builder.show();
        });

		return linearLayout;
	}

	// 일정 수정 또는 일정 만들기 후 실행
	private void addNewScheduleComplete(int workedDay ){
		scheduleContainer.removeAllViewsInLayout();
		addScheduleButtons();
	}

	private void refreshScheduleContainer ( ){
		scheduleContainer.removeAllViewsInLayout ( );
		addScheduleButton();
	}

	// 일정 추가 버튼 추가
	private void addScheduleButton ( ){
		Log.e("MTCMakeScheAc 405", "이거는 음..");
		if (isModifyCourse && mtcSmartTourProductsVOArrayList.size ( ) != 0 ) // 수정
			addScheduleButtons ( );
		else // 새로 만들기
			scheduleContainer.addView ( addScheduleBtn );
	}

	// 일정 추가 버튼 추가
	private void addScheduleButtons ( ){
		if ( mtcSmartTourProductsVOArrayList.get ( 0 ).getScheduleArrayList ( ).size ( ) == 0 ){
			scheduleContainer.addView ( addScheduleBtn );
			tourDaysText.setText ( "- 일" );
			return;
		}

		int count = mtcSmartTourProductsVOArrayList.size();
		tourDaysText.setText(count + " 일");

		for ( int i = 0 ; i < count ; i++ ){
			LinearLayout scheduleLayout = getScheduleLayout ( i );
			scheduleContainer.addView ( scheduleLayout );
		}
		scheduleContainer.addView(addScheduleBtn);
	}

	// 이미지 추가 버튼 넣기
	private void initProductImages(){
		RecyclerView horizontalImageList = (RecyclerView) findViewById(R.id.image_gallery);
		LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
		horizontalImageList.setLayoutManager(layoutManager);
		horizontalImageListAdapter = new HorizontalRecyclerViewAdapter(this, selectImages);
		horizontalImageList.setAdapter(horizontalImageListAdapter);

		findViewById(R.id.change_img).setOnClickListener (v -> {
			Dialog dialog = new Dialog(this);
			dialog.setContentView(R.layout.dialog_image_select);

			RecyclerView recyclerView = dialog.findViewById(R.id.list_view);
			recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

			LinkedList<String> imageList = new LinkedList<>();
			ArrayList<ScheduleVO> vos = new ArrayList<>();
			ArrayList<String > VOIds = new ArrayList<>();

			//중복된 scheduleVO 제거함
			for (MTCSmartTourProductsVO stpVO:
					mtcSmartTourProductsVOArrayList) {
				for (int i = 0 ; i < stpVO.getScheduleArrayList().size() ; i++){
					String str = stpVO.getScheduleArrayList().get(i).getContentID();
					boolean check = true;

					for (int j = 0 ; j < VOIds.size(); j++){
						if (VOIds.contains(str)){
							check = false;
							break;
						}
					}
					if (check){
						VOIds.add(str);
						vos.add(stpVO.getScheduleArrayList().get(i));
					}
				}
				Log.e(TAG, "vos.size(): " + vos.size() );
			}
			for (ScheduleVO sVO :vos) {
				if (sVO.getImageArray() != null){
					imageList.addAll(Arrays.asList(sVO.getImageArray()));
				}
				else {
					imageList.add(sVO.getImage());
				}
			}

			ImageRecyclerAdapter adapter = new ImageRecyclerAdapter(this, imageList, selectImages);
			recyclerView.setAdapter(adapter);

			dialog.findViewById(R.id.select_ok).setOnClickListener(v1 -> {
				selectImages.clear();
				selectImages.addAll(adapter.getSelectedImageList());

				Log.e(TAG, "selectImages.size() " + selectImages.size() );
				horizontalImageListAdapter.setImages(selectImages);
				imageCountText.setText("" + selectImages.size());

				dialog.cancel();
			});

			dialog.findViewById(R.id.select_cancel).setOnClickListener(v1 -> dialog.cancel());
			dialog.show();
        });
	}

	@Override
	public void onClick ( View v ){
		if ( v.getId ( ) == addScheduleBtn.getId ( ) ){ 	// 새로운 일정 추가
			newDateNum = scheduleContainer.getChildCount ( );
			( ( CommonData ) getApplication ( ) ).setMtcSmartTourProductsVO ( new MTCSmartTourProductsVO ( ) );
			Intent intent = new Intent ( this , MTCMakeScheduleActivity.class );
			intent.putExtra ( "data" , tourProductVO );
			intent.putExtra ( "day" , newDateNum );
			startActivityForResult ( intent , REQUEST_CODE_SCHEDULE );
		}
	}

	@Override
	protected void onActivityResult ( int requestCode , int resultCode , Intent data ){
		super.onActivityResult(requestCode, resultCode, data);

		// 일정만들기 및 수정
		if ( requestCode == REQUEST_CODE_SCHEDULE && resultCode == RESULT_OK ){
			scheduleChanged = true;

			int workedDay = data.getIntExtra("data", 0);
			int position = workedDay - 1;
			int count = mtcSmartTourProductsVOArrayList.size();

			mtcSmartTourProductsVO = ((CommonData) getApplication()).getMtcSmartTourProductsVO();

			if (position < count) {
				Log.e(TAG, "onActivityResult 스케줄수정 " );
				mtcSmartTourProductsVOArrayList.set(position, mtcSmartTourProductsVO);
			}
			else{
				Log.e(TAG, "onActivityResult 스케줄추가 " );
				mtcSmartTourProductsVOArrayList.add(mtcSmartTourProductsVO);
			}

			addNewScheduleComplete( workedDay );
		}
	}

    private boolean scheduleChanged = false;

	private void executeUpdateTask( ){  //완료시
		Log.e(TAG, "  업로드중 ");
		MyScheduleUpdateRequestVO myScheduleUpdateRequestVO = new MyScheduleUpdateRequestVO ( );
		myScheduleUpdateRequestVO.setImagePathArrayList ( new ArrayList <> ( ) );
		myScheduleUpdateRequestVO.setUpdateJSONObject ( getRequestJSONObject ( ) );

		for (String anImageArray :
				selectImages) {
			if (anImageArray != null)
				myScheduleUpdateRequestVO.getImagePathArrayList().add( CommonData.getImageAddress() + anImageArray);
		}

		MyScheduleUpdateTask myScheduleUpdateTask = new MyScheduleUpdateTask( this );
		myScheduleUpdateTask.execute ( myScheduleUpdateRequestVO );
	}

	public JSONObject getRequestJSONObject ( ){  				//저장시 데이터들
		Log.e("MTC M C Activity 772", "getRequestJSONObject");

		JSONObject containerJSONObject = new JSONObject ( );
		JSONObject jsonObject = new JSONObject ( );

		try{
			jsonObject.put ( "ht_type" , 1 );
			jsonObject.put ( "ht_name" , courseNameText.getText ( ).toString ( ) );
			jsonObject.put ( "nickname" , CommonData.getUserVO ( ).getNickname ( ) );
			jsonObject.put ( "ht_rcmd_comt" , ( tourProductVO.getHcnt_rcmd_comt( ) == null ? "" : tourProductVO.getHcnt_rcmd_comt( ) ) );
			jsonObject.put ( "email" , CommonData.getUserVO ( ).getEmail ( ) );
			jsonObject.put ( "ht_image" , getImageString( ) );
			jsonObject.put ( "days" , mtcSmartTourProductsVOArrayList.size ( ) );
			jsonObject.put ( "ht_intro" , courseDescriptionText.getText ( ).toString ( ) );

			if ( tourProductVO.getHt_id ( ) != null ) {
				jsonObject.put("ht_id", tourProductVO.getHt_id());
			}

            jsonObject.put ( "schedule_changed" , String.valueOf(scheduleChanged));

			containerJSONObject.put ( "tour" , jsonObject );

            if(scheduleChanged) {
                for ( int i = 0 ; i < mtcSmartTourProductsVOArrayList.size ( ) ; i++ ){
                    JSONArray dayJsonArray = new JSONArray ( );
                    for ( int k = 0 ; k < mtcSmartTourProductsVOArrayList.get ( i ).getMtcScheduleArrayList ( ).size ( ) ; k++ ){
                        JSONObject object = new JSONObject ( );
                        object.put ( "memo" , getMemoArrayToString ( mtcSmartTourProductsVOArrayList.get ( i ).getMtcScheduleArrayList ( ).get ( k ).getMemo ( ) ) == null ? "" : getMemoArrayToString ( mtcSmartTourProductsVOArrayList.get ( i ).getMtcScheduleArrayList ( ).get ( k ).getMemo ( ) ) );

                        MTCScheduleVO temp = mtcSmartTourProductsVOArrayList.get ( i ).getMtcScheduleArrayList ( ).get(k);
                        if(temp != null) {
                            object.put ( "distance", temp.getDistance() == null ? "0" : temp.getDistance());
                            object.put ( "lead_time" , temp.getLead_time ( ) == null ? "0" : temp.getLead_time ( ) );
                            object.put ( "geometry" , temp.getGeomery ( ) == null ? "LINESTRING EMPTY" : temp.getGeomery ( ) );
                        }

                        object.put ( "seq" , mtcSmartTourProductsVOArrayList.get ( i ).getMtcScheduleArrayList ( ).get ( k ).getSeq ( ) );

                        if( mtcSmartTourProductsVOArrayList.get ( i ).getScheduleArrayList().get(k).getHealingContentsVO() != null)
                            object.put ( "hcnt_id" , mtcSmartTourProductsVOArrayList.get ( i ).getScheduleArrayList().get(k).getHealingContentsVO().getHcnt_id() );
                        else
                            object.put ( "ccnt_id" , mtcSmartTourProductsVOArrayList.get ( i ).getScheduleArrayList().get(k).getSightSeeingContentsVO().getCcnt_id() );

                        dayJsonArray.put ( k , object );
                    }
                    containerJSONObject.put ( i + 1 + "" , dayJsonArray );
                }
            }

		}
		catch ( JSONException e ){
			e.printStackTrace ( );
		}
		return containerJSONObject;
	}

	private String getImageString( ){
		String str = "";

		if (  selectImages.size() != 0 && selectImages.get(0) != null){
			str = selectImages.get(0);

			for ( int i = 1 ; i < selectImages.size() ; i++ ){
				if ( selectImages.get(i) != null ){
					str = str + "," + selectImages.get(i);
				}
			}
		}



		Log.e(TAG, "getImageString: " + str );

		return str;
	}

	private JSONArray getMemoArrayToString ( ArrayList < String > strings ){
		JSONArray arr = new JSONArray ( );
		for ( int i = 0 ; i < strings.size ( ) ; i++ ){
			arr.put ( strings.get ( i ) );
		}
		return arr;
	}

	@Override
	public void setJSONObjectResult ( Object result ){
		if ( result == "success" ){
			AlertDialog.Builder builder = new AlertDialog.Builder ( this );
			builder.setMessage ( "상품 업로드가 완료되었습니다." ).setCancelable ( false ).setPositiveButton ( "확인" , (dialog, id) -> {
                Intent intent = getIntent ( );
                setResult ( RESULT_OK , intent );
                finishActivity ( MakeMyProductActivity.REQUEST_CODE_SCHEDULE );
                finish ( );
            });
			AlertDialog alert = builder.create ( );
			alert.show ( );
		}
		else{
			Toast.makeText ( this , "상품 업로드를 실패했습니다." , Toast.LENGTH_SHORT ).show ( );
		}
	}

	@Override
	public void setJSONArrayResult ( ArrayList < ScheduleVO > resultList ){
		if ( resultList == null ){
			scheduleContainer.addView ( addScheduleBtn ); // 수정
			return;
		}
		SmartTourProductsVO smartTourProductsVO = new SmartTourProductsVO();
		smartTourProductsVO.setScheduleArrayList(resultList);

		// 총 일정 날짜
		int count = Integer.valueOf ( smartTourProductsVO.getScheduleArrayList ( ).get ( 0 ).getDays() );
		// 일정별 데이타 분류
		for ( int i = 0 ; i < count ; i++ ){
			MTCSmartTourProductsVO mtcSmartTourProductsVO = new MTCSmartTourProductsVO ( );
			mtcSmartTourProductsVO.setTourProductVO ( tourProductVO );
			mtcSmartTourProductsVO.setScheduleArrayList ( ScheduleParser.getSchedule ( smartTourProductsVO.getScheduleArrayList ( ) , i + 1 ) );  //날짜별로 가져오기
			mtcSmartTourProductsVO.setMtcScheduleArrayList ( new ArrayList <> ( ) );
			mtcSmartTourProductsVOArrayList.add ( mtcSmartTourProductsVO );

			for ( int k = 0 ; k < mtcSmartTourProductsVOArrayList.get ( i ).getScheduleArrayList ( ).size ( ) ; k++ ){
				RouteVO temp = mtcSmartTourProductsVOArrayList.get ( i ).getScheduleArrayList ( ).get ( k ).getRoute();

				MTCScheduleVO mtcScheduleVO = new MTCScheduleVO ( );

				if(temp != null)
					mtcScheduleVO.setDistance ( temp.getDistance() );
				else
					mtcScheduleVO.setDistance ( null );

				mtcScheduleVO.setMemo ( mtcSmartTourProductsVOArrayList.get ( i ).getScheduleArrayList ( ).get ( k ).getMemo ( ) );

				if(temp != null)
					mtcScheduleVO.setLead_time ( temp.getLead_time() );
				else
					mtcScheduleVO.setLead_time(null);

				if(temp != null)
					mtcScheduleVO.setGeomery ( temp.getGeometry() );
				else
					mtcScheduleVO.setGeomery(null);
				mtcScheduleVO.setSeq ( k + 1 + "" );
				mtcScheduleVO.setHcnt_id ( mtcSmartTourProductsVOArrayList.get ( i ).getScheduleArrayList ( ).get ( k ).getContentID ( ) );
				mtcSmartTourProductsVOArrayList.get ( i ).getMtcScheduleArrayList ( ).add ( mtcScheduleVO );
			}
		}
		CommonData.setSmartTourProductsVO(smartTourProductsVO);

		addScheduleButton();					// 일정 버튼추가
	}
}
