package jejusmarttour.main_mytourproduct;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.HealingContentsVO;
import syl.com.jejusmarttour.R;

public class MTCGetHCNTSListViewAdapter extends BaseAdapter
{
	private static final String TAG = MTCGetHCNTSListViewAdapter.class.toString ( );

	private ArrayList < HealingContentsVO > listData;
	private Context context;
	private LayoutInflater inflater;
	private OnCheckedChangeListener onCheckedChangeListener;

	public MTCGetHCNTSListViewAdapter ( Context context , ArrayList < HealingContentsVO > listData , OnCheckedChangeListener onCheckedChangeListener )
	{
		this.context = context;
		this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
		this.onCheckedChangeListener = onCheckedChangeListener;
		this.listData = listData;
	}

	// 현재 아이템의 수를 리턴
	@Override
	public int getCount ( )
	{
		return listData.size ( );
	}

	// 현재 아이템의 오브젝트를 리턴, Object를 상황에 맞게 변경하거나 리턴받은 오브젝트를 캐스팅해서 사용
	@Override
	public Object getItem ( int position )
	{
		return listData.get ( position );
	}

	// 아이템 position의 ID 값 리턴
	@Override
	public long getItemId ( int position )
	{
		return position;
	}

	// 출력 될 아이템 관리
	@Override
	public View getView ( int position , View convertView , ViewGroup parent )
	{
		MTCGetCNTViewHolder viewHolder;

		// 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
		if ( convertView == null )
		{
			// view가 null일 경우 커스텀 레이아웃을 얻어 옴
			convertView = inflater.inflate ( R.layout.adapter_myfavorite_listview , parent , false );

			viewHolder = new MTCGetCNTViewHolder ( );
			viewHolder.checkBox = ( CheckBox ) convertView.findViewById ( R.id.checkbox );
			viewHolder.imageView = ( ImageView ) convertView.findViewById ( R.id.search_item_icon );
			viewHolder.titleText = ( TextView ) convertView.findViewById ( R.id.mtc_title_text );
			viewHolder.timeText = ( TextView ) convertView.findViewById ( R.id.mtc_session_time_text );
			viewHolder.subTitleText = ( TextView ) convertView.findViewById ( R.id.htp_subtitle );
//			viewHolder.linearLayout = ( LinearLayout ) convertView.findViewById ( R.id.mtc_group_layout );
//			viewHolder.groupText = ( TextView ) convertView.findViewById ( R.id.mtc_group_text );
			viewHolder.deleteBtn = ( Button ) convertView.findViewById ( R.id.mtc_delete_btn );

			convertView.setTag ( viewHolder );
		}
		else
		{
			viewHolder = ( MTCGetCNTViewHolder ) convertView.getTag ( );
		}

		Glide.with ( context ).load ( CommonData.getLocationIconId ( listData.get ( position ).getHcnt_type ( ) ) ).thumbnail ( 0.1f ).into ( viewHolder.imageView );
		viewHolder.checkBox.setOnCheckedChangeListener ( onCheckedChangeListener );
		viewHolder.checkBox.setChecked ( ( ( ListView ) parent ).isItemChecked ( position ) );
		viewHolder.checkBox.setFocusable ( false );
		viewHolder.checkBox.setClickable ( false );
		viewHolder.titleText.setText ( listData.get ( position ).getHcnt_name ( ) == null ? "이름없음" : listData.get ( position ).getHcnt_name ( ) );
		viewHolder.timeText.setText ( listData.get ( position ).getHcnt_duration ( ) == null ? "- 분" : SmartTourUtils.getDuration ( listData.get ( position ).getHcnt_duration ( ) ) );
		viewHolder.subTitleText.setText ( listData.get ( position ).getHcnt_addr ( ) == null ? "주소없음" : listData.get ( position ).getHcnt_addr ( ) );

		return convertView;
	}

	@Override
	public void notifyDataSetChanged ( )
	{
		super.notifyDataSetChanged ( );
	}

	// 외부에서 아이템 추가 요청 시 사용
	public void add ( HealingContentsVO vo )
	{
		listData.add ( vo );
	}

	// 외부에서 아이템 삭제 요청 시 사용
	public void remove ( int position )
	{
		listData.remove ( position );
	}

	public void removeAll ( )
	{
		listData.removeAll ( listData );
	}
}
