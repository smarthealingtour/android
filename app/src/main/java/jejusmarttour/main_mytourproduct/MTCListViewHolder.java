package jejusmarttour.main_mytourproduct;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MTCListViewHolder
{
	TextView titleText;
	TextView dateText;
	TextView subTitleText;
	TextView contentText;
	TextView timeText;
	TextView costText;

	ImageView keywordImage_01;
	ImageView keywordImage_02;
	ImageView keywordImage_03;
	ImageView keywordImage_04;
	ImageView keywordImage_05;
	ImageView keywordImage_06;
	ImageView keywordImage_07;
	
	ImageView modifytCourseBtn;
	ImageView shareCourseBtn;
	ImageView shareCourseDeleteBtn;
	ImageView deleteCourseBtn;

	ToggleButton selectToggle;
	View viewBtn;
}
