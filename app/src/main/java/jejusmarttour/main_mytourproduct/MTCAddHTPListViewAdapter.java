package jejusmarttour.main_mytourproduct;

import java.util.ArrayList;

import jejusmarttour.common.KeywordData;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;

public class MTCAddHTPListViewAdapter extends BaseAdapter
{
	private static final String TAG = MTCAddHTPListViewAdapter.class.toString ( );

	private ArrayList < TourProductVO > listData;
	private Context context;
	private LayoutInflater inflater;
	private OnClickListener mOnClickListener;

	public MTCAddHTPListViewAdapter ( Context context , ArrayList < TourProductVO > listData , OnClickListener mOnClickListener )
	{
		this.context = context;
		this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
		this.listData = listData;
		this.mOnClickListener = mOnClickListener;
	}

	// 현재 아이템의 수를 리턴
	@Override
	public int getCount ( )
	{
		return listData.size ( );
	}

	// 현재 아이템의 오브젝트를 리턴, Object를 상황에 맞게 변경하거나 리턴받은 오브젝트를 캐스팅해서 사용
	@Override
	public Object getItem ( int position )
	{
		return listData.get ( position );
	}

	// 아이템 position의 ID 값 리턴
	@Override
	public long getItemId ( int position )
	{
		return position;
	}

	// 출력 될 아이템 관리
	@Override
	public View getView ( int position , View convertView , ViewGroup parent )
	{
		MTCListViewHolder viewHolder;

		// 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
		if ( convertView == null )
		{
			// view가 null일 경우 커스텀 레이아웃을 얻어 옴
			convertView = inflater.inflate ( R.layout.adapter_mtc_add_htp_listview , parent , false );

			viewHolder = new MTCListViewHolder ( );
			viewHolder.titleText = ( TextView ) convertView.findViewById ( R.id.mtc_title_text );
			viewHolder.dateText = ( TextView ) convertView.findViewById ( R.id.mtc_date_text );
			viewHolder.subTitleText = ( TextView ) convertView.findViewById ( R.id.mtc_subtitle_text );
			viewHolder.contentText = ( TextView ) convertView.findViewById ( R.id.mtc_description_text );
			viewHolder.timeText = ( TextView ) convertView.findViewById ( R.id.mtc_session_time_text );
			viewHolder.costText = ( TextView ) convertView.findViewById ( R.id.mtc_price_text );
			viewHolder.keywordImage_01 = ( ImageView ) convertView.findViewById ( R.id.keyword_01 );
			viewHolder.keywordImage_02 = ( ImageView ) convertView.findViewById ( R.id.keyword_02 );
			viewHolder.keywordImage_03 = ( ImageView ) convertView.findViewById ( R.id.keyword_03 );
			viewHolder.keywordImage_04 = ( ImageView ) convertView.findViewById ( R.id.keyword_04 );
			viewHolder.keywordImage_05 = ( ImageView ) convertView.findViewById ( R.id.keyword_05 );
			viewHolder.keywordImage_06 = ( ImageView ) convertView.findViewById ( R.id.keyword_06 );
			viewHolder.keywordImage_07 = ( ImageView ) convertView.findViewById ( R.id.keyword_07 );
			viewHolder.selectToggle = ( ToggleButton ) convertView.findViewById ( R.id.toggle_select_course );

			convertView.setTag ( viewHolder );
		}
		else
		{
			viewHolder = ( MTCListViewHolder ) convertView.getTag ( );
		}

		viewHolder.titleText.setText ( listData.get ( position ).getHt_name ( ) == null ? "제목없음" : listData.get ( position ).getHt_name ( ) );
		viewHolder.dateText.setText ( listData.get ( position ).getHt_create_time ( ) == null ? "등록일없음" : listData.get ( position ).getHt_create_time ( ) );
		viewHolder.subTitleText.setText ( listData.get ( position ).getNickname ( ) == null ? "제공자없음" : listData.get ( position ).getNickname ( ) );
		viewHolder.contentText.setText ( listData.get ( position ).getHt_intro ( ) == null ? "내용없음" : listData.get ( position ).getHt_intro ( ) );
		viewHolder.timeText.setText ( listData.get ( position ).getDuraion ( ) == null ? "- 분" : SmartTourUtils.getDuration ( listData.get ( position ).getDuraion ( ) ) );
		viewHolder.costText.setText ( listData.get ( position ).getCost ( ) == null ? "- 원" : SmartTourUtils.getCost ( listData.get ( position ).getCost ( ) ) );

		ImageView [ ] imageViews = { viewHolder.keywordImage_01 , viewHolder.keywordImage_02 , viewHolder.keywordImage_03 , viewHolder.keywordImage_04 , viewHolder.keywordImage_05 , viewHolder.keywordImage_06 , viewHolder.keywordImage_07 };

		for ( int i = 0; i < imageViews.length; i++ )
		{
			imageViews [ i ].setVisibility ( View.GONE );
		}

		ArrayList < Integer > arrayList = KeywordData.getKeywordIconImage ( listData.get ( position ).getHt_hcdArray ( ) , listData.get ( position ).getHt_scdArray ( ) );

		if ( arrayList != null )
		{
			for ( int i = 0; i < arrayList.size ( ); i++ )
			{
				imageViews [ i ].setVisibility ( View.VISIBLE );
				Glide.with ( context ).load ( arrayList.get ( i ) ).thumbnail ( 0.1f ).into ( imageViews [ i ] );
			}
		}
		viewHolder.selectToggle.setTag ( position );
		viewHolder.selectToggle.setFocusable ( false );
		viewHolder.selectToggle.setFocusableInTouchMode ( false );
		viewHolder.selectToggle.setChecked ( listData.get ( position ).isChecked ( ) );
		viewHolder.selectToggle.setOnClickListener ( mOnClickListener );

		return convertView;
	}

	@Override
	public void notifyDataSetChanged ( )
	{
		super.notifyDataSetChanged ( );
	}

	// 외부에서 아이템 추가 요청 시 사용
	public void add ( TourProductVO vo )
	{
		listData.add ( vo );
	}

	// 외부에서 아이템 삭제 요청 시 사용
	public void remove ( int position )
	{
		listData.remove ( position );
	}

	public void removeAll ( )
	{
		listData.removeAll ( listData );
	}
}
