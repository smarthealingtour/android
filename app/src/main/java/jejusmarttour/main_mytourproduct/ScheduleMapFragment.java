package jejusmarttour.main_mytourproduct;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.data.kml.KmlLayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.map.CommonData_;
import jejusmarttour.map.util;
import jejusmarttour.task.MyHtourGeometryTask;
import jejusmarttour.util.CustomViewPager;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.MTCScheduleVO;
import jejusmarttour.vo.MTCSmartTourProductsVO;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;
//import ktmap.android.map.layer.LayerManager;

public class ScheduleMapFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = ScheduleMapFragment.class.toString();
    private MTCSmartTourProductsVO mtcSmartTourProductsVO;

    private int day = -1;

    private ArrayList<RouteVO> geometry_data;
    private ArrayList<HealingContentsVO> geometry_data2;

    public Boolean isScheduleChanged = false;
    private View view = null;

    private CustomViewPager viewPager = null;

    private GoogleMap mMap;
    private MapView gMapView;

    private String cName = "myHtour";
    private String[] kml_raw;

    public static ScheduleMapFragment newinstance(MTCSmartTourProductsVO mtcSmartTourProductsVO, int newDateNum, CustomViewPager viewPager) {
        ScheduleMapFragment scheduleMapFragment = new ScheduleMapFragment();
        scheduleMapFragment.mtcSmartTourProductsVO = mtcSmartTourProductsVO;
        scheduleMapFragment.day = newDateNum;
        scheduleMapFragment.viewPager = viewPager;
        return scheduleMapFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;

        try {
            view = inflater.inflate(R.layout.fragment_schedule_map, container, false);
            gMapView = view.findViewById(R.id.myHtourMap);
            gMapView.onCreate(savedInstanceState);

            gMapView.getMapAsync(this);
        } catch (InflateException e) {
            Log.e(TAG, e.getMessage());
        }
        int size = mtcSmartTourProductsVO.getScheduleArrayList().size();

        kml_raw = new String[size];

        for (int i = 0; size > i; i++) {
            if ( mtcSmartTourProductsVO.getScheduleArrayList().get(i).getHealingContentsVO()!=null ){
                if ( mtcSmartTourProductsVO.getScheduleArrayList().get(i).getHealingContentsVO().getHcnt_kml() != null ) {
                    kml_raw[i] = mtcSmartTourProductsVO.getScheduleArrayList().get(i).getHealingContentsVO().getHcnt_kml();
                }
            }

        }

        CommonData.recentCname = cName;

        setSchChangedReceiver();
        ((MTCMakeScheduleActivity) getActivity()).setScheduleMapFragmentTag(getTag());

        return view;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = new String[]{ Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            requestPermissions(permissions, 1);
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMinZoomPreference(8);
        mMap.setMaxZoomPreference(23);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(33.386600, 126.547384), (float) 9.2));
        Log.e("지도야","왜안뜨니");
    }

    private void retrieveFileFromUrl(String kml) {
        //new DownloadKmlFile("http://googlemaps.github.io/kml-samples/morekml/Polygons/Polygons.Google_Campus.kml").execute();
        //여기에 서버 주소랑 같이 kml 파일 열 수 있도록 수정
        Log.e("이거 kml 주소", kml);
        new DownloadKmlFile(kml).execute();
    }

    private void moveCameraToKml(int index) {
        if (geometry_data.size() != 0) {
            for (int i = 0; geometry_data.size() > i; i++) {
                String here = geometry_data.get(i).getGeometry();
                Log.e("이게 마커 데이터", here);
                int idx = here.indexOf(" ");
                String lat;
                String lng;

                lat = here.substring(0, idx);
                lng = here.substring(idx + 1);

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))) //coord x,y를 latlng 형태로, 좌표는 healingcontentsvo에서 받아와야하고, ScheduleVO에서 이 vo를 포함하고 있음, task는 scheduletaskvo를 이용
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                if (index == 0) {
                    getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)), 15));
                }
            }
        }
        if (geometry_data2.size() != 0) {
            for (int i = 0; geometry_data2.size() > i; i++) {
                String lat;
                String lng;

                lat = geometry_data2.get(i).getHcnt_coord_x();
                lng = geometry_data2.get(i).getHcnt_coord_y();

                Log.e("marker","x : "+lat+ " y : "+lng);

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))) //coord x,y를 latlng 형태로, 좌표는 healingcontentsvo에서 받아와야하고, ScheduleVO에서 이 vo를 포함하고 있음, task는 scheduletaskvo를 이용
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                if (index == 0) {
                    getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)), 15));
                }
            }
        }
    }

    protected GoogleMap getMap() {
        return mMap;
    }

    public void setSchChangedReceiver() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ArrayList<MTCScheduleVO> list = (ArrayList<MTCScheduleVO>)intent.getSerializableExtra("changedSchduleArrayList");
                ArrayList<ScheduleVO> list2 = (ArrayList<ScheduleVO>)intent.getSerializableExtra("makeScheduleArrayList");
//                geometry_data = (ArrayList<RouteVO>) ((ArrayList<ScheduleVO>) intent.getSerializableExtra("makeScheduleArrayList")).get(i).getHealingContentsVO().getHt_kml();
                try {
//                    Log.e("뭘까도대체14", "mtcScheduleArrayList : " + list.size());
                }catch (NullPointerException e){
                    Log.e("뭘까도대체14", "널포 : ");
                }
                //요거 주석처리하니까 일정수정 리스트에서 코스만들기쪽으로 잘넘어옴 메모 안건드려도. 하지만 코스만들기에서 완료가 안되네 서버문제인가
                //mtcSmartTourProductsVO.setMtcScheduleArrayList(null);
                //mtcSmartTourProductsVO.setMtcScheduleArrayList(list);

//                mtcSmartTourProductsVO.setScheduleArrayList(null);
//                mtcSmartTourProductsVO.setScheduleArrayList(list2);

                isScheduleChanged = true;
            }
        }, new IntentFilter("mtc"));

        requestGeometry();
//        if(kml_raw != null) {
//            for ( int i = 0; i < kml_raw.length; i++ ) {
//                String url = null;
//                if(kml_raw[i] !=null) {
//                    url = CommonData.getImageAddress();
//                    url += kml_raw[i];
//                    retrieveFileFromUrl(url);
//                }
//            }
//        }
    }

    private boolean isNotShow = true;
    private boolean isRouteShow = false;
    private boolean isRouteDraw = false;

    public void showRoute(String ids) { // 경로보기에서 요청
        isRouteShow = true;
        viewPager.setCurrentItem(1);
        requestGeometry(ids);
    }

    @Override
    public void setUserVisibleHint ( boolean isVisibleToUser ){
        super.setUserVisibleHint ( isVisibleToUser );
        if( isVisibleToUser) {
            if(!isRouteShow) {
                if(isScheduleChanged || isRouteDraw || isNotShow) {
                    requestGeometry();
                    isNotShow = false;
                }
            }
        }
    }

    private void requestGeometry() {

        if(mtcSmartTourProductsVO.getMtcScheduleArrayList().isEmpty())
            return;

        MyHtourGeometryTask task  = new MyHtourGeometryTask(getActivity(), this);
        task.execute(ArraysToString( mtcSmartTourProductsVO.getMtcScheduleArrayList() ) );
        if(kml_raw != null) {
            for ( int i = 0; i < kml_raw.length; i++ ) {
                String url = null;
                if(kml_raw[i] != null) {
                    url = CommonData.getImageAddress();
                    url += kml_raw[i];
                    retrieveFileFromUrl(url);
                }
            }
        }
        isRouteDraw = false;
    }

    private void requestGeometry(String ids) {
        MyHtourGeometryTask task  = new MyHtourGeometryTask(getActivity(), this);
        task.execute( ids );

        isRouteDraw = true;
        isRouteShow = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        gMapView.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        gMapView.onLowMemory();
    }

    @Override
    public void onPause() {
        gMapView.onPause();
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        gMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        gMapView.onStop();
    }

    public void setJsonResult(JSONArray result) throws JSONException {
        if(result == null) {
            Toast.makeText(getActivity(), "경로요청에 실패했습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        final int numberOfItemsInResp = result.length ( );

        geometry_data = new ArrayList<>();
        geometry_data2 = new ArrayList<>();

        for ( int i = 0; i < numberOfItemsInResp; i++ ) {
            RouteVO routeVO = new RouteVO();
            HealingContentsVO healingContentsVO = new HealingContentsVO();
            JSONObject jsonObject = new JSONObject(result.getString(i));
            healingContentsVO.setHcnt_type( jsonObject.has ( "hcnt_type" ) ? jsonObject.getString ( "hcnt_type" ) : jsonObject.getString("type") );
            try {
                if( healingContentsVO.getHcnt_type().equals("line") || healingContentsVO.getHcnt_type().equals("zone") ) {
                } else {
                    routeVO.setGeometry(jsonObject.getString("geometry"));
                    geometry_data.add(routeVO);
                }
            } catch (NullPointerException e)
            {
                Log.e ( this.toString() , e.toString ( ) );
            }
            moveCameraToKml(i);
        }
        isScheduleChanged = false;
    }

    public String ArraysToString(ArrayList<MTCScheduleVO> list) {
        String result = "";
        for(int i = 0; i < list.size(); i++) {
            result += "," + list.get(i).getHcnt_id();
        }
        return result.substring(1);
    }

    @Override
    public void onDestroy ( ) {
        gMapView.onDestroy();
        super.onDestroy ( );

        CommonData_ common = CommonData_.getInstance ( getActivity ( ) );
        if ( common != null )
            common.dispose ( );

        util.recursiveRecycle(getActivity().getWindow().getDecorView());

        System.gc ( );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(this.toString(), "onRequestPermissionsResult: 지도퍼미션상태가" );
        if (requestCode == 1){
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.e(this.toString(), "onRequestPermissionsResult: 퍼미션 확인 완료" );
            } else {
                Toast.makeText(getActivity(), "지도보기 권한이 필요합니다!", Toast.LENGTH_SHORT).show();
                getActivity().fileList();
            }
        }
    }

    private class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
        private final String mUrl;

        public DownloadKmlFile(String url) {
            mUrl = url;
            Log.e("주소 맞나?", url.toString());
        }

        protected byte[] doInBackground(String... params) {
            try {
                InputStream is =  new URL(mUrl).openStream();
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[16384];
                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                return buffer.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(byte[] byteArr) {
            try {
                if (byteArr != null){
                    KmlLayer kmlLayer = new KmlLayer(mMap, new ByteArrayInputStream(byteArr),
                            getContext());
                    kmlLayer.addLayerToMap();
                    moveCameraToKml(0);
                }
            } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}