package jejusmarttour.main_mytourproduct;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import jejusmarttour.util.CustomViewPager;
import jejusmarttour.vo.MTCSmartTourProductsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class MTCMakeScheduleActivity extends AppCompatActivity{
	private static final String TAG = MTCMakeScheduleActivity.class.toString ( );

	private final int MAX_PAGE = 2;
	private Fragment cur_fragment = new Fragment ( );

	private CustomViewPager viewPager;
	private PagerSlidingTabStrip tab;

	private MTCSmartTourProductsVO mtcSmartTourProductsVO;

	private TourProductVO tourProductVO;
	// 수정인지 아닌지
	private boolean isModifyCourse = false;
	private int newDateNum = 1;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_mtcmake_schedule );

		Intent intent = getIntent ( );
		tourProductVO = ( TourProductVO ) intent.getSerializableExtra ( "data" );
		isModifyCourse = intent.getBooleanExtra ( "isModifyCourse" , false );
		newDateNum = intent.getIntExtra("day", 1);

		if (isModifyCourse) {
			mtcSmartTourProductsVO = ((CommonData) getApplication()).getMtcSmartTourProductsVO();
		}

		try{
			Log.e(toString(), "===============>" + mtcSmartTourProductsVO.getScheduleArrayList().size());
		}
		catch (NullPointerException e){
			Log.e(toString(), "===============> mtcSmartTourProductsVO 널포");
		}


		if ( null == mtcSmartTourProductsVO ){  //신규만들기
			mtcSmartTourProductsVO = new MTCSmartTourProductsVO ( );
		}

		mtcSmartTourProductsVO.setTourProductVO ( tourProductVO );

		initActionBar ( );
		initView ( );
	}

	// 스와이프 ON OFF
	public void swipeOnOff ( boolean useSwipe ){
		viewPager.setSwipeable ( useSwipe );
	}

	private void initView ( ){
		viewPager = ( CustomViewPager ) findViewById ( R.id.mtc_make_schedule_viewpager );
		viewPager.setAdapter ( new ViewPagerAdapter ( getSupportFragmentManager ( ) ) );
		viewPager.setOffscreenPageLimit ( 1 );
		tab = ( PagerSlidingTabStrip ) findViewById ( R.id.mtc_make_schedule_tabs );
		tab.setShouldExpand(true);
		setTabStyle();
		tab.setViewPager ( viewPager );
	}

	// TAB 스타일 바꾸기
	@SuppressLint ( "NewApi" )
	@SuppressWarnings ( "deprecation" )
	private void setTabStyle ( ){
		tab.setTextSize ( getResources ( ).getDimensionPixelSize ( R.dimen.font_size_03 ) );
		tab.setDividerColor ( getResources ( ).getColor ( R.color.white ) );
		tab.setBackgroundColor ( getResources ( ).getColor ( R.color.white ) );
		tab.setIndicatorColor ( getResources ( ).getColor ( R.color.statusbar ) );
		tab.setTextColor ( getResources ( ).getColor ( R.color.font_color_black ) );
}

	// 액션바 설정
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_make_my_tour_course , null );
		ImageView cancelBtn = customView.findViewById ( R.id.cancel_btn );
		ImageView saveBtn = customView.findViewById ( R.id.save_btn );
		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( getTitleText ( ) );

		cancelBtn.setOnClickListener (view -> {
			Log.e(TAG, "onClick: 스케줄취소");
			Intent intent = getIntent ( );
            intent.putExtra("data", newDateNum);
            setResult(RESULT_CANCELED, intent );
            finish ( );
        });
		saveBtn.setOnClickListener(view -> {
			if(mtcSmartTourProductsVO.getScheduleArrayList().size() < 2) {
				Toast.makeText(this, "장소가 부족합니다.", Toast.LENGTH_SHORT).show();
				return ;
			}
			Intent intent = getIntent ( );
			intent.putExtra ( "data" , newDateNum );
			setResult ( RESULT_OK , intent );
			finishActivity ( MakeMyProductActivity.REQUEST_CODE_SCHEDULE );
			finish ( );
        });
		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	private String getTitleText ( ){

		return newDateNum + "일차 일정만들기";
	}

	// 데이타 저장
	public void setScheduleData(MTCSmartTourProductsVO productsVO ){
		Log.e( toString() , "setScheduleData1" + mtcSmartTourProductsVO.getScheduleArrayList().get(0).getContentName());
		mtcSmartTourProductsVO = productsVO;
		Log.e( toString() , "setScheduleData2" + mtcSmartTourProductsVO.getScheduleArrayList().get(0).getContentName());
		( ( CommonData ) getApplication ( ) ).setMtcSmartTourProductsVO ( productsVO );
	}

	@Override
	public void onBackPressed() {
		LinearLayout layout = ( LinearLayout ) View.inflate ( this , R.layout.popup_cancel , null );

		AlertDialog.Builder alertBox = new AlertDialog.Builder ( this );
		alertBox.setView ( layout );
		alertBox.create ( );
		final DialogInterface popup = alertBox.show ( );

		TextView titleText = layout.findViewById(R.id.title_text);
		titleText.setText(newDateNum+"일차");
		Button cancel = layout.findViewById ( R.id.cancel);
		cancel.setOnClickListener(v ->{
			popup.dismiss();
			finish();
		});

		Button noCancel = layout.findViewById(R.id.no_cancel);
		noCancel.setOnClickListener(v -> popup.dismiss());

	}

	class ViewPagerAdapter extends FragmentPagerAdapter{
		public ViewPagerAdapter ( FragmentManager fm )
		{
			super ( fm );
		}

		private int positionNum = 0;

		@Override
		public Fragment getItem ( int position ){
			if ( position < 0 || MAX_PAGE <= position )
				return null;

			positionNum = position;
			switch ( position ){
				case 0 :  //일정관리

					Log.e(toString(), "date " + newDateNum + " isModifyCourse " + isModifyCourse);
					//cur_fragment = ScheduleManagerFragment.newInstance ( mtcSmartTourProductsVO , isModifyCourse , newDateNum );

					for (ScheduleVO vo :
						mtcSmartTourProductsVO.getScheduleArrayList()
						 ) {
						Log.e(TAG, "getItem: " + vo.getContentName());
					}

					cur_fragment = ScheduleManegeFragment.newInstance(mtcSmartTourProductsVO, isModifyCourse , newDateNum, viewPager );
					break;
				case 1 :   //지도보기
					cur_fragment = ScheduleMapFragment.newinstance ( mtcSmartTourProductsVO, newDateNum, viewPager );
					break;
			}
			return cur_fragment;
		}

		public int getCurrentPosition ( ){
			return positionNum;
		}

		@Override
		public int getCount ( ){
			return MAX_PAGE;
		}

		@Override
		public CharSequence getPageTitle ( int position ){
			return getResources ( ).getStringArray ( R.array.make_schedule_titles ) [ position ];
		}
	}

    public String tag = "";

    public void setScheduleMapFragmentTag(String tag) {
        this.tag = tag;
    }

    public String getScheduleMapFragmentTag() { return tag; }

}
