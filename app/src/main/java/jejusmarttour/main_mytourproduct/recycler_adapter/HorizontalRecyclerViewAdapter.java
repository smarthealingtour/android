package jejusmarttour.main_mytourproduct.recycler_adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import jejusmarttour.common.CommonData;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2018-02-25.
 */

public class HorizontalRecyclerViewAdapter extends RecyclerView.Adapter<HorizontalRecyclerViewAdapter.HRViewHolder> {
    private Context context;
    private ArrayList<String> images;

    public HorizontalRecyclerViewAdapter(Context context, List<String> images){
        this.context = context;
        this.images = new ArrayList<>(images);
    }

    @Override
    public HRViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_img2, parent, false);

        /*LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMarginStart(5);
        layoutParams.setMarginEnd(5);

        view.setLayoutParams( layoutParams );*/

        return new HRViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HRViewHolder holder, int position) {
        Glide.with ( context ).load (CommonData.getImageAddress() + images.get(position)).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void setImages(List<String> images){
        this.images.clear();
        this.images.addAll(images);

        notifyDataSetChanged();
    }

    class HRViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        ImageView imageView;

        HRViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            imageView = itemView.findViewById(R.id.img);
        }
    }
}
