package jejusmarttour.main_mytourproduct;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;

import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import syl.com.jejusmarttour.R;

public class MTCAddSiteActivity extends AppCompatActivity
{
	private static final String TAG = MTCAddSiteActivity.class.toString ( );

	private RadioGroup radioGroupTab;
	private ViewPager viewPager;
	private final PageListener pageListener = new PageListener ( );

	private final int MAX_PAGE = 2;
	private Fragment cur_fragment = new Fragment ( );

	Spinner spinner1;
	Spinner spinner2;
	Spinner spinner3;

	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_mtcadd_sitectivity );

		initSpinner();
		initActionBar ( );
		initViewPager ( );
	}

	private void initSpinner(){
		spinner1 = (Spinner)findViewById(R.id.spinner1);
		spinner2 = (Spinner)findViewById(R.id.spinner2);
		spinner3 = (Spinner)findViewById(R.id.spinner3);

		final Resources.Theme theme = getResources().newTheme();
		theme.applyStyle(R.style.SpinnerDropDownStyle, true);

		/*ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
				R.array.depth1, R.layout.spinner_layout);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter1.setDropDownViewTheme( theme );*/

		ArrayList list = new ArrayList();
		list.add("전체    ");
		list.add("관심    ");
		list.add("주변    ");

		ArrayList list2 = new ArrayList();
		list2.add("전체    ");
		list2.add("자연    ");
		list2.add("문화    ");
		list2.add("레저    ");
		list2.add("체험    ");

		ArrayList list_d2= new ArrayList();
		list_d2.add("전체    ");
		list_d2.add("해변    ");
		list_d2.add("오름    ");
		list_d2.add("공원    ");
		list_d2.add("올레    ");
		list_d2.add("계곡    ");
		list_d2.add("폭포    ");

		final ArrayList list_d3 = new ArrayList();
		list_d3.add("전체    ");
		list_d3.add("명소    ");
		list_d3.add("테마파크    ");
		list_d3.add("박물관    ");
		list_d3.add("유적지    ");

		final ArrayList list_d4 = new ArrayList();
		list_d4.add("전체    ");
		list_d4.add("수상    ");
		list_d4.add("지상    ");
		list_d4.add("상공    ");

		final ArrayList list_d5 = new ArrayList();
		list_d5.add("전체    ");
		list_d5.add("농장    ");
		list_d5.add("승마    ");
		list_d5.add("해녀    ");
		list_d5.add("낚시    ");
		list_d5.add("잠수함    ");

		ArrayAdapter<ArrayList> adapter1 = new ArrayAdapter(this, R.layout.spinner_layout, list);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter1.setDropDownViewTheme( theme );

		ArrayAdapter<ArrayList> adapter2 = new ArrayAdapter(this, R.layout.spinner_layout, list2);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter2.setDropDownViewTheme( theme );

		ArrayAdapter<ArrayList> adapter3 = new ArrayAdapter(this, R.layout.spinner_layout, new ArrayList());
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter3.setDropDownViewTheme( theme );

		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
		spinner3.setAdapter(adapter3);

		spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				switch ( position ){
					case 1:
						spinner3.setBackgroundResource(R.drawable.bg_spinner);
						adapter3.clear();
						adapter3.addAll(list_d2);
						break;
					case 2:
						spinner3.setBackgroundResource(R.drawable.bg_spinner);
						adapter3.clear();
						adapter3.addAll(list_d3);
						break;
					case 3:
						spinner3.setBackgroundResource(R.drawable.bg_spinner);
						adapter3.clear();
						adapter3.addAll(list_d4);
						break;
					case 4:
						spinner3.setBackgroundResource(R.drawable.bg_spinner);
						adapter3.clear();
						adapter3.addAll(list_d5);
						break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	private void initViewPager ( )
	{
		viewPager = ( ViewPager ) findViewById ( R.id.viewpager );
		viewPager.setAdapter ( new ViewPagerAdapter ( getSupportFragmentManager ( ) ) );
		viewPager.setOffscreenPageLimit ( MAX_PAGE - 1 );
		viewPager.setOnPageChangeListener ( pageListener );
	}

	// 탭 토글 클릭
	public void onToggle ( View view )
	{
		boolean isChecked = ( ( ToggleButton ) view ).isChecked ( );
		Log.d ( TAG , "isChecked : " + isChecked );

		if ( isChecked == false )
		{
			( ( ToggleButton ) view ).setChecked ( true );
			return;
		}

		( ( RadioGroup ) view.getParent ( ) ).check ( view.getId ( ) );
		viewPager.setCurrentItem ( Integer.valueOf ( view.getTag ( ).toString ( ) ) );
	}

	// 액션바 설정
	private void initActionBar ( )
	{
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_make_my_tour_course , null );
		ImageView cancelBtn = ( ImageView ) customView.findViewById ( R.id.cancel_btn );
		TextView titleTextView = ( TextView ) customView.findViewById ( R.id.title_text );
		titleTextView.setText ( getResources ( ).getString ( R.string.title_activity_mtcadd_site ) );

		cancelBtn.setOnClickListener ( new View.OnClickListener ( )
		{
			@Override
			public void onClick ( View view )
			{
				finish ( );
			}
		} );

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	// 토글 버튼
	final RadioGroup.OnCheckedChangeListener ToggleListener = new RadioGroup.OnCheckedChangeListener ( )
	{
		@Override
		public void onCheckedChanged ( final RadioGroup radioGroup , final int i )
		{
			for ( int j = 0 ; j < radioGroup.getChildCount ( ) ; j++ )
			{
				final ToggleButton view = ( ToggleButton ) radioGroup.getChildAt ( j );
				view.setChecked ( view.getId ( ) == i );
			}
		}
	};

	// 액션바 메뉴
	@Override
	public boolean onCreateOptionsMenu ( Menu menu )
	{
		getMenuInflater ( ).inflate ( R.menu.save , menu );
		return true;
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item )
	{
		int id = item.getItemId ( );

		if ( id == R.id.action_save )
		{
			Intent intent = getIntent ( );
			intent.putExtra ( "data" , "value" );
			setResult ( RESULT_OK , intent );
			finishActivity ( ScheduleManegeFragment.REQUEST_CODE_SITE );
			finish ( );
			return true;
		}

		return super.onOptionsItemSelected ( item );
	}

	private class PageListener implements ViewPager.OnPageChangeListener
	{

		@Override
		public void onPageScrolled ( int position , float positionOffset , int positionOffsetPixels )
		{
		}

		@Override
		public void onPageScrollStateChanged ( int state )
		{
		}

		@Override
		public void onPageSelected ( int position )
		{
			setChangeTabItem ( position );
		}

	}

	private ToggleButton [ ] getToggleButtons ( )
	{
		ToggleButton [ ] buttons = { ( ToggleButton ) radioGroupTab.getChildAt ( 0 ) , ( ToggleButton ) radioGroupTab.getChildAt ( 1 ) };
		return buttons;
	}

	private void setChangeTabItem ( int position )
	{
		getToggleButtons ( ) [ position ].setChecked ( true );
		validateToogleButton ( position );
	}

	private void validateToogleButton ( int position )
	{
		for ( int i = 0 ; i < getToggleButtons ( ).length ; i++ )
		{
			if ( i != position )
				getToggleButtons ( ) [ i ].setChecked ( false );
		}
	}

	// 뷰페이지 어뎁터
	class ViewPagerAdapter extends FragmentPagerAdapter
	{
		private int position = 0;

		public ViewPagerAdapter ( FragmentManager fm )
		{
			super ( fm );
		}

		@Override
		public Fragment getItem ( int position )
		{
			if ( position < 0 || MAX_PAGE <= position )
				return null;

			this.position = position;

			switch ( position )
			{
				case 0 :
					cur_fragment = new MTCGetHCNTFragment ( ).newInstance ( );
					break;
				case 1 :
					cur_fragment = new MTCGetAllHCNTFragment( ).newInstance ( );
					break;
			}
			return cur_fragment;
		}

		public int getCurrentPosition ( )
		{
			return position;
		}

		@Override
		public int getCount ( )
		{
			return MAX_PAGE;
		}

		@Override
		public CharSequence getPageTitle ( int position )
		{
			return getResources ( ).getStringArray ( R.array.htp_main_titles ) [ position ];
		}
	}
}
