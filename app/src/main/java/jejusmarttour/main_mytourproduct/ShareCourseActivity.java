package jejusmarttour.main_mytourproduct;

import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;
import jejusmarttour.common.CommonData;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.task.MTCShareUpdateTask;
import jejusmarttour.vo.UpdateResultVO;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ShareCourseActivity extends AppCompatActivity implements OnClickListener , JSONObjectResult < UpdateResultVO >
{
	private static final String TAG = ShareCourseActivity.class.toString ( );

	private TourProductVO tourProductVO;
	private MTCShareUpdateTask mtcShareUpdateTask;
	
	private Button shareBtn;
	private Button cancelBtn;

	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_share_course );

		Intent intent = getIntent ( );
		tourProductVO = (TourProductVO) intent.getSerializableExtra ( "data" );
		
		TextView nameText = (TextView) findViewById ( R.id.share_course_name_text );
		nameText.setText(tourProductVO.getHt_name());
		
		shareBtn = (Button)findViewById ( R.id.share_course_ok_btn );
		cancelBtn = (Button)findViewById ( R.id.share_course_cancel_btn );
		
		shareBtn.setOnClickListener ( this );
		cancelBtn.setOnClickListener ( this );
		
		initActionBar ( );
	}

	// 커스텀 액션바 적용
	private void initActionBar ( )
	{
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_close , null );
		TextView titleTextView = ( TextView ) customView.findViewById ( R.id.title_text );

		titleTextView.setText ( getResources ( ).getString ( R.string.title_activity_share_course ) );

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	// 액션바 메뉴
	@Override
	public boolean onCreateOptionsMenu ( Menu menu )
	{
		getMenuInflater ( ).inflate ( R.menu.close , menu );
		return true;
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item )
	{
		int id = item.getItemId ( );

		if ( id == R.id.action_close )
		{
			finish ( );
			return true;
		}

		return super.onOptionsItemSelected ( item );
	}

	@Override
	public void onClick ( View v )
	{
		if(v.getId ( ) == shareBtn.getId ( ) )
		{
			//공유 TASK 실행
			mtcShareUpdateTask = new MTCShareUpdateTask ( this );
			mtcShareUpdateTask.execute(CommonData.getUserVO().getEmail(), tourProductVO.getHt_id());
		}
		else
		{
			finish ( );
		}
	}

	@Override
	public void setJSONObjectResult ( UpdateResultVO result )
	{
		mtcShareUpdateTask.cancel ( true );
		
		if ( result == null )
		{
			Log.d ( TAG , "코스공유가 정상적으로 업데이트 되지않았습니다." );
			finish ( );
			return;
		}
		
		if ( result.getHt_status().equals ( "true" ) )
		{
			Toast.makeText ( this , "공유되었습니다." , Toast.LENGTH_SHORT ).show ( );
			finishActivity(MyTourProductActivity.REQUEST_CODE);
			finish();
		}
	}
}
