package jejusmarttour.main_mytourproduct;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import jejusmarttour.common.CommonData;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2018-02-24.
 */

public class ImageRecyclerAdapter extends RecyclerView.Adapter<ImageRecyclerAdapter.ImageViewHolder> {
    private List<String> imageList;
    private Context context;
    private LinkedList<String> selectedImageList;

    public ImageRecyclerAdapter(Context context, List<String> imageList, List<String> selectedImageList){
        this.imageList = imageList;
        this.context = context;
        if (selectedImageList == null)
            this.selectedImageList = new LinkedList<>();
        else
            this.selectedImageList = new LinkedList<>(selectedImageList);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_img_fitxy, parent, false);

        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        /*GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams)holder.itemView.getLayoutParams();
        layoutParams.height = layoutParams.width;
        holder.itemView.requestLayout();*/

        //기존에 선택됬던 이미지 표시
        if (selectedImageList.contains(imageList.get(position))){
            holder.edgeBackground.setBackgroundColor(Color.RED);
        }

        Glide.with ( context ).load ( CommonData.getImageAddress () + imageList.get(position ) ).into ( holder.imageView );

        //이미지 선택시 테두리 색 바꿔서 표시
        holder.itemView.setOnClickListener( v -> {
            if (selectedImageList.size() >= 5){
                if (selectedImageList.contains(imageList.get(position))){
                    holder.edgeBackground.setBackgroundColor(Color.WHITE);
                    selectedImageList.remove(imageList.get(position));
                }
                else {
                    Toast.makeText(context, "이미지는 최대 5개 까지 선택하실 수 있습니다.", Toast.LENGTH_SHORT).show();
                }
            }
            else if (selectedImageList.contains(imageList.get(position))){
                holder.edgeBackground.setBackgroundColor(Color.WHITE);
                selectedImageList.remove(imageList.get(position));
            }
            else {
                holder.edgeBackground.setBackgroundColor(Color.RED);
                selectedImageList.add(imageList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public ArrayList<String> getSelectedImageList(){
        ArrayList<String> arrayList = new ArrayList<>(selectedImageList);

        return arrayList;
    }

    class ImageViewHolder extends ViewHolder{
        ImageView imageView;
        LinearLayout edgeBackground;
        View itemView;

        public ImageViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            imageView = itemView.findViewById(R.id.img);
            edgeBackground = itemView.findViewById(R.id.edge_background);
        }
    }
}
