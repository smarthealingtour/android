package jejusmarttour.main_mytourproduct;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_tourproduct.TourProductIntroduceActivity;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.MyTourCourseTask;
import jejusmarttour.vo.MyTourCourseRequestVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class MTCAddHTPActivity extends AppCompatActivity implements JSONArrayResult , AbsListView.OnScrollListener{
	private static final String TAG = MTCAddHTPActivity.class.toString ( );

	private ListView listView;
	private MTCAddHTPListViewAdapter listViewAdapter;
	private MyTourCourseTask searchProductTask;
	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;
	private ArrayList < TourProductVO > listData;

	private TourProductVO selectedVO;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_mtcadd_htp );

		listData = new ArrayList < TourProductVO > ( );
		lockListView = true;

		selectedVO = new TourProductVO ( );

		listViewAdapter = new MTCAddHTPListViewAdapter ( this , listData , mOnClickListener );
		listView = ( ListView ) findViewById ( R.id.mtc_listview );
		listView.setChoiceMode ( ListView.CHOICE_MODE_MULTIPLE );
		listView.setOnScrollListener ( this );
		listView.setAdapter ( listViewAdapter );
		listView.setOnItemClickListener ( onClickListItem );

		initActionBar ( );

		executeTask ( );
	}

	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_listview , null );
		TextView titleTextView = ( TextView ) customView.findViewById ( R.id.title_text );
		titleTextView.setText ( getResources ( ).getString ( R.string.title_activity_mtcget_htp ) );
		LinearLayout backBtn = ( LinearLayout ) customView.findViewById ( R.id.back_btn );

		backBtn.setOnClickListener ( new View.OnClickListener( ){
			@Override
			public void onClick ( View view )
			{
				finish ( );
			}
		} );

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	@Override
	public void setJSONArrayResult ( ArrayList resultList ){
		lockListView = true;
		searchProductTask.cancel ( true );

		if ( resultList == null ){
			return;
		}
		else{
			InsertItemToListView ( resultList );
		}

		lockListView = false;

	}

	private void startMTCGetItemInHTPScheduleActivity( ){
		Intent intent = new Intent ( this , MTCGetItemInHTPSchduleActivity.class );
		intent.putExtra ( "data" , selectedVO );
		startActivity ( intent );
		finish ( );
	}

	// 리스트뷰 아덥터 이벤트
	View.OnClickListener mOnClickListener = new View.OnClickListener ( ){
		@Override
		public void onClick ( View view ){
			if ( view.getId ( ) == R.id.toggle_select_course ){
				ToggleButton toggleButton = ( ToggleButton ) view;

				int currentPosition = Integer.valueOf ( toggleButton.getTag ( ).toString ( ) );
				Log.d ( TAG , "modify position=" + currentPosition );

				if ( toggleButton.isChecked ( ) == false ){
					listData.get ( currentPosition ).setIsChecked ( false );
					toggleButton.setChecked ( false );
					selectedVO = null;
					listViewAdapter.notifyDataSetChanged ( );
					return;
				}

				for ( int i = 0 ; i < listView.getChildCount ( ) ; i++ ){
					( ( ToggleButton ) ( ( LinearLayout ) ( ( LinearLayout ) listView.getChildAt ( i ) ).getChildAt ( 0 ) ).getChildAt ( 1 ) ).setChecked ( false );
					listData.get ( i ).setIsChecked ( false );
				}

				// toggleButton.setChecked ( true );
				// listData.get ( currentPosition ).setIsChecked ( true );
				selectedVO = listData.get ( currentPosition );

				listViewAdapter.notifyDataSetChanged ( );

				if ( selectedVO != null ){
					startMTCGetItemInHTPScheduleActivity( );
				}
				else{
					Toast.makeText ( getApplicationContext ( ) , "상품이 선택되지 않았습니다." , Toast.LENGTH_SHORT ).show ( );
				}
			}
		}

	};

	private void startHTPGetScheduleActivity(int position ){
		SmartTourProductsVO smartTourProductsVO = new SmartTourProductsVO ( );
		smartTourProductsVO.setTourProductVO ( listData.get ( position ) );
		CommonData.setSmartTourProductsVO ( smartTourProductsVO );
		Intent intent = new Intent ( this , TourProductIntroduceActivity.class );
		startActivity ( intent );
	}

	// 아이템 터치 이벤트
	private AdapterView.OnItemClickListener onClickListItem = new AdapterView.OnItemClickListener ( ){
		@Override
		public void onItemClick ( AdapterView < ? > parent , View view , int position , long id ){
			startHTPGetScheduleActivity( position );
		}
	};

	private void InsertItemToListView ( ArrayList < TourProductVO > resultList ){
		for ( int i = 0 ; i < resultList.size ( ) ; i++ )
		{
			// listData.add ( resultList.get ( i ) );
			listViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		listViewAdapter.notifyDataSetChanged ( );
	}

	private void executeTask ( ){
		MyTourCourseRequestVO vo = new MyTourCourseRequestVO ( );
		vo.setEmail ( CommonData.getUserVO ( ).getEmail ( ) );
		vo.setPage ( Integer.toString ( requestPageNumber ) );
		vo.setType ( "htour" );

		searchProductTask = new MyTourCourseTask ( this );
		searchProductTask.execute ( vo );
	}
	/*
	 * 
	 * // 액션바 메뉴
	 * 
	 * @Override public boolean onCreateOptionsMenu ( Menu menu ) {
	 * getMenuInflater ( ).inflate ( R.menu.save , menu ); return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected ( MenuItem item ) { int id
	 * = item.getItemId ( );
	 * 
	 * if ( id == R.id.action_save ) { if ( selectedVO != null ) { // Intent
	 * intent = getIntent ( ); // intent.putExtra ( "data" , selectedVO ); //
	 * setResult ( RESULT_OK , intent ); // finishActivity (
	 * ScheduleManagerFragment.REQUEST_CODE_SITE ); Intent intent = new Intent (
	 * this , MTCGetItemInHTPSchduleActivity.class ); intent.putExtra ( "data" ,
	 * selectedVO ); startActivity ( intent ); finish ( ); return true; } else {
	 * Toast.makeText ( this , "상품이 선택되지 않았습니다." , Toast.LENGTH_SHORT ).show (
	 * ); } }
	 * 
	 * return super.onOptionsItemSelected ( item ); }
	 */

	@Override
	public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount ){
		int lastInScreen = firstVisibleItem + visibleItemCount;

		if ( ( lastInScreen == totalItemCount ) && lockListView == false ){
			currentPageNumber += 1;
			requestPageNumber += 1;

			executeTask ( );

			lockListView = true;
		}
	}

	@Override
	public void onScrollStateChanged ( AbsListView view , int scrollState ){

	}

}
