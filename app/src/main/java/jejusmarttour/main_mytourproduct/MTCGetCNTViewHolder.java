package jejusmarttour.main_mytourproduct;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MTCGetCNTViewHolder
{
	CheckBox checkBox;
	ImageView imageView;
	ImageView watchImageView;
	TextView titleText;
	TextView timeText;
	TextView subTitleText;
	TextView groupText;
	TextView keywordTitle;
	Button deleteBtn;

	LinearLayout linearLayout;
	LinearLayout group_linearLayout;

}
