package jejusmarttour.main_mytourproduct;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_tourproduct.TourProductIntroduceActivity;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.task.MTCShareDeleteTask;
import jejusmarttour.task.MTCShareUpdateTask;
import jejusmarttour.task.MyTourCourseTask;
import jejusmarttour.vo.MyTourCourseRequestVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import jejusmarttour.vo.UpdateResultVO;
import syl.com.jejusmarttour.R;

public class MyTourProductActivity extends AppCompatActivity implements  JSONArrayResult < TourProductVO > , OnScrollListener , JSONObjectResult < UpdateResultVO >{
	private static final String TAG = MyTourProductActivity.class.toString ( );

	private MTCListViewAdapter listViewAdapter;
	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private ArrayList < TourProductVO > listData;
	private LinearLayout makeCourseBtn;

	private MTCShareDeleteTask mtcShareDeleteTask;

	public static final int REQUEST_CODE = 0;
	private static final int MAKE_NEW_COURSE = 999999999;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_my_tour_course );

		initListView ( );
		initMakeCourseBtn();
		initActionBar ( );

		executeTask ( );
	}

	private void initListView(){
		listData = new ArrayList <> ( );
		lockListView = true;

		listViewAdapter = new MTCListViewAdapter ( this , listData , mOnClickListener );
		Log.e(TAG, "listData.size()1: " + listData.size() );
		ListView listView = (ListView) findViewById(R.id.my_tour_course_listview);

		final Intent intent = new Intent ( this , TourProductIntroduceActivity.class );

		listView.setOnItemClickListener ((parent, view, position, id) -> {
            SmartTourProductsVO smartTourProductsVO = new SmartTourProductsVO ( );
            smartTourProductsVO.setTourProductVO ( listData.get ( position ) );
            CommonData.setSmartTourProductsVO ( smartTourProductsVO );
            startActivity ( intent );
        });

		listView.setOnScrollListener ( this );
		listView.setAdapter ( listViewAdapter );

		// ListView 아이템 터치 시 이벤트 추가
		// listView.setOnItemClickListener ( onClickListItem );
	}

	private void initMakeCourseBtn(){
		makeCourseBtn = ( LinearLayout ) findViewById ( R.id.make_course_btn );
		makeCourseBtn.setOnClickListener(v -> {
            if ( v.getId ( ) == makeCourseBtn.getId ( ) ){
                gotoMakeCourseActivity( MAKE_NEW_COURSE );
            }
        });
	}

	// 나의 코스 만들기 로 이동
	private void gotoMakeCourseActivity(int position ){
		Intent intent = new Intent ( this , MakeMyProductActivity.class );
		TourProductVO tourProductVO;

		if ( position == MAKE_NEW_COURSE ){
			tourProductVO = new TourProductVO ( );
			intent.putExtra ( "isModify" , false );
		}
		else{
			tourProductVO = listData.get ( position );
			intent.putExtra ( "isModify" , true );
		}

		intent.putExtra ( "data" , tourProductVO );
		startActivityForResult ( intent , MakeMyProductActivity.REQUEST_CODE_SCHEDULE );
	}

	// 리스트뷰 어댑터 이벤트
	private OnClickListener mOnClickListener = new OnClickListener ( ){
		@Override
		public void onClick ( View v ){
			int position;

			switch ( v.getId ( ) ){
				case R.id.mtc_modify_course_btn : // 코스수정
					Log.d ( TAG , "modify position=" + v.getTag ( ).toString ( ) );
					position = Integer.valueOf ( v.getTag ( ).toString ( ) );
					gotoMakeCourseActivity( position );
					break;
				case R.id.mtc_share_course_btn : // 코스 공개
					Log.d ( TAG , "share position=" + v.getTag ( ).toString ( ) );
					position = Integer.valueOf ( v.getTag ( ).toString ( ) );

					if(listData.get(position).getHt_imgArray() != null && !listData.get(position).getDays().equals("0"))
						gotoShareCourse ( position );
					else
						Toast.makeText(MyTourProductActivity.this, "여행상품이 완성되지 않았습니다.", Toast.LENGTH_SHORT).show();
					break;
				case R.id.mtc_share_course_delete_btn : // 코스 비공개
					Log.d ( TAG , "share position=" + v.getTag ( ).toString ( ) );
					position = Integer.valueOf ( v.getTag ( ).toString ( ) );
					shareCourseDelete ( position );
					break;
				case R.id.mtc_delete_course_btn : // 코스 삭제
					Log.d ( TAG , "delete position=" + v.getTag ( ).toString ( ) );
					position = Integer.valueOf ( v.getTag ( ).toString ( ) );
					courseDelete ( position );
					break;
			}
		}
	};

	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_listview , null );
		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( getResources ( ).getString ( R.string.title_activity_my_tour_course ) );
		LinearLayout backBtn = customView.findViewById ( R.id.back_btn );

		backBtn.setOnClickListener (view -> finish ( ));

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	private void executeTask ( ){
		MyTourCourseRequestVO vo = new MyTourCourseRequestVO ( );
		vo.setEmail ( CommonData.getUserVO ( ).getEmail ( ) );
		vo.setPage ( Integer.toString ( requestPageNumber ) );
		vo.setType ( "htour" );

		MyTourCourseTask searchProductTask = new MyTourCourseTask(this);
		searchProductTask.execute ( vo );
	}

	// 코스 삭제
	private void courseDelete ( final int position ){
		mtcShareDeleteTask = new MTCShareDeleteTask ( this );

		final AlertDialog.Builder builder = new AlertDialog.Builder ( this );
		builder.setMessage ( "정말로 삭제하시겠습니까?" );
		builder.setPositiveButton ( "확인" , (dialog, which) ->
				mtcShareDeleteTask.execute (
						CommonData.getUserVO ( ).getEmail ( ) , listData.get ( position ).getHt_id ( ) ));

		builder.setNegativeButton ( "취소" , (dialog, which) -> dialog.dismiss ( ));
		builder.show ( );
	}

	//공유삭제
	private void shareCourseDelete ( int position ){
		MTCShareUpdateTask mtcShareUpdateTask = new MTCShareUpdateTask(this);
		mtcShareUpdateTask.execute ( CommonData.getUserVO ( ).getEmail ( ) , listData.get ( position ).getHt_id ( ) );
	}

	// 코스 공유
	private void gotoShareCourse ( int position ){
		Intent intent = new Intent ( this , ShareCourseActivity.class );
		intent.putExtra ( "data" , listData.get ( position ) );

		startActivityForResult ( intent , REQUEST_CODE );
	}

	// 공유페이지 종료 후 리스트뷰 리프레쉬
	@Override
	protected void onActivityResult ( int requestCode , int resultCode , Intent intent ){
		super.onActivityResult ( requestCode , resultCode , intent );

/*		if ( requestCode == MakeMyProductActivity.REQUEST_CODE_SCHEDULE )
		{
			initData ( );
			executeTask ( );
		}*/
		initData ( );
		executeTask ( );
	}

	private void InsertItemToListView ( ArrayList < TourProductVO > resultList ){
		for ( int i = 0 ; i < resultList.size ( ) ; i++ ){
			listData.add ( resultList.get ( i ) );
			listViewAdapter.add ( resultList.get ( i ) );

		}
		Log.e(TAG, "listData.size(): " + listData.size() );
		// 리스트뷰 리로딩
		listViewAdapter.notifyDataSetChanged ( );
	}

	@Override
	public void setJSONArrayResult ( ArrayList<TourProductVO> resultList ){
		lockListView = true;

		if ( resultList == null ){
			return;
		}
		else{
			InsertItemToListView ( resultList );
		}

		lockListView = false;
	}



	@Override
	public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount ){
		int lastInScreen = firstVisibleItem + visibleItemCount;

		if ( ( lastInScreen == totalItemCount ) && !lockListView){
			currentPageNumber += 1;
			requestPageNumber += 1;

			executeTask ( );

			lockListView = true;
		}
	}

	@Override
	public void onScrollStateChanged ( AbsListView view , int scrollState ){

	}

	@Override
	public void setJSONObjectResult ( UpdateResultVO result ){
		if ( result == null ){
			Log.d ( TAG , "정상적으로 실행되지 않았습니다." );
			return;
		}

		if ( result.isMTCShare() ){
			if ( result.getHt_status ( ).equals ( "false" ) ){
				Toast.makeText ( this , "비공개되었습니다." , Toast.LENGTH_SHORT ).show ( );
				initData ( );
				executeTask ( );
			}
		}
		else if (!result.isMTCShare() && result.getResult ( ).equals ( "success" ) ){
			Toast.makeText ( this , "삭제되었습니다." , Toast.LENGTH_SHORT ).show ( );
			initData ( );
			executeTask ( );
		}
	}

	private void initData ( ){
		currentPageNumber = 0;
		requestPageNumber = 1;

		lockListView = true;
		listData.clear();
		listViewAdapter.removeAll ( );
		listViewAdapter.notifyDataSetChanged ( );
		// listData = new ArrayList < TourProductVO > ( );
	}
}
