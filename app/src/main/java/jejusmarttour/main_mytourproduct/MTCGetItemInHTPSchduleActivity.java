package jejusmarttour.main_mytourproduct;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.osy.myfragment.ScheduleManegeFragment;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.parser.ScheduleParser;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.ScheduleTask;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class MTCGetItemInHTPSchduleActivity extends AppCompatActivity implements JSONArrayResult < ScheduleVO >
{
	private static final String TAG = MTCGetItemInHTPSchduleActivity.class.toString ( );

	private TourProductVO selectedVO;
	private ScheduleTask scheduleTask;
	private SmartTourProductsVO smartTourProductsVO;
	private ArrayList < SmartTourProductsVO > smartTourProductsVOs;
	private ArrayList < ScheduleVO > scheduleVOArrayList;

	private LinearLayout allSchduleContainer;

	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_mtcget_item_in_htpschdule );

		Intent intent = getIntent ( );
		selectedVO = ( TourProductVO ) intent.getSerializableExtra ( "data" );
		smartTourProductsVOs = new ArrayList < SmartTourProductsVO > ( );
		scheduleVOArrayList = new ArrayList < ScheduleVO > ( );

		executeTask ( );
	}

	private void initActionBar ( )
	{
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_listview , null );
		TextView titleTextView = ( TextView ) customView.findViewById ( R.id.title_text );
		titleTextView.setText ( selectedVO.getHt_name ( ) );
		LinearLayout backBtn = (LinearLayout) customView.findViewById ( R.id.back_btn );

		backBtn.setOnClickListener ( new View.OnClickListener( )
		{
			@Override
			public void onClick ( View view )
			{
				finish ( );
			}
		} );

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	private void executeTask ( )
	{
		scheduleTask = new ScheduleTask ( this );
		scheduleTask.execute ( selectedVO.getHt_id ( ) );
	}

	@Override
	public void setJSONArrayResult ( ArrayList < ScheduleVO > resultList )
	{
		scheduleTask.cancel ( true );

		if ( resultList == null )
		{
			return;
		}
		smartTourProductsVO = new SmartTourProductsVO ( );
		smartTourProductsVO.setTourProductVO ( selectedVO );
		smartTourProductsVO.setScheduleArrayList ( resultList );

		int length = Integer.valueOf ( smartTourProductsVO.getScheduleArrayList ( ).get ( 0 ).getDays ( ) );

		for ( int i = 1; i <= length; i++ )
		{
			SmartTourProductsVO productsVO = new SmartTourProductsVO ( );
			productsVO.setScheduleArrayList ( ScheduleParser.getSchedule ( smartTourProductsVO.getScheduleArrayList ( ) , i ) );
			smartTourProductsVOs.add ( productsVO );
		}

		initView ( );
		initActionBar ( );
	}

	private void initView ( )
	{
		if ( smartTourProductsVOs == null )
			return;

		allSchduleContainer = ( LinearLayout ) findViewById ( R.id.all_schdule_container );

		for ( int i = 0; i < smartTourProductsVOs.size ( ); i++ )
		{
			final LinearLayout daysContainer = ( LinearLayout ) View.inflate ( this , R.layout.layout_get_htp_schedule , null );
			daysContainer.setTag ( i );
			CheckBox checkBox = ( CheckBox ) daysContainer.findViewById ( R.id.day_check_box );
			checkBox.setText ( "  " + ( i + 1 ) + "일" );
			final LinearLayout contentsContainer = ( LinearLayout ) daysContainer.findViewById ( R.id.schedule_container );

			checkBox.setOnClickListener ( new View.OnClickListener ( )
			{
				@Override
				public void onClick ( View v )
				{
					CheckBox checkBox1 = ( CheckBox ) v;

					validateDaysContents ( Integer.valueOf ( daysContainer.getTag ( ).toString ( ) ) , checkBox1.isChecked ( ) );

					int position = Integer.valueOf ( daysContainer.getTag ( ).toString ( ) );
					int count = contentsContainer.getChildCount ( );

					for ( int q = 0; q < count; q++ )
					{
						CheckBox checkBox2 = ( ( CheckBox ) ( ( LinearLayout ) contentsContainer.getChildAt ( q ) ).getChildAt ( 0 ) );
						checkBox2.setChecked ( checkBox1.isChecked ( ) );
					}

				}
			} );

			for ( int k = 0; k < smartTourProductsVOs.get ( i ).getScheduleArrayList ( ).size ( ); k++ )
			{
				LinearLayout contentLayout = ( LinearLayout ) View.inflate ( this , R.layout.layout_get_schdule_item , null );
				contentLayout.setTag ( k );

				final ScheduleVO scheduleVO = smartTourProductsVOs.get ( i ).getScheduleArrayList ( ).get ( k );

				contentLayout.setOnClickListener ( new View.OnClickListener ( )
				{
					@Override
					public void onClick ( View v )
					{
						startDelinInfoActivity ( scheduleVO );
					}
				} );

				CheckBox checkBox1 = ( CheckBox ) contentLayout.findViewById ( R.id.checkbox );
				checkBox1.setOnClickListener ( new View.OnClickListener ( )
				{
					@Override
					public void onClick ( View v )
					{
						CheckBox checkBox2 = ( CheckBox ) v;

						if ( checkBox2.isChecked ( ) == true )
						{
							scheduleVOArrayList.add ( scheduleVO );
						}
						else
						{
							for ( int j = 0; j < scheduleVOArrayList.size ( ); j++ )
							{
								if ( scheduleVOArrayList.get ( j ) == scheduleVO )
								{
									scheduleVOArrayList.remove ( scheduleVO );
								}
							}
						}

						logTest ( );
					}
				} );
				ImageView iconImage = ( ImageView ) contentLayout.findViewById ( R.id.search_item_icon );
				TextView titleText = ( TextView ) contentLayout.findViewById ( R.id.mtc_title_text );
				TextView keywordText = ( TextView ) contentLayout.findViewById ( R.id.mtc_keyword_text );
				TextView addressText = ( TextView ) contentLayout.findViewById ( R.id.mtc_address_text );
				TextView telText = ( TextView ) contentLayout.findViewById ( R.id.mtc_tel_text );
				TextView timeText = ( TextView ) contentLayout.findViewById ( R.id.mtc_time_text );

				int iconId = 0;

				if ( scheduleVO.getHealingContentsVO( ) != null )
				{
					iconId = CommonData.getLocationIconId ( scheduleVO.getHealingContentsVO( ).getHcnt_type ( ) );

					titleText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_name ( ) == null ? "제목없음" : scheduleVO.getHealingContentsVO( ).getHcnt_name ( ) );
					keywordText.setText ( scheduleVO == null ? "키워드없음" : getKeyword ( scheduleVO ) );
					addressText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_addr ( ) == null ? "주소없음" : scheduleVO.getHealingContentsVO( ).getHcnt_addr ( ) );
					telText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_tel ( ) == null ? "전화번호없음" : scheduleVO.getHealingContentsVO( ).getHcnt_tel ( ) );
					timeText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_duration ( ) == null ? "- 분" : SmartTourUtils.getDuration ( scheduleVO.getHealingContentsVO( ).getHcnt_duration ( ) ) );
				}
				else
				{
					iconId = CommonData.getLocationIconId ( "point" );

					titleText.setText ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( ) == null ? "제목없음" : scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( ) );
					keywordText.setText ( scheduleVO == null ? "키워드없음" : getKeyword ( scheduleVO ) );
					addressText.setText ( "주소없음" );
					telText.setText ( "전화번호없음" );
					timeText.setText ( "- 분" );
				}
				Glide.with ( this ).load ( iconId ).into ( iconImage );

				contentsContainer.addView ( contentLayout );
			}

			allSchduleContainer.addView ( daysContainer );
		}

	}

	private String getKeyword ( ScheduleVO vo )
	{
		String str = null;

		if ( vo.getHealingContentsVO( ) != null )
		{
			if ( vo.getHealingContentsVO( ).getHcnt_hcd_name ( ) != null )
			{
				if ( vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).size ( ) > 0 )
					str = vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).get ( 0 ).getCode_name ( );
			}
			else if ( vo.getHealingContentsVO( ).getHcnt_scd_name ( ) != null )
			{
				if ( vo.getHealingContentsVO( ).getHcnt_scd_name ( ).size ( ) > 0 )
					str = vo.getHealingContentsVO( ).getHcnt_scd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getHealingContentsVO( ).getHcnt_scd_name ( ).get ( 0 ).getCode_name ( );
			}
		}
		else if ( vo.getSightSeeingContentsVO( ) != null )
		{
			if ( vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ) != null )
			{
				if ( vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).size ( ) > 0 )
					str = vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).get ( 0 ).getCode_name ( );
			}
		}

		return str;
	}

	private void validateDaysContents ( int day , boolean isAdd )
	{
		ArrayList < ScheduleVO > scheduleVOs = smartTourProductsVOs.get ( day ).getScheduleArrayList ( );
		ArrayList < ScheduleVO > removeVOs = new ArrayList < ScheduleVO > ( );

		for ( int i = 0; i < scheduleVOArrayList.size ( ); i++ )
		{
			for ( int k = 0; k < scheduleVOs.size ( ); k++ )
			{
				if ( scheduleVOArrayList.get ( i ) == scheduleVOs.get ( k ) )
				{
					removeVOs.add ( scheduleVOs.get ( k ) );
				}
			}
		}

		for ( int i = 0; i < removeVOs.size ( ); i++ )
		{
			for ( int k = 0; k < scheduleVOArrayList.size ( ); k++ )
			{
				if ( scheduleVOArrayList.get ( k ) == removeVOs.get ( i ) )
				{
					scheduleVOArrayList.remove ( removeVOs.get ( i ) );
				}
			}
		}

		if ( isAdd == true )
		{
			for ( int k = 0; k < scheduleVOs.size ( ); k++ )
			{
				scheduleVOArrayList.add ( scheduleVOs.get ( k ) );
			}
		}

		logTest ( );
	}

	private void logTest ( )
	{
		for ( int p = 0; p < scheduleVOArrayList.size ( ); p++ )
		{
			if ( scheduleVOArrayList.get ( p ).getHealingContentsVO( ) != null )
			{
				Log.d ( TAG , "scheduleVOArrayList " + p + "  : " + scheduleVOArrayList.get ( p ).getHealingContentsVO( ).getHcnt_name ( ) );
			}
			else
			{
				Log.d ( TAG , "scheduleVOArrayList " + p + "  : " + scheduleVOArrayList.get ( p ).getSightSeeingContentsVO( ).getCcnt_name ( ) );
			}
		}
	}

	private void startDelinInfoActivity ( ScheduleVO vo )
	{
		CommonData.setScheduleVO ( vo );
		Intent intent = new Intent ( this , DetailInfoActivity.class );
		startActivity ( intent );
	}

	// 액션바 메뉴
	@Override
	public boolean onCreateOptionsMenu ( Menu menu )
	{
		getMenuInflater ( ).inflate ( R.menu.save , menu );
		return true;
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item )
	{
		int id = item.getItemId ( );

		if ( id == R.id.action_save )
		{
			if ( scheduleVOArrayList != null )
			{
				( ( CommonData ) getApplication ( ) ).setMakeScheduleArrayList(scheduleVOArrayList);
				Intent intent = getIntent ( );
				intent.putExtra("data", "value");
				setResult ( ScheduleManegeFragment.REQUEST_CODE_SELECT_COURSE , intent );

				// 요거는 저 request코드로 실행된 액티비티들을 종료시키는 것.
				finishActivity ( ScheduleManegeFragment.REQUEST_CODE_SELECT_COURSE );
				finish ( );
				return true;
			}
			else
			{
				//여기 버그인듯 선택안됬다고 안뜸
				Toast.makeText ( this , "상품이 선택되지 않았습니다." , Toast.LENGTH_SHORT ).show ( );
			}
		}

		return super.onOptionsItemSelected ( item );
	}

}
