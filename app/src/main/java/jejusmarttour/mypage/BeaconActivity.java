package jejusmarttour.mypage;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.tamra.Tamra;
import com.kakao.oreum.tamra.base.Config;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.error.TamraError;
import com.kakao.oreum.tamra.error.TamraErrorFilters;
import com.kakao.oreum.tamra.error.TamraInitException;
import jejusmarttour.adapter.MainCategoryPagerAdapter;
import jejusmarttour.util.Constants;
import syl.com.jejusmarttour.R;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconActivity extends AppCompatActivity {

    private static final String TAG = BeaconActivity.class.toString( );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView(); // initialized your own view.
        initTamra();
    }

    private void initTamra() {
        Config config = Config.forTesting(
                getApplicationContext(),
                Constants.appKey).onSimulation("My Device");
        Tamra.init(config);

        // if you want to monitor all regions.
        for (Region region : Regions.all()) {
            Tamra.startMonitoring(region);
        }

        // check errors
        Tamra.recentErrors()
                .filter(TamraErrorFilters.causedBy(TamraInitException.class))
                .foreach(tamraError -> Log.d(TAG, tamraError.toString()));

        Tamra.recentErrors()
                .foreach(new Consumer<TamraError>() {
                    @Override
                    public void accept(TamraError tamraError) {
                        Log.d(TAG, tamraError.toString());
                    }
                });

    }

    private void initView(){
        setContentView(R.layout.beacon_main);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        assert(viewPager != null);
        viewPager.setAdapter(new MainCategoryPagerAdapter(getSupportFragmentManager()));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        assert(tabLayout != null);
        tabLayout.setupWithViewPager(viewPager);

    }
}
