package jejusmarttour.mypage;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.kakao.kakaotalk.KakaoTalkService;
import com.kakao.kakaotalk.callback.TalkResponseCallback;
import com.kakao.kakaotalk.response.KakaoTalkProfile;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;

import java.io.File;

import jejusmarttour.KakaoLoginActivity;
import jejusmarttour.NickNameActivity;
import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.MyTourProductActivity;
import jejusmarttour.main_mytourproduct.osy.interest_activity.InterestActivity;
import jejusmarttour.map.yoMapActivity;
import jejusmarttour.util.CommonUtil;
import jejusmarttour.util.ImageLoader;
import syl.com.jejusmarttour.R;

public class MyPageActivity extends AppCompatActivity implements OnClickListener{
	private ImageView kakao_user_photo;
	private static Uri mImageCaptureUri;
	private ImageLoader iLoader;

	private static final String TAG = MyPageActivity.class.toString ( );
	public static final int PICK_FROM_CAMERA = 0x1;
	public static final int PICK_FROM_ALBUM = 0x2;
	public static final int CROP_FROM_CAMERA = 0x3;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView(R.layout.activity_my_page);

		getWindow().getDecorView().setBackgroundResource(R.color.white);
		//profile 셋팅
		String imgResource = "userimg_icon";
		iLoader = new ImageLoader(this, getResources().getIdentifier(imgResource, "drawable", getPackageName()));

		kakao_user_photo = (ImageView) findViewById(R.id.kakao_user_photo);
		((TextView) findViewById(R.id.kakao_user_nm)).setText(CommonData.getUserVO().getNickname());
		((TextView) findViewById(R.id.kakao_email_txt)).setText(CommonData.getUserVO().getEmail());

		String photoStr = CommonData.getUserProfileImage(this);
		if(photoStr.equals("")){
			KakaoTalkService.requestProfile(new TalkResponseCallback<KakaoTalkProfile>() {
				@Override
				public void onNotKakaoTalkUser() {
					// TODO Auto-generated method stub
				}

				@Override
				public void onSessionClosed (ErrorResult errorResult) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onNotSignedUp () {
					// TODO Auto-generated method stub
				}

				@Override
				public void onSuccess (KakaoTalkProfile result) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onSuccessForUiThread(KakaoTalkProfile resultObj) {
					// TODO Auto-generated method stub
					/*Log.d(TAG, "UserProfile : " + resultObj.getThumbnailURL());
					CommonData.setUserProfileImage(MyPageActivity.this, resultObj.getThumbnailURL());
					iLoader.DisplayCornerImage(resultObj.getThumbnailURL(), kakao_user_photo, 1, 1, 0);*/
				}

			});
		}
		else{
			iLoader.DisplayCornerImage(photoStr, kakao_user_photo, 1, 1, 0);
		}

		kakao_user_photo.setOnClickListener(this);
		findViewById(R.id.kakao_user_nm).setOnClickListener(this);
		findViewById(R.id.kakao_logut_btn).setOnClickListener(this);
		findViewById(R.id.map_view_btn).setOnClickListener(this);
		findViewById(R.id.my_place_btn).setOnClickListener(this);
		findViewById(R.id.my_course_btn).setOnClickListener(this);
		findViewById(R.id.use_info_btn).setOnClickListener(this);
		findViewById(R.id.notice_btn).setOnClickListener(this);
		findViewById(R.id.beacon_btn).setOnClickListener(this);

		initActionBar();

	}


	// 커스텀 액션바 적용
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);

		// 액션바 그림자 지우기
		actionBar.setElevation(0);

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_mypage , null );

		actionBar.setCustomView(customView);
		actionBar.setDisplayShowCustomEnabled(true);
	}

	// 로그아웃
	private void onClickLogout ( ){
		UserManagement.requestLogout(new LogoutResponseCallback() {

			@Override
			public void onCompleteLogout(){
				redirectLoginActivity();
			}

			private void redirectLoginActivity(){
				Intent it = new Intent(MyPageActivity.this, KakaoLoginActivity.class);
				startActivity(it);
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu ( Menu menu ){
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater ( ).inflate(R.menu.close, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item ){
		int id = item.getItemId ( );
		if ( id == R.id.action_close ){
			finish ( );
			return true;
		}
		return super.onOptionsItemSelected ( item );
	}

	@Override
	public void onClick(View v) {
		Intent it = null;
		switch(v.getId()){
			case R.id.kakao_user_photo://사진 클릭시
				mImageCaptureUri = CommonUtil.createSaveCropFile();
				AlertDialog.Builder imgDiralg = new AlertDialog.Builder(this);
				imgDiralg.setMessage("사진을 선택해 주세요")
						.setPositiveButton("카메라", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();

								Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
								// Crop된 이미지를 저장할 파일의 경로를 생성
								mImageCaptureUri = CommonUtil.createSaveCropFile();
								intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
								startActivityForResult(intent, PICK_FROM_CAMERA);
							}
						})
						.setNegativeButton("갤러리", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();

								Intent intent = new Intent(Intent.ACTION_PICK);
								intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
								startActivityForResult(intent, PICK_FROM_ALBUM);
							}
						})
						.show();
				break;

			case R.id.kakao_user_nm:		//닉네임 클릭시
				it = new Intent(this, NickNameActivity.class);
				it.putExtra("userId", CommonData.getUserVO ( ).getEmail());
				it.putExtra("mode", "modi");
				break;

			case R.id.kakao_logut_btn:		//로그아웃
				AlertDialog.Builder alert_confirm = new AlertDialog.Builder(this);
				alert_confirm.setMessage("로그아웃 하시겠습니까?").setCancelable(false).setPositiveButton("확인",
						(dialog, which) -> {
                            onClickLogout();
                            // 'YES'
                        }).setNegativeButton("취소",
						(dialog, which) -> {
                            // 'No'
                            return;
                        });
				AlertDialog alert = alert_confirm.create();
				alert.show();
				break;

			case R.id.map_view_btn: //지도보기
				it = new Intent(this, yoMapActivity.class);
				break;

			case R.id.my_place_btn://나의 관심장소
				it = new Intent(this, InterestActivity.class);
				break;

			case R.id.my_course_btn://나의 여행 코스
				it = new Intent(this, MyTourProductActivity.class);
				it.putExtra("MODIFY", true);
				break;

			case R.id.use_info_btn://이용안내
				//Toast.makeText(getApplicationContext(), "작업중입니다.", Toast.LENGTH_SHORT).show();
				it = new Intent(this, InfoActivity.class);
				it.putExtra("TYPE", 1);
				break;

			case R.id.notice_btn://공지사항
				//Toast.makeText(getApplicationContext(), "작업중입니다.", Toast.LENGTH_SHORT).show();
				it = new Intent(this, InfoActivity.class);
				it.putExtra("TYPE", 2);
				break;

			case R.id.beacon_btn://비콘알림
				it = new Intent(this, BeaconActivity.class);
				break;
		}

		if(it != null){
			startActivity(it);
		}
	}

	public void setUserPhoto(Uri mImageCaptureUri){
		String full_path = mImageCaptureUri.getPath();
		CommonData.setUserProfileImage(this, full_path);
		iLoader.DisplayCornerImage(full_path, kakao_user_photo, 1, 1, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			switch(requestCode){
				case PICK_FROM_ALBUM:
					mImageCaptureUri = data.getData();
					File original_file = CommonUtil.getImageFile(this, mImageCaptureUri);

					mImageCaptureUri = CommonUtil.createSaveCropFile();
					File cpoy_file = new File(mImageCaptureUri.getPath());
					CommonUtil.copyFile(original_file, cpoy_file);

				case PICK_FROM_CAMERA:
					Intent intent = new Intent("com.android.camera.action.CROP");
					intent.setDataAndType(mImageCaptureUri, "image/*");
					// Crop한 이미지를 저장할 Path
					intent.putExtra("output", mImageCaptureUri);
					startActivityForResult(intent, CROP_FROM_CAMERA);
					break;

				case CROP_FROM_CAMERA:
					setUserPhoto(mImageCaptureUri);
					break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
