package jejusmarttour.mypage;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import syl.com.jejusmarttour.R;

/**
 * Created by A on 2015-10-25.
 */
public class InfoListAdapter extends ArrayAdapter {

    Context mContext;
    ArrayList cData;
    int textResource;

    LayoutInflater inflater;
    InfoHolder holder;

    public InfoListAdapter(Context context, int resource, ArrayList objects) {
        super(context, resource, objects);

        mContext = context;
        cData = objects;
        textResource = resource;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            holder = new InfoHolder();
            view = inflater.inflate(textResource, parent, false);

            holder.infoTitle = (TextView) view.findViewById(R.id.infoTitle);
            holder.infoContent = (TextView) view.findViewById(R.id.infoContent);
            holder.infoDate = (TextView) view.findViewById(R.id.infoDate);
            holder.icon = (ImageView) view.findViewById(R.id.icon);
            holder.contentLayout = (LinearLayout) view.findViewById(R.id.contentLayout);
            holder.contentLayout.setVisibility(View.GONE);
            holder.info_item = (LinearLayout) view.findViewById(R.id.info_item);
            holder.info_item.setFocusable(false);
            holder.info_item.setClickable(true);
            holder.info_item.setOnClickListener(itemClick);

            view.setTag(holder);
        }else
            holder = (InfoHolder) view.getTag();

        holder.info_item.setTag(holder);

        Object content = null;

        if(cData != null && cData.size() > position)
            content = cData.get(position);

        if(content != null){
            //TODO
            if(((Map<String, String>)cData.get(position)).containsKey("notify_id")){
                Log.i("qtam", ((Map<String, String>)cData.get(position)).get("notify_title"));

                holder.infoTitle.setText(((Map<String, String>)cData.get(position)).get("notify_title"));
                holder.infoContent.setText(((Map<String, String>)cData.get(position)).get("notify_subt"));
                holder.infoDate.setText(((Map<String, String>)cData.get(position)).get("notify_date"));
                //holder.infoTitle.setText(((Map<String, String>)cData.get(position)).get("notify_title"));
            }
            else {
                holder.infoTitle.setText(((Map<String, String>)cData.get(position)).get("board_title"));
                holder.infoContent.setText(((Map<String, String>)cData.get(position)).get("board_subt"));
                holder.infoDate.setText(((Map<String, String>)cData.get(position)).get("board_date"));
            }
        }

        return view;
    }

    class InfoHolder{
        TextView infoTitle, infoContent, infoDate;
        ImageView icon;
        LinearLayout contentLayout, info_item;
    }

    View.OnClickListener itemClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            InfoHolder holder = (InfoHolder)view.getTag();
            if(holder.contentLayout.getVisibility() == View.VISIBLE){
                holder.contentLayout.setVisibility(View.GONE);
                holder.icon.setImageResource(R.drawable.btn_down);
            }else {
                holder.contentLayout.setVisibility(View.VISIBLE);
                holder.icon.setImageResource(R.drawable.btn_up);
            }
        }
    };
}
