package jejusmarttour.mypage;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jejusmarttour.task.InfoNoticeTask;
import jejusmarttour.task.JSONArrayResult;
import syl.com.jejusmarttour.R;

/**
 * Created by A on 2015-10-25.
 */
public class InfoActivity extends AppCompatActivity implements JSONArrayResult , AbsListView.OnScrollListener
{
    private static final String TAG = InfoActivity.class.toString ( );

    private ListView list_view;
    private InfoListAdapter adapter;

    private ArrayList listData;

    int type; //1:이용안내, 2:공지사항


    private boolean lockListView;
    private int currentPageNumber = 0;
    private int requestPageNumber = 1;



    @Override
    protected void onCreate ( Bundle savedInstanceState )
    {
        super.onCreate ( savedInstanceState );
        setContentView(R.layout.activity_info);

        type = getIntent().getIntExtra("TYPE", 1);

        listData = new ArrayList();

        list_view = (ListView) findViewById(R.id.list_view);
        adapter = new InfoListAdapter(this, R.layout.info_item, listData);
        list_view.setAdapter(adapter);

        initActionBar();
        executeTask();
    }


    // 커스텀 액션바 적용
    private void initActionBar ( )
    {
        ActionBar actionBar = getSupportActionBar ( );
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);

        // 액션바 그림자 지우기
        actionBar.setElevation(0);

        LayoutInflater mInflater = LayoutInflater.from ( this );

        View customView = mInflater.inflate(R.layout.actionbar_listview, null);
        TextView titleTextView = ( TextView ) customView.findViewById ( R.id.title_text );
        if(type == 1){
            titleTextView.setText("이용안내");
        }else{
            titleTextView.setText("공지사항");
        }

        LinearLayout backBtn = (LinearLayout) customView.findViewById ( R.id.back_btn );

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        actionBar.setCustomView(customView);
        actionBar.setDisplayShowCustomEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu ( Menu menu )
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater ( ).inflate(R.menu.close, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected ( MenuItem item )
    {
        int id = item.getItemId();
        if ( id == R.id.action_close )
        {
            finish ( );
            return true;
        }
        return super.onOptionsItemSelected ( item );
    }


    //=====================================================
    //data 처리
    //========================================================

    private void executeTask ( )
    {
        //TODO 이용안내 공지사항 통신 추가
        InfoNoticeTask task = new InfoNoticeTask(this);
        Map<String, String> param = new HashMap<String, String>();



        if(type == 1){  //이용안내
            param.put("url", "notify");
            param.put("notify_id", "NTF_0001");
        }else{  //공지사항
            param.put("url", "board");
            param.put("board_id", "BRD_0001");
        }

        param.put("page", requestPageNumber + "");

        task.execute(param);
    }

    @Override
    public void setJSONArrayResult ( ArrayList resultList )
    {
        lockListView = true;

        if ( resultList == null )
        {
            return;
        }
        else
        {
            InsertItemToListView ( resultList );
        }

        lockListView = false;
    }



    public Integer getCurrentPage ( )
    {
        return currentPageNumber;
    }

    // 아이템 터치 이벤트
    private AdapterView.OnItemClickListener onClickListItem = new AdapterView.OnItemClickListener( )
    {

        @Override
        public void onItemClick ( AdapterView < ? > arg0 , View arg1 , int position , long arg3 )
        {
        //    ( (InfoActivity) getActivity ( ) ).onProItemClick ( (ScheduleVO) adapter.getItem ( position ) );
        }
    };

    private void InsertItemToListView ( ArrayList <Map<String, Object> > resultList )
    {
        for ( int i = 0; i < resultList.size ( ); i++ )
        {
            System.out.println(resultList.get ( i ) );
            adapter.add ( resultList.get ( i ) );
        }
        // 리스트뷰 리로딩
        adapter.notifyDataSetChanged();
    }



    @Override
    public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount )
    {
        int lastInScreen = firstVisibleItem + visibleItemCount;

        if ( ( lastInScreen == totalItemCount ) && lockListView == false )
        {
            currentPageNumber += 1;
            requestPageNumber += 1;

            executeTask ( );

            lockListView = true;
        }
    }


    @Override
    public void onScrollStateChanged ( AbsListView view , int scrollState )
    {

    }
}