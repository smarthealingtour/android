package jejusmarttour;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.RelativeLayout;

import syl.com.jejusmarttour.R;

public class  IntroActivity extends Activity
{
	private static final String TAG = IntroActivity.class.toString ( );
	
	RelativeLayout layout;
	Handler handler;
	Animation fade;

	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_intro );

		layout = ( RelativeLayout ) findViewById ( R.id.intro );

		/*
		 * fade = AnimationUtils.loadAnimation ( this , android.R.anim.fade_in
		 * ); layout.startAnimation ( fade );
		 */
		// 1500 이후 핸들러 실행
		handler = new Handler ( );
		handler.postDelayed ( introRun , 1500 );
	}

	Runnable introRun = new Runnable ( )
	{
		@Override
		public void run ( )
		{
			startLoginActivity ( );
		}
	};

	// 페이드 아웃 애니메이션
	private void startLoginActivity ( )
	{
		final Intent intent = new Intent ( IntroActivity.this , KakaoLoginActivity.class );

		fade = AnimationUtils.loadAnimation ( this , android.R.anim.fade_out );

		fade.setAnimationListener ( new AnimationListener ( ){

			@Override
			public void onAnimationStart ( Animation animation ){

			}

			@Override
			public void onAnimationRepeat ( Animation animation ){

			}

			@Override
			public void onAnimationEnd ( Animation animation ){
				layout.setVisibility ( layout.GONE );
				startActivity ( intent );
				finish ( );
			}
		} );
		layout.startAnimation ( fade );
	}

	// 뒤로가기 버튼 클릭시 introRun 삭제
	@Override
	public void onBackPressed ( )
	{
		super.onBackPressed ( );
		handler.removeCallbacks ( introRun );
	}

	@Override
	protected void onDestroy ( )
	{
		super.onDestroy ( );
		System.gc ( );
		finish ( );
	}
}