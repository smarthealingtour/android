package jejusmarttour;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.common.KeywordData;
import jejusmarttour.user.UserVO;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.KeywordCategoryVO;
import syl.com.jejusmarttour.R;

public class SelectKeywordActivity extends AppCompatActivity implements OnClickListener , OnCheckedChangeListener
{
	private static final String TAG = SelectKeywordActivity.class.toString ( );

	private LinearLayout selectedLayout;
	private UserVO userVO;

	private ArrayList <KeywordCategoryVO> keywordArrayList;

	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_select_keyword );

		userVO = new UserVO ( );
		userVO = CommonData.getUserVO();
		keywordArrayList = new ArrayList < KeywordCategoryVO > ( );
		selectedLayout = ( LinearLayout ) findViewById ( R.id.selected_keywords_layout );

		initActionBar ( );
		initToggleButton ( );
	}

	// 커스텀 액션바 적용
	private void initActionBar ( )
	{
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_keyword , null );
		TextView titleTextView = ( TextView ) customView.findViewById ( R.id.title_text );
		titleTextView.setText ( R.string.title_activity_select_keyword );

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	private ToggleButton nature_01;
	private ToggleButton nature_02;
	private ToggleButton nature_03;
	private ToggleButton culture_01;
	private ToggleButton culture_02;
	private ToggleButton culture_03;
	private ToggleButton culture_04;
	private ToggleButton sight_01;
	private ToggleButton sight_02;
	private ToggleButton sight_03;
	private ToggleButton sight_04;
	private ToggleButton taste_01;
	private ToggleButton taste_02;
	private ToggleButton taste_03;
	private ToggleButton exercise_01;
	private ToggleButton exercise_02;
	private ToggleButton exercise_03;
	private ToggleButton exercise_04;
	private ToggleButton emotion_01;
	private ToggleButton emotion_02;
	private ToggleButton emotion_03;
	private ToggleButton emotion_04;
	private ToggleButton intelligent_01;
	private ToggleButton intelligent_02;
	private ToggleButton intelligent_03;
	private ToggleButton intelligent_04;
	private ToggleButton intelligent_05;

	private void initToggleButton ( )
	{
		nature_01 = ( ToggleButton ) findViewById ( R.id.nature_toggle_01 );
		nature_02 = ( ToggleButton ) findViewById ( R.id.nature_toggle_02 );
		nature_03 = ( ToggleButton ) findViewById ( R.id.nature_toggle_03 );
		nature_01.setTag ( "A0101" );
		nature_02.setTag ( "A0102" );
		nature_03.setTag ( "A0103" );

		culture_01 = ( ToggleButton ) findViewById ( R.id.culture_toggle_01 );
		culture_02 = ( ToggleButton ) findViewById ( R.id.culture_toggle_02 );
		culture_03 = ( ToggleButton ) findViewById ( R.id.culture_toggle_03 );
		culture_04 = ( ToggleButton ) findViewById ( R.id.culture_toggle_04 );
		culture_01.setTag ( "A0201" );
		culture_02.setTag ( "A0202" );
		culture_03.setTag ( "A0203" );
		culture_04.setTag ( "A0204" );

		sight_01 = ( ToggleButton ) findViewById ( R.id.sight_toggle_01 );
		sight_02 = ( ToggleButton ) findViewById ( R.id.sight_toggle_02 );
		sight_03 = ( ToggleButton ) findViewById ( R.id.sight_toggle_03 );
		sight_04 = ( ToggleButton ) findViewById ( R.id.sight_toggle_04 );
		sight_01.setTag ( "P0101" );
		sight_02.setTag ( "P0102" );
		sight_03.setTag ( "P0103" );
		sight_04.setTag ( "P0104" );

		taste_01 = ( ToggleButton ) findViewById ( R.id.taste_toggle_01 );
		taste_02 = ( ToggleButton ) findViewById ( R.id.taste_toggle_02 );
		taste_03 = ( ToggleButton ) findViewById ( R.id.taste_toggle_03 );
		taste_01.setTag ( "P0201" );
		taste_02.setTag ( "P0202" );
		taste_03.setTag ( "P0203" );

		exercise_01 = ( ToggleButton ) findViewById ( R.id.exercise_toggle_01 );
		exercise_02 = ( ToggleButton ) findViewById ( R.id.exercise_toggle_02 );
		exercise_03 = ( ToggleButton ) findViewById ( R.id.exercise_toggle_03 );
		exercise_04 = ( ToggleButton ) findViewById ( R.id.exercise_toggle_04 );
		exercise_01.setTag ( "P0301" );
		exercise_02.setTag ( "P0302" );
		exercise_03.setTag ( "P0303" );
		exercise_04.setTag ( "P0304" );

		emotion_01 = ( ToggleButton ) findViewById ( R.id.emotion_toggle_01 );
		emotion_02 = ( ToggleButton ) findViewById ( R.id.emotion_toggle_02 );
		emotion_03 = ( ToggleButton ) findViewById ( R.id.emotion_toggle_03 );
		emotion_04 = ( ToggleButton ) findViewById ( R.id.emotion_toggle_04 );
		emotion_01.setTag ( "P0401" );
		emotion_02.setTag ( "P0402" );
		emotion_03.setTag ( "P0403" );
		emotion_04.setTag ( "P0404" );

		intelligent_01 = ( ToggleButton ) findViewById ( R.id.intelligent_toggle_01 );
		intelligent_02 = ( ToggleButton ) findViewById ( R.id.intelligent_toggle_02 );
		intelligent_03 = ( ToggleButton ) findViewById ( R.id.intelligent_toggle_03 );
		intelligent_04 = ( ToggleButton ) findViewById ( R.id.intelligent_toggle_04 );
		intelligent_05 = ( ToggleButton ) findViewById ( R.id.intelligent_toggle_05 );
		intelligent_01.setTag ( "P0501" );
		intelligent_02.setTag ( "P0502" );
		intelligent_03.setTag ( "P0503" );
		intelligent_04.setTag ( "P0504" );
		intelligent_05.setTag ( "P0505" );

		nature_01.setOnCheckedChangeListener ( this );
		nature_02.setOnCheckedChangeListener ( this );
		nature_03.setOnCheckedChangeListener ( this );
		culture_01.setOnCheckedChangeListener ( this );
		culture_02.setOnCheckedChangeListener ( this );
		culture_03.setOnCheckedChangeListener ( this );
		culture_04.setOnCheckedChangeListener ( this );
		sight_01.setOnCheckedChangeListener ( this );
		sight_02.setOnCheckedChangeListener ( this );
		sight_03.setOnCheckedChangeListener ( this );
		sight_04.setOnCheckedChangeListener ( this );
		taste_01.setOnCheckedChangeListener ( this );
		taste_02.setOnCheckedChangeListener ( this );
		taste_03.setOnCheckedChangeListener ( this );
		exercise_01.setOnCheckedChangeListener ( this );
		exercise_02.setOnCheckedChangeListener ( this );
		exercise_03.setOnCheckedChangeListener ( this );
		exercise_04.setOnCheckedChangeListener ( this );
		emotion_01.setOnCheckedChangeListener ( this );
		emotion_02.setOnCheckedChangeListener ( this );
		emotion_03.setOnCheckedChangeListener ( this );
		emotion_04.setOnCheckedChangeListener ( this );
		intelligent_01.setOnCheckedChangeListener ( this );
		intelligent_02.setOnCheckedChangeListener ( this );
		intelligent_03.setOnCheckedChangeListener ( this );
		intelligent_04.setOnCheckedChangeListener ( this );
		emotion_04.setOnCheckedChangeListener ( this );

		setUserHasKeyword ( );
	}

	// 유저정보에 있는 키워드를 토글 선택상태로 표시
	private void setDefaultToggleButtonValue ( String keyword , boolean isRemove )
	{
		ToggleButton [ ] toggleButtons = { nature_01 , nature_02 , nature_03 , culture_01 , culture_02 , culture_03 , culture_04 , sight_01 , sight_02 , sight_03 , sight_04 , taste_01 , taste_02 , taste_03 , exercise_01 , exercise_02 , exercise_03 , exercise_04 , emotion_01 , emotion_02 , emotion_03 , emotion_04 , intelligent_01 , intelligent_02 , intelligent_03 , intelligent_04 , emotion_04 };

		for ( int i = 0 ; i < toggleButtons.length ; i++ )
		{
			if ( toggleButtons [ i ].getTag ( ).toString ( ).equals ( keyword ) == true )
			{
				if ( isRemove == false )
					toggleButtons [ i ].setChecked ( true );
				else
					toggleButtons [ i ].setChecked ( false );
			}
		}
	}

	// 유저가 저장한 키워드 셋팅
	private void setUserHasKeyword ( )
	{
		selectedLayout.removeAllViewsInLayout ( );
		selectedLayout.invalidate ( );

		addSelectedKeyword ( );
	}

	private void addSelectedKeyword ( )
	{
		if ( userVO.getHcdArray ( ) != null )
		{
			for ( int i = 0 ; i < userVO.getHcdArray ( ).length ; i++ )
			{
				TextView textView = KeywordData.getSelectedKeywordTextView(this, userVO.getHcdArray()[i], CommonData.getKeywordArrayList());
				textView.setTag ( userVO.getHcdArray ( ) [ i ] );
				setDefaultToggleButtonValue ( textView.getTag ( ).toString ( ) , false);
				textView.setOnClickListener ( this );
				selectedLayout.addView ( textView );
			}
		}
		if ( userVO.getScdArray ( ) != null )
		{
			for ( int i = 0 ; i < userVO.getScdArray ( ).length ; i++ )
			{
				TextView textView = KeywordData.getSelectedKeywordTextView ( this , userVO.getScdArray ( ) [ i ] , CommonData.getKeywordArrayList ( ) );
				textView.setTag ( userVO.getScdArray ( ) [ i ] );
				setDefaultToggleButtonValue ( textView.getTag ( ).toString ( ) , false);
				textView.setOnClickListener ( this );
				selectedLayout.addView ( textView );
			}
		}
	}

	@Override
	public void onClick ( View v )
	{
		removeKeyword ( v.getTag ( ).toString ( ) );
	}

	@SuppressWarnings ( "deprecation" )
	@Override
	public void onCheckedChanged ( CompoundButton buttonView , boolean isChecked )
	{
		if ( buttonView.isChecked ( ) == true )
		{
			buttonView.setTextColor ( getResources ( ).getColor ( R.color.white ) );
			addKeyword ( buttonView.getTag ( ).toString ( ) );
		}
		else
		{
			buttonView.setTextColor ( getResources ( ).getColor ( R.color.font_color ) );
			removeKeyword ( buttonView.getTag ( ).toString ( ) );
		}
	}

	// 키워드 등록
	private void addKeyword ( String keyword ) throws NullPointerException
	{
		String str = null;

		if ( keyword.contains ( "A" ) )
		{
			if ( userVO.getHcd ( ) != null && userVO.getHcd ( ).contains ( keyword ) )
				return;

			if ( userVO.getHcd ( ) != null )
				str = userVO.getHcd ( ) + "," + keyword;
			else
				str = keyword;

			userVO.setHcd ( str );

			Log.d ( TAG , "addKeyword A : " + userVO.getHcd ( ) );
		}
		else if ( keyword.contains ( "P" ) )
		{
			if ( userVO.getScd ( ) != null && userVO.getScd ( ).contains ( keyword ) )
				return;

			if ( userVO.getScd ( ) != null )
				str = userVO.getScd ( ) + "," + keyword;
			else
				str = keyword;

			userVO.setScd ( str );

			Log.d ( TAG , "addKeyword P : " + userVO.getScd ( ) );
		}
		else if ( keyword.contains ( "D" ) )
		{

		}

		setUserHasKeyword ( );
	}

	// 키워드 삭제
	private void removeKeyword ( String keyword ) throws NullPointerException
	{
		String str = null;
		String [ ] strArray;

		if ( keyword.contains ( "A" ) )
		{
			if ( userVO.getHcd ( ) == null || userVO.getHcd ( ).isEmpty ( ) == true )
				return;

			if ( userVO.getHcd ( ).contains ( keyword ) )
			{
				strArray = SmartTourUtils.getValidateStringAraay(userVO.getHcd());
				str = SmartTourUtils.removeStringInStringArray ( strArray , keyword );
				userVO.setHcd ( str );
			}
			
			

			if ( userVO.getHcd ( ) != null )
				Log.d ( TAG , "removeKeyword A : " + userVO.getHcd ( ) );
			else
				Log.d ( TAG , "removeKeyword A : " + "NULL" );
		}
		else if ( keyword.contains ( "P" ) )
		{
			if ( userVO.getScd ( ) == null || userVO.getScd ( ).isEmpty ( ) == true )
				return;

			if ( userVO.getScd ( ).contains ( keyword ) )
			{
				strArray = SmartTourUtils.getValidateStringAraay ( userVO.getScd ( ) );
				str = SmartTourUtils.removeStringInStringArray ( strArray , keyword );
				userVO.setScd ( str );
			}

			if ( userVO.getScd ( ) != null )
				Log.d ( TAG , "removeKeyword P : " + userVO.getScd ( ) );
			else
				Log.d ( TAG , "removeKeyword P : " + "NULL" );
		}
		else if ( keyword.contains ( "D" ) )
		{

		}
		
		setDefaultToggleButtonValue ( keyword , true);
		setUserHasKeyword ( );
	}

	// 유저 키워드 업데이트
	private void updateUserKeyword ( )
	{
		CommonData.setUserVO ( userVO );
	}

	@Override
	public void onBackPressed ( )
	{
		updateUserKeyword ( );
		super.onBackPressed ( );
	}

	@Override
	public boolean onCreateOptionsMenu ( Menu menu )
	{
		getMenuInflater ( ).inflate ( R.menu.close , menu );
		return true;
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item )
	{
		int id = item.getItemId ( );
		if ( id == R.id.action_close )
		{
			updateUserKeyword ( );
			finish ( );
			return true;
		}
		return super.onOptionsItemSelected ( item );
	}

}
