package jejusmarttour.examples;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import jejusmarttour.annotation.SampleInfo;
import jejusmarttour.examples.adapter.MessageListAdapter;
import syl.com.jejusmarttour.R;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public abstract class ExampleActivity extends AppCompatActivity {
    private MessageListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);

        initMessageView();
        initHeader();
    }

    private void initHeader() {
        SampleInfo sampleInfo = getClass().getAnnotation(SampleInfo.class);
        TextView titleView = (TextView) findViewById(R.id.exampleTitle);
        assert(titleView != null);
        titleView.setText(sampleInfo.title());

        TextView descriptionView = (TextView) findViewById(R.id.exampleDescription);
        assert(descriptionView != null);
        descriptionView.setText(sampleInfo.description());
    }

    private void initMessageView() {
        ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new MessageListAdapter(this);
        assert(listView != null);
        listView.setAdapter(adapter);

        Button clearButton = (Button) findViewById(R.id.clearButton);
        assert(clearButton != null);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.clearAllMessages();
                Toast.makeText(getBaseContext(), "메시지 목록을 비웠습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void addMessage(String message) {
        adapter.addMessage(message);
    }

}