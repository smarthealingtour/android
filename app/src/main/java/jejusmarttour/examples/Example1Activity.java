
package jejusmarttour.examples;

import android.os.Bundle;
import android.util.Log;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.tamra.Tamra;
import com.kakao.oreum.tamra.base.Config;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.base.TamraObserver;
import com.kakao.oreum.tamra.error.TamraErrorFilters;
import com.kakao.oreum.tamra.error.TamraInitException;

import jejusmarttour.annotation.SampleInfo;
import jejusmarttour.util.Constants;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */

@SampleInfo(
        index = 1,
        title = "초기화 및 기본 사용",
        description = "TamraSDK 사용을 위한 초기화 및 기본 사용 예제"
)

public class Example1Activity extends ExampleActivity {

    private Spot recently = null;
    private static final String TAG = Example1Activity.class.toString( );

    private final TamraObserver tamraObserver = new TamraObserver() {
        @Override
        public void didEnter(Region region) {
            addMessage("'" + region.name() + "' 지역에 진입했습니다.");
        }

        @Override
        public void didExit(Region region) {
            addMessage("'" + region.name() + "' 지역에서 이탈했습니다.");
        }

        @Override
        public void ranged(NearbySpots nearbySpots) {
            Optional<Spot> nearestSpot = nearbySpots.orderBy(Spot.ACCURACY_ORDER).first();
            nearestSpot.ifPresent(new Consumer<Spot>() {
                @Override
                public void accept(Spot spot) {
                    if (recently != null && spot.id() == recently.id()) {
                        return;
                    }

                    addMessage("근처에 '" + spot.description() + "'를 발견했습니다.");
                    recently = spot;

                    // OreumZigi에서 등록한 커스텀 데이터는 아래와 같이 String(JSON) 형태로 받아 쓸 수 있다.
                    // String json = spot.data().orElse('');
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initTamra();

        Tamra.addObserver(tamraObserver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Tamra.removeObserver(tamraObserver);
    }

    private void initTamra() {
//        Config config = Config.forTesting(
//                getApplicationContext(),
//                Constants.appKey).observerSensitivity(ObserverSensitivity.Balanced);
//        Tamra.init(config);
        Config config = Config.forTesting(
                getApplicationContext(),
                Constants.appKey).onSimulation();
        Tamra.init(config);

        for (Region region : Regions.all()) {
            Tamra.startMonitoring(region);
        }

        Tamra.recentErrors()
                .filter(TamraErrorFilters.causedBy(TamraInitException.class))
                .foreach(tamraError -> Log.d(TAG, tamraError.toString()));

        addMessage("TamraSDK 초기화를 완료했습니다.");
    }
}
