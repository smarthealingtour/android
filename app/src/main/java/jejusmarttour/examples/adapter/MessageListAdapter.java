package jejusmarttour.examples.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class MessageListAdapter extends BaseAdapter {

    private Activity activity;
    private List<Message> messages = new ArrayList<>();

    public MessageListAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageItemView itemView;

        if (convertView == null) {
            itemView = new MessageItemView(activity.getBaseContext());
        } else {
            itemView = (MessageItemView) convertView;
        }

        itemView.updateMessage(messages.get(position));

        return itemView;
    }

    public void addMessage(String message) {
        messages.add(new Message(message, new Date()));
        refresh();
    }

    public void clearAllMessages() {
        messages.clear();
        refresh();
    }

    private void refresh() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

}
