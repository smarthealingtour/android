package jejusmarttour.examples.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import jejusmarttour.supports.DateUtils;
import syl.com.jejusmarttour.R;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class MessageItemView extends LinearLayout {

    private TextView messageView;
    private TextView messagedAtView;

    public MessageItemView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_message, this, true);

        messageView = (TextView) findViewById(R.id.message);
        messagedAtView = (TextView) findViewById(R.id.messagedAt);
    }

    public void updateMessage(Message message) {
        messageView.setText(message.content());
        messagedAtView.setText(DateUtils.defaultFormat(message.messagedAt()));
    }

}

