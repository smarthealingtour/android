package jejusmarttour.examples.adapter;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class Message {

    private String content;
    private Date messagedAt;

    Message(String content, Date messagedAt) {
        this.content = content;
        this.messagedAt = messagedAt;
    }

    public String content() {
        return content;
    }

    public Date messagedAt() {
        return messagedAt;
    }

}
