package jejusmarttour.model;

import android.app.Activity;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class SampleItem {

    private String title;
    private Class<? extends Activity> activityClass;

    public SampleItem(String title, Class<? extends Activity> activityClass) {
        this.title = title;
        this.activityClass = activityClass;
    }

    public String title() {
        return title;
    }

    public Class<? extends Activity> activityClass() {
        return activityClass;
    }

}
