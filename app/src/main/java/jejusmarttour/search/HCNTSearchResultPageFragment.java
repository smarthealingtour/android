package jejusmarttour.search;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.task.HealingContentsTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.user.UserVO;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import syl.com.jejusmarttour.R;

public class HCNTSearchResultPageFragment extends Fragment implements JSONArrayResult , OnScrollListener{
	private static final String TAG = HCNTSearchResultPageFragment.class.toString ( );

	private String searchStr;

	private HealingContentsTask searchProductTask;
	private SearchProductRequestVO vo;

	private HCNTSearchResultListViewAdapter listViewAdapter;
	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private ArrayList < HealingContentsVO > listData;

	private TextView countView;
	private boolean fromMap = false;
	private boolean fromRoute = false;

	private TextView resultText;

	public static HCNTSearchResultPageFragment newInstance ( String searchStr , boolean fromMap , boolean fromRoute ) {
		HCNTSearchResultPageFragment fragment = new HCNTSearchResultPageFragment ( );
		fragment.searchStr = searchStr;
		fragment.fromMap = fromMap;
		fragment.fromRoute = fromRoute;
		return fragment;
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ) {
		View view = null;

		try {
			view = inflater.inflate ( R.layout.fragment_search_result_view , container , false );
		}
		catch ( InflateException e ) {
			e.printStackTrace ( );
		}

		resultText = view.findViewById ( R.id.no_result_text );
		resultText.setVisibility ( View.GONE );

		listData = new ArrayList <> ( );
		lockListView = true;

		TextView searchView = view.findViewById ( R.id.search_text );
		// countView = ( TextView ) view.findViewById ( R.id.count_text );

		if (!TextUtils.isEmpty(searchStr)) {
			searchView.setText ( searchStr );
		}
		else {
			searchView.setText ( "전체" );
		}

		listViewAdapter = new HCNTSearchResultListViewAdapter ( getActivity ( ) , listData );
		ListView listView = view.findViewById ( R.id.list_view);
		listView.setOnScrollListener ( this );
		listView.setAdapter ( listViewAdapter );

		// ListView 아이템 터치 시 이벤트 추가
		listView.setOnItemClickListener ( onClickListItem );

		executeTask ( );

		return view;
	}

	// 검색 실행
	private void executeTask ( ) {
		searchProductTask = new HealingContentsTask ( getActivity ( ) , this );
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) , searchStr ) );
	}

	// 요청할 파라미터
	private SearchProductRequestVO getParam ( String page , String name ) {
		vo = new SearchProductRequestVO ( );
		vo.setHt_name ( name );
		vo.setType ( "0" );
		vo.setPage ( page );
		vo.setSort ( ( ( CommonData ) getActivity ( ).getApplication ( ) ).getSortType ( ) );

		UserVO userVO;
		userVO = CommonData.getUserVO ( );

		if ( userVO.getHcdArray ( ) != null )
			vo.setHcd ( userVO.getHcd ( ) );

		if ( userVO.getScdArray ( ) != null )
			vo.setScd ( userVO.getScd ( ) );

		return vo;
	}

	@Override
	public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount ) {
		int lastInScreen = firstVisibleItem + visibleItemCount;

		if ( ( lastInScreen == totalItemCount ) && !lockListView) {
			currentPageNumber += 1;
			requestPageNumber += 1;

			executeTask ( );

			lockListView = true;
		}
	}

	@Override
	public void onScrollStateChanged ( AbsListView arg0 , int arg1 ) { }

	private void InsertItemToListView ( ArrayList < HealingContentsVO > resultList ) {
		for ( int i = 0 ; i < resultList.size ( ) ; i++ ) {
			listViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		listViewAdapter.notifyDataSetChanged ( );
	}

	// 아이템 터치 이벤트
	private OnItemClickListener onClickListItem = new OnItemClickListener ( ) {
		@Override
		public void onItemClick ( AdapterView < ? > arg0 , View arg1 , int position , long arg3 ) {
			ScheduleVO scheduleVO = new ScheduleVO ( );
			scheduleVO.setHealingContentsVO( listData.get ( position ) );
			CommonData.setScheduleVO ( scheduleVO );

			if ( fromRoute ) { // 경로탐색에서 넘어왔을 때
				Intent intent = new Intent ( );
				intent.putExtra ( "name" , scheduleVO.getHealingContentsVO( ).getHcnt_name ( ) );
				intent.putExtra ( "x" , scheduleVO.getHealingContentsVO( ).getHcnt_coord_x ( ) );
				intent.putExtra ( "y" , scheduleVO.getHealingContentsVO( ).getHcnt_coord_y ( ) );
				intent.putExtra ( "id" , scheduleVO.getHealingContentsVO( ).getHcnt_id ( ) );
                intent.putExtra ( "address" , scheduleVO.getHealingContentsVO( ).getHcnt_addr() );
                intent.putExtra ( "tel" , scheduleVO.getHealingContentsVO( ).getHcnt_tel() );
                intent.putExtra ( "type" , scheduleVO.getHealingContentsVO( ).getHcnt_type() );

				getActivity ( ).setResult ( 0 , intent );
				getActivity ( ).finish ( );
			}
			else {
				Intent intent = new Intent ( getActivity ( ) , DetailInfoActivity.class );
				intent.putExtra ( "fromMap" , fromMap );
				startActivity ( intent );
			}
		}
	};

	@Override
	public void setJSONArrayResult ( ArrayList resultList ) {
		lockListView = true;
		searchProductTask.cancel ( true );

		if ( resultList == null ) {
			// countView.setText ( "총 0건" );
			if ( listData.size ( ) == 0 )
				resultText.setVisibility ( View.VISIBLE );
			return;
		}
		else {
			resultText.setVisibility ( View.GONE );
			// int count = resultList.size ( );
			// Log.d ( TAG , "검색 결과 : " + count + "건" );
			// countView.setText ( "총 " + count + "건" );
			InsertItemToListView ( resultList );
		}

		lockListView = false;
	}
}
