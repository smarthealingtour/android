package jejusmarttour.search;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_spot.detailinfo.FoodStayDetailInfoActivity;
import jejusmarttour.task.FoodStayTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.user.UserVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

public class CCNTSearchResultPageFragment extends Fragment implements JSONArrayResult<ScheduleVO> , OnScrollListener{
	private String searchStr;
	private FoodStayTask searchProductTask;
	private SearchProductRequestVO vo;

	private CCNTSearchResultListViewAdapter listViewAdapter;
	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private ArrayList < SightSeeingContentsVO > listData;

	private TextView countView;
	private boolean fromMap = false;
	private boolean fromRoute = false;

	private TextView resultText;
	private String type;

	public static CCNTSearchResultPageFragment newInstance ( String searchStr , boolean fromMap , boolean fromRoute , String type){
		CCNTSearchResultPageFragment fragment = new CCNTSearchResultPageFragment ( );
		fragment.searchStr = searchStr;
		fragment.fromMap = fromMap;
		fragment.fromRoute = fromRoute;
		fragment.type = type;
		return fragment;
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
		View view = null;

		try{
			view = inflater.inflate ( R.layout.fragment_search_result_view , container , false );
		}
		catch ( InflateException e ){
			e.printStackTrace ( );
		}

		resultText = view.findViewById ( R.id.no_result_text );
		resultText.setVisibility ( View.GONE );

		listData = new ArrayList <> ( );
		lockListView = true;

		TextView searchView = view.findViewById ( R.id.search_text );
		// countView = ( TextView ) view.findViewById ( R.id.count_text );

		if ( !TextUtils.isEmpty(searchStr )){
			searchView.setText ( searchStr );
		}
		else{
			searchView.setText ( "전체" );
		}

		listViewAdapter = new CCNTSearchResultListViewAdapter ( getActivity ( ) , listData );
		ListView listView =  view.findViewById ( R.id.list_view);
		listView.setOnScrollListener ( this );
		listView.setAdapter ( listViewAdapter );

		// ListView 아이템 터치 시 이벤트 추가
		listView.setOnItemClickListener ( onClickListItem );

		executeTask ( );

		return view;
	}

	// 검색 실행
	private void executeTask ( ){
		searchProductTask = new FoodStayTask( getActivity ( ) , this ,type);
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) , searchStr ) );
	}

	// 요청할 파라미터
	private SearchProductRequestVO getParam ( String page , String name ){
		vo = new SearchProductRequestVO ( );
		vo.setHt_name ( name );
		vo.setType ( "1" );
		vo.setPage ( page );
		vo.setSort ( ( ( CommonData ) getActivity ( ).getApplication ( ) ).getSortType ( ) );

		UserVO userVO = CommonData.getUserVO ( );

		if ( userVO.getHcdArray ( ) != null )
			vo.setHcd ( userVO.getHcd ( ) );

		if ( userVO.getScdArray ( ) != null )
			vo.setScd ( userVO.getScd ( ) );

		return vo;
	}

	@Override
	public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount ){
		int lastInScreen = firstVisibleItem + visibleItemCount;

		if ( ( lastInScreen == totalItemCount ) && !lockListView){
			currentPageNumber += 1;
			requestPageNumber += 1;

			executeTask ( );

			lockListView = true;
		}
	}

	@Override
	public void onScrollStateChanged ( AbsListView arg0 , int arg1 ){

	}

	private void InsertItemToListView ( ArrayList < SightSeeingContentsVO > resultList ){
		for ( int i = 0 ; i < resultList.size ( ) ; i++ ){
			listViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		listViewAdapter.notifyDataSetChanged ( );
	}

	// 아이템 터치 이벤트
	private OnItemClickListener onClickListItem = new OnItemClickListener ( ){
		@Override
		public void onItemClick ( AdapterView < ? > arg0 , View arg1 , int position , long arg3 ){
			ScheduleVO scheduleVO = new ScheduleVO ( );
			scheduleVO.setSightSeeingContentsVO( listData.get ( position ) );
			CommonData.setScheduleVO ( scheduleVO );

			if ( fromRoute ){
				Intent intent = new Intent ( );
				intent.putExtra ( "name" , scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( ) );
				intent.putExtra ( "x" , scheduleVO.getSightSeeingContentsVO( ).getCcnt_coord_x ( ) );
				intent.putExtra ( "y" , scheduleVO.getSightSeeingContentsVO( ).getCcnt_coord_y ( ) );
				intent.putExtra ( "id" , scheduleVO.getSightSeeingContentsVO( ).getCcnt_id ( ) );
                intent.putExtra ( "address" , "" );
                intent.putExtra ( "tel" , "" );
                intent.putExtra ( "type" , "point" );
				getActivity ( ).setResult ( 0 , intent );
				getActivity ( ).finish ( );
			}
			else{
				Intent intent = new Intent ( getActivity ( ) , FoodStayDetailInfoActivity.class );
				intent.putExtra ( "fromMap" , fromMap );
				startActivity ( intent );
			}
		}
	};

	@Override
	public void setJSONArrayResult ( ArrayList<ScheduleVO> resultList ){
		lockListView = true;
		searchProductTask.cancel ( true );

		if ( resultList == null ){
			// countView.setText ( "총 0건" );
			if ( listData.size ( ) == 0 )
				resultText.setVisibility ( View.VISIBLE );
			return;
		}
		else{
			resultText.setVisibility ( View.GONE );
			// int count = resultList.size ( );
			// Log.d ( TAG , "검색 결과 : " + count + "건" );
			// countView.setText ( "총 " + count + "건" );
//			listData.addAll(resultList);

			ArrayList<SightSeeingContentsVO> ss = new ArrayList<>();
			for (ScheduleVO vo:
				 resultList) {
				ss.add(vo.getSightSeeingContentsVO());
			}
			InsertItemToListView ( ss );
		}

		lockListView = false;
	}
}