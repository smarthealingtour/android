package jejusmarttour.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.ToggleButton;

import syl.com.jejusmarttour.R;

public class SearchProductActivity extends AppCompatActivity implements OnClickListener{
    private String searchStr;
    private boolean fromMap = false;

    private final String TYPE_OF_FOOD = "B";
    private final String TYPE_OF_STAY = "C";

    @SuppressWarnings ( "deprecation" )
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView(R.layout.activity_search_product);

        // 키보드 숨기기
        InputMethodManager immhide = ( InputMethodManager ) getSystemService(Activity.INPUT_METHOD_SERVICE);
        immhide.toggleSoftInput ( InputMethodManager.HIDE_IMPLICIT_ONLY , 0 );

        Intent intent = getIntent ( );
        searchStr = intent.getStringExtra ( "data" );

        if(intent.getBooleanExtra("fromMap", false))
            fromMap = true;

        initViewPager ( );
        initActionBar ( );
    }

    // 커스텀 액션바 적용
    private void initActionBar ( ) {
        ActionBar actionBar = getSupportActionBar ( );
        actionBar.setDisplayShowHomeEnabled ( false );
        actionBar.setDisplayShowTitleEnabled(false);

        // 액션바 그림자 지우기
        actionBar.setElevation(0);

        LayoutInflater mInflater = LayoutInflater.from ( this );

        View customView = mInflater.inflate ( R.layout.actionbar_close , null );
        TextView titleTextView = customView.findViewById ( R.id.title_text );
        titleTextView.setText ( R.string.title_activity_search );

        actionBar.setCustomView(customView);
        actionBar.setDisplayShowCustomEnabled(true);
    }

    // 액션바 메뉴
    @Override
    public boolean onCreateOptionsMenu ( Menu menu ) {
        getMenuInflater ( ).inflate ( R.menu.close , menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected ( MenuItem item ) {
        int id = item.getItemId ( );

        if ( id == R.id.action_close ) {
            finish ( );
            return true;
        }

        return super.onOptionsItemSelected ( item );
    }

    @Override
    public void onBackPressed ( ){
        super.onBackPressed ( );
        finish ( );
    }

    /// 토글 버튼 및 뷰페이저
    private final int MAX_PAGE = 4;
    private Fragment cur_fragment = new Fragment ( );
    private ToggleButton tab1;
    private ToggleButton tab2;
    private ToggleButton tab3;
    private ToggleButton tab4;

    private ViewPager viewPager;
    private final PageListener pageListener = new PageListener ( );

    private void initViewPager ( ){
        tab1 = ( ToggleButton ) findViewById ( R.id.tab1 );
        tab2 = ( ToggleButton ) findViewById ( R.id.tab2 );
        tab3 = ( ToggleButton ) findViewById ( R.id.tab3 );
        tab4 = ( ToggleButton ) findViewById ( R.id.tab4 );

        tab1.setOnClickListener ( this );
        tab2.setOnClickListener ( this );
        tab3.setOnClickListener ( this );
        tab4.setOnClickListener ( this );

        tab1.setChecked ( true );

        viewPager = ( ViewPager ) findViewById ( R.id.viewpager );
        viewPager.setAdapter ( new ViewPagerAdapter ( getSupportFragmentManager ( ) ) );
        viewPager.setOffscreenPageLimit ( MAX_PAGE - 1 );
        viewPager.setOnPageChangeListener ( pageListener );
    }

    private ToggleButton [ ] getToggleButtons ( ){
        ToggleButton [ ] buttons = { tab1 , tab2 , tab3 , tab4 };
        return buttons;
    }

    @Override
    public void onClick ( View v ){
        if ( v.getId ( ) == tab1.getId ( ) ) {
            setTabItem ( 0 );
        }
        else if ( v.getId ( ) == tab2.getId ( ) ) {
            setTabItem ( 1 );
        }
        else if ( v.getId ( ) == tab3.getId ( ) ) {
            setTabItem ( 2 );
        }
        else if ( v.getId ( ) == tab4.getId ( ) ) {
            setTabItem ( 3 );
        }
    }

    private void setTabItem ( int position ){
        if ( getToggleButtons ( ) [ position ].isChecked ( ) == true ) {
            setCurrentItem ( position );
            validateTogleButton( position );
        }
        else {
            getToggleButtons ( ) [ position ].setChecked ( true );
        }
    }

    private void setChangeTabItem ( int position ){
        getToggleButtons ( ) [ position ].setChecked ( true );
        validateTogleButton( position );
    }

    private void validateTogleButton(int position ){
        for ( int i = 0; i < getToggleButtons ( ).length; i++ ){
            if ( i != position )
                getToggleButtons ( ) [ i ].setChecked ( false );
        }
    }

    private void setCurrentItem ( int position ) {
        viewPager.setCurrentItem ( position );
    }

    private class PageListener implements OnPageChangeListener{

        @Override
        public void onPageScrolled ( int position , float positionOffset , int positionOffsetPixels ) {}

        @Override
        public void onPageScrollStateChanged ( int state ) {}

        @Override
        public void onPageSelected ( int position )
        {
            setChangeTabItem ( position );
        }

    }

    // 뷰페이지 어뎁터
    class ViewPagerAdapter extends FragmentPagerAdapter{
        private int position = 0;

        public ViewPagerAdapter ( FragmentManager fm )
        {
            super ( fm );
        }

        @Override
        public Fragment getItem ( int position ){
            if ( position < 0 || MAX_PAGE <= position )
                return null;

            this.position = position;

            switch ( position ){
                case 0 :
                    cur_fragment = HTPSearchResultPageFragment.newInstance ( searchStr, fromMap);
                    break;
                case 1 :
                    cur_fragment = HCNTSearchResultPageFragment.newInstance ( searchStr, fromMap, false );
                    break;
                case 2 :
                    cur_fragment = CCNTSearchResultPageFragment.newInstance ( searchStr, fromMap, false , TYPE_OF_FOOD);
                    break;
                case 3 :
                    cur_fragment = CCNTSearchResultPageFragment.newInstance ( searchStr, fromMap, false , TYPE_OF_STAY);
                    break;
            }
            return cur_fragment;
        }

        public int getCurrentPosition ( )
        {
            return position;
        }

        @Override
        public int getCount ( )
        {
            return MAX_PAGE;
        }

        @Override
        public CharSequence getPageTitle ( int position ){
            return getResources ( ).getStringArray ( R.array.htp_main_titles ) [ position ];
        }
    }
}
