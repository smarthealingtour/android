package jejusmarttour.search;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_tourproduct.TourProductIntroduceActivity;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.SearchProductTask;
import jejusmarttour.user.UserVO;
import jejusmarttour.vo.SearchProductRequestVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class HTPSearchResultPageFragment extends Fragment implements JSONArrayResult , OnScrollListener{
	private static final String TAG = HTPSearchResultPageFragment.class.toString ( );

	private String searchStr;

	private SearchProductTask searchProductTask;
	private SearchProductRequestVO vo;

	private HTPSearchResultListViewAdapter listViewAdapter;
	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private ArrayList < TourProductVO > listData;

	private TextView countView;
	private boolean fromMap = false;

	private TextView resultText;

	public static HTPSearchResultPageFragment newInstance ( String searchStr , boolean fromMap ){
		HTPSearchResultPageFragment fragment = new HTPSearchResultPageFragment ( );
		fragment.searchStr = searchStr;
		fragment.fromMap = fromMap;
		return fragment;
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState )
	{
		View view = null;

		try{
			view = inflater.inflate ( R.layout.fragment_search_result_view , container , false );
		}
		catch ( InflateException e ){
			e.printStackTrace ( );
		}

		listData = new ArrayList < TourProductVO > ( );
		lockListView = true;

		TextView searchView = ( TextView ) view.findViewById ( R.id.search_text );
		// countView = ( TextView ) view.findViewById ( R.id.count_text );

		if ( TextUtils.isEmpty ( searchStr ) == false ){
			searchView.setText ( searchStr );
		}
		else{
			searchView.setText ( "전체" );
		}

		resultText = ( TextView ) view.findViewById ( R.id.no_result_text );
		resultText.setVisibility ( View.GONE );

		listViewAdapter = new HTPSearchResultListViewAdapter ( getActivity ( ) , listData );
		ListView listView = ( ListView ) view.findViewById ( R.id.list_view);
		listView.setOnScrollListener ( this );
		listView.setAdapter ( listViewAdapter );

		// ListView 아이템 터치 시 이벤트 추가
		listView.setOnItemClickListener ( onClickListItem );

		executeTask ( );

		return view;
	}

	// 검색 실행
	private void executeTask ( ){
		searchProductTask = new SearchProductTask ( getActivity ( ) , this );
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) , searchStr ) );
	}

	// 요청할 파라미터
	private SearchProductRequestVO getParam ( String page , String name ){
		vo = new SearchProductRequestVO ( );
		vo.setHt_name ( name );
		vo.setType ( "0" );
		vo.setPage ( page );
		vo.setSort ( ( ( CommonData ) getActivity ( ).getApplication ( ) ).getSortType ( ) );

		UserVO userVO = new UserVO ( );
		userVO = CommonData.getUserVO ( );

		if ( userVO.getHcdArray ( ) != null )
			vo.setHcd ( userVO.getHcd ( ) );

		if ( userVO.getScdArray ( ) != null )
			vo.setScd ( userVO.getScd ( ) );

		return vo;
	}

	@Override
	public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount )
	{
		int lastInScreen = firstVisibleItem + visibleItemCount;

		if ( ( lastInScreen == totalItemCount ) && lockListView == false )
		{
			currentPageNumber += 1;
			requestPageNumber += 1;

			executeTask ( );

			lockListView = true;
		}
	}

	@Override
	public void onScrollStateChanged ( AbsListView arg0 , int arg1 )
	{

	}

	private void InsertItemToListView ( ArrayList < TourProductVO > resultList )
	{
		for ( int i = 0 ; i < resultList.size ( ) ; i++ )
		{
			listViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		listViewAdapter.notifyDataSetChanged ( );
	}

	// 아이템 터치 이벤트
	private OnItemClickListener onClickListItem = new OnItemClickListener ( )
	{
		@Override
		public void onItemClick ( AdapterView < ? > arg0 , View arg1 , int position , long arg3 )
		{
			SmartTourProductsVO smartTourProductsVO = new SmartTourProductsVO ( );
			smartTourProductsVO.setTourProductVO ( listData.get ( position ) );
			CommonData.setSmartTourProductsVO ( smartTourProductsVO );

			Intent intent = new Intent ( getActivity ( ) , TourProductIntroduceActivity.class );
			intent.putExtra ( "fromMap" , fromMap );
			startActivity ( intent );

		}
	};

	@Override
	public void setJSONArrayResult ( ArrayList resultList )
	{
		lockListView = true;
		searchProductTask.cancel ( true );

		if ( resultList == null )
		{
			// countView.setText ( "총 0건" );
			if ( listData.size ( ) == 0 )
				resultText.setVisibility ( View.VISIBLE );
			return;
		}
		else
		{
			resultText.setVisibility ( View.GONE );
			// int count = resultList.size ( );
			// Log.d ( TAG , "검색 결과 : " + count + "건" );
			// countView.setText ( "총 " + count + "건" );
			InsertItemToListView ( resultList );
		}

		lockListView = false;
	}

}
