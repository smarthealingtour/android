package jejusmarttour.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

public class CCNTSearchResultListViewAdapter extends BaseAdapter{
	private static final String TAG = CCNTSearchResultListViewAdapter.class.toString ( );

	private ArrayList <SightSeeingContentsVO> listData;
	private Context context;
	private LayoutInflater inflater;

	public CCNTSearchResultListViewAdapter(Context context, ArrayList<SightSeeingContentsVO> listData){
		this.context = context;
		this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
		this.listData = listData;
	}

	// 현재 아이템의 수를 리턴
	@Override
	public int getCount ( )
	{
		return listData.size ( );
	}

	// 현재 아이템의 오브젝트를 리턴, Object를 상황에 맞게 변경하거나 리턴받은 오브젝트를 캐스팅해서 사용
	@Override
	public Object getItem ( int position )
	{
		return listData.get ( position );
	}

	// 아이템 position의 ID 값 리턴
	@Override
	public long getItemId ( int position )
	{
		return position;
	}
	
	// 출력 될 아이템 관리
	@Override
	public View getView ( int position , View convertView , ViewGroup parent ){
		SearchResultViewHolder viewHolder;

		// 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
		if ( convertView == null ){
			// view가 null일 경우 커스텀 레이아웃을 얻어 옴
			convertView = inflater.inflate ( R.layout.search_result_listview_adapter , parent , false );

			viewHolder = new SearchResultViewHolder ( );
			viewHolder.titleText = ( TextView ) convertView.findViewById ( R.id.htp_title );
			viewHolder.subTitleText = ( TextView ) convertView.findViewById ( R.id.htp_subtitle );

			convertView.setTag ( viewHolder );
		}
		else{
			viewHolder = ( SearchResultViewHolder ) convertView.getTag ( );
		}

		//Glide.with ( context ).load ( listData.get ( position ).getHt_img ( ) ).thumbnail ( 0.1f ).into ( viewHolder.imageView );
		viewHolder.titleText.setText ( listData.get ( position ).getCcnt_name() == null ? "이름없음" : listData.get ( position ).getCcnt_name());
		viewHolder.subTitleText.setText ( listData.get ( position ).getCcnt_addr() == null ? "설명없음" : listData.get ( position ).getCcnt_addr());

		return convertView;
	}

	@Override
	public void notifyDataSetChanged ( ){
		super.notifyDataSetChanged ( );
	}

	// 외부에서 아이템 추가 요청 시 사용
	public void add ( SightSeeingContentsVO vo ){
		listData.add ( vo );
	}

	// 외부에서 아이템 삭제 요청 시 사용
	public void remove ( int position ){
		listData.remove ( position );
	}
	
	public void removeAll(){
		listData.removeAll ( listData );
	}
}
