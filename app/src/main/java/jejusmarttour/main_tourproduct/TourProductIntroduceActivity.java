/**
 *
 */
package jejusmarttour.main_tourproduct;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.FontAppCompatActivity;
import jejusmarttour.common.CommonData;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.ScheduleTask;
import jejusmarttour.util.CustomViewPager;
import jejusmarttour.util.MyPagerSlidingTabStrip;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class TourProductIntroduceActivity extends FontAppCompatActivity implements JSONArrayResult < ScheduleVO >{
    private static final String TAG = TourProductIntroduceActivity.class.toString ( );

    private final int MAX_PAGE = 4;
    private Fragment cur_fragment = new Fragment ( );
    private ScheduleTask scheduleTask;

    private CustomViewPager viewPager;
    private MyPagerSlidingTabStrip tab;
    private TourProductVO tourProductVO;
    private SmartTourProductsVO stpVO;

    private View introduceTab;
    private View calendarTab;
    private View mapTab;
    private View reviewTab;
    private View clickedTab;

    @Override
    protected void onCreate ( Bundle savedInstanceState ){
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_healing_tour_product_intorduce );

        tourProductVO = CommonData.getSmartTourProductsVO ( ).getTourProductVO ( );

        scheduleTask = new ScheduleTask ( this );
        scheduleTask.execute ( tourProductVO.getHt_id ( ) );
    }

    // 액션바 설정
    private void initActionBar ( )  {
        ActionBar actionBar = getSupportActionBar ( );
        actionBar.setDisplayShowHomeEnabled ( false );
        actionBar.setDisplayShowTitleEnabled ( false );

        // 액션바 그림자 지우기
        actionBar.setElevation ( 0 );

        LayoutInflater mInflater = LayoutInflater.from ( this );

        View customView = mInflater.inflate ( R.layout.actionbar_listview , null );
        TextView titleTextView = customView.findViewById ( R.id.title_text );
        titleTextView.setText ( tourProductVO.getHt_name ( ) );

        LinearLayout backBtn = customView.findViewById ( R.id.back_btn );
        backBtn.setOnClickListener (view -> finish ( ));

        actionBar.setCustomView ( customView );
        actionBar.setDisplayShowCustomEnabled ( true );
    }

    //요거 바꾸면 위의 소개, 지도탭 바뀜
    public void initCustomTab(){
        introduceTab = getLayoutInflater().inflate(R.layout.view_tab,null);
        introduceTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_introduce_clicked));
        introduceTab.setTag( R.drawable.icon_introduce);
        TextView tv1 = introduceTab.findViewById(R.id.text);
        tv1.setText("소개");
        tv1.setTextColor( getResources().getColor(R.color.statusbar) );

        //클릭된 탭 설정(맨 처음 탭이니까)
        clickedTab = introduceTab;

        calendarTab = getLayoutInflater().inflate(R.layout.view_tab,null);
        calendarTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_calendar));
        calendarTab.setTag( R.drawable.icon_calendar);
        TextView tv2 = calendarTab.findViewById(R.id.text);
        tv2.setText("일정");
        tv2.setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );

        mapTab = getLayoutInflater().inflate(R.layout.view_tab,null);
        mapTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_map));
        mapTab.setTag( R.drawable.icon_map);
        TextView tv3 = mapTab.findViewById(R.id.text);
        tv3.setText("지도");
        tv3.setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );

        reviewTab = getLayoutInflater().inflate(R.layout.view_tab,null);
        reviewTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_review));
        reviewTab.setTag( R.drawable.icon_review);
        TextView tv4 = reviewTab.findViewById(R.id.text);
        tv4.setText("리뷰");
        tv4.setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );
    }

    private void initView ( ) {
        viewPager = ( CustomViewPager ) findViewById ( R.id.htp_intorduce_viewpager );
        viewPager.setAdapter ( new ViewPagerAdapter ( getSupportFragmentManager ( ) ) );
        viewPager.setOffscreenPageLimit ( 4 );

        tab = ( MyPagerSlidingTabStrip ) findViewById ( R.id.htp_intorduce_tabs );
        tab.setShouldExpand(false);
        tab.setTabChangeListener(position -> {
            clickedTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable((Integer)clickedTab.getTag()));
            ((TextView)clickedTab.findViewById(R.id.text)).setTextColor(((CommonData)getApplication()).getTabTextColorStateList());

            if (position == 0){
                introduceTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_introduce_clicked));
                ((TextView)introduceTab.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.statusbar));

                clickedTab = introduceTab;
            }
            else if (position == 1){
                calendarTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_calendar_clicked));
                ((TextView)calendarTab.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.statusbar));

                clickedTab = calendarTab;
            }
            else if (position == 2){
                mapTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_map_clicked));
                ((TextView)mapTab.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.statusbar));

                clickedTab = mapTab;
            }
            else {
                reviewTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_review_clicked));
                ((TextView)reviewTab.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.statusbar));

                clickedTab = reviewTab;
            }
        });
        setTabStyle ( );
        tab.setViewPager ( viewPager );
    }

    // 스와이프 ON OFF
    public void swipeOnOff ( boolean useSwipe ){
        viewPager.setSwipeable ( useSwipe );
    }

    // TAB 스타일 바꾸기
    @SuppressLint ( "NewApi" )
    @SuppressWarnings ( "deprecation" )
    private void setTabStyle ( ){
        tab.setDividerColor ( getResources ( ).getColor ( R.color.white ) );
        tab.setBackgroundColor ( getResources ( ).getColor ( R.color.white ) );
        tab.setIndicatorColor ( getResources ( ).getColor ( R.color.statusbar ) );
        tab.setTabPaddingLeftRight ( tab.getTabPaddingLeftRight ( ) - 14 );
    }

    @Override
    public void setJSONArrayResult ( ArrayList < ScheduleVO > resultList ) {
        scheduleTask.cancel ( true );

        if ( resultList == null ){
            return;
        }
        stpVO = new SmartTourProductsVO ( );
        stpVO.setTourProductVO ( tourProductVO );
        stpVO.setScheduleArrayList ( resultList );

        initCustomTab();
        initView ( );
        initActionBar ( );
    }

    @Override
    public boolean onCreateOptionsMenu ( Menu menu ) {
        getMenuInflater ( ).inflate ( R.menu.main , menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected ( MenuItem item ) {
        int id = item.getItemId ( );

        if ( id == R.id.action_settings ){
            CommonData.gotoMyInfo ( this );
            return true;
        }

        return super.onOptionsItemSelected ( item );
    }

    // 뷰페이지 어뎁터
    class ViewPagerAdapter extends FragmentPagerAdapter implements MyPagerSlidingTabStrip.CustomTabProvider{
        public ViewPagerAdapter ( FragmentManager fm ){
            super ( fm );
        }

        @Override
        public Fragment getItem ( int position ) {
            if ( position < 0 || MAX_PAGE <= position )
                return null;

            switch ( position ) {
                case 0 :
                    cur_fragment = TPIntroduceFragment.newInstance ( stpVO );
                    break;
                case 1 :
                    cur_fragment = yoHTPScheduleFragment.newInstance ( stpVO );
                    break;
                case 2 :
                    cur_fragment = HTPMapFragment.newInstance ( stpVO, viewPager );
                    break;
                case 3 :
                    cur_fragment = HTPReviewFragment.newInstance ( stpVO );
                    break;
            }
            return cur_fragment;
        }

        @Override
        public int getCount ( ){
            return MAX_PAGE;
        }

        @Override
        public CharSequence getPageTitle ( int position ) {
            return getResources ( ).getStringArray ( R.array.htp_sub_titles ) [ position ];
        }

        @Override
        public View getCustomTab(int position) {
            if (position == 0){
                return introduceTab;
            }
            else if (position == 1){
                return calendarTab;
            }
            else if (position == 2){
                return mapTab;
            }
            else {
                return reviewTab;
            }
        }
    }

    public String tag = "";

    public void setHTPMapFragmentTag(String tag){
        this.tag = tag;
    }

    public String getHTPMapFragmentTag() { return tag; }
}
