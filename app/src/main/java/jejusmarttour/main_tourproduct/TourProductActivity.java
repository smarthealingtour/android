package jejusmarttour.main_tourproduct;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import jejusmarttour.FontAppCompatActivity;
import jejusmarttour.common.CommonData;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.search.SearchProductActivity;
import jejusmarttour.util.MyPagerSlidingTabStrip;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class TourProductActivity extends FontAppCompatActivity implements OnClickListener, SearchView.OnQueryTextListener{
	private static final String TAG = TourProductActivity.class.toString ( );

	private final int MAX_PAGE = 2;
	private Fragment cur_fragment = new Fragment ( );
	private HTPProViewFragment htpProViewFragment;
	private HTPAmateurViewFragment htpAmateurViewFragment;

	private MyPagerSlidingTabStrip tab;

	private MenuItem mSearchItem;

	private View customTab1;
	private View customTab2;

	// 맞춤키워드 후 리프레쉬
	private static final int REQUEST_CODE = 0;

	private ColorStateList stateList;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_healing_tour_product );

		initFragment ( );	//뷰페이지들 설정
		initCustomTab();	//전문가,사용자탭 설정
		initViewPager();	//뷰페이저 설정

		((RadioGroup) findViewById(R.id.toggleGroup)).setOnCheckedChangeListener(ToggleListener);

		/*//키워드
		LinearLayout selectedLayout = (LinearLayout) findViewById(R.id.selected_keywords_layout);
		//밑에 키워드 뜨는거
		HorizontalScrollView horizontalScrollView = (HorizontalScrollView) findViewById(R.id.selected_keywords_layout_container);

		( ( RadioGroup ) findViewById ( R.id.toggleGroup ) ).setOnCheckedChangeListener ( ToggleListener );*/

//		ImageView keywordBtn = ( ImageView ) findViewById ( R.id.htp_keyword_btn );
//		keywordBtn.setOnClickListener ( this );

		/*setUserHasKeyword ( );*/
	}

	private void initFragment ( ){
		htpProViewFragment = new HTPProViewFragment ( ); // 전문가
		htpAmateurViewFragment = new HTPAmateurViewFragment ( ); // 함께추천
	}

	// TAB 스타일 바꾸기
	@SuppressWarnings ( "deprecation" )
	private void setTabStyle ( ){
		tab.setDividerColor ( getResources ( ).getColor ( R.color.gray ) );
		tab.setBackgroundColor ( getResources ( ).getColor ( R.color.gray ) );
		tab.setIndicatorColor ( getResources ( ).getColor ( R.color.statusbar ) );
		tab.setTextColor ( getResources ( ).getColor ( R.color.white ) );
	}

	private void initCustomTab(){
		customTab1 = getLayoutInflater().inflate(R.layout.view_tab,null);
		customTab1.setTag("expert");

		customTab2 = getLayoutInflater().inflate(R.layout.view_tab,null);
		customTab2.findViewById(R.id.first_img).setVisibility(View.GONE);
		customTab2.findViewById(R.id.second_img).setVisibility(View.VISIBLE);		//유저이미지가 작아서 따로 만들어서사용
		customTab2.setTag("user");
		TextView tv = customTab2.findViewById(R.id.text);
		tv.setText("사용자 추천");

		//텍스트뷰 클릭시 글자색 바꿀라구
		int[][] states = new int[][]{new int[]{ android.R.attr.state_pressed},new int[]{} }; //첫번째는 클릭, 두번째꺼는 평상시
		int[] color = new int[]{Color.parseColor("#ED6148"),Color.parseColor("#575757")};
		stateList = new ColorStateList(states,color);
		tv.setTextColor(stateList);
	}

	private void initViewPager(){
		ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
		viewPager.setAdapter ( new ViewPagerAdapter ( getSupportFragmentManager ( ) )  );
		viewPager.setOffscreenPageLimit ( 2 );		//뷰페이저 스크린수최대 2개
		tab = (MyPagerSlidingTabStrip) findViewById ( R.id.htp_tabs );
		tab.setShouldExpand(true);
		tab.setTabChangeListener( position -> {
            if (position == 0)	clickExpert();
            else 			  	clickUser();
        });

		setTabStyle ( );
		tab.setViewPager (viewPager);
	}

	/*// 유저가 저장한 키워드 셋팅
	private void setUserHasKeyword ( ){
		selectedLayout.removeAllViewsInLayout ( );
		selectedLayout.invalidate ( );

		if ( userVO.hasKeywords ( ) == false ){
			horizontalScrollView.setVisibility ( View.GONE );
			return;
		}

		horizontalScrollView.setVisibility ( View.VISIBLE );
		addSelectedKeyword ( );
	}*/

	// 토글 버튼
	final RadioGroup.OnCheckedChangeListener ToggleListener = (radioGroup, i) -> {
        for ( int j = 0; j < radioGroup.getChildCount ( ); j++ ){
            final ToggleButton view = ( ToggleButton ) radioGroup.getChildAt ( j );
            view.setChecked ( view.getId ( ) == i );

            if ( i == R.id.htp_currently_btn ){
                ( ( CommonData ) getApplication ( ) ).setSortType ( "date" );
            }
            else{
                ( ( CommonData ) getApplication ( ) ).setSortType ( "cart" );
            }

            if ( view.isChecked()){
                view.setTextColor ( radioGroup.getResources ( ).getColor ( R.color.white ) );
				view.setBackgroundColor(radioGroup.getResources().getColor(R.color.statusbar));
            }
            else{
                view.setTextColor ( radioGroup.getResources ( ).getColor ( R.color.statusbar ) );
				view.setBackgroundColor(radioGroup.getResources().getColor(R.color.white));
            }

            refreshListView ( );
        }
    };

	private void refreshListView ( ){
		htpProViewFragment.refreshListView ( );
		htpAmateurViewFragment.refreshListView ( );
	}

	public void onToggle ( View view ){
		( ( RadioGroup ) view.getParent ( ) ).check ( view.getId ( ) );
	}

	/*private void addSelectedKeyword ( )	{
		if ( userVO.getHcdArray ( ) != null ){
			for ( int i = 0; i < userVO.getHcdArray ( ).length; i++ ){
				TextView textView = KeywordData.getSelectedKeywordTextView ( this , userVO.getHcdArray ( ) [ i ] , CommonData.getKeywordArrayList ( ) );
				textView.setTag ( userVO.getHcdArray ( ) [ i ] );
				selectedLayout.addView ( textView );
			}
		}
		if ( userVO.getScdArray ( ) != null ){
			for ( int i = 0; i < userVO.getScdArray ( ).length; i++ ){
				TextView textView = KeywordData.getSelectedKeywordTextView ( this , userVO.getScdArray ( ) [ i ] , CommonData.getKeywordArrayList ( ) );
				textView.setTag ( userVO.getScdArray ( ) [ i ] );
				selectedLayout.addView ( textView );
			}
		}
	}*/

	public void onProItemClick ( TourProductVO vo )	{
		Intent intent = new Intent ( this , TourProductIntroduceActivity.class );
		SmartTourProductsVO smartTourProductsVO = new SmartTourProductsVO ( );
		smartTourProductsVO.setTourProductVO ( vo );
		CommonData.setSmartTourProductsVO ( smartTourProductsVO );
		startActivity ( intent );
	}

	public void onAmateurItemClick ( TourProductVO vo )	{
		Intent intent = new Intent ( this , TourProductIntroduceActivity.class );
		SmartTourProductsVO smartTourProductsVO = new SmartTourProductsVO ( );
		smartTourProductsVO.setTourProductVO(vo);
		CommonData.setSmartTourProductsVO(smartTourProductsVO);
		startActivity(intent);
	}

	public void onProItemClick ( ScheduleVO vo ){
		CommonData.setScheduleVO(vo);
		Intent intent = new Intent ( this , DetailInfoActivity.class );
		startActivity(intent);
	}

	// 액션바 메뉴
	@Override
	public boolean onCreateOptionsMenu ( Menu menu ){
		ActionBar actionBar = getSupportActionBar();

		// 액션바 그림자 지우기
		actionBar.setElevation(0);

		LayoutInflater mInflater = LayoutInflater.from ( this );
		View customView = mInflater.inflate(R.layout.actionbar_listview, null);

		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( R.string.title_activity_healing_tour_product );

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.search, menu);

		LinearLayout backBtn = customView.findViewById ( R.id.back_btn );

		backBtn.setOnClickListener (view -> finish ( ));
		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled(true);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setOnQueryTextListener(this);
		mSearchItem = menu.getItem(0);

		MenuItem search_result_delete = menu.findItem(R.id.search_result_delete);

//            if(searchResult != null){
//                if(searchResult.openShowResult.getVisibility() == View.VISIBLE)
//                    search_result_delete.setVisible(true);
//            }

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item ){
		int id = item.getItemId ( );

		if ( id == R.id.action_settings ){
			CommonData.gotoMyInfo ( this );
			return true;
		}
		else if (id  == android.R.id.home ) {
			finish();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onQueryTextSubmit(String query) {

		mSearchItem.collapseActionView();//검색후 닫기
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setTitle(query);
		searchProduct(query);

		return false;
	}

	//검색 수행
	private void searchProduct(String query) {
		Intent intent = new Intent( this , SearchProductActivity.class );
		intent.putExtra("data", query);
		startActivity(intent);
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}//query callback

	@Override
	protected void onActivityResult ( int requestCode , int resultCode , Intent data )	{
		super.onActivityResult ( requestCode , resultCode , data );

		if ( requestCode == REQUEST_CODE ){
			/*setUserHasKeyword ( );*/
			refreshListView ( );
		}
	}

	@Override
	public void onClick ( View v )	{
		/*if ( v.getId ( ) == R.id.htp_keyword_btn ) // 맞춤 키워드 클릭
		{
			Intent intent = new Intent ( this , SelectKeywordActivity.class );
			startActivityForResult ( intent , RERQUEST_KEYWORD );
			//finish ( );
		}*/
	}

	private void clickExpert(){
		customTab1.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_expert_clicked));
		TextView tv1 = customTab1.findViewById(R.id.text);
		tv1.setTextColor( getResources().getColor(R.color.statusbar));

		customTab2.findViewById(R.id.second_img).setBackground(getResources().getDrawable(R.drawable.icon_user_recom));
		TextView tv = customTab2.findViewById(R.id.text);
		tv.setTextColor(stateList);
	}

	private void clickUser(){
		customTab1.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_expert_recom));
		TextView tv1 = customTab1.findViewById(R.id.text);
		tv1.setTextColor( stateList);

		customTab2.findViewById(R.id.second_img).setBackground(getResources().getDrawable(R.drawable.ic_user_recom_clicked));
		TextView tv2 = customTab2.findViewById(R.id.text);
		tv2.setTextColor(getResources().getColor(R.color.statusbar));
	}

	// 뷰페이지 어뎁터 탭 선택
	class ViewPagerAdapter extends FragmentPagerAdapter	implements MyPagerSlidingTabStrip.CustomTabProvider{
		private int position = 0;

		public ViewPagerAdapter ( FragmentManager fm )
		{
			super ( fm );
		}

		@Override
		public Fragment getItem ( int position ){
			if ( position < 0 || MAX_PAGE <= position )
				return null;

			this.position = position;

			switch ( position )	{
				case 0 :
					cur_fragment = htpProViewFragment; // 전문가
					break;
				case 1 :
					cur_fragment = htpAmateurViewFragment; // 함께 추천
					break;
			}
			return cur_fragment;
		}

		public int getCurrentPosition ( ){
			return position;
		}

		@Override
		public int getCount ( ){
			return MAX_PAGE;
		}

		@Override
		public CharSequence getPageTitle ( int position ){
			return getResources ( ).getStringArray ( R.array.htp_main_titles ) [ position ];
		}

		@Override
		public View getCustomTab(int position) {
			if (position == 0){
				return customTab1;
			}
			else if (position == 1){
				return customTab2;
			}
			return null;
		}
	}
}