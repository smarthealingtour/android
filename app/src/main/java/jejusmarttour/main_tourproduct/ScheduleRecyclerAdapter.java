package jejusmarttour.main_tourproduct;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2018-03-06.
 */

public class ScheduleRecyclerAdapter extends RecyclerView.Adapter<ScheduleRecyclerAdapter.ScheduleViewHolder> {
    private Context context;
    private ArrayList<ScheduleVO> provider;
    private int day = 0;

    ScheduleRecyclerAdapter(Context context, List<ScheduleVO> provider){
        this.context = context;
        this.provider = new ArrayList<>(provider);
    }

    @Override
    public ScheduleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext()).inflate(R.layout.item_schedule, parent, false);

        return new ScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScheduleViewHolder holder, int position) {
        holder.sequence.setText( "" + (position+1) );

        //날짜 변경시점 표시
        if ( provider.get(position).getSeq().equals("1")){
            day = Integer.parseInt(provider.get(position).getDay());
            holder.day.setText( day + "Days");
            holder.dayLayout.setVisibility(View.VISIBLE);
        }
        else holder.dayLayout.setVisibility(View.GONE);

        if (provider.get(position).getImageArray() != null){
            Glide.with ( context ).load ( CommonData.getImageAddress () + provider.get(position).getImageArray()[0] ).into ( holder.img );
        }
        else {
            Glide.with ( context ).load ( CommonData.getImageAddress () + provider.get(position).getImage() ).into ( holder.img );
        }

        if (provider.get(position).getHealingContentsVO() != null){
            HealingContentsVO hcvo = provider.get(position).getHealingContentsVO();

            holder.title.setText(hcvo.getHcnt_name());
            holder.address.setText(hcvo.getHcnt_addr());
            holder.time.setText(hcvo.getHcnt_duration() + "분");
            holder.cost.setText(hcvo.getHcnt_cost_adult());

            holder.moveTime.setText(" - 분");
            holder.moveDistance.setText(" - Km");
        }
        else {
            SightSeeingContentsVO scvo = provider.get(position).getSightSeeingContentsVO();

            holder.title.setText(scvo.getCcnt_name());
            holder.address.setText(scvo.getCcnt_addr());
            holder.time.setText(scvo.getCcnt_duration());
            holder.cost.setText(" - ");

            holder.moveTime.setText(" - 분");
            holder.moveDistance.setText(" - Km");
        }

        //메모추가
        ArrayList<String> memos = provider.get(position).getMemo();
        holder.memoLayout.removeAllViews();
        for (String memo :
                memos) {

            Log.e("메모내용","memo: " + memo );
            memoViewCreate(holder.memoLayout,memo);
        }

        if ( position == getItemCount()-1 ){
            holder.distanceLayout.setVisibility(View.GONE);
        }
    }

    void changeProvider(List<ScheduleVO> provider){
        this.provider.clear();
        this.provider.addAll(provider);
    }

    private void memoViewCreate(ViewGroup parent, String memo){
        View view = LayoutInflater.from( parent.getContext()).inflate(R.layout.view_schedule_memo, parent, false);
        parent.addView(view);

        ((TextView)view.findViewById(R.id.memo_text)).setText(memo);
        view.findViewById(R.id.delete_btn).setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return provider.size();
    }

    class ScheduleViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        FrameLayout dayLayout;
        LinearLayout memoLayout;
        LinearLayout distanceLayout;
        TextView day;
        TextView sequence;
        TextView title;
        TextView address;
        TextView time;
        TextView cost;
        TextView moveTime;
        TextView moveDistance;


        ScheduleViewHolder(View itemView) {
            super(itemView);
            dayLayout = itemView.findViewById(R.id.day_layout);
            memoLayout = itemView.findViewById(R.id.memo_layout);
            distanceLayout = itemView.findViewById(R.id.distance_layout);
            day = itemView.findViewById(R.id.day_text);
            sequence = itemView.findViewById(R.id.num_sequence);
            img = itemView.findViewById(R.id.title_img);
            title = itemView.findViewById(R.id.title_text);
            address = itemView.findViewById(R.id.address_text);
            time = itemView.findViewById(R.id.time_text);
            cost = itemView.findViewById(R.id.cost_text);
            moveDistance = itemView.findViewById(R.id.moving_distance);
            moveTime = itemView.findViewById(R.id.moving_time);
        }
    }
}
