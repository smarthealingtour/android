package jejusmarttour.main_tourproduct;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.common.KeywordData;
import jejusmarttour.main_mytourproduct.MyTourProductActivity;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.main_spot.detailinfo.FoodStayDetailInfoActivity;
import jejusmarttour.main_tourproduct.test.SequenceAdapter;
import jejusmarttour.parser.ScheduleParser;
import jejusmarttour.task.HTPCartTask;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductSequenceVO;
import jejusmarttour.vo.TourProductVO;
import jejusmarttour.vo.UpdateCartRequestVO;
import jejusmarttour.vo.UpdateResultVO;
import syl.com.jejusmarttour.R;

public class TPIntroduceFragment extends Fragment implements OnClickListener , OnGlobalLayoutListener , JSONObjectResult < UpdateResultVO >{
	private static final String TAG = TPIntroduceFragment.class.toString ( );

	private TextView contentText;
	private boolean isCloseTextView = true;
	private ImageButton cartBtn;
	private Button moreBtn;
	private TourProductVO tourProductVO;
	private ArrayList < ScheduleVO > scheduleArrayList;

	private int MAX_PAGE = 0;
	private int mPrevPosition; // 이전에 선택되었던 포지션 값

	private LinearLayout mPageMark; // 현재 몇 페이지 인지 나타내는 뷰
	private SmartTourProductsVO smartTourProductsVO;

	private ViewGroup keywordDetailContainer;

	private HTPCartTask htpCartTask;

	private RadioGroup radioGroup;
	private ArrayList < ScheduleVO > scheduleVOs;

	private int MAX_DAYS = 1;
	private int CURRENT_DAY = 0;

	private RecyclerView sequenceRecyclerView;
	private SequenceAdapter sequenceAdapter;

	public static TPIntroduceFragment newInstance (SmartTourProductsVO smartTourProductsVO ){
		TPIntroduceFragment fragment = new TPIntroduceFragment( );
		fragment.tourProductVO = new TourProductVO ( );
		fragment.smartTourProductsVO = smartTourProductsVO;

		fragment.tourProductVO = smartTourProductsVO.getTourProductVO ( );
		fragment.scheduleArrayList = smartTourProductsVO.getScheduleArrayList ( );

		return fragment;
	}

	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ) {
		View view = null;
		try	{
			view = inflater.inflate ( R.layout.fragment_htpintorduce , container , false );
		}
		catch ( InflateException e ){
			e.printStackTrace ( );
		}

		assert view != null;
		sequenceRecyclerView = view.findViewById(R.id.sequence_list);

		// 이미지 뷰 페이저
		mPageMark = view.findViewById ( R.id.page_mark );
		ViewPager mPager = view.findViewById(R.id.viewpager);

		contentText = view.findViewById ( R.id.htp_description );
		TextView titleText = view.findViewById ( R.id.htp_title_text );

		// 스와이프 OFF
		( (TourProductIntroduceActivity) getActivity ( ) ).swipeOnOff ( false );

        if(tourProductVO.getHt_imgArray() != null) {
            MAX_PAGE = tourProductVO.getHt_imgArray ( ).length;
        }
		mPager.setAdapter ( new ImageViewPagerAdapter ( getActivity ( ) , tourProductVO.getHt_imgArray ( ) , MAX_PAGE ) );

        contentText.setText ( tourProductVO.getHt_intro ( ) );
		titleText.setText ( tourProductVO.getHt_name ( ) );

        if(MAX_PAGE > 1)
		    mPager.setOffscreenPageLimit ( MAX_PAGE - 1 );
			mPager.addOnPageChangeListener (new OnPageChangeListener ( ){
			@Override
			public void onPageSelected ( int position )	{
				mPageMark.getChildAt ( mPrevPosition ).setBackgroundResource ( R.drawable.page_not ); // 이전
				mPageMark.getChildAt ( position ).setBackgroundResource ( R.drawable.page_select ); // 현재
				mPrevPosition = position; // 이전 포지션 값을 현재로 변경
			}

			@Override
			public void onPageScrolled ( int position , float positionOffset , int positionOffsetPixels ){
			}

			@Override
			public void onPageScrollStateChanged ( int state ){
			}
		} );

		TextView durationText = view.findViewById ( R.id.htp_session_time );
		TextView coatText = view.findViewById ( R.id.htp_price );

		coatText.setText ( tourProductVO.getCost ( ) == null ? "- 원" : SmartTourUtils.getCost ( tourProductVO.getCost ( ) ) );
		durationText.setText ( tourProductVO.getDuraion ( ) == null ? "- 분" : SmartTourUtils.getDuration ( tourProductVO.getDuraion ( ) ) );

		initPageMark ( ); // 현재 페이지 표시하는 뷰 초기화

		cartBtn = view.findViewById ( R.id.cart_btn );
		moreBtn = view.findViewById ( R.id.htp_more_btn );
		keywordDetailContainer = view.findViewById ( R.id.keyword_detail_layout );

		cartBtn.setOnClickListener ( this );
		moreBtn.setOnClickListener ( this );

		addDetailKeyword ( );
		view.getViewTreeObserver ( ).addOnGlobalLayoutListener ( this );

		// 상품일정 토글 바
		radioGroup = view.findViewById ( R.id.toggleGroup );
		initScheduleToggleGroup ( );

		addSchedule ( CURRENT_DAY );

		return view;
	}

	 // 이미지 뷰페이저 하단의 현재 페이지 표시하는 뷰 초기화
	private void initPageMark ( ){
		for ( int i = 0 ; i < MAX_PAGE ; i++ ){
			ImageView iv = new ImageView ( getActivity ( ) ); // 페이지
			iv.setLayoutParams ( new LayoutParams ( LayoutParams.WRAP_CONTENT , LayoutParams.WRAP_CONTENT ) );

			// 첫 페이지 표시 이미지 이면 선택된 이미지로
			if ( i == 0 )
				iv.setBackgroundResource ( R.drawable.page_select );
			else // 나머지는 선택안된 이미지로
				iv.setBackgroundResource ( R.drawable.page_not );

			// LinearLayout에 추가
			mPageMark.addView ( iv );
		}
		mPrevPosition = 0; // 이전 포지션 값 초기화
	}

	// 상품 구성 및 여행순서 아이템 만들기
	@SuppressLint ( "Recycle" )
	private void getSequenceLayout ( ArrayList <TourProductSequenceVO> sequenceArr ){

		LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
		sequenceRecyclerView.setLayoutManager(layoutManager);

		if ( sequenceAdapter == null ){
			sequenceAdapter = new SequenceAdapter(sequenceArr);
			sequenceAdapter.setItemClickListener(v -> {
				Intent intent;
				ScheduleVO scheduleVO = smartTourProductsVO.getScheduleArrayList ( ).get ( Integer.valueOf ( v.getTag ( ).toString ( ) ) );
                CommonData.setScheduleVO ( scheduleVO );
                if (scheduleVO.getHealingContentsVO() != null){
					intent = new Intent ( getActivity ( ) , DetailInfoActivity.class );
				}
				else {
					intent = new Intent( getActivity(), FoodStayDetailInfoActivity.class);
				}

                startActivity ( intent );
            });

			sequenceRecyclerView.setAdapter( sequenceAdapter );
		}
		else {
			sequenceAdapter.setVOs(sequenceArr);
		}

	}

	// 여행상품목록 데이터 가져오기
	private ArrayList <TourProductSequenceVO> getSequenceData ( ){
		ArrayList <TourProductSequenceVO> arItem = new ArrayList<>();

		for ( int i = 0 ; i < scheduleVOs.size ( ) ; i++ ){
			TourProductSequenceVO vo = new TourProductSequenceVO( );

			vo.setCategory ( getKeyword ( scheduleVOs.get ( i ) ) );
			vo.setSeq( Integer.parseInt(scheduleVOs.get(i).getSeq()) );

			if ( scheduleVOs.get ( i ).getHealingContentsVO( ) != null ){
				vo.setTitle ( scheduleVOs.get ( i ).getHealingContentsVO( ).getHcnt_name ( ) );
				vo.setIconId ( CommonData.getLocationIconId ( scheduleVOs.get ( i ).getHealingContentsVO( ).getHcnt_type ( ) ) );
			}
			else if ( scheduleVOs.get ( i ).getSightSeeingContentsVO( ) != null ){
				vo.setTitle ( scheduleVOs.get ( i ).getSightSeeingContentsVO( ).getCcnt_name ( ) );
				vo.setIconId ( CommonData.getLocationIconId ( "point" ) );
			}

			arItem.add ( vo );
		}

		return arItem;
	}

	@Override
	public void onClick ( View v ){
		if ( v.getId ( ) == moreBtn.getId ( ) ){
			if (isCloseTextView){
				isCloseTextView = false;
				contentText.setMaxLines ( 100 );
				moreBtn.setText ( getResources ( ).getString ( R.string.more_write_btn_close ) );
				contentText.invalidate ( );
			}
			else{
				isCloseTextView = true;
				contentText.setMaxLines ( 5 );
				moreBtn.setText ( getResources ( ).getString ( R.string.more_write_btn ) );
				contentText.invalidate ( );
			}

		}
		else if ( v.getId ( ) == cartBtn.getId ( ) ){
//			addCartPopUp ( );
            htpCartTask = new HTPCartTask ( getActivity ( ) , this );

            UpdateCartRequestVO requestVO = new UpdateCartRequestVO ( );
            requestVO.setType ( "add" );
            requestVO.setEmail ( CommonData.getUserVO ( ).getEmail ( ) );

            requestVO.setUser ( false );
            requestVO.setProductID ( tourProductVO.getHt_id ( ) );

            htpCartTask.execute ( requestVO );
		}
	}

	// 장바구니 담기 팝업
	private void addCartPopUp ( ){
		LinearLayout layout = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.popup_cart , null );

		TextView title_text = layout.findViewById ( R.id.title_text );

		((TextView)layout.findViewById ( R.id.popup_title )).setText("나의 여행상품");
		title_text.setText ( smartTourProductsVO.getTourProductVO ( ).getHt_name ( ) + " 을(를)" );

		AlertDialog.Builder alertBox = new AlertDialog.Builder ( getActivity ( ) );
		alertBox.setView ( layout );
		alertBox.create ( );
		final DialogInterface popup = alertBox.show ( );

		Button cartBtn = layout.findViewById ( R.id.goto_cart_btn );

		cartBtn.setOnClickListener (v -> {
            Intent intent = new Intent ( getActivity ( ) , MyTourProductActivity.class );
            startActivity ( intent );
            getActivity ( ).finish ( );
        });

		Button cancelBtn =  layout.findViewById ( R.id.cancel_btn );
		cancelBtn.setOnClickListener (v -> popup.dismiss ( ));

	}

	@Override
	public void onGlobalLayout ( ){
		int lineCount = contentText.getLineCount ( );

		if ( lineCount < 5 ){
			moreBtn.setVisibility ( View.GONE );
		}
		else{
			moreBtn.setVisibility ( View.VISIBLE );
		}
		super.onResume ( );
	}

	// 상품목록 맞춤 키워드 카테고리 가져오기
	private String getKeyword ( ScheduleVO vo ){
		String str = "";

		if ( vo.getHealingContentsVO( ) != null ){
			if ( vo.getHealingContentsVO( ).getHcnt_hcd_name ( ) != null ){
				if ( vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).size ( ) > 0 )
					str = vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).get ( 0 ).getCode_name ( );
			}
			else if ( vo.getHealingContentsVO( ).getHcnt_scd_name ( ) != null ){
				if ( vo.getHealingContentsVO( ).getHcnt_scd_name ( ).size ( ) > 0 )
					str = vo.getHealingContentsVO( ).getHcnt_scd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getHealingContentsVO( ).getHcnt_scd_name ( ).get ( 0 ).getCode_name ( );
			}
		}
		else if ( vo.getSightSeeingContentsVO( ) != null ){
			if ( vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ) != null ){
				if ( vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).size ( ) > 0 )
					str = vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).get ( 0 ).getCode_name ( );
			}
		}

		return str;
	}

	// keywordDetailContainer 에 담길 키워드
	private void addDetailKeyword ( ){
		ArrayList < View > arrayList = new ArrayList<>();

		TextView textView;

		if ( tourProductVO.getHt_hcdArray ( ) != null ){
			for ( int i = 0 ; i < tourProductVO.getHt_hcdArray ( ).length ; i++ ){
				ImageView imageView = getImageView ( );
				Glide.with ( getActivity ( ) ).load ( KeywordData.getKeywordIconImage ( tourProductVO.getHt_hcdArray ( ) [ i ] ) ).thumbnail ( 0.1f ).into ( imageView );
				textView = KeywordData.getKeywordDetailTextView ( getActivity ( ) , tourProductVO.getHt_hcdArray ( ) [ i ] , CommonData.getKeywordArrayList ( ) );
				LinearLayout layout = getLinearLayout ( );
				layout.addView ( textView );

				if (!isContainCategory(i, tourProductVO.getHt_hcdArray()))
					arrayList.add ( imageView );

				arrayList.add ( layout );
			}
		}

		if ( tourProductVO.getHt_scdArray ( ) != null ){
			for ( int i = 0 ; i < tourProductVO.getHt_scdArray ( ).length ; i++ ){
				ImageView imageView = getImageView ( );
				Glide.with ( getActivity ( ) ).load ( KeywordData.getKeywordIconImage ( tourProductVO.getHt_scdArray ( ) [ i ] ) ).thumbnail ( 0.1f ).into ( imageView );
				textView = KeywordData.getKeywordDetailTextView ( getActivity ( ) , tourProductVO.getHt_scdArray ( ) [ i ] , CommonData.getKeywordArrayList ( ) );
				LinearLayout layout = getLinearLayout ( );
				layout.addView ( textView );

				if (!isContainCategory(i, tourProductVO.getHt_scdArray()))
					arrayList.add ( imageView );

				arrayList.add ( layout );
			}
		}

		// Display display = ( ( WindowManager ) getActivity().getSystemService
		// ( Context.WINDOW_SERVICE ) ).getDefaultDisplay ( );
		keywordDetailContainer.removeAllViews ( );

		for ( int i = 0 ; i < arrayList.size ( ) ; i++ ){
			keywordDetailContainer.addView ( arrayList.get ( i ) );
		}

	}

	private boolean isContainCategory ( int position , String [ ] strings ){
		if ( strings == null )
			return true;

		if ( position == 0 )
			return false;

		if ( position >= 1 ){
			if (!SmartTourUtils.replaceSpaceToBlank(strings[position - 1]).contains(SmartTourUtils.replaceSpaceToBlank(strings[position]).substring(0, 3)))
				return false;
		}

		return true;
	}

	private ImageView getImageView ( ){
		DisplayMetrics dm = getActivity ( ).getResources ( ).getDisplayMetrics ( );
		int size = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 30 , dm );

		ImageView imageView = new ImageView ( getActivity ( ) );
		imageView.setLayoutParams ( new LayoutParams ( size , size ) );
		imageView.setScaleType ( ScaleType.FIT_XY );

		return imageView;
	}

	private LinearLayout getLinearLayout ( ){
		DisplayMetrics dm = getActivity ( ).getResources ( ).getDisplayMetrics ( );
		int size = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 30 , dm );

		LinearLayout layout = new LinearLayout ( getActivity ( ) );
		layout.setLayoutParams ( new LayoutParams ( LinearLayout.LayoutParams.WRAP_CONTENT , size ) );
		layout.setGravity ( Gravity.CENTER_VERTICAL );

		return layout;
	}

	@Override
	public void setJSONObjectResult ( UpdateResultVO result ){
		htpCartTask.cancel ( true );

		if ( result == null ){
			Log.d ( TAG , "결과값이 존재하지 않습니다." );
			return;
		}

		if ( result.getResult ( ).equals ( "success" ) ){
			Toast.makeText ( getActivity ( ) , "저장되었습니다." , Toast.LENGTH_SHORT ).show ( );
            addCartPopUp ( );
		}
		else if ( result.getResult ( ).equals ( "duplicate" ) ){
			Toast.makeText ( getActivity ( ) , "이미 보유하신 상품입니다." , Toast.LENGTH_SHORT ).show ( );
		}
	}

	private void addSchedule ( int position ){
		scheduleVOs = ScheduleParser.getSchedule ( scheduleArrayList , position );

		ArrayList <TourProductSequenceVO> sequenceArr = getSequenceData ( );

		getSequenceLayout ( sequenceArr );
	}

	// 일정보기 일자별 토글 버튼 생성
	private void initScheduleToggleGroup ( ){
		MAX_DAYS = Integer.valueOf ( scheduleArrayList.get ( 0 ).getDays ( ) );

		if ( MAX_DAYS == 1 ){
			LinearLayout toggleView = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.toggle_schedule , null );
			toggleView.setTag ( toggleView.getId ( ) );
			ToggleButton toggleButton = toggleView.findViewWithTag ( "toggle_btn" );
			toggleButton.setChecked ( true );
			toggleButton.setTextOn ( "1일" );
			toggleButton.setTextOff ( "1일" );
			toggleButton.setText ( "1일" );
			radioGroup.addView ( toggleView );
			radioGroup.invalidate ( );
			return;
		}

		for ( int i = 0 ; i <= MAX_DAYS ; i++ ){
			LinearLayout toggleView = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.toggle_schedule , null );
			toggleView.setTag ( i );
			ToggleButton toggleButton = toggleView.findViewWithTag ( "toggle_btn" );
			toggleButton.setId ( 9999 + i );
			toggleButton.setOnClickListener (view -> {
                Log.d ( TAG , "일정 보기 postion : " + ( ( LinearLayout ) view.getParent ( ) ).getTag ( ).toString ( ) );

                boolean isChecked = ( ( ToggleButton ) view ).isChecked ( );
                Log.d ( TAG , "isChecked : " + isChecked );

                if (!isChecked){
                    ( ( ToggleButton ) view ).setChecked ( true );
                    return;
                }

                addSchedule ( Integer.valueOf ( ( ( LinearLayout ) view.getParent ( ) ).getTag ( ).toString ( ) ) );
                radioGroup.check ( view.getId ( ) );
            });

			if ( i == 0 ){
				toggleButton.setChecked ( true );
				toggleButton.setTextOn ( "전체" );
				toggleButton.setTextOff ( "전체" );
				toggleButton.setText ( "전체" );
			}
			else{
				toggleButton.setTextOn ( i + "일" );
				toggleButton.setTextOff ( i + "일" );
				toggleButton.setText ( i + "일" );
			}
			radioGroup.addView ( toggleView );
		}
		radioGroup.invalidate ( );
		radioGroup.setOnCheckedChangeListener ( ToggleListener );
	}

	// 토글 버튼
	final RadioGroup.OnCheckedChangeListener ToggleListener = (radioGroup, id) -> {
        for ( int j = 0 ; j < radioGroup.getChildCount ( ) ; j++ ){
            final LinearLayout view = ( LinearLayout ) radioGroup.getChildAt ( j );
            final ToggleButton toggleButton = view.findViewWithTag ( "toggle_btn" );

            toggleButton.setChecked ( toggleButton.getId ( ) == id );

            if (toggleButton.isChecked()){
                toggleButton.setTextColor ( radioGroup.getResources ( ).getColor ( R.color.font_color_black ) );
            }
            else{
                toggleButton.setTextColor ( radioGroup.getResources ( ).getColor ( R.color.font_color ) );
            }
        }
    };

}
