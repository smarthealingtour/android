package jejusmarttour.main_tourproduct;

import com.bumptech.glide.Glide;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import syl.com.jejusmarttour.R;

public class ImageViewPageFragment extends Fragment
{
	private int imageSrc;

	public static ImageViewPageFragment create ( int pageNumber , String  image )
	{
		ImageViewPageFragment fragment = new ImageViewPageFragment ( );
		Bundle args = new Bundle ( );
		args.putString ( "image" , image );
		fragment.setArguments ( args );
		return fragment;
	}

	@Override
	public void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate ( savedInstanceState );
		imageSrc = getArguments ( ).getInt ( "image" );
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState )
	{
		ViewGroup rootView = ( ViewGroup ) inflater.inflate ( R.layout.fragment_image_viewpage , container , false );
		ImageView imageView = ( ( ImageView ) rootView.findViewById ( R.id.viewpage_image ) );
		Glide.with ( getActivity() ).load ( imageSrc ).thumbnail ( 0.1f ).into ( imageView );
		return rootView;
	}
}
