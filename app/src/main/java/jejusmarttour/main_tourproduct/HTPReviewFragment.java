package jejusmarttour.main_tourproduct;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.task.ObjectResult;
import jejusmarttour.task.ReviewCountTask;
import jejusmarttour.task.ReviewParseTask;
import jejusmarttour.task.ReviewUpdateTask;
import jejusmarttour.vo.HTPReviewVO;
import jejusmarttour.vo.ReviewCountVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.UpdateResultVO;
import syl.com.jejusmarttour.R;

public class HTPReviewFragment extends Fragment
		implements OnClickListener , JSONArrayResult , OnScrollListener , JSONObjectResult < UpdateResultVO > , ObjectResult{
	private static final String TAG = HTPReviewFragment.class.toString ( );

	private TextView reviewCountText;
	private HTPReviewAdapter listViewAdapter;
	private ReviewParseTask reviewParseTask;
	private ReviewUpdateTask reviewUpdateTask;
	private ReviewCountTask reviewCountTask;
	private EditText editText;
	private String [ ] data = new String [ 2 ];

	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private final int MIN_REVIEW_TEXT_LENGTH = 10;
	private SmartTourProductsVO smartTourProductsVO = null;
    String [ ] countStr = null;

	public static HTPReviewFragment newInstance ( SmartTourProductsVO smartTourProductsVO ){
		HTPReviewFragment fragment = new HTPReviewFragment ( );
		fragment.smartTourProductsVO = smartTourProductsVO;

		return fragment;
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
		View view = null;

		try{
			view = inflater.inflate ( R.layout.fragment_htpreview , container , false );
		}
		catch ( InflateException e ){
			e.printStackTrace ( );
		}

		assert view != null;
		reviewCountText = view.findViewById ( R.id.htp_review_count_text );
		Button reviewWriteBtn = view.findViewById ( R.id.htp_review_write_btn );
		reviewWriteBtn.setOnClickListener ( this );

		ArrayList<HTPReviewVO> listData = new ArrayList<>();
		lockListView = true;

		listViewAdapter = new HTPReviewAdapter ( getActivity ( ) , listData);
		ListView listView = view.findViewById(R.id.htp_review_listview);
		listView.setOnScrollListener ( this );
		listView.setAdapter ( listViewAdapter );

		editText = view.findViewById ( R.id.nick_edit_text );

		reviewCountTask = new ReviewCountTask ( getContext ( ) , this );

		countStr = new String [ 2 ];

		countStr [ 0 ] = "ht";
		countStr [ 1 ] = smartTourProductsVO.getTourProductVO ( ).getHt_id ( );
		reviewCountTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , countStr );

		initData ( );
		executeTask ( );

		return view;
	}

	private void initData ( ){
		reviewCountText.setText ( 0 + "건의 후기가 등록되었습니다." );
		currentPageNumber = 0;
		requestPageNumber = 1;
		listViewAdapter.removeAll ( );
		removeEditText ( );
		removePreferences();
	}

	private void setReviewCountText ( String count ){
		reviewCountText.setText ( count + "건의 후기가 등록되었습니다." );
	}

	private void executeTask ( ){
		// TASK 전달 파라미터값
		data [ 0 ] = ( smartTourProductsVO.getTourProductVO ( ).getHt_id ( ) );// "HILLING
																																															// NO.02";
		data [ 1 ] = Integer.toString ( requestPageNumber );

		// ListView 아이템 터치 시 이벤트 추가
		reviewParseTask = new ReviewParseTask ( getActivity ( ) , this );
		reviewParseTask.execute ( data );
	}

	private void InsertItemToListView ( ArrayList < HTPReviewVO > resultList ){

		for ( int i = 0; i < resultList.size ( ); i++ ){
			listViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		listViewAdapter.notifyDataSetChanged ( );
	}

	@Override
	public void onClick ( View v ){
		if (validateReviewText()){
			reviewUpdateTask = new ReviewUpdateTask ( getActivity ( ) , this );
			reviewUpdateTask.execute ( CommonData.getUserVO ( ).getEmail ( ) , data [ 0 ] , editText.getText ( ).toString ( ) , "pro" );
		}
	}

	private void removeEditText ( )
	{
		editText.setText(null);
	}

	private boolean validateReviewText ( ){
		if ( editText.getText ( ) != null )
		{
			/*if ( editText.getText ( ).toString ( ).length ( ) < MIN_REVIEW_TEXT_LENGTH)
			{
				Toast.makeText ( getActivity ( ) , MIN_REVIEW_TEXT_LENGTH + "자 이상 적어주세요." , Toast.LENGTH_SHORT ).show ( );
				return false;
			}*/
			if (editText.getText ( ).toString ( ).length ( ) == 0){
				Toast.makeText ( getActivity ( ) , "리뷰를 작성해주세요." , Toast.LENGTH_SHORT ).show ( );
				return false;
			}
		}
		return true;
	}

	@SuppressLint ( "ShowToast" )
	@Override
	public void setJSONArrayResult ( ArrayList resultList ){
		lockListView = true;
		reviewParseTask.cancel ( true );

		if ( resultList == null ){
			return;
		}
		else{
			InsertItemToListView ( resultList );
		}

		lockListView = false;
	}

	@Override
	public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount ){
		int lastInScreen = firstVisibleItem + visibleItemCount;

		if ( ( lastInScreen == totalItemCount ) && !lockListView){
			currentPageNumber += 1;
			requestPageNumber += 1;

			data [ 1 ] = Integer.toString ( requestPageNumber );

			reviewParseTask = new ReviewParseTask ( getActivity ( ) , this );
			reviewParseTask.execute ( data );

			lockListView = true;
		}
	}

	public Integer getCurrentPage ( ){
		return currentPageNumber;
	}

	@Override
	public void onScrollStateChanged ( AbsListView view , int scrollState ){
		// TODO Auto-generated method stub
	}

	@Override
	public void setJSONObjectResult ( UpdateResultVO result ){
		reviewUpdateTask.cancel ( true );

		if ( result == null ){
			Log.d ( TAG , "리뷰가 정상적으로 업데이트 되지않았습니다." );
			return;
		}

		if ( result.getResult ( ).equals ( "success" ) ){
			Toast.makeText ( getActivity ( ) , "저장되었습니다." , Toast.LENGTH_SHORT ).show ( );

			InputMethodManager imm = (InputMethodManager) getContext().getSystemService( getContext().INPUT_METHOD_SERVICE );
			imm.hideSoftInputFromWindow(editText.getWindowToken(),0);

            reviewCountTask = new ReviewCountTask ( getContext() , this );
            reviewCountTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR ,  countStr );
			initData ( );
			executeTask ( );
		}

	}

	@Override
	public void setObjectResult ( Object result ){
		reviewCountTask.cancel ( true );

		if ( result == null ){
			Log.d(TAG, "리뷰 카운트를 가져오지 못했습니다.");
			return;
		}

		setReviewCountText ( ( ( ReviewCountVO ) result ).getCount ( ) );
	}

	@Override
	public void onStop ( ){
		super.onStop ( );
		savePreferences(editText.getText().toString());
	}

	@Override
	public void onResume ( ){
		super.onResume();
		if ( !getPreferences().equals("") )	{
			editText.setText ( getPreferences ( ) );
		}
	}

	// 값 불러오기
	private String getPreferences ( ){
		SharedPreferences pref = getActivity ( ).getSharedPreferences ( "pref" , getActivity ( ).MODE_PRIVATE );
		return pref.getString ( "memo" , "" );
	}

	// 값 저장하기
	private void savePreferences ( String memo ){
		SharedPreferences pref = getActivity ( ).getSharedPreferences ( "pref" , getActivity ( ).MODE_PRIVATE );
		SharedPreferences.Editor editor = pref.edit ( );
		editor.putString("memo", memo);
		editor.commit();
	}

	// 값(Key Data) 삭제하기
	private void removePreferences ( ){
		SharedPreferences pref = getActivity ( ).getSharedPreferences ( "pref" , getActivity ( ).MODE_PRIVATE );
		SharedPreferences.Editor editor = pref.edit ( );
		editor.remove("memo");
		editor.commit();
	}
}
