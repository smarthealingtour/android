package jejusmarttour.main_tourproduct;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.SearchProductTask;
import jejusmarttour.user.UserVO;
import jejusmarttour.vo.SearchProductRequestVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class HTPAmateurViewFragment extends Fragment implements JSONArrayResult , OnScrollListener{
	private static final String TAG = HTPAmateurViewFragment.class.toString ( );

	private ListView listView;
	private HTPAmateurListViewAdapter listViewAdapter;
	private SearchProductTask searchProductTask;
	private SearchProductRequestVO vo;

	private boolean lockListView;
	private int currentPageNumber = 0;
	private int requestPageNumber = 1;

	private ArrayList < TourProductVO > listData;

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
		View view = null;

		try{
			view = inflater.inflate ( R.layout.fragment_listview2, container , false );
		}
		catch ( InflateException e ){
			e.printStackTrace ( );
		}

		listData = new ArrayList < TourProductVO > ( );
		lockListView = true;

		listViewAdapter = new HTPAmateurListViewAdapter ( getActivity ( ) , listData );
		listView = view.findViewById ( R.id.list_view);
		listView.setOnScrollListener ( this );
		listView.setAdapter ( listViewAdapter );

		// ListView 아이템 터치 시 이벤트 추가
		listView.setOnItemClickListener ( onClickListItem );

		executeTask ( );

		return view;
	}

	private void initData ( ){
		currentPageNumber = 0;
		requestPageNumber = 1;
		listViewAdapter.removeAll ( );
	}

	// 리스트뷰 리프레쉬
	public void refreshListView ( ){
		initData ( );
		executeTask ( );
	}

	// 요청할 파라미터
	private SearchProductRequestVO getParam ( String page ){
		vo = new SearchProductRequestVO ( );
		vo.setType ( "1" );
		vo.setPage ( page );
		vo.setSort ( ( ( CommonData ) getActivity ( ).getApplication ( ) ).getSortType ( ) );

		UserVO userVO = CommonData.getUserVO ( );

		if ( userVO.getHcdArray ( ) != null )
			vo.setHcd ( userVO.getHcd ( ) );

		if ( userVO.getScdArray ( ) != null )
			vo.setScd ( userVO.getScd ( ) );

		return vo;
	}

	public Integer getCurrentPage ( )
	{
		return currentPageNumber;
	}

	// 아이템 터치 이벤트
	private OnItemClickListener onClickListItem = new OnItemClickListener ( ){
		@Override
		public void onItemClick ( AdapterView < ? > arg0 , View arg1 , int position , long arg3 ){
			( (TourProductActivity) getActivity ( ) ).onAmateurItemClick ( ( TourProductVO ) listViewAdapter.getItem ( position ) );
		}
	};

	private void InsertItemToListView ( ArrayList < TourProductVO > resultList ){
		for ( int i = 0; i < resultList.size ( ); i++ ){
			listViewAdapter.add ( resultList.get ( i ) );
		}
		// 리스트뷰 리로딩
		listViewAdapter.notifyDataSetChanged ( );
	}

	@Override
	public void setJSONArrayResult ( ArrayList resultList ){
		lockListView = true;
		searchProductTask.cancel ( true );

		if ( resultList == null ){
			return;
		}
		else{
			InsertItemToListView ( resultList );
		}

		lockListView = false;
	}

	private void executeTask ( ){
		searchProductTask = new SearchProductTask ( getActivity ( ) , this );

		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( requestPageNumber ) ) );
	}

	@Override
	public void onScroll ( AbsListView view , int firstVisibleItem , int visibleItemCount , int totalItemCount ){
		int lastInScreen = firstVisibleItem + visibleItemCount;

		if ( ( lastInScreen == totalItemCount ) && lockListView == false ){
			currentPageNumber += 1;
			requestPageNumber += 1;

			executeTask ( );

			lockListView = true;
		}
	}

	@Override
	public void onScrollStateChanged ( AbsListView view , int scrollState ){

	}
}
