package jejusmarttour.main_tourproduct;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2017-09-23.
 */

public class HTPInfoTabFragment extends Fragment implements ViewTreeObserver.OnGlobalLayoutListener {
    private int LAYOUT_WIDTH;
    private boolean isCloseTextView = true;

    private HTPInfoProvider provider;
    private Context context;
    private TextView infoText;

    private Button clickedButton;
    private Button moreBtn;

    private ScheduleVO scheduleVO;

    public static HTPInfoTabFragment newInstance(Context context, ScheduleVO scheduleVO){
        HTPInfoTabFragment INSTANCE = new HTPInfoTabFragment();
        INSTANCE.context = context;
        INSTANCE.scheduleVO = scheduleVO;

        return INSTANCE;
    }

    public View onCreateView (LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
        View view = null;
        try{
            view = inflater.inflate ( R.layout.fragment_information_tab , container , false );
        }
        catch ( InflateException e ){
            e.printStackTrace ( );
        }

        LinearLayout infoTabs = view.findViewById(R.id.infoTabs);
        LAYOUT_WIDTH = getActivity().getWindowManager().getDefaultDisplay().getWidth();

        infoText = view.findViewById(R.id.info_text);
        moreBtn = view.findViewById(R.id.more_btn);
        moreBtn.setOnClickListener(view1 -> {
            if ( isCloseTextView == true ){
                isCloseTextView = false;
                infoText.setMaxLines ( 100 );
                moreBtn.setText ( getResources ( ).getString ( R.string.more_write_btn_close ) );
                infoText.invalidate ( );
            }
            else{
                isCloseTextView = true;
                infoText.setMaxLines ( 5 );
                moreBtn.setText ( getResources ( ).getString ( R.string.more_write_btn ) );
                infoText.invalidate ( );
            }
        });

        provider = new HTPInfoProvider( scheduleVO );
        int getNumOfTab = provider.getNumOfTab();

        for (int i = 0 ; i < getNumOfTab; i++){
            String title = provider.getTabName(i);
            final String info = provider.getInfoText(i);

            Button button = new Button(getActivity());
            Log.e(this.toString(), "onCreateView: 레이아웃 가로 : " + LAYOUT_WIDTH );
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LAYOUT_WIDTH/getNumOfTab - 26, 140 );
            button.setLayoutParams(layoutParams);
            button.setText(title);
            button.setTextColor(getResources().getColor(R.color.base_orange));
            button.setBackgroundColor(Color.WHITE);

            button.setOnClickListener( ( v )->{
                changeToOriginTabColor(clickedButton);

                clickedButton = ((Button)v);
                clickedButton.setTextColor(Color.WHITE);
                clickedButton.setBackgroundColor(getResources().getColor(R.color.base_orange));

                infoText.setText( info );
            });

            if (i == 0){
                addDivider(infoTabs);

                clickedButton = button;
                clickedButton.setTextColor(Color.WHITE);
                clickedButton.setBackgroundColor(getResources().getColor(R.color.base_orange));
                infoText.setText( info );
            }

            infoTabs.addView(button);

            addDivider(infoTabs);
        }
        view.getViewTreeObserver ( ).addOnGlobalLayoutListener ( this );
        return view;
    }

    @Override
    public void onGlobalLayout ( ){
        int lineCount = infoText.getLineCount ( );

        if ( lineCount < 5 ){
            moreBtn.setVisibility ( View.GONE );
        }
        else{
            moreBtn.setVisibility ( View.VISIBLE );
        }

        super.onResume ( );
    }

    private void changeToOriginTabColor(Button button){
        button.setTextColor(getResources().getColor(R.color.base_orange));
        button.setBackgroundColor(Color.WHITE);
    }

    private void addDivider(LinearLayout infoTabs){
        TextView v = new TextView(context);
        v.setWidth(1);
        v.setHeight(infoTabs.getHeight());
        v.setBackgroundColor(getResources().getColor(R.color.base_orange));
        infoTabs.addView(v);
    }
}
