package jejusmarttour.main_tourproduct;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.common.KeywordData;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.map.util;
import jejusmarttour.parser.ScheduleParser;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SmartTourProductsVO;
import syl.com.jejusmarttour.R;

public class yoHTPScheduleFragment extends Fragment implements View.OnClickListener{
	private static final String TAG = yoHTPScheduleFragment.class.toString ( );

	private ArrayList < ScheduleVO > scheduleVOArrayList;
	private ArrayList < ScheduleVO > scheduleVOs;
	private int MAX_DAYS = 1;
	private int CURRENT_DAY = 0;

	public static yoHTPScheduleFragment newInstance(SmartTourProductsVO smartTourProductsVO){
		yoHTPScheduleFragment fragment = new yoHTPScheduleFragment( );
		fragment.scheduleVOArrayList = smartTourProductsVO.getScheduleArrayList ( );

		return fragment;
	}

	@Override
	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
		View view = null;

		try{
			view = inflater.inflate ( R.layout.fragment_schedule_introduce , container , false );
		}
		catch ( InflateException e ){
			e.printStackTrace ( );
		}

		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		scheduleVOs = ScheduleParser.getSchedule ( scheduleVOArrayList , 0 );

		RecyclerView recyclerView = view.findViewById(R.id.list_view);
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		recyclerView.setAdapter(new ScheduleRecyclerAdapter(getActivity(), scheduleVOArrayList));

	}

	private void addSchedule (int position ){
		scheduleVOs = ScheduleParser.getSchedule ( scheduleVOArrayList , position );
//		addProduct ( );
	}

	private void addProduct ( ){
		for ( int k = 0; k < scheduleVOs.size ( ); k++ ){
			LinearLayout contentLayout = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.layout_schedule_contents , null );
			contentLayout.setTag ( k );
			contentLayout.setOnClickListener ( this );

			ImageView iconImage = contentLayout.findViewById ( R.id.location_icon );

			int iconId = 0;

			// point 아이콘 삽입
			if ( scheduleVOs.get ( k ).getHealingContentsVO( ) != null ) {	// 힐링명소
				iconId = CommonData.getLocationIconId ( scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_type ( ) );
			}
			else{	// 관광장소
				iconId = CommonData.getLocationIconId ( "point" );
			}

			Glide.with ( getActivity ( ) ).load ( iconId ).into ( iconImage );

			TextView titleText = contentLayout.findViewById ( R.id.title_text );
			TextView categoryText = contentLayout.findViewById ( R.id.category_text );
			TextView addressText = contentLayout.findViewById ( R.id.address_text );
			TextView telephoneText = contentLayout.findViewById ( R.id.telephone_text );
			TextView timeText = contentLayout.findViewById ( R.id.average_time_text );

			if ( scheduleVOs.get ( k ).getHealingContentsVO( ) != null ){
				titleText.setText ( scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_name ( ) == null ? "이름없음" : scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_name ( ) );
				categoryText.setText ( scheduleVOs.get ( k ) == null ? "" : KeywordData.getKeyword(scheduleVOs.get(k)) );
				addressText.setText ( scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_addr ( ) == null ? "주소없음" : scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_addr ( ) );
				telephoneText.setText ( scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_tel ( ) == null ? "전화번호없음" : scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_tel ( ) );
				timeText.setText ( scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_duration ( ) == null ? "- 분" : SmartTourUtils.getDuration ( scheduleVOs.get ( k ).getHealingContentsVO( ).getHcnt_duration ( ) ) );
			}
			else if ( scheduleVOs.get ( k ).getSightSeeingContentsVO( ) != null ){
				titleText.setText ( scheduleVOs.get ( k ).getSightSeeingContentsVO( ).getCcnt_name ( ) == null ? "이름없음" : scheduleVOs.get ( k ).getSightSeeingContentsVO( ).getCcnt_name ( ) );
				categoryText.setText ( scheduleVOs.get ( k ) == null ? "" : KeywordData.getKeyword(scheduleVOs.get(k)) );
				addressText.setText ( "주소없음" );
				telephoneText.setText ( "전화번호없음" );
				timeText.setText ( "- 분" );
			}
			// 거리 삽입
			if ( CURRENT_DAY == 0 || Integer.valueOf ( scheduleVOs.get ( k ).getSeq ( ) ) != 1 ){
				if ( scheduleVOs.get ( k ).getRoute ( ) != null ){
					addDistance(scheduleVOs.get(k).getRoute(), scheduleVOs);
				}
			}
			// 힐링명소, 관광장소 삽입
			// 메모 삽입
			if ( scheduleVOs.get ( k ).getMemo ( ) != null ){
				for ( int i = 0; i < scheduleVOs.get ( k ).getMemo ( ).size ( ); i++ )
				{
					addMemo ( scheduleVOs.get ( k ).getMemo ( ).get ( i ), scheduleVOs.get ( k ).getMemoWriter().get ( i ) );
				}
			}
		}
	}

	private void addMemo ( String memo, String memoWriter ){
		LinearLayout contentLayout = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.layout_schedule_memo , null );
		ImageView imageView = contentLayout.findViewById ( R.id.user_image );

		Resources resources = this.getResources ( );
		Bitmap bitmap = BitmapFactory.decodeResource ( resources , R.drawable.mypage_no_img_icon );
		RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create ( resources , bitmap );
		roundedBitmapDrawable.setCornerRadius ( Math.max ( bitmap.getWidth ( ) , bitmap.getHeight ( ) ) / 2.0f );

//		imageView.setImageDrawable ( roundedBitmapDrawable );
        imageView.setVisibility(View.INVISIBLE);

		TextView textView = contentLayout.findViewById ( R.id.memo_text );
        textView.setText ( memo );
        textView.append("\n");

        TextView writer = contentLayout.findViewById ( R.id.writer );
        writer.setTypeface(null, Typeface.BOLD|Typeface.ITALIC);
        writer.setPaintFlags(writer.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        writer.setText(memoWriter);

	}

	private void addDistance(final RouteVO vo, final ArrayList<ScheduleVO> scheduleVO){
		LinearLayout contentLayout = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.layout_schedule_distance , null );

		TextView distanceView = contentLayout.findViewById ( R.id.distance_text );
		distanceView.setText ( "약 " + SmartTourUtils.getDistance ( vo.getDistance ( ) ) );

		TextView timeView = contentLayout.findViewById ( R.id.time_text );
		timeView.setText ( "도보 약 " + SmartTourUtils.getDuration ( vo.getLead_time ( ) ) );

        contentLayout.setOnClickListener(view -> {
			if(util.isRouteExist(vo)) {
				try {
					String tag =  ((TourProductIntroduceActivity)getActivity()).getHTPMapFragmentTag();
					HTPMapFragment fragment = (HTPMapFragment)getActivity().getSupportFragmentManager().findFragmentByTag(tag);
					fragment.showRoute(vo, scheduleVO);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}else
				Toast.makeText(getActivity(), "경로정보가 없습니다.", Toast.LENGTH_SHORT).show();
        });
	}

	@Override
	public void onClick ( View v ){
		ScheduleVO scheduleVO = scheduleVOs.get ( Integer.valueOf ( String.valueOf ( v.getTag ( ) ) ) );

		CommonData.setScheduleVO ( scheduleVO );

		Intent intent = new Intent ( getActivity ( ) , DetailInfoActivity.class );
		startActivity ( intent );
	}

	// 일정보기 일자별 토글 버튼 생성
	private void initScheduleToggleGroup ( ){
		MAX_DAYS = Integer.valueOf ( scheduleVOArrayList.get ( 0 ).getDays ( ) );

		if ( MAX_DAYS == 1 ){
			LinearLayout toggleView = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.toggle_schedule , null );
			toggleView.setTag ( toggleView.getId ( ) );
			ToggleButton toggleButton = toggleView.findViewWithTag ( "toggle_btn" );
			toggleButton.setChecked ( true );
			toggleButton.setTextOn ( "1일" );
			toggleButton.setTextOff ( "1일" );
			toggleButton.setText ( "1일" );
			return;
		}

		for ( int i = 0; i <= MAX_DAYS; i++ ){
			LinearLayout toggleView = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.toggle_schedule , null );
			toggleView.setTag ( i );
			ToggleButton toggleButton = toggleView.findViewWithTag ( "toggle_btn" );
			toggleButton.setId ( 9999 + i );
			toggleButton.setOnClickListener (view -> {
                Log.d ( TAG , "일정 보기 postion : " + ( ( LinearLayout ) view.getParent ( ) ).getTag ( ).toString ( ) );

                boolean isChecked = ( ( ToggleButton ) view ).isChecked ( );
                Log.d ( TAG , "isChecked : " + isChecked );

                if (!isChecked){
                    ( ( ToggleButton ) view ).setChecked ( true );
                    return;
                }

                addSchedule ( Integer.valueOf ( ( ( LinearLayout ) view.getParent ( ) ).getTag ( ).toString ( ) ) );
            });

			if ( i == 0 ){
				toggleButton.setChecked ( true );
				toggleButton.setTextOn ( "전체" );
				toggleButton.setTextOff ( "전체" );
				toggleButton.setText ( "전체" );
			}
			else{
				toggleButton.setTextOn ( i + "일" );
				toggleButton.setTextOff ( i + "일" );
				toggleButton.setText ( i + "일" );
			}
		}
	}

	// 토글 버튼
	final RadioGroup.OnCheckedChangeListener ToggleListener = (radioGroup, id) -> {
        for ( int j = 0; j < radioGroup.getChildCount ( ); j++ ){
            final LinearLayout view = ( LinearLayout ) radioGroup.getChildAt ( j );
            final ToggleButton toggleButton = view.findViewWithTag ( "toggle_btn" );

            toggleButton.setChecked ( toggleButton.getId ( ) == id );

            if (toggleButton.isChecked()){
                toggleButton.setTextColor ( radioGroup.getResources ( ).getColor ( R.color.font_color_black ) );
            }
            else{
                toggleButton.setTextColor ( radioGroup.getResources ( ).getColor ( R.color.font_color ) );
            }
        }
    };
}
