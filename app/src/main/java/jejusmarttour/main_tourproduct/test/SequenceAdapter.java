package jejusmarttour.main_tourproduct.test;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.vo.TourProductSequenceVO;
import syl.com.jejusmarttour.R;

public class SequenceAdapter extends RecyclerView.Adapter<SequenceAdapter.MyViewHolder>{
    private ArrayList<TourProductSequenceVO> vos;
    private final int Sequence_Start = 0;
    private final int Sequence_Child = 1;
    private View.OnClickListener itemClickListener;

    public SequenceAdapter(ArrayList<TourProductSequenceVO> vos){
        this.vos = vos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from( parent.getContext());

        final View v;

        switch (viewType) {
            case Sequence_Start:
                v = inflater.inflate(R.layout.view_sequence_start, parent, false);
                break;
            case Sequence_Child:
                v = inflater.inflate(R.layout.view_sequence_child, parent, false);
                break;
            default:
                throw new IllegalStateException("Unexpected viewType (= " + viewType + ")");
        }

        return new MyViewHolder( v );
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.titleText.setText(vos.get(position).getTitle());
        holder.setTextViewTag( position );
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (vos.get(position).getSeq() == 1){
            return Sequence_Start;
        }
        else return Sequence_Child;
    }

    @Override
    public int getItemCount() {
        return vos.size();
    }

    public void setVOs(ArrayList<TourProductSequenceVO> vos){
        this.vos = vos;
    }

    public void setItemClickListener(View.OnClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView titleText;
        View itemView;

        private MyViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;

            titleText = itemView.findViewById(R.id.title_text);
            titleText.setOnClickListener(itemClickListener);
        }
        private void setTextViewTag(int i){
            titleText.setTag(i);
        }
    }
}
