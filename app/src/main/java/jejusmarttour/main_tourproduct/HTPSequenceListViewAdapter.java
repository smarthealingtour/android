package jejusmarttour.main_tourproduct;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jejusmarttour.vo.TourProductSequenceVO;
import syl.com.jejusmarttour.R;

public class HTPSequenceListViewAdapter extends BaseAdapter
{
	private static final String TAG = HTPSequenceListViewAdapter.class.toString ( );

	//private ArrayList < TourProductVO > listData;
	private ArrayList <TourProductSequenceVO> listData;
	private Context context;
	private LayoutInflater inflater;

	public HTPSequenceListViewAdapter ( Context context , ArrayList <TourProductSequenceVO> listData )
	{
		this.context = context;
		this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
		this.listData = listData;
	}

	// 현재 아이템의 수를 리턴
	@Override
	public int getCount ( )
	{
		return listData.size ( );
	}

	// 현재 아이템의 오브젝트를 리턴, Object를 상황에 맞게 변경하거나 리턴받은 오브젝트를 캐스팅해서 사용
	@Override
	public Object getItem ( int position )
	{
		return listData.get ( position );
	}

	// 아이템 position의 ID 값 리턴
	@Override
	public long getItemId ( int position )
	{
		return position;
	}
	
	// 출력 될 아이템 관리
	@Override
	public View getView ( int position , View convertView , ViewGroup parent ){
		HTPSequenceListViewHolder viewHolder;

		// 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
		if ( convertView == null )
		{
			// view가 null일 경우 커스텀 레이아웃을 얻어 옴
			convertView = inflater.inflate ( R.layout.htp_sequence_listview_adapter , parent , false );

			viewHolder = new HTPSequenceListViewHolder ( );
			viewHolder.imageView = ( ImageView ) convertView.findViewById ( R.id.sequence_icon );
			viewHolder.titleText = ( TextView ) convertView.findViewById ( R.id.sequence_title );
			viewHolder.numberText = ( TextView ) convertView.findViewById ( R.id.sequence_num );
			viewHolder.categoryText = ( TextView ) convertView.findViewById ( R.id.sequence_category );

			convertView.setTag ( viewHolder );
		}
		else
		{
			viewHolder = ( HTPSequenceListViewHolder ) convertView.getTag ( );
		}

		Glide.with ( context ).load ( listData.get ( position ).getIconId ( ) ).thumbnail ( 0.1f ).into ( viewHolder.imageView );
		
		viewHolder.titleText.setText ( listData.get ( position ).getTitle ( ) );
		viewHolder.numberText.setText ( listData.get ( position ).getSeq( ) );
		viewHolder.categoryText.setText ( listData.get ( position ).getCategory ( ) );

		return convertView;
	}
	
	//지도 아이콘 가져오기
	private int getLocationIconImage()
	{
		return 0;
	}

	@Override
	public void notifyDataSetChanged ( )
	{
		super.notifyDataSetChanged ( );
	}

	// 외부에서 아이템 추가 요청 시 사용
	public void add ( TourProductSequenceVO vo )
	{
		listData.add ( vo );
	}

	// 외부에서 아이템 삭제 요청 시 사용
	public void remove ( int position )
	{
		listData.remove ( position );
	}
}
