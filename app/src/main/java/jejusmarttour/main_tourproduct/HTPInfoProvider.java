package jejusmarttour.main_tourproduct;

import android.util.Log;

import java.util.ArrayList;

import jejusmarttour.util.TabNameUtil;
import jejusmarttour.vo.ScheduleVO;

/**
 * Created by Osy on 2017-09-24.
 */

public class HTPInfoProvider {

    private int numOfTab;
    private ArrayList<TabNameUtil> tabNames;
    private String tabCode;
    private ArrayList<String> infoTexts;

    public HTPInfoProvider(ScheduleVO scheduleVO){
        if (scheduleVO.getHealingContentsVO() != null){
            this.tabCode = scheduleVO.getHealingContentsVO().getHcnt_tab();
        }
        else
            this.tabCode = "";


        setTabNames();
        setInfoText( scheduleVO);
    }

    private void setTabNames(){
        tabNames = TabNameUtil.getTabNameCode(tabCode);
        numOfTab = tabNames.size();
    }

    private void setInfoText(ScheduleVO scheduleVO){
        infoTexts = new ArrayList<>();

        infoTexts.add(scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt());
        infoTexts.add(scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt_sec());
        infoTexts.add(scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt_thd());
        infoTexts.add(scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt_fth());
        infoTexts.add(scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt_fiv());

        Log.e(toString(), "getHcnt_rcmd_comt: " + scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt() );
        Log.e(toString(), "getHcnt_rcmd_comt_sec: " + scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt_sec() );
        Log.e(toString(), "getHcnt_rcmd_comt_thd: " + scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt_thd() );
        Log.e(toString(), "getHcnt_rcmd_comt_fth: " + scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt_fth() );
        Log.e(toString(), "getHcnt_rcmd_comt_fiv: " + scheduleVO.getHealingContentsVO().getHcnt_rcmd_comt_fiv() );
    }

    public int getNumOfTab(){
        return numOfTab;
    }

    public String getTabName(int position){
        Log.e(toString(), "getTabName: " + tabNames.get(position).name() );
        return tabNames.get(position).name();
    }

    public String getInfoText(int position){
        return infoTexts.get(position);
    }

}
