package jejusmarttour.main_tourproduct;

import java.util.ArrayList;
import com.bumptech.glide.Glide;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import jejusmarttour.common.CommonData;
import jejusmarttour.common.KeywordData;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class HTPAmateurListViewAdapter extends BaseAdapter
{
	private static final String TAG = HTPAmateurListViewAdapter.class.toString ( );

	private ArrayList < TourProductVO > listData;
	private Context context;
	private LayoutInflater inflater;

	public HTPAmateurListViewAdapter ( Context context , ArrayList < TourProductVO > listData )
	{
		this.context = context;
		this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
		this.listData = listData;
	}

	// 현재 아이템의 수를 리턴
	@Override
	public int getCount ( )
	{
		return listData.size ( );
	}

	// 현재 아이템의 오브젝트를 리턴, Object를 상황에 맞게 변경하거나 리턴받은 오브젝트를 캐스팅해서 사용
	@Override
	public Object getItem ( int position )
	{
		return listData.get ( position );
	}

	// 아이템 position의 ID 값 리턴
	@Override
	public long getItemId ( int position )
	{
		return position;
	}

	// 출력 될 아이템 관리
	@Override
	public View getView ( int position , View convertView , ViewGroup parent )
	{
		HTPListViewHolder viewHolder;

		// 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
		if ( convertView == null )
		{
			// view가 null일 경우 커스텀 레이아웃을 얻어 옴
			convertView = inflater.inflate ( R.layout.htp_all_view_listview_adapter , parent , false );

			viewHolder = new HTPListViewHolder ( );
			viewHolder.imageView = ( ImageView ) convertView.findViewById ( R.id.htp_image_view );
			viewHolder.titleText = ( TextView ) convertView.findViewById ( R.id.htp_title );
			viewHolder.subTitleText = ( TextView ) convertView.findViewById ( R.id.htp_subtitle );
			viewHolder.contentText = ( TextView ) convertView.findViewById ( R.id.htp_description );

			viewHolder.reviewText = ( TextView ) convertView.findViewById ( R.id.htp_reply_text );
			viewHolder.cartText = ( TextView ) convertView.findViewById ( R.id.htp_buyer_text );
			viewHolder.distanceText = ( TextView ) convertView.findViewById ( R.id.htp_distance_text );
			viewHolder.daysText = ( TextView ) convertView.findViewById ( R.id.htp_days_text );
			viewHolder.priceText = ( TextView ) convertView.findViewById ( R.id.htp_price );
			viewHolder.sessionText = ( TextView ) convertView.findViewById ( R.id.htp_session_time );

			viewHolder.keywordImage_01 = ( ImageView ) convertView.findViewById ( R.id.keyword_01 );
			viewHolder.keywordImage_02 = ( ImageView ) convertView.findViewById ( R.id.keyword_02 );
			viewHolder.keywordImage_03 = ( ImageView ) convertView.findViewById ( R.id.keyword_03 );
			viewHolder.keywordImage_04 = ( ImageView ) convertView.findViewById ( R.id.keyword_04 );
			viewHolder.keywordImage_05 = ( ImageView ) convertView.findViewById ( R.id.keyword_05 );
			viewHolder.keywordImage_06 = ( ImageView ) convertView.findViewById ( R.id.keyword_06 );
			viewHolder.keywordImage_07 = ( ImageView ) convertView.findViewById ( R.id.keyword_07 );

			convertView.setTag ( viewHolder );
		}
		else
		{
			viewHolder = ( HTPListViewHolder ) convertView.getTag ( );
		}

		if ( listData.get ( position ).getHt_imgArray ( ) [ 0 ] != null )
		{
			String address = CommonData.getImageAddress ( listData.get ( position ).getHt_imgArray ( ) [ 0 ] );
			Log.d ( TAG, address );
			Glide.with ( context ).load ( address ).override ( 150 , 70 ).thumbnail ( 0.1f ).into ( viewHolder.imageView );
		}
		else
			viewHolder.imageView.setImageDrawable ( context.getResources ( ).getDrawable ( R.drawable.my_tour_course_no_img_icon ) );

		viewHolder.titleText.setText ( listData.get ( position ).getHt_name ( ) );
		viewHolder.subTitleText.setText ( listData.get ( position ).getNickname ( ) );
		viewHolder.contentText.setText ( listData.get ( position ).getHt_intro ( ) );

		viewHolder.reviewText.setText ( listData.get ( position ).getComment_count ( ) );
		viewHolder.cartText.setText ( listData.get ( position ).getCart_count ( ) );

		viewHolder.daysText.setText ( listData.get ( position ).getDays ( ).equals("1") == true ? listData.get ( position ).getDays ( ) + " day" : listData.get ( position ).getDays ( ) + " day");
		viewHolder.priceText.setText ( listData.get ( position ).getCost ( ) == null ? "- 원" : SmartTourUtils.getCost ( listData.get ( position ).getCost ( ) ) );
		viewHolder.sessionText.setText ( listData.get ( position ).getDuraion ( ) == null ? "- 분" : SmartTourUtils.getDuration(listData.get(position).getDuraion()) );
		viewHolder.distanceText.setText ( listData.get ( position ).getDistance ( ) == null ? "- km" : SmartTourUtils.getDistance(listData.get(position).getDistance()) );

		ImageView [ ] imageViews = { viewHolder.keywordImage_01 , viewHolder.keywordImage_02 , viewHolder.keywordImage_03 , viewHolder.keywordImage_04 , viewHolder.keywordImage_05 , viewHolder.keywordImage_06 , viewHolder.keywordImage_07 };

		for ( int i = 0; i < imageViews.length; i++ )
		{
			imageViews [ i ].setVisibility ( View.GONE );
		}

		ArrayList < Integer > arrayList = KeywordData.getKeywordIconImage ( listData.get ( position ).getHt_hcdArray ( ) , listData.get ( position ).getHt_scdArray ( ) );

		if ( arrayList != null )
		{
			for ( int i = 0; i < arrayList.size ( ); i++ )
			{
				imageViews [ i ].setVisibility ( View.VISIBLE );
				Glide.with ( context ).load ( arrayList.get ( i ) ).thumbnail ( 0.1f ).into ( imageViews [ i ] );
			}
		}

		return convertView;
	}

	@Override
	public void notifyDataSetChanged ( )
	{
		super.notifyDataSetChanged ( );
	}

	// 외부에서 아이템 추가 요청 시 사용
	public void add ( TourProductVO vo )
	{
		listData.add ( vo );
	}

	// 외부에서 아이템 삭제 요청 시 사용
	public void remove ( int position )
	{
		listData.remove ( position );
	}

	public void removeAll ( )
	{
		listData.removeAll ( listData );
	}
}
