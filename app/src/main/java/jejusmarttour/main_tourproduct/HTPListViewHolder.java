package jejusmarttour.main_tourproduct;

import android.widget.ImageView;
import android.widget.TextView;

public class HTPListViewHolder
{
	ImageView imageView;
	TextView titleText;
	TextView subTitleText;
	TextView contentText;
	TextView reviewText;
	TextView cartText;
	TextView distanceText;
	TextView daysText;
	TextView priceText;
	TextView sessionText;

	ImageView keywordImage_01;
	ImageView keywordImage_02;
	ImageView keywordImage_03;
	ImageView keywordImage_04;
	ImageView keywordImage_05;
	ImageView keywordImage_06;
	ImageView keywordImage_07;
}
