package jejusmarttour.main_tourproduct;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.data.kml.KmlLayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.map.CommonData_;
import jejusmarttour.map.util;
import jejusmarttour.task.HealingContentsTask;
import jejusmarttour.user.UserVO;
import jejusmarttour.util.CustomViewPager;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import jejusmarttour.vo.SmartTourProductsVO;
import syl.com.jejusmarttour.R;

public class HTPMapFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = HTPMapFragment.class.toString();
    //    LayerManager lm = null;
//    private MyMapView mMapView = null; //kt 지도를 사용하기 위한 지도 뷰
//    private overlay_ overlay = null; //오버레이 정보
//    FrameLayout mMainLayout = null; //기존의 프레임 레이아웃으로 지도를 띄우고 있었음
    private SmartTourProductsVO smartTourProductsVO;
    private Boolean isCreateView = false;
    //    private FrameLayout info = null;
    View view = null;
//    int px = 0;

    String cName = "htour";

    private RadioGroup radioGroup;
    private ArrayList<ScheduleVO> scheduleVOArrayList; //scheduleVO 는 여러가지 vo포함 그중에서 healingcontentvo가 좌표 정보 가지고 있음 //여기서 좌표만 빼면 된다.
    private ArrayList<ScheduleVO> scheduleVOs; //안쓰고 있음
    private ArrayList<RouteVO> geometry_data;
    private ArrayList<HealingContentsVO> geometry_data2;
    private SearchProductRequestVO ht_vo;
    private int MAX_DAYS = 1;
    private int CURRENT_DAY = 0;
    private CustomViewPager viewPager;
    public boolean isRouteShow = false;
    public boolean isScheduleShow = false;
    private JSONArray contentGeometry = null;
    private RouteVO vo = null;

    private GoogleMap mMap;
    MapView gMapView;

    private HealingContentsTask searchProductTask;

    double[] coord_x;
    double[] coord_y;

    /*
    * scheduleVOArrayList, smartTourProductsVO.getScheduleArrayList();
    * 이거랑 같이 해야되는게
    * schduleVOArrayList.getHealingContentsVO(); 이게 hcnt_id 를 받아오고
    *
    * */
    public static HTPMapFragment newInstance(SmartTourProductsVO smartTourProductsVO, CustomViewPager viewPager) {
        HTPMapFragment fragment = new HTPMapFragment();
        fragment.smartTourProductsVO = smartTourProductsVO;
        fragment.scheduleVOArrayList = smartTourProductsVO.getScheduleArrayList();
        fragment.viewPager = viewPager;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            view = inflater.inflate(R.layout.fragment_htpmap, container, false);
            gMapView = (MapView) view.findViewById(R.id.htourMap);
            gMapView.onCreate(savedInstanceState);

            gMapView.getMapAsync(this);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        radioGroup = (RadioGroup) view.findViewById(R.id.toggleGroupOnMap);

        /*
        * 여기 부분이 데이터 받아서 좌표 정리하는 부분
        * retrieveFileFromUrl(url)이 kml 파일이 있을 때 지도위에 띄우는 매소드
        * size는 받아온 데이터 크기만큼(관광지 수 만큼)
        * 힐링 컨텐츠 ( hcnt )에 해당하는 게 관광지
        * 사이트 씨잉 ( ccnt )에 해당하는 게 숙식
        * line, zone과 point 구분, 그리고 숙식시설 나눠서 각각의 경우 다르게 좌표를 다르게 가져오게 수정
        * */
        int size = smartTourProductsVO.getScheduleArrayList().size();
        coord_x = new double[size];
        coord_y = new double[size];

        for (int i = 0; size > i; i++) {
            if (smartTourProductsVO.getScheduleArrayList().get(i).getHealingContentsVO() != null) {
                HealingContentsVO hcnt = smartTourProductsVO.getScheduleArrayList().get(i).getHealingContentsVO();
                if (hcnt.getHcnt_type().equals("point")) {
                    coord_x[i] = Double.parseDouble(hcnt.getHcnt_coord_x());
                    coord_y[i] = Double.parseDouble(hcnt.getHcnt_coord_y());
                } else {
                    coord_x[i] = Double.parseDouble(hcnt.getHcnt_coord_x());
                    ;
                    coord_y[i] = Double.parseDouble(hcnt.getHcnt_coord_y());
                    ;
                    if (hcnt.getHcnt_kml() != null) {
                        String url = null;
                        url = CommonData.getImageAddress();
                        url += hcnt.getHcnt_kml().toString();
                        retrieveFileFromUrl(url);
                    }
                    if (hcnt.getHcnt_inv_hcnt() != null) {
//                        executeTask(hcnt.getHcnt_inv_hcnt());
                    }
                }
            } else {
                SightSeeingContentsVO ccnt = smartTourProductsVO.getScheduleArrayList().get(i).getSightSeeingContentsVO();
                if (ccnt != null){
                    if (ccnt.getCcnt_coord_x() != null){
                        coord_x[i] = Double.parseDouble(ccnt.getCcnt_coord_x());
                    }
                    if (ccnt.getCcnt_coord_y() != null){
                        coord_y[i] = Double.parseDouble(ccnt.getCcnt_coord_y());
                    }
                }

            }
        }
//        mMainLayout = ( FrameLayout ) view.findViewById ( R.id.htourMapFrame ); // 지도

//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        ViewGroup.LayoutParams params = mMainLayout.getLayoutParams();
//        params.height = metrics.heightPixels - (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
//        mMainLayout.setLayoutParams(params);
//
//        info = ( FrameLayout ) view.findViewById ( R.id.info );
//        px = ( int ) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, info.getLayoutParams().height, getActivity().getResources().getDisplayMetrics() );
//        info.setTranslationY( px );
//
//        lm = new LayerManager ( mMainLayout );
//        mMapView = new MyMapView ( getActivity ( ) );
//
//        lm.addLayer ( mMapView , "baseMap" );
//        mMapView.setLayerManager ( lm );
//
//        overlay = new overlay_( getActivity ( ) , mMapView );
//        lm.addLayer ( overlay , "overlay" );
//        overlay.setLayerManager ( lm );
//
//        mMapView.setZoomLevel ( 4 );
//        mMapView.setMapResolution(1002);
//        mMapView.setMapCenter(util.mapCenter);
//        mMapView.invalidate();
//        mMapView.setVisibility ( View.VISIBLE );
//
        CommonData.recentCname = cName;

        initScheduleToggleGroup();

        ((TourProductIntroduceActivity) getActivity()).setHTPMapFragmentTag(getTag());

        return view;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setMinZoomPreference(8);
        mMap.setMaxZoomPreference(17);
        for ( int i = 0; coord_x.length > i; i++ ){
            add_marker(coord_x[i], coord_y[i], i);
        }
    }

    private void retrieveFileFromUrl(String kml) {
        //new DownloadKmlFile("http://googlemaps.github.io/kml-samples/morekml/Polygons/Polygons.Google_Campus.kml").execute();
        Log.e("이거 kml 주소", kml);
        new DownloadKmlFile(kml).execute();
    }

    private void add_marker( double x, double y, int i ) {
        mMap.addMarker( new MarkerOptions()
                .position(new LatLng(x, y))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        if ( i== 0 ) {
            getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(x, y), 15));
        }
    }

    protected GoogleMap getMap() {
        return mMap;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void initScheduleToggleGroup() {

        if(radioGroup.getClipChildren())
            radioGroup.removeAllViews();

        MAX_DAYS = Integer.valueOf ( scheduleVOArrayList.get ( 0 ).getDays ( ) );

        if ( MAX_DAYS == 1 )
        {
            LinearLayout toggleView = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.toggle_schedule , null );
            toggleView.setTag ( toggleView.getId ( ) );
            ToggleButton toggleButton = ( ToggleButton ) toggleView.findViewWithTag ( "toggle_btn" );
            toggleButton.setChecked ( true );
            toggleButton.setTextOn ( "1일" );
            toggleButton.setTextOff ( "1일" );
            toggleButton.setText ( "1일" );
            radioGroup.addView ( toggleView );
            radioGroup.invalidate ( );
            return;
        }

        for ( int i = 0; i <= MAX_DAYS; i++ )
        {
            LinearLayout toggleView = ( LinearLayout ) View.inflate ( getActivity ( ) , R.layout.toggle_schedule , null );
            toggleView.setTag ( i );
            final ToggleButton toggleButton = ( ToggleButton ) toggleView.findViewWithTag ( "toggle_btn" );
            toggleButton.setId ( 9999 + i );
            toggleButton.setOnClickListener ( new View.OnClickListener ( )
            {
                @Override
                public void onClick ( View view )
                {

                    int schedule =  Integer.parseInt((  ( LinearLayout ) view.getParent ( ) ).getTag ( ).toString ( ));

                    boolean isChecked = ( ( ToggleButton ) view ).isChecked ( );
                    Log.d ( TAG , "isChecked : " + isChecked );

                    if ( isChecked == false )
                    {
                        ( ( ToggleButton ) view ).setChecked ( true );
                        return;
                    }
//                    executeTask(schedule); //  지오메트리 콜
                    radioGroup.check ( view.getId ( ) );
                }
            } );

            if ( i == 0 )
            {
                toggleButton.setChecked ( true );
                toggleButton.setTextOn ( "전체" );
                toggleButton.setTextOff ( "전체" );
                toggleButton.setText ( "전체" );
            }
            else
            {
                toggleButton.setTextOn ( i + "일" );
                toggleButton.setTextOff ( i + "일" );
                toggleButton.setText ( i + "일" );
            }
            radioGroup.addView ( toggleView );
        }
        radioGroup.invalidate ( );
        radioGroup.setOnCheckedChangeListener ( ToggleListener );
    }

    // 토글 버튼
    final RadioGroup.OnCheckedChangeListener ToggleListener = new RadioGroup.OnCheckedChangeListener ( )
    {
        @Override
        public void onCheckedChanged ( final RadioGroup radioGroup , final int id )
        {

            for ( int j = 0; j < radioGroup.getChildCount ( ); j++ )
            {
                final LinearLayout view = ( LinearLayout ) radioGroup.getChildAt ( j );
                final ToggleButton toggleButton = ( ToggleButton ) view.findViewWithTag ( "toggle_btn" );

                toggleButton.setChecked ( toggleButton.getId ( ) == id );


                if ( toggleButton.isChecked ( ) == true )
                {
                    toggleButton.setTextColor ( radioGroup.getResources ( ).getColor ( R.color.font_color_black ) );
                }
                else
                {
                    toggleButton.setTextColor ( radioGroup.getResources ( ).getColor ( R.color.font_color ) );
                }
            }
        }
    };

    public void showRoute(RouteVO vo, ArrayList<ScheduleVO> schedule) throws JSONException {
        isRouteShow = true;
//        info.setTranslationY( px );

//        overlay.setInfo(info, px);
        ((HorizontalScrollView)radioGroup.getParent()).setVisibility(View.INVISIBLE);

//        if(overlay != null) {
//            overlay.showBtnSpotRoute(vo, schedule);
//            overlay.setVisibility(View.VISIBLE);
//        }
        viewPager.setCurrentItem(2);
        if(this.vo == null)
            this.vo = vo;

    }

    @Override
    public void setUserVisibleHint ( boolean isVisibleToUser )
    {
        super.setUserVisibleHint ( isVisibleToUser );
        if ( isVisibleToUser )
        {
            if (!isRouteShow && !isScheduleShow &&  smartTourProductsVO.getTourProductVO ( ) != null )
            {
//                if(overlay != null && overlay.isRouteShow) {
//                    lm.removeLayer(overlay);
//                    overlay = new overlay_( getActivity ( ) , mMapView );
//                    lm.addLayer ( overlay , "overlay" );
//                    overlay.setLayerManager ( lm );
//
//                    info.setTranslationY( px );
                initScheduleToggleGroup();
//                }
//                executeTask(0);
                isCreateView = true;
//            } else if( isRouteShow ) {
                if( isRouteShow ) {
                    isRouteShow = false;
                    isScheduleShow = false;
                }
                else {
                    if(!CommonData.recentCname.equals(cName)){
//                    if(overlay != null) {
//                        lm.removeLayer(overlay);
//                        overlay = new overlay_( getActivity ( ) , mMapView );
//                        lm.addLayer ( overlay , "overlay" );
//                        overlay.setLayerManager ( lm );
//
//                        info.setTranslationY( px );
                        initScheduleToggleGroup();
                    }
//                    executeTask(0);
                    isCreateView = true;
                }
            }
            CommonData.recentCname = cName;
        }
    }

    public void onResume() {
        super.onResume();
//
//        if(mMapView != null)
//            mMapView.setVisibility(View.VISIBLE);
//
        if( !CommonData.recentCname.equals("htour")){
//            if(overlay != null) {
//                lm.removeLayer(overlay);
//                overlay = new overlay_( getActivity ( ) , mMapView );
//                lm.addLayer ( overlay , "overlay" );
//                overlay.setLayerManager ( lm );
//
//                info.setTranslationY( px );
            initScheduleToggleGroup();
//
//                overlay.setInfo(info, px);
        }
//
        if(isScheduleShow) {
//                executeTask(0);
//                }
//                info.setTranslationY( px );
            ((HorizontalScrollView)radioGroup.getParent()).setVisibility(View.VISIBLE);
//                try {
//                    overlay.setHtourInGeometry(contentGeometry, info, px, true, scheduleVOArrayList, this.schedule);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                overlay.setVisibility(View.VISIBLE);
            isScheduleShow = true;
//            }
//            else if(viewPager != null) {
            if(viewPager != null) {
                if(viewPager.getCurrentItem() == 2 && vo != null) {
                    try {
                        showRoute(vo, scheduleVOArrayList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    isRouteShow = false;
                    isScheduleShow = false;
                }
            }
            CommonData.recentCname = cName;
        }

        gMapView.onResume();
    }
    //
    @Override
    public void onStop() {
        super.onStop();
//
//        if(mMapView != null)
//            mMapView.setVisibility(View.INVISIBLE);
    }
    //
//    private int schedule = -1;
//
//    private void executeTask (int schedule)
//{
//    this.schedule = schedule;
//    GeometryTask task = new GeometryTask(getActivity(), this, schedule);
//    task.execute(smartTourProductsVO.getTourProductVO().getHt_id());
//
//    isScheduleShow = true;
////        info.setTranslationY( px );
////        ((HorizontalScrollView)radioGroup.getParent()).setVisibility(View.VISIBLE);
////        if(contentGeometry == null) {
////            GeometryTask task = new GeometryTask(getActivity(), this, schedule);
////            task.execute(smartTourProductsVO.getTourProductVO().getHt_id());
////        }
////        else if(schedule > 0) {
////            GeometryTask task = new GeometryTask(getActivity(), this, schedule);
////            task.execute(smartTourProductsVO.getTourProductVO().getHt_id());
////        }
////        else {
////            try {
////                if(overlay.isRouteShow || !isScheduleShow) {
////                    GeometryTask task = new GeometryTask(getActivity(), this, schedule);
////                    task.execute(smartTourProductsVO.getTourProductVO().getHt_id());
////                }
////                else
////                    overlay.setHtourInGeometry(contentGeometry, info, px, true, scheduleVOArrayList, this.schedule);
////            } //catch (JSONException e) {
////            catch (Exception e) {
////                e.printStackTrace();
////            }
////        }
////        overlay.setVisibility(View.VISIBLE);
////        isScheduleShow = true;
//}
    private  void executeTask(String str) {
        searchProductTask = new HealingContentsTask ( getActivity ( ) , this );
        searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam ( Integer.toString ( 0 ) , str ) );
    }

    // 요청할 파라미터
    private SearchProductRequestVO getParam ( String page , String name ) {
        ht_vo = new SearchProductRequestVO ( );
        ht_vo.setHt_name ( name );
        ht_vo.setType ( "0" );
        ht_vo.setPage ( page );
        ht_vo.setSort ( ( ( CommonData ) getActivity ( ).getApplication ( ) ).getSortType ( ) );

        UserVO userVO = CommonData.getUserVO ( );

        if ( userVO.getHcdArray ( ) != null )
            ht_vo.setHcd ( userVO.getHcd ( ) );

        if ( userVO.getScdArray ( ) != null )
            ht_vo.setScd ( userVO.getScd ( ) );

        return ht_vo;
    }

    public void setJsonResult ( JSONArray result )  throws JSONException
    {
        //geometry_data를 잘라서 latlng 으로 만들어야된다. VO뭐 써야되지
        /* RouteVO 필드
        * distance, start, end, route_id, lead_time, ht_id, geometry, position, contentID
        */
        Log.e("이거", "이거");
//        final int numberOfItemsInResp = result.length ( );
//
//        geometry_data = new ArrayList<RouteVO>();
//        geometry_data2 = new ArrayList<HealingContentsVO>();
//
//        for ( int i = 0; i < numberOfItemsInResp; i++ ) {
//            RouteVO routeVO = new RouteVO();
//            HealingContentsVO healingContentsVO = new HealingContentsVO();
//            JSONObject jsonObject = new JSONObject(result.getString(i));
//            healingContentsVO.setHcnt_type( jsonObject.has ( "hcnt_type" ) ? jsonObject.getString ( "hcnt_type" ) : jsonObject.getString("type") );
//            try {
//                if( healingContentsVO.getHcnt_type().equals("line") || healingContentsVO.getHcnt_type().equals("zone") ) {
//                    healingContentsVO.setHcnt_coord_x(jsonObject.getString("hcnt_coord_x"));
//                    healingContentsVO.setHcnt_coord_y(jsonObject.getString("hcnt_coord_y"));
//                    geometry_data2.add(healingContentsVO);
//                } else {
//                    routeVO.setGeometry(jsonObject.getString("geometry"));
//                    geometry_data.add(routeVO);
//                }
//            } catch (NullPointerException e)
//            {
//                Log.e ( this.toString() , e.toString ( ) );
//            }
//            if( i == 0 ) {
//                moveCameraToKml();
//            }
//        }
//        if ( overlay != null )
//        {
//            overlay.setHtourInGeometry(result, info, px, true, scheduleVOArrayList, this.schedule);
//            overlay.setVisibility(View.VISIBLE);
//            contentGeometry = result;
//        }
    }

    @Override
    public void onDestroy ( )
    {
        super.onDestroy ( );

//        if ( mMapView != null )
//        {
//            mMapView.destroyDrawingCache ( );
//            if ( mMapView.commonUtils != null )
//                mMapView.commonUtils.clearData ( );
//        }

//        if( overlay != null )
//            util.recursiveRecycle(overlay);

        CommonData_ common = CommonData_.getInstance ( getActivity ( ) );
        if ( common != null )
            common.dispose ( );

        util.recursiveRecycle(getActivity().getWindow().getDecorView());

        System.gc ( );
    }

    private class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
        private final String mUrl;

        public DownloadKmlFile(String url) {
            mUrl = url;
        }

        //태스크 이용하는 부분
        protected byte[] doInBackground(String... params) {
            try {
                InputStream is =  new URL(mUrl).openStream();
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[16384];
                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                return buffer.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(byte[] byteArr) {
            try {
                if (byteArr != null){
                    KmlLayer kmlLayer = new KmlLayer(mMap, new ByteArrayInputStream(byteArr),
                            getContext());//kml레이어를 받아온 데이터로 이용
                    kmlLayer.addLayerToMap();//지도 위에 레이어 표시(kml라인 표시)
                }

            } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}