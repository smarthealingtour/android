package jejusmarttour.main_tourproduct;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.vo.HTPReviewVO;
import syl.com.jejusmarttour.R;

public class HTPReviewAdapter extends BaseAdapter
{
	private static final String TAG = HTPReviewAdapter.class.toString ( );

	private ArrayList <HTPReviewVO> listData;
	private Context context;
	private LayoutInflater inflater;

	public HTPReviewAdapter ( Context context , ArrayList< HTPReviewVO > listData  )
	{
		this.context = context;
		this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
		this.listData = listData;
	}

	// 현재 아이템의 수를 리턴
	@Override
	public int getCount ( )
	{
		return listData.size ( );
	}

	// 현재 아이템의 오브젝트를 리턴, Object를 상황에 맞게 변경하거나 리턴받은 오브젝트를 캐스팅해서 사용
	@Override
	public HTPReviewVO getItem ( int position )
	{
		return listData.get ( position );
	}

	// 아이템 position의 ID 값 리턴
	@Override
	public long getItemId ( int position )
	{
		return position;
	}



	// 출력 될 아이템 관리
	@Override
	public View getView ( int position , View convertView , ViewGroup parent )
	{
		ReviewViewHolder viewHolder;
		// 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
		if ( convertView == null )
		{
			// view가 null일 경우 커스텀 레이아웃을 얻어 옴
			convertView = inflater.inflate ( R.layout.htp_review_listview_adapter , parent , false );
			
			viewHolder = new ReviewViewHolder ( );
			
			viewHolder.imageView = ( ImageView ) convertView.findViewById ( R.id.review_img_view );
			viewHolder.nickNameText = ( TextView ) convertView.findViewById ( R.id.review_nickname_text );
			viewHolder.writeTimeText = ( TextView ) convertView.findViewById ( R.id.review_write_time_text );
			viewHolder.contentText = ( TextView ) convertView.findViewById ( R.id.review_content_text );
			
			convertView.setTag ( viewHolder );
		}
		else
		{
			viewHolder = (ReviewViewHolder) convertView.getTag ( );
		}
		
		Resources resources = convertView.getResources ( );
		Bitmap bitmap = BitmapFactory.decodeResource ( resources , R.drawable.mypage_no_img_icon );
		RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create ( resources , bitmap );
		roundedBitmapDrawable.setCornerRadius ( Math.max ( bitmap.getWidth ( ) , bitmap.getHeight ( ) ) / 2.0f );
		
		viewHolder.imageView.setImageDrawable ( roundedBitmapDrawable );

		// TextView에 현재 position의 문자열 추가
		viewHolder.nickNameText.setText ( ( ( HTPReviewVO ) listData.get ( position ) ).getNickname ( ) );
		viewHolder.writeTimeText.setText ( ( ( HTPReviewVO ) listData.get ( position ) ).getComment_date ( ) );
		viewHolder.contentText.setText ( ( ( HTPReviewVO ) listData.get ( position ) ).getComment_subt ( ) );
		
		return convertView;
	}
	
	// 외부에서 아이템 추가 요청 시 사용
	public void add ( HTPReviewVO vo )
	{
		listData.add ( vo );
	}

	// 외부에서 아이템 삭제 요청 시 사용
	public void remove ( int position )
	{
		listData.remove ( position );
	}
	
	public void removeAll()
	{
		listData.removeAll ( listData );
		notifyDataSetChanged();
	}

}
