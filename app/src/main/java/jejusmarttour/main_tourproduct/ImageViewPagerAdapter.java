package jejusmarttour.main_tourproduct;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import jejusmarttour.common.CommonData;
import jejusmarttour.multitouchimageview.MultiTouchImageViewActivity;
import syl.com.jejusmarttour.R;

public class ImageViewPagerAdapter extends PagerAdapter {
    private Context context;
    private String[] imageData;
    private int MAX_PAGE;
    private LayoutInflater inflater;

    private SparseArray<View> views = new SparseArray<View>();
    private boolean isImageNull = false;

    private DisplayImageOptions options;

    public ImageViewPagerAdapter(Context context, String[] imageData, int maxPage) {
        super();
        this.context = context;
        this.imageData = imageData;
        MAX_PAGE = maxPage;
        inflater = LayoutInflater.from(context);

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.my_tour_course_no_img_icon)
                .showImageOnFail(R.drawable.my_tour_course_no_img_icon)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration
                .Builder(context)
                .defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);
    }

    @Override
    public int getCount() {
        return MAX_PAGE;
    }

    // 뷰페이저에서 사용할 뷰객체 생성/등록
    @Override
    public Object instantiateItem(View pager, final int position) {
        View view = inflater.inflate(R.layout.layout_image_view, null);
        ImageView imageView = view.findViewById(R.id.viewpager_imageview);
        imageView.setOnClickListener(v -> {
            Intent intent = new Intent(context, MultiTouchImageViewActivity.class);
            intent.putExtra("data", imageData[position]);
            context.startActivity(intent);
        });

        ImageLoader.getInstance().displayImage( CommonData.getImageAddress ( imageData[position]), imageView, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }
                Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            }
        });


//		Glide.with ( context ).load ( CommonData.getImageAddress ( imageData [ position ] ) ).thumbnail ( 0.1f ).into ( imageView );

        ((ViewPager) pager).addView(view, 0); // 뷰 페이저에 추가
        views.put(position, view);
        return view;
    }

    // 뷰 객체 삭제.
    @Override
    public void destroyItem(View pager, int position, Object o) {
        View view = (View) o;
        ((ViewPager) pager).removeView(view);
        views.remove(position);
        view = null;
    }

    @Override
    public void notifyDataSetChanged() {
        int key = 0;
        for (int i = 0; i < views.size(); i++) {
            key = views.keyAt(i);
            View view = views.get(key);
            // refresh할 작업들
        }
        super.notifyDataSetChanged();
    }

    // instantiateItem메소드에서 생성한 객체를 이용할 것인지
    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    @Override
    public void finishUpdate(View arg0) {
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void startUpdate(View arg0) {
    }

}
