package jejusmarttour.util;

import android.content.Context;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;

public class LayoutUtils
{
	private static final String TAG = LayoutUtils.class.toString ( );

	public static void populateText ( LinearLayout containLayout , View [ ] views , Context context )
	{
		Display display = ( ( WindowManager ) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay ( );
		containLayout.removeAllViews ( );
		int maxWidth = display.getWidth ( ) - 20;

		LayoutParams params;
		LinearLayout newLL = new LinearLayout ( context );
		newLL.setLayoutParams ( new LayoutParams ( LayoutParams.FILL_PARENT , LayoutParams.WRAP_CONTENT ) );
		newLL.setGravity ( Gravity.LEFT );
		newLL.setOrientation ( LinearLayout.HORIZONTAL );

		int widthSoFar = 0;

		for ( int i = 0; i < views.length; i++ )
		{
			LinearLayout LL = new LinearLayout ( context );
			LL.setOrientation ( LinearLayout.HORIZONTAL );
			LL.setGravity ( Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM );
			LL.setLayoutParams ( new ListView.LayoutParams ( LayoutParams.WRAP_CONTENT , LayoutParams.WRAP_CONTENT ) );
			// my old code
			// TV = new TextView(mContext);
			// TV.setText(textArray[i]);
			// TV.setTextSize(size); <<<< SET TEXT SIZE
			// TV.measure(0, 0);
			views [ i ].measure ( 0 , 0 );
			params = new LayoutParams ( views [ i ].getMeasuredWidth ( ) , LayoutParams.WRAP_CONTENT );
			// params.setMargins(5, 0, 5, 0); // YOU CAN USE THIS
			// LL.addView(TV, params);
			LL.addView ( views [ i ] , params );
			LL.measure ( 0 , 0 );
			widthSoFar += views [ i ].getMeasuredWidth ( );// YOU MAY NEED TO
															// ADD THE MARGINS
			if ( widthSoFar >= maxWidth )
			{
				containLayout.addView ( newLL );

				newLL = new LinearLayout ( context );
				newLL.setLayoutParams ( new LayoutParams ( LayoutParams.FILL_PARENT , LayoutParams.WRAP_CONTENT ) );
				newLL.setOrientation ( LinearLayout.HORIZONTAL );
				newLL.setGravity ( Gravity.LEFT );
				params = new LayoutParams ( LL.getMeasuredWidth ( ) , LL.getMeasuredHeight ( ) );
				newLL.addView ( LL , params );
				widthSoFar = LL.getMeasuredWidth ( );
			}
			else
			{
				newLL.addView ( LL );
			}
		}
		containLayout.addView ( newLL );
	}
}
