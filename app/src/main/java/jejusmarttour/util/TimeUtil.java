package jejusmarttour.util;

/**
 * Created by Osy on 2017-09-26.
 */

public class TimeUtil {
    private int hour = 0, minute = 0;
    private boolean outDay;

    public void setTime(int hour, int minute){
        this.hour = hour;
        this.minute = minute;

        convertToHour();
        outDayCheck();

    }
    public void setTime(TimeUtil tu){
        hour = tu.getHour();
        minute = tu.getMinute();

        convertToHour();
        outDayCheck();
    }

    public void addTime(int hour, int minute){
        this.hour += hour;
        this.minute += minute;

        convertToHour();
        outDayCheck();
    }
    public void addTime(TimeUtil tu){
        this.hour += tu.getHour();
        this.minute += tu.getMinute();

        convertToHour();
        outDayCheck();
    }

    private void convertToHour(){
        if (minute < 0){
            hour--;
            minute += 60;
        }
        else if (minute >= 60){
            hour++;
            minute -= 60;
        }
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public String getTimeString(){
        String timeString;

        if (hour < 10)  timeString = "0" + hour;
        else            timeString = Integer.toString(hour);

        if (minute < 10) timeString += " : 0" + minute;
        else             timeString += " : "  + minute;
        return timeString;
    }

    private void outDayCheck(){
        if (hour < 0 ){
            hour += 24;
            outDay = true;
        }
        else if (hour > 24) {
            hour -= 24;
            outDay = true;
        }
        else outDay = false;
    }

    public boolean isOutDay(){
        return outDay;
    }
}