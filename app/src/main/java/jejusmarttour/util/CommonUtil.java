package jejusmarttour.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CommonUtil {
	

	/**************************************
	  DATA Encode/Decode
	 ***************************************/

	public static String changeHourMin(int min){

		//시, 분, 초 선언
		int hours, minute;
		String strHourMin = "";
		//시간공식
		hours = min / 60;//시 공식
		minute = min % 60;//분을 구하기위해서 입력되고 남은값에서 또 60을 나눈다.

		if(hours != 0){
			strHourMin = hours + "시간 ";
		}

		if(minute != 0){
			strHourMin =+ minute + "분";
		}

		return strHourMin;
	}

	public static Uri createSaveCropFile(){
		Uri uri;
		String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
		uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));
		return uri;
	}

	public static File getImageFile(Context mContext , Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		if (uri == null) {
			uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		}

		Cursor mCursor = mContext.getContentResolver().query(uri, projection, null, null,
				MediaStore.Images.Media.DATE_MODIFIED + " desc");
		if(mCursor == null || mCursor.getCount() < 1) {
			return null; // no cursor or no record
		}
		int column_index = mCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		mCursor.moveToFirst();

		String path = mCursor.getString(column_index);

		if (mCursor !=null ) {
			mCursor.close();
			mCursor = null;
		}

		return new File(path);
	}

	public static boolean copyFile(File srcFile, File destFile) {
		boolean result = false;
		try {
			InputStream in = new FileInputStream(srcFile);
			try {
				result = copyToFile(in, destFile);
			} finally  {
				in.close();
			}
		} catch (IOException e) {
			result = false;
		}
		return result;
	}

	public static boolean copyToFile(InputStream inputStream, File destFile) {
		try {
			OutputStream out = new FileOutputStream(destFile);
			try {
				byte[] buffer = new byte[4096];
				int bytesRead;
				while ((bytesRead = inputStream.read(buffer)) >= 0) {
					out.write(buffer, 0, bytesRead);
				}
			} finally {
				out.close();
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}


	/************************************************************
	 * json String => Map
	 ************************************************************/
	public static Map<String, Object> jsonParser(String returnMsg) {

		JsonParser jsParser = new JsonParser();
		JsonReader jReader = new JsonReader(new StringReader(returnMsg));

		JsonElement firstElement = jsParser.parse(jReader);

		Map<String, Object> resultMap = (Map<String, Object>) jsonToJava(firstElement);

		return resultMap;

	}

	/************************************************************
	 * json String => ArrayList
	 ************************************************************/
	public static ArrayList<Map<String, Object>> jsonArrayParser(String returnMsg) {

		JsonParser jsParser = new JsonParser();
		JsonReader jReader = new JsonReader(new StringReader(returnMsg));

		JsonElement firstElement = jsParser.parse(jReader);

		ArrayList<Map<String, Object>> resultArrayList = (ArrayList<Map<String, Object>>) jsonToJava(firstElement);

		return resultArrayList;

	}

	/************************************************************
	 * json String => Object
	 ************************************************************/
	public static Object jsonToJava(JsonElement jsonElement) {

		if (jsonElement.isJsonArray()) {

			System.out.println("jsonArray");

			JsonArray jsonArray = jsonElement.getAsJsonArray();

			List<Object> assembledList = new ArrayList<Object>();

			Iterator<?> jsonIterator = jsonArray.iterator();

			while (jsonIterator.hasNext()) {

				JsonElement entryElement = (JsonElement) jsonIterator.next();

				Object entryObj = jsonToJava(entryElement);

				assembledList.add(entryObj);

			}

			return assembledList;


		}else if (jsonElement.isJsonObject()) {

				// System.out.println("jsonObject");

				JsonObject jsonObj = jsonElement.getAsJsonObject();

				Map<String, Object> assembledMap = new HashMap<String, Object>();

				for (Map.Entry<String, JsonElement> entry : jsonObj.entrySet()) {

					String key = entry.getKey();

					JsonElement value = entry.getValue();

					Object entryObj = jsonToJava(value);

					assembledMap.put(key, entryObj);

				}

				return assembledMap;

			}  else if (jsonElement.isJsonPrimitive()) {

			// System.out.println("jsonPrimitive");

			JsonPrimitive jsonPrimitive = jsonElement.getAsJsonPrimitive();

			if (jsonPrimitive.isBoolean()) {
				return jsonPrimitive.getAsBoolean();
			} else if (jsonPrimitive.isNumber()) {
				return jsonPrimitive.getAsInt();
			} else if (jsonPrimitive.isString()) {
				return jsonPrimitive.getAsString();
			} else {
				return null;
			}

		} else if (jsonElement.isJsonNull()) {

			return null;

		} else {

			return null;

		}

	}


}
