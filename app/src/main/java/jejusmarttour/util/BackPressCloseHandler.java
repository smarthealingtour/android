package jejusmarttour.util;

import android.app.Activity;
import android.widget.Toast;

//뒤로가기 두번 종료
public class BackPressCloseHandler{
	private long backKeyPressedTime = 0;
	private Toast toast;

	//toast 메시지를 띄우기위해서 필요한 변수
	private Activity activity;

	public BackPressCloseHandler ( Activity context ){
		this.activity = context;
	}

	//시간을 계산하여 2초 이내에 한번더 누르면 종료 아니면 초기화 시간 재측정 및 showGuide()메서드 실행
	public void onBackPressed ( ){
		if ( System.currentTimeMillis ( ) > backKeyPressedTime + 2000 )	{
			backKeyPressedTime = System.currentTimeMillis ( );
			showGuide ( );
			return;
		}
		if ( System.currentTimeMillis ( ) <= backKeyPressedTime + 2000 ){
			activity.finish ( );
			toast.cancel ( );
		}
	}

	private void showGuide ( )	{
		toast = Toast.makeText ( activity , "\'뒤로\'버튼을 한번 더 누르시면 종료됩니다." , Toast.LENGTH_SHORT );
		toast.show ( );
	}
}
