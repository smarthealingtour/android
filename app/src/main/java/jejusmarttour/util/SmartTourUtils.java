package jejusmarttour.util;

import java.util.HashMap;

public class SmartTourUtils
{
	// 문자열 빈공간 없앰
	public static String replaceSpaceToBlank ( String str ){
		if ( str == null )
			return null;

		String data = str.replace ( " " , "" );

		return data;
	}

	// 공백 없에고 ","로 배열 나눔
	public static String [ ] getValidateStringAraay ( String str ){
		if ( str == null )
			return null;

		String [ ] array;
		str.replace ( " " , "" );
		array = str.split ( "," );
		return array;
	}

	// 공백 없에고 ":"로 배열 나눔
	public static HashMap < String , String [ ] > getValidateRelativeMemoMap ( String [ ] strArray ){
		if ( strArray == null )
			return null;

		HashMap < String , String [ ] > map = new HashMap < String , String [ ] > ( );
		String [ ] array;
		String [ ] memoArray;

		for ( int i = 0; i < strArray.length; i++ ){
			String str = strArray [ i ];
			str.replace ( " " , "" );
			array = str.split ( ":" );
			memoArray = array [ 1 ].split ( "/" );
			map.put ( array [ 0 ] , memoArray );
		}

		return map;
	}

	// key 값 의 데이터를 삭제하고...
	public static String removeStringInStringArray ( String [ ] strArray , String key ){
		String retrunStr = null;

		for ( int i = 0; i < strArray.length; i++ ){
			if ( strArray [ i ].equals ( key ) != true && strArray [ i ] != null ){
				if ( retrunStr == null )
					retrunStr = strArray [ i ].toString ( );
				else
					retrunStr += "," + strArray [ i ].toString ( );
			}
		}

		return retrunStr;
	}

	public static String getValidateKeywordName ( String str ){
		String retrunStr = replaceSpaceToBlank ( str );

		retrunStr.replace ( "·" , "" );

		return retrunStr;
	}

	// 가격 포멧
	public static String getCost ( String cost ){
		if(cost == null)
			return "- 원";

		if ( cost.equals ( "0" ) || cost.equals ( "" ))
			return "- 원";

		return String.format ( "%,.0f 원" , Double.valueOf ( cost ) );
	}

	// 여행 시간 포멧
	public static String getDuration ( String duration ){
		if(duration == null)
			return "- 분";

		if ( duration.equals ( "0" ) || duration.equals ( "" ) )
			return "- 분";

		if ( Integer.valueOf ( duration ) < 60 )
			return duration + " 분";

		if ( ( Integer.valueOf ( duration ) % 60 ) == 0 )
			return String.valueOf ( ( int ) Math.floor ( Integer.valueOf ( duration ) / 60 ) ) + " 시간 ";

		return String.valueOf ( ( int ) Math.floor ( Integer.valueOf ( duration ) / 60 ) ) + " 시간 " + ( Integer.valueOf ( duration ) % 60 ) + " 분";
	}

	// 거리 포멧
	public static String getDistance ( String distance ){
		if(distance == null)
			return "- km";

		if ( distance.equals ( "0" ) || distance.equals ( "" ) )
			return "- km";

        Double temp = Double.parseDouble(distance);

		if ( temp.intValue() < 1000 )
			return String.format ( "%.1f km" , Double.valueOf ( temp.intValue() ) / 1000 );

		return String.valueOf ( Math.round ( temp.intValue() ) / 1000 ) + " km";
	}
}
