package jejusmarttour.util;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class CheckOnLine
{
	private static final String TAG = CheckOnLine.class.toString ( );

	@SuppressWarnings ( "deprecation" )
	public static boolean isOnline ( Context context )
	{ 
		// network 연결 상태 확인
		ConnectivityManager cm = ( ConnectivityManager ) context.getSystemService ( Context.CONNECTIVITY_SERVICE );

		boolean isConnected = false;

		if ( cm.getActiveNetworkInfo ( ) != null )
		{
			NetworkInfo activeNetwork = cm.getActiveNetworkInfo ( );
			
			switch ( activeNetwork.getType ( ) )
			{
				case ConnectivityManager.TYPE_WIMAX :
					// Log.d("Test","Wibro 네트워크연결");
					isConnected = true;
					break;
				case ConnectivityManager.TYPE_WIFI :
					// Log.d("Test","WiFi 네트워크연결");
					isConnected = true;
					break;
				case ConnectivityManager.TYPE_MOBILE :
					// Log.d("Test","3G 네트워크연결");
					isConnected = true;
					break;
			}
		}
		else
		{
			Log.d ( TAG , "네트워크 연결이 안됩니다." );
			AlertDialog alertDialog = new AlertDialog.Builder ( context ).create ( );
			alertDialog.setTitle ( "네트워크 에러" );
			alertDialog.setMessage ( "네트워크 연결이 되지 않았습니다. 네트워크 연결을 확인해주세요" );
			alertDialog.setButton ( "OK" , (dialog, which) -> {
                //어플 종료
                android.os.Process.killProcess ( android.os.Process.myPid ( ) );
            });
			alertDialog.show ( );
		}

		return isConnected;
	}
}
