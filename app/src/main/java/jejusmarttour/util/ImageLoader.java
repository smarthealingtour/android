package jejusmarttour.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

public class ImageLoader {
	
	public static String packName;
	
	private MemoryCache memoryCache=new MemoryCache();
	private FileCache fileCache;
	private Map<ImageView, String> imageViews=Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
	private HashSet<String> bitmapStr = new HashSet<String>();
	private ExecutorService executorService; 
	private int type, pix;
	private String effectRgb;
	private Context mContext;
	private boolean isBitmap;

	private int m_iOpt = 0;

	public ImageLoader(Context context){
		if(packName == null)
			packName = context.getPackageName();
		
		isBitmap = true;
		mContext = context;
		noImgBitmap = null;
		img_list = -1;
		fileCache=new FileCache(context);
		executorService=Executors.newFixedThreadPool(5);
	}
	
	public ImageLoader(Context context, int resoucre){
		if(packName == null)
			packName = context.getPackageName();
		
		isBitmap = true;
		mContext = context;
		img_list = resoucre;
		noImgBitmap = null;
		fileCache=new FileCache(context);
		executorService=Executors.newFixedThreadPool(5);
	}
	
	public ImageLoader(Context context, Drawable bit){
		if(packName == null)
			packName = context.getPackageName();
		
		isBitmap = true;
		mContext = context;
		img_list = -1;
		noImgBitmap = bit;
		fileCache=new FileCache(context);
		executorService=Executors.newFixedThreadPool(5);
	}

	private Drawable noImgBitmap;
	private int img_list;//이미지 없음 이미지 대체

	public void DisplayImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt)
	{
		m_iOpt = iOpt;
		
		this.isBitmap = isBitmap;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, String effect)
	{
		m_iOpt = iOpt;
		
		this.isBitmap = isBitmap;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, int resource)
	{
		m_iOpt = iOpt;
		
		this.isBitmap = isBitmap;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		img_list = resource;
		noImgBitmap = null;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, int resource, String effect)
	{
		m_iOpt = iOpt;
		
		this.isBitmap = isBitmap;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		img_list = resource;
		noImgBitmap = null;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, Drawable bit)
	{
		m_iOpt = iOpt;
		
		this.isBitmap = isBitmap;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = bit;
		img_list = -1;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, Drawable bit, String effect)
	{
		m_iOpt = iOpt;
		
		this.isBitmap = isBitmap;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = bit;
		img_list = -1;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, int type, int pix)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		this.isBitmap = isBitmap;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, int type, int pix, String effect)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		this.isBitmap = isBitmap;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, int type, int pix, int noImg)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		this.isBitmap = isBitmap;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = null;
		img_list = noImg;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, int type, int pix, int noImg, String effect)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		this.isBitmap = isBitmap;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = null;
		img_list = noImg;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, int type, int pix, Drawable bit)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		this.isBitmap = isBitmap;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = bit;
		img_list = -1;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, boolean isBitmap, int iOpt, int type, int pix, Drawable bit, String effect)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		this.isBitmap = isBitmap;
		
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = bit;
		img_list = -1;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			if(isBitmap)
				imageView.setImageBitmap(bitmap);
			else{
				imageView.setImageBitmap(null);
				 Drawable d = new BitmapDrawable(bitmap);
				 imageView.setBackgroundDrawable(d);
			}
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, int iOpt)
	{
		m_iOpt = iOpt;
		
		isBitmap = true;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, int iOpt, String effect)
	{
		m_iOpt = iOpt;
		
		isBitmap = true;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, int iOpt, int resource)
	{
		m_iOpt = iOpt;
		
		isBitmap = true;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		img_list = resource;
		noImgBitmap = null;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, int iOpt, int resource, String effect)
	{
		m_iOpt = iOpt;
		
		isBitmap = true;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		img_list = resource;
		noImgBitmap = null;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, int iOpt, Drawable bit)
	{
		m_iOpt = iOpt;
		
		isBitmap = true;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = bit;
		img_list = -1;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayImage(String key_PathOrUrl, ImageView imageView, int iOpt, Drawable bit, String effect)
	{
		m_iOpt = iOpt;
		
		isBitmap = true;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap=memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = bit;
		img_list = -1;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, int iOpt, int type, int pix)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		isBitmap = true;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, int iOpt, int type, int pix, String effect)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		isBitmap = true;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, int iOpt, int type, int pix, int noImg)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		isBitmap = true;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = null;
		img_list = noImg;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, int iOpt, int type, int pix, int noImg, String effect)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		isBitmap = true;
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = null;
		img_list = noImg;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, int iOpt, int type, int pix, Drawable bit)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		
		isBitmap = true;
		effectRgb = null;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = bit;
		img_list = -1;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}
	
	public void DisplayCornerImage(String key_PathOrUrl, ImageView imageView, int iOpt, int type, int pix, Drawable bit, String effect)
	{
		m_iOpt = iOpt;
		this.type = type;
		this.pix = pix;
		isBitmap = true;
		
		effectRgb = effect;
		imageViews.put(imageView, key_PathOrUrl);
		Bitmap bitmap = memoryCache.get(key_PathOrUrl); //부드러운 참조로 bitmap 얻는다.
		bitmapStr.add(key_PathOrUrl);
		noImgBitmap = bit;
		img_list = -1;

		if(bitmap!=null)  //이미 map에 들어가 있으면 넣고 
		{
			imageView.setImageBitmap(bitmap);
		}
		else //없으면 다시 재로딩해서 넣는다.
		{
			try{
				queuePhoto(key_PathOrUrl, imageView); //다시 재로딩해서 넣는다.
			}catch(OutOfMemoryError e){}
			
			if(noImgBitmap != null){
				imageView.setImageDrawable(noImgBitmap);
			}else if(img_list != -1)
				imageView.setImageResource(img_list);
		}
	}

	public void setNoImg(int drawable){
		noImgBitmap = null;
		img_list = drawable;
	}
	
	public void setNoImg(Drawable bit){
		img_list = -1;
		noImgBitmap = bit;
	}

	public void readyImage_PreAndAfter(/*String preImg_PathOrUrl, */
			final String afterImg_PathOrUrl, final int iOpt, String effect)
	{
		effectRgb = effect;
		m_iOpt = iOpt;
		Thread threadImgBitmap = new Thread( new Runnable() {
			public void run()
			{
				setMemoryCache(afterImg_PathOrUrl);
			}
		});
		threadImgBitmap.start();
	}


	private void queuePhoto(String PathOrUrl, ImageView imageView)
	{
		PhotoToLoad p=new PhotoToLoad(PathOrUrl, imageView);
		executorService.submit(new PhotosLoader(p));
	}

	private Bitmap getBitmap(String url) 
	{
		File f=fileCache.getFile(url);

		//from SD cache
		Bitmap b = decodeFile(f);
		if(b!=null)
			return b;

		//from web
		try {
			Bitmap bitmap=null;
			url = regularTokenPattern(url);
			URL imageUrl = new URL(url.replaceAll(" ", "%20"));
			HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is=conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			CopyStream(is, os);
			os.close();
			bitmap = decodeFile(f);
			
			return bitmap;
		} catch (Exception ex){
			Log.e("error", ex.toString());
			ex.printStackTrace();
			return null;
		}
	}
	
	String regularTokenPattern(String urlData){
		String returnUrl = urlData;
		String han = "";
	    if( urlData != null ){ 
	      String patternStr = "([ㄱ-ㅎㅏ-ㅣ가-힣]{1,})"; 
	      Pattern pattern = Pattern.compile(patternStr); 
	      Matcher matcher = pattern.matcher(urlData); 

	      while(matcher.find()) { 
	    	  han = matcher.group(1);
	    	  String encode = "";
	    	  try {
	    		  encode = URLEncoder.encode(han, "UTF-8");
	    	  } catch (UnsupportedEncodingException e) {
	    	  }
	    	  returnUrl = returnUrl.replace(han, encode);
	      }
	    } 
	    
	    return returnUrl;
	}

	//decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f){
		try {
			//decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);

			final int REQUIRED_SIZE;

			switch (m_iOpt) {
			case 1:
				REQUIRED_SIZE=480;
				break;
			case 2:
				REQUIRED_SIZE=240;
				break;
			case 3:
				REQUIRED_SIZE=120;
				break;

			default:
				REQUIRED_SIZE=60;
				break;
			}

			int width_tmp=o.outWidth, height_tmp=o.outHeight;

			int scale=1;
			while(true){
				if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
					break;
				width_tmp/=2;
				height_tmp/=2;
				scale*=2;
			}

//			Log.d("imsi", "\nscale : "+scale + "\no.outWidth : " + o.outWidth
//					+ "\no.outHeight : " + o.outHeight
//					+ "\nREQUIRED_SIZE : " + REQUIRED_SIZE);
			//decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;

			o2.inPreferredConfig = Config.RGB_565; //16bit
			o2.inPurgeable = true;
			o2.inInputShareable=true;
			o2.inDither = false;
			o2.inTempStorage = new byte[32 * 1024];
			
			if(type > 0){
				return setRoundCorner(BitmapFactory.decodeStream(new FileInputStream(f), null, o2), type, pix);
			}else
				return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
			//            return getRoundedCornerBitmap(
			//             BitmapFactory.decodeStream(new FileInputStream(f), null, o2), 13);

		} catch (FileNotFoundException e) {}
		return null;
	}
	
	/*private  static Bitmap setRoundCorner(Bitmap bitmap, int type, int pixel) {
		if(bitmap == null)
			return null;
		
		RoundDrawable rd = new RoundDrawable(bitmap, type, pixel);
		
		return rd.getBitmap();
	}*/
	
	private  static Bitmap setRoundCorner(Bitmap bitmap, int type, int pixel) {
		if(bitmap == null)
			return null;
		
    	Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),  Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        
        int mBitmapWidth = bitmap.getWidth();
        int mBitmapHeight = bitmap.getHeight();
        
        int color = 0xff424242;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        
        Rect rect = new Rect(0, 0, mBitmapWidth, mBitmapHeight);
        Rect rect1 = new Rect(pixel, 0, mBitmapWidth - pixel, pixel);
        Rect rect2_1 = new Rect(0, pixel, mBitmapWidth, mBitmapHeight);
        Rect rect2_2 = new Rect(0, 0, mBitmapWidth, mBitmapHeight - pixel);
        Rect rect3 = new Rect(pixel, mBitmapHeight - pixel, mBitmapWidth - pixel, mBitmapHeight);
        
        RectF mRectF = new RectF(rect);
        
        canvas.drawARGB(0, 0, 0, 0);
        
        switch(type){
        case 1:
//        	Log.d("img","width:" + mBitmapWidth);
//        	Log.d("img", "height:" + mBitmapHeight);
        	if(mBitmapWidth > mBitmapHeight){
        		int left = (int)((mBitmapWidth - mBitmapHeight) / 2f);
        		mRectF = new RectF(new Rect(left, 0, left + mBitmapHeight, mBitmapHeight));
        	}else if(mBitmapWidth < mBitmapHeight){
        		int top = (int)((mBitmapHeight - mBitmapWidth) / 2f);
        		mRectF = new RectF(new Rect(0, top, mBitmapWidth, top + mBitmapWidth));
        	}
        	
        	canvas.drawOval(mRectF, paint);
//        	int size = (bitmap.getWidth()/2);
//        	
//        	if(mBitmapWidth > mBitmapHeight){
//        		size =  (bitmap.getHeight()/2);
//        	}
//        	
//    		canvas.drawCircle(size, size, size, paint);
//    		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
//    		canvas.drawBitmap(bitmap, rect, rect, paint);
        	break;
        case 2:
        	canvas.drawRoundRect(mRectF, pixel, pixel, paint);
        	break;
        case 3:
			canvas.drawRect(rect1, paint);
			canvas.drawRect(rect2_1, paint);
			canvas.drawCircle(pixel, pixel, pixel, paint);
			canvas.drawCircle(mBitmapWidth - pixel, pixel, pixel, paint);
        	break;
        case 4:
			canvas.drawRect(rect2_2, paint);
			canvas.drawRect(rect3, paint);
			canvas.drawCircle(pixel, mBitmapHeight - pixel, pixel, paint);
			canvas.drawCircle(mBitmapWidth - pixel, mBitmapHeight - pixel, pixel, paint);
        	break;
        }
        
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        
        bitmap.recycle();
        
        return output;
    }

	//Task for the queue
	private class PhotoToLoad
	{
		public String PathOrUrl;
		public ImageView imageView;
		public PhotoToLoad(String p, ImageView i){
			PathOrUrl=p; 
			imageView=i;
		}
	}

	private class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;
		PhotosLoader(PhotoToLoad photoToLoad){
			this.photoToLoad=photoToLoad;
		}

		@Override
		public void run() {
			if(imageViewReused(photoToLoad))
				return;

			Bitmap bmp = setMemoryCache(photoToLoad.PathOrUrl);

			if(imageViewReused(photoToLoad))
				return;

			BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
			Activity a=(Activity)photoToLoad.imageView.getContext();
			a.runOnUiThread(bd);
		}
	}

	private Bitmap setMemoryCache(String PathOrUrl){

		Bitmap bmp = memoryCache.get(PathOrUrl);

		if(bmp != null){
			return bmp;
		}
		//Bitmap bmp = null;


		if(PathOrUrl.startsWith("http")){
			bmp = getBitmap(PathOrUrl); //url을 통해서 bitmap얻기
		}else if(PathOrUrl.contains("R.drawable")){
			String p = PathOrUrl.substring(11);
			
			int tmpID = getResourceIdByName("drawable", p);
			if(tmpID != 0){
				try{
				bmp = ((BitmapDrawable) mContext.getResources().getDrawable(tmpID)).getBitmap();
				}catch(Exception e){e.printStackTrace();}
			}
			
		} else {
			File f = new File(PathOrUrl);
			bmp = decodeFile(f);
		}
		
		if(bmp != null && effectRgb != null && !PathOrUrl.contains("R.drawable")){
			bmp = overlayMark(bmp);
		}

		memoryCache.put(PathOrUrl, bmp);

		return bmp;
	}
	
	private Bitmap overlayMark(Bitmap bmp1){
		Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(bmp1, 0, 0, null);
		int color = Integer.parseInt(effectRgb, 16);
		canvas.drawColor(color);
		return bmOverlay;
	}

	private boolean imageViewReused(PhotoToLoad photoToLoad){
		String tag=imageViews.get(photoToLoad.imageView);
		if(tag==null || !tag.equals(photoToLoad.PathOrUrl))
			return true;
		return false;
	}

	//Used to display bitmap in the UI thread
	private class BitmapDisplayer implements Runnable
	{
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p)
		{
			bitmap=b;photoToLoad=p;
		}

		public void run()
		{
			if(imageViewReused(photoToLoad))
				return;

			if(bitmap!=null){
				if(isBitmap)
					photoToLoad.imageView.setImageBitmap(bitmap);
				else{
					photoToLoad.imageView.setImageBitmap(null);
					photoToLoad.imageView.setBackgroundDrawable(new BitmapDrawable(bitmap));
				}
			}else {
				if(noImgBitmap != null){
					photoToLoad.imageView.setImageDrawable(noImgBitmap);
				}else
					photoToLoad.imageView.setImageResource(img_list);
			}
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}

	public static void CopyStream(InputStream is, OutputStream os)
	{
		final int buffer_size=1024;
		try
		{
			byte[] bytes=new byte[buffer_size];
			for(;;)
			{
				int count=is.read(bytes, 0, buffer_size);
				if(count==-1)
					break;
				os.write(bytes, 0, count);
			}
		}
		catch(Exception ex){}
	}

	public void removeBitmap(){
		if(bitmapStr.size() > 0){
			for (Iterator iter = bitmapStr.iterator(); iter.hasNext();){
				String id = (String) iter.next();
				if(memoryCache.get(id) != null)
					memoryCache.get(id).recycle();
			}
		}
	}
	
	
	public static int getResourceIdByName(String className, String name) {
	    Class r = null;
	    int id = 0;
	    try {
	        r = Class.forName(packName + ".R");

	        Class[] classes = r.getClasses();
	        Class desireClass = null;

	        for (int i = 0; i < classes.length; i++) {
	            if (classes[i].getName().split("\\$")[1].equals(className)) {
	                desireClass = classes[i];

	                break;
	            }
	        }

	        if (desireClass != null) {
	            id = desireClass.getField(name).getInt(desireClass);
	        }

	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	    } catch (IllegalArgumentException e) {
	        e.printStackTrace();
	    } catch (SecurityException e) {
	        e.printStackTrace();
	    } catch (IllegalAccessException e) {
	        e.printStackTrace();
	    } catch (NoSuchFieldException e) {
	        e.printStackTrace();
	    }

	    return id;
	}

}
