package jejusmarttour.util.code_data;

import java.util.ArrayList;

/**
 * Created by Osy on 2017-11-29.
 */

public enum StaySpinnerTree {
    INSTANCE;

    private ConvertNodesToList convertNodesToList;
    public static StaySpinnerTree getInstance() {
        return INSTANCE;
    }

    private MyNode.BranchNode<MyNode.BranchNode> rootNode;

    private MyNode.BranchNode<MyNode.BranchNode> indoor;
    private MyNode.BranchNode<MyNode.BranchNode> outdoor;

    StaySpinnerTree(){
        convertNodesToList = ConvertNodesToList.getInstance();
        init();
    }

    private void init(){
        rootNode = new MyNode.BranchNode<>("숙박장소","C");

        indoor = new MyNode.BranchNode<>("실내","1");
        outdoor = new MyNode.BranchNode<>("야외","2");

        initIndoor();
        initOutdoor();

        rootNode.addChild(indoor);
        rootNode.addChild(outdoor);
    }
    private void initIndoor(){
        indoor.addChild(new MyNode.BranchNode("전체",""));

        indoor.addChild(new MyNode.BranchNode("리조트","01"));
        indoor.addChild(new MyNode.BranchNode("콘도","02"));

        MyNode.BranchNode<MyNode.LeafNode> hotel = new MyNode.BranchNode<>("호텔","03");
            hotel.addChild(new MyNode.LeafNode("전체",""));
            hotel.addChild(new MyNode.LeafNode("특1급호텔","01"));
            hotel.addChild(new MyNode.LeafNode("특2급호텔","02"));
            hotel.addChild(new MyNode.LeafNode("1급호텔","03"));
            hotel.addChild(new MyNode.LeafNode("2급호텔","04"));
            hotel.addChild(new MyNode.LeafNode("3급호텔","05"));
            hotel.addChild(new MyNode.LeafNode("일반급호텔","06"));
            hotel.addChild(new MyNode.LeafNode("호텔기타","07"));
        indoor.addChild(hotel);

        indoor.addChild(new MyNode.BranchNode("모텔","04"));
        indoor.addChild(new MyNode.BranchNode("여관","05"));
        indoor.addChild(new MyNode.BranchNode("민박","06"));
        indoor.addChild(new MyNode.BranchNode("전통","07"));
        indoor.addChild(new MyNode.BranchNode("게스트하우스","08"));
        
        MyNode.BranchNode<MyNode.LeafNode> Pension = new MyNode.BranchNode<>("펜션","09");
            Pension.addChild(new MyNode.LeafNode("전체",""));
            Pension.addChild(new MyNode.LeafNode("펜션","01"));
            Pension.addChild(new MyNode.LeafNode("독채펜션","02"));
            Pension.addChild(new MyNode.LeafNode("산장","03"));
        indoor.addChild(Pension);
    }
    private void initOutdoor(){
        outdoor.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode<MyNode.LeafNode> trackOfFirst = new MyNode.BranchNode<>("캠핑","01");
            trackOfFirst.addChild(new MyNode.LeafNode("전체",""));
            trackOfFirst.addChild(new MyNode.LeafNode("카라반/캠핑카","01"));
            trackOfFirst.addChild(new MyNode.LeafNode("오토캠핑장","02"));
            trackOfFirst.addChild(new MyNode.LeafNode("글램핑","03"));
        outdoor.addChild(trackOfFirst);
    }

    public MyNode.BranchNode<MyNode.BranchNode> getRootNode() {
        return rootNode;
    }

    public ArrayList getList(int i2, int i3){

        ArrayList first = convertNodesToList.getNameArray( rootNode.getChildList() );

        if (i2 != -1){
            MyNode.BranchNode secondNode = ((MyNode.BranchNode)rootNode.getChildList().get(i2));
            ArrayList second = convertNodesToList.getNameArray( secondNode.getChildList() );

            if (i3 != -1){
                MyNode.BranchNode leafNode = ((MyNode.BranchNode)secondNode.getChildList().get(i3));
                ArrayList third = convertNodesToList.getNameArray( leafNode.getChildList() );

                return third;
            }
            else return second;
        }
        else return first;

    }

    public String getCode(int i1, int i2, int i3){
        String  s = "C";

        if (i1 != -1){
            MyNode.BranchNode first = (MyNode.BranchNode)rootNode.getChildList().get(i1);
            s+= first.getCode();
            if (i2 != -1){
                MyNode.BranchNode second = (MyNode.BranchNode)first.getChildList().get(i2);
                s+= second.getCode();
                if (i3 != -1){
                    MyNode.LeafNode last = (MyNode.LeafNode)second.getChildList().get(i3);
                    s+= last.getCode();
                }
            }
        }

        return  s;
    }
    public String getCode(String code, int i3){
        int i1 = Integer.parseInt(code.substring(1,2)) - 1;
        int i2 = Integer.parseInt(code.substring(2,4));

        String s = getCode(i1, i2, i3);

        return  s;
    }
}