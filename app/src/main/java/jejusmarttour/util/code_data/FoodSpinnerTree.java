package jejusmarttour.util.code_data;

import java.util.ArrayList;

/**
 * Created by Osy on 2017-11-29.
 */

public enum FoodSpinnerTree {
    INSTANCE;

    public static final int SPINNER_NOT_SELECT = -1;

    private ConvertNodesToList convertNodesToList;
    public static FoodSpinnerTree getInstance() {
        return INSTANCE;
    }

    private MyNode.BranchNode<MyNode.BranchNode> rootNode;

    private MyNode.BranchNode<MyNode.BranchNode> koChiJa;
    private MyNode.BranchNode<MyNode.BranchNode> euBuFu;
    private MyNode.BranchNode<MyNode.BranchNode> cafeBeverage;
    private MyNode.BranchNode<MyNode.BranchNode> fastFood;
    private MyNode.BranchNode<MyNode.BranchNode> schoolFood;
    private MyNode.BranchNode<MyNode.BranchNode> breadIceCream;

    FoodSpinnerTree(){
        convertNodesToList = ConvertNodesToList.getInstance();
        init();
    }

    private void init(){
        rootNode = new MyNode.BranchNode<>("음식점","B");

        koChiJa = new MyNode.BranchNode<>("한/중/일식","1");
        euBuFu = new MyNode.BranchNode<>("양식/뷔페/퓨전","2");
        cafeBeverage = new MyNode.BranchNode<>("카페/음료","3");
        fastFood = new MyNode.BranchNode<>("패스트푸드","4");
        schoolFood = new MyNode.BranchNode<>("분식","5");
        breadIceCream = new MyNode.BranchNode<>("빵/빙과류","6");

        initKoChinJa();
        initEuBuFu();
        initCafeBeverage();
        initFastFood();
        initSchoolFood();
        initBreadIceCream();

        rootNode.addChild(koChiJa);
        rootNode.addChild(euBuFu);
        rootNode.addChild(cafeBeverage);
        rootNode.addChild(fastFood);
        rootNode.addChild(schoolFood);
        rootNode.addChild(breadIceCream);
    }
    private void initKoChinJa(){
        koChiJa.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode<MyNode.LeafNode> soup = new MyNode.BranchNode<>("국,죽,찌게","01");
            soup.addChild(new MyNode.LeafNode("전체",""));
            soup.addChild(new MyNode.LeafNode("해장국","01"));
            soup.addChild(new MyNode.LeafNode("순대국","02"));
            soup.addChild(new MyNode.LeafNode("부대찌개","03"));
            soup.addChild(new MyNode.LeafNode("설렁탕","04"));
            soup.addChild(new MyNode.LeafNode("삼계탕","05"));
            soup.addChild(new MyNode.LeafNode("보신탕","06"));
            soup.addChild(new MyNode.LeafNode("죽","07"));
            soup.addChild(new MyNode.LeafNode("국,죽 체인점","08"));
            soup.addChild(new MyNode.LeafNode("일반","09"));
        koChiJa.addChild(soup);

        MyNode.BranchNode<MyNode.LeafNode> traditional = new MyNode.BranchNode<>("전통음식(한식)","02");
            traditional.addChild(new MyNode.LeafNode("전체",""));
            traditional.addChild(new MyNode.LeafNode("백반","01"));
            traditional.addChild(new MyNode.LeafNode("비빔밥","02"));
            traditional.addChild(new MyNode.LeafNode("한정식","03"));
            traditional.addChild(new MyNode.LeafNode("전통음식","04"));
            traditional.addChild(new MyNode.LeafNode("떡집","05"));
            traditional.addChild(new MyNode.LeafNode("한식일반","06"));
        koChiJa.addChild(traditional);

        koChiJa.addChild(new MyNode.BranchNode("중식","03"));

        MyNode.BranchNode<MyNode.LeafNode> jokbal = new MyNode.BranchNode<>("족발,보쌈","04");
            jokbal.addChild(new MyNode.LeafNode("전체",""));
            jokbal.addChild(new MyNode.LeafNode("족발","01"));
            jokbal.addChild(new MyNode.LeafNode("보쌈","03"));
        koChiJa.addChild(jokbal);

        MyNode.BranchNode<MyNode.LeafNode> japan = new MyNode.BranchNode<>("일식","05");
            japan.addChild(new MyNode.LeafNode("전체",""));
            japan.addChild(new MyNode.LeafNode("샤브샤브","01"));
            japan.addChild(new MyNode.LeafNode("회전초밥","02"));
            japan.addChild(new MyNode.LeafNode("스시","03"));
            japan.addChild(new MyNode.LeafNode("우동","04"));
            japan.addChild(new MyNode.LeafNode("돈까스","05"));
            japan.addChild(new MyNode.LeafNode("횟집","06"));
            japan.addChild(new MyNode.LeafNode("일반","07"));
        koChiJa.addChild(japan);

        MyNode.BranchNode<MyNode.LeafNode> beef = new MyNode.BranchNode<>("고기","06");
            beef.addChild(new MyNode.LeafNode("전체",""));
            beef.addChild(new MyNode.LeafNode("돼지","01"));
            beef.addChild(new MyNode.LeafNode("소","02"));
            beef.addChild(new MyNode.LeafNode("말,양,꼬치","03"));
            beef.addChild(new MyNode.LeafNode("닭,오리","04"));
            beef.addChild(new MyNode.LeafNode("기타","05"));
        koChiJa.addChild(beef);

        MyNode.BranchNode<MyNode.LeafNode> seafood = new MyNode.BranchNode<>("해산물","07");
            seafood.addChild(new MyNode.LeafNode("전체",""));
            seafood.addChild(new MyNode.LeafNode("탕,찌개","05"));
            seafood.addChild(new MyNode.LeafNode("구이","06"));
            seafood.addChild(new MyNode.LeafNode("일반","07"));
        koChiJa.addChild(seafood);

        MyNode.BranchNode<MyNode.LeafNode> noodle = new MyNode.BranchNode<>("면요리","08");
            noodle.addChild(new MyNode.LeafNode("전체",""));
            noodle.addChild(new MyNode.LeafNode("칼국수","01"));
            noodle.addChild(new MyNode.LeafNode("국수","02"));
            noodle.addChild(new MyNode.LeafNode("라면","03"));
            noodle.addChild(new MyNode.LeafNode("냉면","04"));
            noodle.addChild(new MyNode.LeafNode("체인점기타","05"));
            noodle.addChild(new MyNode.LeafNode("일반","06"));
        koChiJa.addChild(noodle);
    }

    private void initEuBuFu(){
        euBuFu.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode<MyNode.LeafNode> familyRestaurant = new MyNode.BranchNode<>("패밀리레스토랑","01");
            familyRestaurant.addChild(new MyNode.LeafNode("전체",""));
            familyRestaurant.addChild(new MyNode.LeafNode("아웃백","01"));
            familyRestaurant.addChild(new MyNode.LeafNode("빕스","02"));
            familyRestaurant.addChild(new MyNode.LeafNode("애슐리","03"));
            familyRestaurant.addChild(new MyNode.LeafNode("한스델리","04"));
            familyRestaurant.addChild(new MyNode.LeafNode("체인점기타","05"));
            familyRestaurant.addChild(new MyNode.LeafNode("일반","06"));
        euBuFu.addChild(familyRestaurant);

        MyNode.BranchNode<MyNode.LeafNode> buffet = new MyNode.BranchNode<>("뷔페","02");
            buffet.addChild(new MyNode.LeafNode("전체",""));
            buffet.addChild(new MyNode.LeafNode("고기뷔페","01"));
            buffet.addChild(new MyNode.LeafNode("씨푸드뷔페","02"));
            buffet.addChild(new MyNode.LeafNode("일반","03"));
        euBuFu.addChild(buffet);

        MyNode.BranchNode<MyNode.LeafNode> fusion = new MyNode.BranchNode<>("퓨전음식점","03");
            fusion.addChild(new MyNode.LeafNode("전체",""));
            fusion.addChild(new MyNode.LeafNode("체인점기타","01"));
            fusion.addChild(new MyNode.LeafNode("일반","02"));
        euBuFu.addChild(fusion);

        MyNode.BranchNode<MyNode.LeafNode> novelty = new MyNode.BranchNode<>("이색음식점","04");
            novelty.addChild(new MyNode.LeafNode("전체",""));
            novelty.addChild(new MyNode.LeafNode("인도음식","01"));
            novelty.addChild(new MyNode.LeafNode("태국음식","02"));
            novelty.addChild(new MyNode.LeafNode("베트남음식","03"));
            novelty.addChild(new MyNode.LeafNode("이탈리아음식","04"));
            novelty.addChild(new MyNode.LeafNode("체인점기타","05"));
            novelty.addChild(new MyNode.LeafNode("일반","06"));
        euBuFu.addChild(novelty);
        
        MyNode.BranchNode<MyNode.LeafNode> westernFood = new MyNode.BranchNode<>("양식","05");
            westernFood.addChild(new MyNode.LeafNode("전체",""));
            westernFood.addChild(new MyNode.LeafNode("스파게티","01"));
            westernFood.addChild(new MyNode.LeafNode("일반","02"));
        euBuFu.addChild(westernFood);
    }
    private void initCafeBeverage(){
        cafeBeverage.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode<MyNode.LeafNode> starbucks = new MyNode.BranchNode<>("카페","01");
            starbucks.addChild(new MyNode.LeafNode("전체",""));
            starbucks.addChild(new MyNode.LeafNode("스타벅스","01"));
            starbucks.addChild(new MyNode.LeafNode("카페베네","02"));
            starbucks.addChild(new MyNode.LeafNode("엔젤리너스","03"));
            starbucks.addChild(new MyNode.LeafNode("띠아모","04"));
            starbucks.addChild(new MyNode.LeafNode("탐앤탐스","05"));
            starbucks.addChild(new MyNode.LeafNode("할리스","06"));
            starbucks.addChild(new MyNode.LeafNode("파스쿠찌","07"));
            starbucks.addChild(new MyNode.LeafNode("투썸플레이스","08"));
            starbucks.addChild(new MyNode.LeafNode("이디야","09"));
            starbucks.addChild(new MyNode.LeafNode("빽다방","10"));
            starbucks.addChild(new MyNode.LeafNode("북카페","11"));
            starbucks.addChild(new MyNode.LeafNode("키즈카페","12"));
            starbucks.addChild(new MyNode.LeafNode("애견카페","13"));
            starbucks.addChild(new MyNode.LeafNode("사주카페","14"));
            starbucks.addChild(new MyNode.LeafNode("체인점기타","15"));
            starbucks.addChild(new MyNode.LeafNode("일반카페","16"));
        cafeBeverage.addChild(starbucks);

        MyNode.BranchNode<MyNode.LeafNode> beverage = new MyNode.BranchNode<>("음료","02");
            beverage.addChild(new MyNode.LeafNode("전체",""));
            beverage.addChild(new MyNode.LeafNode("체인점기타","01"));
            beverage.addChild(new MyNode.LeafNode("일반","02"));
        cafeBeverage.addChild(beverage);
    }
    private void initFastFood(){
        fastFood.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode<MyNode.LeafNode> hamburger = new MyNode.BranchNode<>("햄버거","01");
            hamburger.addChild(new MyNode.LeafNode("전체",""));
            hamburger.addChild(new MyNode.LeafNode("롯데리아","01"));
            hamburger.addChild(new MyNode.LeafNode("맥도날드","02"));
            hamburger.addChild(new MyNode.LeafNode("맘스터치","03"));
            hamburger.addChild(new MyNode.LeafNode("수제버거","04"));
            hamburger.addChild(new MyNode.LeafNode("체인점기타","05"));
            hamburger.addChild(new MyNode.LeafNode("일반","06"));
        fastFood.addChild(hamburger);

        MyNode.BranchNode<MyNode.LeafNode> chicken = new MyNode.BranchNode<>("치킨","02");
            chicken.addChild(new MyNode.LeafNode("전체",""));
            chicken.addChild(new MyNode.LeafNode("굽네치킨","01"));
            chicken.addChild(new MyNode.LeafNode("네네치킨","02"));
            chicken.addChild(new MyNode.LeafNode("본스치킨","03"));
            chicken.addChild(new MyNode.LeafNode("멕시카나","04"));
            chicken.addChild(new MyNode.LeafNode("페리카나","05"));
            chicken.addChild(new MyNode.LeafNode("BBQ","06"));
            chicken.addChild(new MyNode.LeafNode("교촌치킨","07"));
            chicken.addChild(new MyNode.LeafNode("오븐에빠진닭","08"));
            chicken.addChild(new MyNode.LeafNode("호식이두마리","09"));
            chicken.addChild(new MyNode.LeafNode("체인점기타","10"));
            chicken.addChild(new MyNode.LeafNode("일반","11"));
        fastFood.addChild(chicken);

        MyNode.BranchNode<MyNode.LeafNode> pizza = new MyNode.BranchNode<>("피자","03");
            pizza.addChild(new MyNode.LeafNode("전체",""));
            pizza.addChild(new MyNode.LeafNode("피자헛","01"));
            pizza.addChild(new MyNode.LeafNode("미스터피자","02"));
            pizza.addChild(new MyNode.LeafNode("도미노피자","03"));
            pizza.addChild(new MyNode.LeafNode("피자알볼로","04"));
            pizza.addChild(new MyNode.LeafNode("난타5000","05"));
            pizza.addChild(new MyNode.LeafNode("체인점기타","06"));
            pizza.addChild(new MyNode.LeafNode("일반","07"));
        fastFood.addChild(pizza);

        MyNode.BranchNode<MyNode.LeafNode> sandwich = new MyNode.BranchNode<>("샌드위치,토스트","05");
            sandwich.addChild(new MyNode.LeafNode("전체",""));
            sandwich.addChild(new MyNode.LeafNode("이삭토스트","01"));
            sandwich.addChild(new MyNode.LeafNode("석봉토스트","02"));
            sandwich.addChild(new MyNode.LeafNode("체인점기타","03"));
            sandwich.addChild(new MyNode.LeafNode("샌드위치일반","04"));
            sandwich.addChild(new MyNode.LeafNode("토스트일반","05"));
        fastFood.addChild(sandwich);

        fastFood.addChild(new MyNode.BranchNode<>("패스트푸드기타","06"));
    }

    private void initSchoolFood(){
        schoolFood.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode<MyNode.LeafNode> dduckbokki = new MyNode.BranchNode<>("떡볶이","01");
            dduckbokki.addChild(new MyNode.LeafNode("전체",""));
            dduckbokki.addChild(new MyNode.LeafNode("죠스떡볶이","01"));
            dduckbokki.addChild(new MyNode.LeafNode("신전떡볶이","02"));
            dduckbokki.addChild(new MyNode.LeafNode("엽기떡볶이","03"));
            dduckbokki.addChild(new MyNode.LeafNode("체인점기타","04"));
            dduckbokki.addChild(new MyNode.LeafNode("일반","05"));
        schoolFood.addChild(dduckbokki);

        MyNode.BranchNode<MyNode.LeafNode> gimbob = new MyNode.BranchNode<>("김밥","02");
            gimbob.addChild(new MyNode.LeafNode("전체",""));
            gimbob.addChild(new MyNode.LeafNode("김밥천국","01"));
            gimbob.addChild(new MyNode.LeafNode("김가네","02"));
            gimbob.addChild(new MyNode.LeafNode("체인점기타","03"));
            gimbob.addChild(new MyNode.LeafNode("일반","04"));
        schoolFood.addChild(gimbob);

        schoolFood.addChild(new MyNode.BranchNode<>("분식기타","03"));
    }

    private void initBreadIceCream(){
        breadIceCream.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode<MyNode.LeafNode> donut = new MyNode.BranchNode<>("도넛","01");
            donut.addChild(new MyNode.LeafNode("전체",""));
            donut.addChild(new MyNode.LeafNode("던킨도너츠","01"));
            donut.addChild(new MyNode.LeafNode("크리스피크림도넛","02"));
            donut.addChild(new MyNode.LeafNode("체인점기타","03"));
            donut.addChild(new MyNode.LeafNode("일반","04"));
        breadIceCream.addChild(donut);

        MyNode.BranchNode<MyNode.LeafNode> bread = new MyNode.BranchNode<>("제과점","02");
            bread.addChild(new MyNode.LeafNode("전체",""));
            bread.addChild(new MyNode.LeafNode("파리바게트","01"));
            bread.addChild(new MyNode.LeafNode("뚜레주르","02"));
            bread.addChild(new MyNode.LeafNode("크라운베이커리","03"));
            bread.addChild(new MyNode.LeafNode("체인점기타","04"));
            bread.addChild(new MyNode.LeafNode("일반","05"));
        breadIceCream.addChild(bread);

        MyNode.BranchNode<MyNode.LeafNode> iceCream = new MyNode.BranchNode<>("피자","03");
            iceCream.addChild(new MyNode.LeafNode("전체",""));
            iceCream.addChild(new MyNode.LeafNode("배스킨라빈스","01"));
            iceCream.addChild(new MyNode.LeafNode("나뚜르","02"));
            iceCream.addChild(new MyNode.LeafNode("체인점기타","03"));
            iceCream.addChild(new MyNode.LeafNode("일반","04"));
        breadIceCream.addChild(iceCream);
    }

    public MyNode.BranchNode<MyNode.BranchNode> getRootNode() {
        return rootNode;
    }

    public ArrayList getList(int i2, int i3){

        ArrayList<String> first = convertNodesToList.getNameArray( rootNode.getChildList() );

        if (i2 != -1){
            MyNode.BranchNode secondNode = ((MyNode.BranchNode)rootNode.getChildList().get(i2));
            ArrayList<String> second = convertNodesToList.getNameArray( secondNode.getChildList() );

            if (i3 != -1){
                MyNode.BranchNode leafNode = ((MyNode.BranchNode)secondNode.getChildList().get(i3));
                ArrayList<String> third = convertNodesToList.getNameArray( leafNode.getChildList() );

                return third;
            }
            else return second;
        }
        else return first;
    }

    public String getCode(int i1, int i2, int i3){
        String  s = "B";

        if (i1 != SPINNER_NOT_SELECT){
            MyNode.BranchNode first = (MyNode.BranchNode)rootNode.getChildList().get(i1);
            s+= first.getCode();
            if (i2 != SPINNER_NOT_SELECT){
                MyNode.BranchNode second = (MyNode.BranchNode)first.getChildList().get(i2);
                s+= second.getCode();
                if (i3 != SPINNER_NOT_SELECT){
                    MyNode.LeafNode last = (MyNode.LeafNode)second.getChildList().get(i3);
                    s+= last.getCode();
                }
            }
        }

        return  s;
    }
    public String getCode(String code, int i3){
        int i1 = Integer.parseInt(code.substring(1,2)) - 1;
        int i2 = Integer.parseInt(code.substring(2,4));

        String s = getCode(i1, i2, i3);

        return  s;
    }
}