/*
package jejusmarttour.util.code_data;

import android.util.Pair;

import java.util.ArrayList;

*/
/**
 * Created by Osy on 2017-11-23.
 *//*


public enum  CodeTest {
    INSTANCE;

    TreeFactory treeFactory;
    TreeFactory.BranchNode famousSpot;
    TreeFactory.BranchNode trackOfJeju;
    TreeFactory.BranchNode commonSpot;

    public static CodeTest getInstance() {
        return INSTANCE;
    }

    CodeTest(){
        treeFactory = new TreeFactory();
        TreeFactory.BranchNode famousSpot = new TreeFactory.BranchNode("유명관광지", "1");
        TreeFactory.BranchNode trackOfJeju = new TreeFactory.BranchNode("제주의 길", "2");
        TreeFactory.BranchNode commonSpot = new TreeFactory.BranchNode("일반관광지", "3");

        initFamousSpot();
        initTrackOfJeju();
        initCommonSpot();

        treeFactory.addChild(famousSpot);
        treeFactory.addChild(trackOfJeju);
        treeFactory.addChild(commonSpot);
    }

    private void initFamousSpot(){
        ArrayList famousUdo = new ArrayList<>();
        famousUdo.add( Pair.create("전체",""));
        famousUdo.add( Pair.create("Sub zone","01"));
        famousUdo.add( Pair.create("우도팔경","02"));
        famousUdo.add( Pair.create("우도봉","03"));

        ArrayList famousHalla = new ArrayList<>();
        famousHalla.add( Pair.create("전체",""));
        famousHalla.add( Pair.create("sub zone","01"));
        famousHalla.add( Pair.create("한라산국립공원","02"));

        ArrayList famousSanbang = new ArrayList<>();
        famousSanbang.add( Pair.create("전체",""));
        famousSanbang.add( Pair.create("sub zone","01"));

        famousSpot.addChild( new TreeFactory.LeafNode("전체",null));
        famousSpot.addChild( new TreeFactory.LeafNode("우도zone", famousUdo));
        famousSpot.addChild( new TreeFactory.LeafNode("성산일출봉",null));
        famousSpot.addChild( new TreeFactory.LeafNode("한라산", famousHalla));
        famousSpot.addChild( new TreeFactory.LeafNode("월정리해변",null));
        famousSpot.addChild( new TreeFactory.LeafNode("협재해수욕장",null));
        famousSpot.addChild( new TreeFactory.LeafNode("섭지코지",null));
        famousSpot.addChild( new TreeFactory.LeafNode("오설록",null));
        famousSpot.addChild( new TreeFactory.LeafNode("산방산zone", famousSanbang));
        famousSpot.addChild( new TreeFactory.LeafNode("매일올레시장",null));
        famousSpot.addChild( new TreeFactory.LeafNode("카멜리아힐",null));
    }
    private void initTrackOfJeju(){
        ArrayList trackOfFirst = new ArrayList<>();
        trackOfFirst.add( Pair.create("전체",""));
        trackOfFirst.add( Pair.create("1코스","01"));
        trackOfFirst.add( Pair.create("2코스","02"));
        trackOfFirst.add( Pair.create("3코스","03"));

        ArrayList trackOfSecond = new ArrayList<>();
        trackOfSecond.add( Pair.create("전체",""));
        trackOfSecond.add( Pair.create("올레1길","01"));
        trackOfSecond.add( Pair.create("올레1-1길","02"));
        trackOfSecond.add( Pair.create("올레2길","03"));
        trackOfSecond.add( Pair.create("올레3길","04"));
        trackOfSecond.add( Pair.create("올레3-1길","05"));
        trackOfSecond.add( Pair.create("올레4길","06"));
        trackOfSecond.add( Pair.create("올레5길","07"));
        trackOfSecond.add( Pair.create("올레6길","08"));
        trackOfSecond.add( Pair.create("올레7길","09"));
        trackOfSecond.add( Pair.create("올레7-1길","10"));

        ArrayList trackOfThird = new ArrayList<>();
        trackOfThird.add( Pair.create("전체",""));
        trackOfThird.add( Pair.create("올레8길","01"));
        trackOfThird.add( Pair.create("올레9길","02"));
        trackOfThird.add( Pair.create("올레10길","03"));
        trackOfThird.add( Pair.create("올레10-1길","04"));
        trackOfThird.add( Pair.create("올레11길","05"));
        trackOfThird.add( Pair.create("올레12길","06"));
        trackOfThird.add( Pair.create("올레13길","07"));
        trackOfThird.add( Pair.create("올레14길","08"));
        trackOfThird.add( Pair.create("올레14-1길","09"));
        trackOfThird.add( Pair.create("올레15-A길","10"));
        trackOfThird.add( Pair.create("올레15-B길","11"));

        ArrayList trackOfFord = new ArrayList<>();
        trackOfFord.add( Pair.create("전체",""));
        trackOfFord.add( Pair.create("올레16길","01"));
        trackOfFord.add( Pair.create("올레17-A길","02"));
        trackOfFord.add( Pair.create("올레17-B길","03"));
        trackOfFord.add( Pair.create("올레18길","04"));
        trackOfFord.add( Pair.create("올레18-1A길","05"));
        trackOfFord.add( Pair.create("올레18-1B길","06"));
        trackOfFord.add( Pair.create("올레19길","07"));
        trackOfFord.add( Pair.create("올레20길","08"));
        trackOfFord.add( Pair.create("올레21길","09"));

        ArrayList trackOfFifth = new ArrayList<>();
        trackOfFifth.add( Pair.create("전체",""));
        trackOfFifth.add( Pair.create("산책로","01"));
        trackOfFifth.add( Pair.create("밭길","02"));
        trackOfFifth.add( Pair.create("해안길","04"));
        trackOfFifth.add( Pair.create("숲길","05"));
        trackOfFifth.add( Pair.create("지질트레일","06"));
        trackOfFifth.add( Pair.create("기타","07"));

        trackOfJeju.addChild(new TreeFactory.LeafNode("전체",null));
        trackOfJeju.addChild(new TreeFactory.LeafNode("추사유배길", trackOfFirst));
        trackOfJeju.addChild(new TreeFactory.LeafNode("올레1~7길", trackOfSecond));
        trackOfJeju.addChild(new TreeFactory.LeafNode("올레8~15길", trackOfThird));
        trackOfJeju.addChild(new TreeFactory.LeafNode("올레16~21길", trackOfFord));
        trackOfJeju.addChild(new TreeFactory.LeafNode("기타", trackOfFifth));
    }
    private void initCommonSpot(){
        ArrayList spotOfNature = new ArrayList<>();
        spotOfNature.add( Pair.create("전체",""));
        spotOfNature.add( Pair.create("산,오름","01"));
        spotOfNature.add( Pair.create("바다","02"));
        spotOfNature.add( Pair.create("길,올레","03"));
        spotOfNature.add( Pair.create("섬","04"));
        spotOfNature.add( Pair.create("휴양림","05"));
        spotOfNature.add( Pair.create("공원","06"));
        spotOfNature.add( Pair.create("논,밭","07"));
        spotOfNature.add( Pair.create("계곡(강,하천)","08"));
        spotOfNature.add( Pair.create("용천수","09"));
        spotOfNature.add( Pair.create("명소","10"));
        spotOfNature.add( Pair.create("자연물","11"));
        spotOfNature.add( Pair.create("폭포","12"));
        spotOfNature.add( Pair.create("기타","13"));

        ArrayList spotOfCulture = new ArrayList<>();
        spotOfCulture.add( Pair.create("전체",""));
        spotOfCulture.add( Pair.create("문화재,유적","01"));
        spotOfCulture.add( Pair.create("박물관,예술","02"));
        spotOfCulture.add( Pair.create("테마파크","03"));
        spotOfCulture.add( Pair.create("농장,목장","04"));
        spotOfCulture.add( Pair.create("마을,지역","05"));
        spotOfCulture.add( Pair.create("항구,포구","06"));
        spotOfCulture.add( Pair.create("등대","07"));
        spotOfCulture.add( Pair.create("명소","08"));
        spotOfCulture.add( Pair.create("축제","09"));
        spotOfCulture.add( Pair.create("종교","10"));
        spotOfCulture.add( Pair.create("기타","11"));

        ArrayList spotOfLeisure = new ArrayList<>();
        spotOfLeisure.add( Pair.create("전체",""));
        spotOfLeisure.add( Pair.create("수상","01"));
        spotOfLeisure.add( Pair.create("지상","02"));
        spotOfLeisure.add( Pair.create("항공","03"));
        spotOfLeisure.add( Pair.create("공원","04"));
        spotOfLeisure.add( Pair.create("스포츠","05"));
        spotOfLeisure.add( Pair.create("기타","06"));

        ArrayList spotOfExperience = new ArrayList<>();
        spotOfExperience.add( Pair.create("전체",""));
        spotOfExperience.add( Pair.create("농장,목장","01"));
        spotOfExperience.add( Pair.create("승마","02"));
        spotOfExperience.add( Pair.create("해녀","03"));
        spotOfExperience.add( Pair.create("낚시","04"));
        spotOfExperience.add( Pair.create("잠수함","05"));
        spotOfExperience.add( Pair.create("기타","06"));

        commonSpot.addChild(new TreeFactory.LeafNode("전체", null));
        commonSpot.addChild(new TreeFactory.LeafNode("자연", spotOfNature));
        commonSpot.addChild(new TreeFactory.LeafNode("문화", spotOfCulture));
        commonSpot.addChild(new TreeFactory.LeafNode("레저", spotOfLeisure));
        commonSpot.addChild(new TreeFactory.LeafNode("체험", spotOfExperience));
        commonSpot.addChild(new TreeFactory.LeafNode("편의", null));
    }

    public ArrayList getFirstDepthTitleList(){
        ArrayList arrayList = new ArrayList<>();
        arrayList.add(famousSpot.getName());
        arrayList.add(trackOfJeju.getName());
        arrayList.add(commonSpot.getName());

        return arrayList;
    }

    public String getFirstDepthCode(int position){
        return treeFactory.getChildList().get(position).getCode();
    }
}
*/
