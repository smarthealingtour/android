package jejusmarttour.util.code_data;

import java.util.ArrayList;

/**
 * Created by Osy on 2017-11-29.
 */

public enum SpotSpinnerTree {
    INSTANCE;

    private ConvertNodesToList convertNodesToList;
    public static SpotSpinnerTree getInstance() {
        return INSTANCE;
    }

    private MyNode.BranchNode<MyNode.BranchNode> rootNode;

    private MyNode.BranchNode<MyNode.BranchNode> famousSpot;
    private MyNode.BranchNode<MyNode.BranchNode> trackOfJeju;
    private MyNode.BranchNode<MyNode.BranchNode> commonSpot;

    SpotSpinnerTree(){
        convertNodesToList = ConvertNodesToList.getInstance();
        init();
    }

    private void init(){
        rootNode = new MyNode.BranchNode<>("관광명소","A");

        famousSpot = new MyNode.BranchNode<>("유명관광지","1");
        trackOfJeju = new MyNode.BranchNode<>("제주의 길","2");
        commonSpot = new MyNode.BranchNode<>("일반관광지","3");

        initFamous();
        initTrackOfJeju();
        initCommonSpot();

        rootNode.addChild(famousSpot);
        rootNode.addChild(trackOfJeju);
        rootNode.addChild(commonSpot);
    }
    private void initFamous(){
        famousSpot.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode udo = new MyNode.BranchNode("우도zone","01");
            udo.addChild(new MyNode.LeafNode("전체",""));
            udo.addChild(new MyNode.LeafNode("Sub zone","01",true));
            udo.addChild(new MyNode.LeafNode("우도팔경","02",true));
            udo.addChild(new MyNode.LeafNode("우도봉","03",true));
        famousSpot.addChild(udo);
        famousSpot.addChild(new MyNode.BranchNode("성산일출봉","02"));

        MyNode.BranchNode hallaSan = new MyNode.BranchNode("한라산","03");
            hallaSan.addChild(new MyNode.LeafNode("전체",""));
            hallaSan.addChild(new MyNode.LeafNode("관음사탐방로","01",true));
            hallaSan.addChild(new MyNode.LeafNode("돈내코탐방로","02",true));
            hallaSan.addChild(new MyNode.LeafNode("성판악탐방로","03",true));
            hallaSan.addChild(new MyNode.LeafNode("어리목탐방로","04",true));
            hallaSan.addChild(new MyNode.LeafNode("어승생악탐방로","05",true));
            hallaSan.addChild(new MyNode.LeafNode("영실탐방로","06",true));
        famousSpot.addChild(hallaSan);

        famousSpot.addChild(new MyNode.BranchNode("월정리해변","04"));
        famousSpot.addChild(new MyNode.BranchNode("협재해수욕장","05"));
        famousSpot.addChild(new MyNode.BranchNode("섭지코지","06"));
        famousSpot.addChild(new MyNode.BranchNode("오설록","07"));

        MyNode.BranchNode sanbangSan  = new MyNode.BranchNode("산방산zone","08");
            sanbangSan.addChild(new MyNode.LeafNode("전체",""));
            sanbangSan.addChild(new MyNode.LeafNode("Sub zone","01",true));
        famousSpot.addChild(sanbangSan);
        famousSpot.addChild(new MyNode.BranchNode("매일올레시장","09"));
        famousSpot.addChild(new MyNode.BranchNode("카멜리아힐","10"));
    }
    private void initTrackOfJeju(){
        trackOfJeju.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode trackOfFirst = new MyNode.BranchNode("추사유배길","01");
            trackOfFirst.addChild(new MyNode.LeafNode("전체",""));
            trackOfFirst.addChild(new MyNode.LeafNode("1코스","01",true));
            trackOfFirst.addChild(new MyNode.LeafNode("2코스","02",true));
            trackOfFirst.addChild(new MyNode.LeafNode("3코스","03",true));
        trackOfJeju.addChild(trackOfFirst);

        MyNode.BranchNode trackOfSecond = new MyNode.BranchNode("올레1~7길","02");
            trackOfSecond.addChild(new MyNode.LeafNode("전체",""));
            trackOfSecond.addChild(new MyNode.LeafNode("올레1길","01",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레1-1길","02",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레2길","03",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레3길","04",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레3-1길","05",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레4길","06",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레5길","07",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레6길","08",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레7길","09",true));
            trackOfSecond.addChild(new MyNode.LeafNode("올레7-1길","10",true));
        trackOfJeju.addChild(trackOfSecond);

        MyNode.BranchNode trackOfThird = new MyNode.BranchNode("올레8~15길","03");
            trackOfThird.addChild(new MyNode.LeafNode("전체",""));
            trackOfThird.addChild(new MyNode.LeafNode("올레8길","01",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레9길","02",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레10길","03",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레10-1길","04",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레11길","05",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레12길","06",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레13길","07",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레14길","08",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레14-1길","09",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레15-A길","10",true));
            trackOfThird.addChild(new MyNode.LeafNode("올레15-B길","11",true));
        trackOfJeju.addChild(trackOfThird);

        MyNode.BranchNode trackOfFord = new MyNode.BranchNode("올레16~21길","04");
            trackOfFord.addChild(new MyNode.LeafNode("전체","",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레16길","01",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레17-A길","02",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레17-B길","03",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레18길","04",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레18-1A길","05",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레18-1B길","06",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레19길","07",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레20길","08",true));
            trackOfFord.addChild(new MyNode.LeafNode("올레21길","09",true));
        trackOfJeju.addChild(trackOfFord);
        
        MyNode.BranchNode trackOfFifth = new MyNode.BranchNode("기타","05");
            trackOfFifth.addChild(new MyNode.LeafNode("전체",""));
            trackOfFifth.addChild(new MyNode.LeafNode("산책로","01"));
            trackOfFifth.addChild(new MyNode.LeafNode("밭길","02"));
            trackOfFifth.addChild(new MyNode.LeafNode("해안길","03"));
            trackOfFifth.addChild(new MyNode.LeafNode("숲길","04"));
            trackOfFifth.addChild(new MyNode.LeafNode("지질트레일","05"));
            trackOfFifth.addChild(new MyNode.LeafNode("기타","06"));
        trackOfJeju.addChild(trackOfFifth);
    }
    private void initCommonSpot(){
        commonSpot.addChild(new MyNode.BranchNode("전체",""));

        MyNode.BranchNode spotOfNature = new MyNode.BranchNode("자연","01");
        spotOfNature.addChild(new MyNode.LeafNode("전체",""));
        spotOfNature.addChild(new MyNode.LeafNode("산,오름","01"));
        spotOfNature.addChild(new MyNode.LeafNode("바다","02"));
        spotOfNature.addChild(new MyNode.LeafNode("길,올레","03"));
        spotOfNature.addChild(new MyNode.LeafNode("섬","04"));
        spotOfNature.addChild(new MyNode.LeafNode("휴양림","05"));
        spotOfNature.addChild(new MyNode.LeafNode("공원","06"));
        spotOfNature.addChild(new MyNode.LeafNode("논,밭","07"));
        spotOfNature.addChild(new MyNode.LeafNode("계곡,강,하천","08"));
        spotOfNature.addChild(new MyNode.LeafNode("용천수","09"));
        spotOfNature.addChild(new MyNode.LeafNode("명소","10"));
        spotOfNature.addChild(new MyNode.LeafNode("바위,자연물","11"));
        spotOfNature.addChild(new MyNode.LeafNode("폭포","12"));
        spotOfNature.addChild(new MyNode.LeafNode("기타","13"));
        commonSpot.addChild(spotOfNature);

        MyNode.BranchNode spotOfCulture = new MyNode.BranchNode("문화","02");
        spotOfCulture.addChild(new MyNode.LeafNode("전체",""));
        spotOfCulture.addChild(new MyNode.LeafNode("문화재,유적지","01"));
        spotOfCulture.addChild(new MyNode.LeafNode("박물관,미술관","02"));
        spotOfCulture.addChild(new MyNode.LeafNode("테마파크","03"));
        spotOfCulture.addChild(new MyNode.LeafNode("농장,목장","04"));
        spotOfCulture.addChild(new MyNode.LeafNode("마을,지역","05"));
        spotOfCulture.addChild(new MyNode.LeafNode("항구,포구","06"));
        spotOfCulture.addChild(new MyNode.LeafNode("등대","07"));
        spotOfCulture.addChild(new MyNode.LeafNode("명소","08"));
        spotOfCulture.addChild(new MyNode.LeafNode("축제","09"));
        spotOfCulture.addChild(new MyNode.LeafNode("종교,민간신앙","10"));
        spotOfCulture.addChild(new MyNode.LeafNode("기타","10"));
        commonSpot.addChild(spotOfCulture);

        MyNode.BranchNode spotOfLeisure = new MyNode.BranchNode("레저,스포츠","03");
        spotOfLeisure.addChild(new MyNode.LeafNode("전체",""));
        spotOfLeisure.addChild(new MyNode.LeafNode("수상","01"));
        spotOfLeisure.addChild(new MyNode.LeafNode("지상","02"));
        spotOfLeisure.addChild(new MyNode.LeafNode("항공","03"));
        spotOfLeisure.addChild(new MyNode.LeafNode("공원","04"));
        spotOfLeisure.addChild(new MyNode.LeafNode("스포츠","05"));
        spotOfLeisure.addChild(new MyNode.LeafNode("기타","06"));
        commonSpot.addChild(spotOfLeisure);

        MyNode.BranchNode spotOfExperience = new MyNode.BranchNode("체험","04");
        spotOfExperience.addChild(new MyNode.LeafNode("전체",""));
        spotOfExperience.addChild(new MyNode.LeafNode("농장,목장","01"));
        spotOfExperience.addChild(new MyNode.LeafNode("승마","02"));
        spotOfExperience.addChild(new MyNode.LeafNode("해녀","03"));
        spotOfExperience.addChild(new MyNode.LeafNode("낚시","04"));
        spotOfExperience.addChild(new MyNode.LeafNode("잠수함","05"));
        commonSpot.addChild(spotOfExperience);

        MyNode.BranchNode spotOfConvenient = new MyNode.BranchNode("편의","05");
        spotOfConvenient.addChild(new MyNode.LeafNode("전체",""));
        spotOfConvenient.addChild(new MyNode.LeafNode("휴게소","01"));
        spotOfConvenient.addChild(new MyNode.LeafNode("행정기관","02"));
        spotOfConvenient.addChild(new MyNode.LeafNode("안내","03"));
        spotOfConvenient.addChild(new MyNode.LeafNode("교통","04"));
        spotOfConvenient.addChild(new MyNode.LeafNode("병원","05"));
        spotOfConvenient.addChild(new MyNode.LeafNode("마사지","06"));
        spotOfConvenient.addChild(new MyNode.LeafNode("약국","07"));
        spotOfConvenient.addChild(new MyNode.LeafNode("마트","08"));
        spotOfConvenient.addChild(new MyNode.LeafNode("영화,오락","09"));
        spotOfConvenient.addChild(new MyNode.LeafNode("기타","10"));
        commonSpot.addChild(spotOfConvenient);
    }

    public MyNode.BranchNode<MyNode.BranchNode> getRootNode() {
        return rootNode;
    }

    public ArrayList getList(int i2, int i3){

        ArrayList first = convertNodesToList.getNameArray( rootNode.getChildList() );

        if (i2 != -1){
            MyNode.BranchNode secondNode = ((MyNode.BranchNode)rootNode.getChildList().get(i2));
            ArrayList second = convertNodesToList.getNameArray( secondNode.getChildList() );

            if (i3 != -1){
                MyNode.BranchNode leafNode = ((MyNode.BranchNode)secondNode.getChildList().get(i3));
                ArrayList third = convertNodesToList.getNameArray( leafNode.getChildList() );

                return third;
            }
            else return second;
        }
        else return first;

    }

    public String getCode(int i1, int i2, int i3){
        String  s = "";

        if (i1 != -1){
            MyNode.BranchNode first = (MyNode.BranchNode)rootNode.getChildList().get(i1);
            s+= first.getCode();
            if (i2 != -1){
                MyNode.BranchNode second = (MyNode.BranchNode)first.getChildList().get(i2);
                s+= second.getCode();
                if (i3 != -1){
                    MyNode.LeafNode last = (MyNode.LeafNode)second.getChildList().get(i3);
                    s+= last.getCode();
                }
            }
        }

        return  s;
    }

    public String getCode(String code, int i3){
        int i1 = Integer.parseInt(code.substring(0,1)) - 1;
        int i2 = Integer.parseInt(code.substring(1,3));

        String s = getCode(i1, i2, i3);

        return  s;
    }

    public boolean hasSubZone(String code, int i3){
        int i1 = Integer.parseInt(code.substring(0,1)) - 1;
        int i2 = Integer.parseInt(code.substring(1,3));

        boolean hasSubZone = false;
        if (i1 != -1){
            MyNode.BranchNode first = (MyNode.BranchNode)rootNode.getChildList().get(i1);
            if (i2 != -1){
                MyNode.BranchNode second = (MyNode.BranchNode)first.getChildList().get(i2);
                if (i3 != -1){
                    MyNode.LeafNode last = (MyNode.LeafNode)second.getChildList().get(i3);
                    hasSubZone = last.hasSubZone();
                }
            }
        }
        return hasSubZone;
    }
}