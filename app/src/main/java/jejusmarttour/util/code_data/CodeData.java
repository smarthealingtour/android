//package jejusmarttour.util.code_data;
//
//import android.util.Pair;
//
//import java.util.ArrayList;
//
///**
// * Created by Osy on 2017-11-05.
// */
//public enum CodeData {
//    INSTANCE;
//
//    ArrayList< Pair<String,String>> kindOfTour;
//
//    ArrayList< Pair<String,String>> famousSpot;
//    ArrayList< Pair<String,String>> famousUdo;
//    ArrayList< Pair<String,String>> famousHalla;
//    ArrayList< Pair<String,String>> famousSanbang;
//
//    ArrayList< Pair<String,String>> trackOfJeju;
//    ArrayList< Pair<String,String>> trackOfFirst;
//    ArrayList< Pair<String,String>> trackOfSecond;
//    ArrayList< Pair<String,String>> trackOfThird;
//    ArrayList< Pair<String,String>> trackOfFord;
//    ArrayList< Pair<String,String>> trackOfFifth;
//
//    ArrayList< Pair<String,String>> commonTourSpot;
//    ArrayList< Pair<String,String>> spotOfNature;
//    ArrayList< Pair<String,String>> spotOfCulture;
//    ArrayList< Pair<String,String>> spotOfLeisure;
//    ArrayList< Pair<String,String>> spotOfExperience;
//
//    public static CodeData getInstance() {
//        return INSTANCE;
//    }
//
//    CodeData(){
//        initData();
//    }
//
//    private void initData(){
//        kindOfTour = new ArrayList<>();
//        kindOfTour.add( Pair.create("유명관광지    ","1"));
//        kindOfTour.add( Pair.create("제주의 길    " ,"2"));
//        kindOfTour.add( Pair.create("일반관광지    ","3"));
//
//        famousSpot = new ArrayList<>();
//        famousSpot.add( Pair.create("전체    ",""));
//        famousSpot.add( Pair.create("우도Zone    ","01"));
//        famousSpot.add( Pair.create("성산일출봉    ","02"));
//        famousSpot.add( Pair.create("한라산    ","03"));
//        famousSpot.add( Pair.create("월정리해변    ","04"));
//        famousSpot.add( Pair.create("협재해수욕장    ","05"));
//        famousSpot.add( Pair.create("섭지코지    ","06"));
//        famousSpot.add( Pair.create("오설록    ","07"));
//        famousSpot.add( Pair.create("산방산Zone    ","08"));
//        famousSpot.add( Pair.create("매일올레시장    ","09"));
//        famousSpot.add( Pair.create("카멜리아힐    ","10"));
//
//        famousUdo = new ArrayList<>();
//        famousUdo.add( Pair.create("전체    ",""));
//        famousUdo.add( Pair.create("Sub zone    ","01"));
//        famousUdo.add( Pair.create("우도팔경    ","02"));
//        famousUdo.add( Pair.create("우도봉    ","03"));
//
//        famousHalla = new ArrayList<>();
//        famousHalla.add( Pair.create("전체    ",""));
//        famousHalla.add( Pair.create("sub zone    ","01"));
//        famousHalla.add( Pair.create("한라산국립공원    ","02"));
//
//        famousSanbang = new ArrayList<>();
//        famousSanbang.add( Pair.create("전체    ",""));
//        famousSanbang.add( Pair.create("sub zone    ","01"));
//
//        trackOfJeju = new ArrayList<>();
//        trackOfJeju.add( Pair.create("전체    ",""));
//        trackOfJeju.add( Pair.create("추사유배길    ","01"));
//        trackOfJeju.add( Pair.create("올레길1~7    ","02"));
//        trackOfJeju.add( Pair.create("올레길8~15    ","03"));
//        trackOfJeju.add( Pair.create("올레길16~21   ","04"));
//        trackOfJeju.add( Pair.create("기타    ","05"));
//
//        commonTourSpot = new ArrayList<>();
//        commonTourSpot.add( Pair.create("전체    ",""));
//        commonTourSpot.add( Pair.create("자연    ","01"));
//        commonTourSpot.add( Pair.create("문화    ","02"));
//        commonTourSpot.add( Pair.create("레저    ","03"));
//        commonTourSpot.add( Pair.create("체험    ","04"));
//        commonTourSpot.add( Pair.create("편의    ","05"));
//
//        trackOfFirst = new ArrayList<>();
//        trackOfFirst.add( Pair.create("전체    ",""));
//        trackOfFirst.add( Pair.create("1코스    ","01"));
//        trackOfFirst.add( Pair.create("2코스    ","02"));
//        trackOfFirst.add( Pair.create("3코스    ","03"));
//
//        trackOfSecond = new ArrayList<>();
//        trackOfSecond.add( Pair.create("전체    ",""));
//        trackOfSecond.add( Pair.create("올레1길    ","01"));
//        trackOfSecond.add( Pair.create("올레1-1길    ","02"));
//        trackOfSecond.add( Pair.create("올레2길    ","03"));
//        trackOfSecond.add( Pair.create("올레3길    ","04"));
//        trackOfSecond.add( Pair.create("올레3-1길    ","05"));
//        trackOfSecond.add( Pair.create("올레4길    ","06"));
//        trackOfSecond.add( Pair.create("올레5길    ","07"));
//        trackOfSecond.add( Pair.create("올레6길    ","08"));
//        trackOfSecond.add( Pair.create("올레7길    ","09"));
//        trackOfSecond.add( Pair.create("올레7-1길    ","10"));
//
//        trackOfThird = new ArrayList<>();
//        trackOfThird.add( Pair.create("전체    ",""));
//        trackOfThird.add( Pair.create("올레8길    ","01"));
//        trackOfThird.add( Pair.create("올레9길    ","02"));
//        trackOfThird.add( Pair.create("올레10길    ","03"));
//        trackOfThird.add( Pair.create("올레10-1길    ","04"));
//        trackOfThird.add( Pair.create("올레11길    ","05"));
//        trackOfThird.add( Pair.create("올레12길    ","06"));
//        trackOfThird.add( Pair.create("올레13길    ","07"));
//        trackOfThird.add( Pair.create("올레14길    ","08"));
//        trackOfThird.add( Pair.create("올레14-1길    ","09"));
//        trackOfThird.add( Pair.create("올레15-A길    ","10"));
//        trackOfThird.add( Pair.create("올레15-B길    ","11"));
//
//        trackOfFord = new ArrayList<>();
//        trackOfFord.add( Pair.create("전체    ",""));
//        trackOfFord.add( Pair.create("올레16길    ","01"));
//        trackOfFord.add( Pair.create("올레17-A길    ","02"));
//        trackOfFord.add( Pair.create("올레17-B길    ","03"));
//        trackOfFord.add( Pair.create("올레18길    ","04"));
//        trackOfFord.add( Pair.create("올레18-1A길    ","05"));
//        trackOfFord.add( Pair.create("올레18-1B길    ","06"));
//        trackOfFord.add( Pair.create("올레19길    ","07"));
//        trackOfFord.add( Pair.create("올레20길    ","08"));
//        trackOfFord.add( Pair.create("올레21길    ","09"));
//
//        trackOfFifth = new ArrayList<>();
//        trackOfFifth.add( Pair.create("전체    ",""));
//        trackOfFifth.add( Pair.create("산책로    ","01"));
//        trackOfFifth.add( Pair.create("밭길    ","02"));
//        trackOfFifth.add( Pair.create("해안길    ","03"));
//        trackOfFifth.add( Pair.create("해안길    ","04"));
//        trackOfFifth.add( Pair.create("숲길    ","05"));
//        trackOfFifth.add( Pair.create("지질트레일    ","06"));
//        trackOfFifth.add( Pair.create("기타    ","07"));
//
//        spotOfNature = new ArrayList<>();
//        spotOfNature.add( Pair.create("전체    ",""));
//        spotOfNature.add( Pair.create("산,오름    ","01"));
//        spotOfNature.add( Pair.create("바다    ","02"));
//        spotOfNature.add( Pair.create("길,올레    ","03"));
//        spotOfNature.add( Pair.create("섬    ","04"));
//        spotOfNature.add( Pair.create("휴양림    ","05"));
//        spotOfNature.add( Pair.create("공원    ","06"));
//        spotOfNature.add( Pair.create("논,밭    ","07"));
//        spotOfNature.add( Pair.create("계곡(강,하천)    ","08"));
//        spotOfNature.add( Pair.create("용천수    ","09"));
//        spotOfNature.add( Pair.create("명소    ","10"));
//        spotOfNature.add( Pair.create("자연물    ","11"));
//        spotOfNature.add( Pair.create("폭포    ","12"));
//        spotOfNature.add( Pair.create("기타    ","13"));
//
//        spotOfCulture = new ArrayList<>();
//        spotOfCulture.add( Pair.create("전체    ",""));
//        spotOfCulture.add( Pair.create("문화재,유적    ","01"));
//        spotOfCulture.add( Pair.create("박물관,예술    ","02"));
//        spotOfCulture.add( Pair.create("테마파크    ","03"));
//        spotOfCulture.add( Pair.create("농장,목장    ","04"));
//        spotOfCulture.add( Pair.create("마을,지역    ","05"));
//        spotOfCulture.add( Pair.create("항구,포구    ","06"));
//        spotOfCulture.add( Pair.create("등대    ","07"));
//        spotOfCulture.add( Pair.create("명소    ","08"));
//        spotOfCulture.add( Pair.create("축제    ","09"));
//        spotOfCulture.add( Pair.create("종교    ","10"));
//        spotOfCulture.add( Pair.create("기타    ","11"));
//
//        spotOfLeisure = new ArrayList<>();
//        spotOfLeisure.add( Pair.create("전체    ",""));
//        spotOfLeisure.add( Pair.create("수상    ","01"));
//        spotOfLeisure.add( Pair.create("지상    ","02"));
//        spotOfLeisure.add( Pair.create("항공    ","03"));
//        spotOfLeisure.add( Pair.create("공원    ","04"));
//        spotOfLeisure.add( Pair.create("스포츠    ","05"));
//        spotOfLeisure.add( Pair.create("기타    ","06"));
//
//        spotOfExperience = new ArrayList<>();
//        spotOfExperience.add( Pair.create("전체    ",""));
//        spotOfExperience.add( Pair.create("농장,목장    ","01"));
//        spotOfExperience.add( Pair.create("승마    ","02"));
//        spotOfExperience.add( Pair.create("해녀    ","03"));
//        spotOfExperience.add( Pair.create("낚시    ","04"));
//        spotOfExperience.add( Pair.create("잠수함    ","05"));
//        spotOfExperience.add( Pair.create("기타    ","06"));
//
//    }
//
//    public ArrayList< Pair<String,String>> getKindOfTour(){
//        return kindOfTour;
//    }
//
//    public ArrayList< Pair<String,String>> getFamousSpot() {
//        return famousSpot;
//    }
//
//    public ArrayList< Pair<String,String>> getTrackOfJeju() {
//        return trackOfJeju;
//    }
//
//    public ArrayList< Pair<String,String>> getTrack(int position) {
//        ArrayList< Pair<String,String>> arrayList = new ArrayList<>();
//        switch (position){
//            case 1:
//                arrayList = trackOfFirst;
//                break;
//            case 2:
//                arrayList = trackOfSecond;
//                break;
//            case 3:
//                arrayList = trackOfThird;
//                break;
//            case 4:
//                arrayList = trackOfFord;
//                break;
//            case 5:
//                arrayList = trackOfFifth;
//                break;
//        }
//        return arrayList;
//    }
//
//    public ArrayList<Pair<String, String>> getCommonTourSpot() {
//        return commonTourSpot;
//    }
//
//    public ArrayList< Pair<String,String>> getSpot(int position){
//        ArrayList< Pair<String,String>> arrayList = new ArrayList<>();
//        switch (position){
//            case 1:
//                arrayList = spotOfNature;
//                break;
//            case 2:
//                arrayList = spotOfCulture;
//                break;
//            case 3:
//                arrayList = spotOfLeisure;
//                break;
//            case 4:
//                arrayList = spotOfExperience;
//                break;
//            /*case 4:
//                break;*/
//        }
//        return arrayList;
//    }
//
//    public ArrayList<Pair<String, String>> getFamousUdo() {
//        return famousUdo;
//    }
//
//    public ArrayList<Pair<String, String>> getFamousHalla() {
//        return famousHalla;
//    }
//
//    public ArrayList<Pair<String, String>> getFamousSanbang() {
//        return famousSanbang;
//    }
//
//    public class LinkArray{
//
//    }
//}
