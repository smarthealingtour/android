package jejusmarttour.util.code_data;

import java.util.List;

/**
 * Created by Osy on 2017-11-22.
 */

public class TreeFactory {
    private String treeTitle;
    private int height;
    private List nodeList;

    public TreeFactory(String treeTitle, int height, List nodeList){
        this.treeTitle = treeTitle;
        this.height = height;
        this.nodeList = nodeList;
    }
    public void setLeafNodeList(List<String> nameList, List<String> codeList){
        MyNode.BranchNode<MyNode.LeafNode> branchNode = new MyNode.BranchNode<>();
        for (int i = 0; i < nameList.size(); i++) {
            MyNode.LeafNode leafNode = new MyNode.LeafNode(nameList.get(i), codeList.get(i));
        }

    }
    public void setNodeList(List list, int depth){
        this.nodeList.remove(depth);
        this.nodeList.add(depth, list);
    }
}
