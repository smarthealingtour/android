package jejusmarttour.util.code_data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osy on 2017-11-29.
 */

public class MyNode {
    private String name;

    public String getName() {
        return name;
    }

    public MyNode(){
        this.name = "";
    }
    public MyNode(String name){
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    };

    public interface MyCode {
        void setCode(String code);
        String getCode();
    }

    //중간에 분기되는 노드. 우리앱에서는 첫번째, 두번째 스피너가 해당됨
    public static class BranchNode<T> extends MyNode implements MyCode {
        private String code;
        private List<T> childList;

        public BranchNode(){
            this("","");
        }

        public BranchNode(String name, String code){
            super( name );
            childList = new ArrayList<>();
            this.code = code;
        }

        public BranchNode(String name){
            super( name );
        }

        public List getChildList() {
            return childList;
        }

        public void addChild(T child){
            childList.add( child);
        }

        @Override
        public void setCode(String code) {
            this.code = code;
        }
        @Override
        public String getCode() {
            return code;
        }
    }

    //마지막 노드. 앱에서는 세번째스피너가 해당됨
    public static class LeafNode extends MyNode implements MyCode {
        private String code;
        private boolean hasSubZone;

        public LeafNode(String name, String code){
            super(name);
            this.code = code;
            this.hasSubZone = false;
        }
        public LeafNode(String name, String code, boolean hasSubZone){
            super(name);
            this.code = code;
            this.hasSubZone = hasSubZone;
        }
        public boolean hasSubZone(){
            return hasSubZone;
        }
        @Override
        public void setCode(String code) {
            this.code = code;
        }
        @Override
        public String getCode() {
            return code;
        }
    }
}
