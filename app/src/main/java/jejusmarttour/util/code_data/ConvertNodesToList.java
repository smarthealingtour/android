package jejusmarttour.util.code_data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osy on 2017-11-20.
 */

//CodeData가 ArrayList<Pair<String, String>을 리턴하는 것을 ArrayList<String>으로 리턴할 수 있게 도와주는 클래스
public enum ConvertNodesToList {
    INSTANCE;

    public static ConvertNodesToList getInstance() {
        return INSTANCE;
    }

    public ArrayList<String> getNameArray(List< MyNode> nodes){
        ArrayList<String> arrayList = new ArrayList<>();

        for (MyNode node:
                nodes) {
            arrayList.add(node.getName());
        }

        return arrayList;
    }
    public ArrayList<String> getCodeArray(List< MyNode> nodes){
        ArrayList<String> arrayList = new ArrayList<>();

        for (MyNode node:
                nodes) {
            arrayList.add( ((MyNode.MyCode)node).getCode() );
        }

        return arrayList;
    }
}
