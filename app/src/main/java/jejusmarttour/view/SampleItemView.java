package jejusmarttour.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import syl.com.jejusmarttour.R;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class SampleItemView extends LinearLayout {

    private TextView titleView;

    public SampleItemView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_text, this, true);

        titleView = (TextView) findViewById(R.id.itemTitle);
    }

    public void setTitle(String title) {
        titleView.setText(title);
    }

}
