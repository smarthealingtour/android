package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.KeywordVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;

public class GetContentsTask extends AsyncTask < String , Integer , ArrayList < ScheduleVO > >
{
	private static final String TAG = GetContentsTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private JSONArrayResult jsonArrayResult;
	private String type;

	public GetContentsTask ( Context context , JSONArrayResult jsonArrayResult){
		this.context = context;
		this.jsonArrayResult = jsonArrayResult;
	}

	@Override
	protected void onPreExecute ( ){
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList < ScheduleVO > doInBackground ( String ... params ){
		URL url;
		type = params [ 0 ];
		String ht_id = params [ 1 ];
		String response;
		JSONArray responseJSON;
		ArrayList < ScheduleVO > arrayList = null;

		try{
			String urlStr = CommonData.getContentsAddres ( type , ht_id );

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 ){
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 ){
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONArray ( response );

			arrayList = getResultList ( responseJSON );

			// br.close ( );
			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( IOException | JSONException e )	{
			Log.e ( TAG , e.toString ( ) );
		}

		if ( arrayList == null || arrayList.size ( ) <= 0 ){
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;
	}

	// JSON 파싱
	private ArrayList < ScheduleVO > getResultList ( JSONArray jsonArray ){
		if ( jsonArray == null )
			return null;

		ArrayList < ScheduleVO > arrayList = new ArrayList<> ( );

		for ( int i = 0; i < jsonArray.length ( ); i++ ){
			JSONArray hcdKeywordArray = new JSONArray ( );
			JSONArray scdKeywordArray = new JSONArray ( );
			JSONArray cdKeywordArray = new JSONArray ( );
			JSONObject jsonObject;
			ScheduleVO scheduleVO = new ScheduleVO ( );

			scheduleVO.setHealingContentsVO( null );
			scheduleVO.setSightSeeingContentsVO( null );

			try{
				jsonObject = jsonArray.getJSONObject ( i );

				if (type.equals("hcnt")){
					HealingContentsVO healingContentsVO = new HealingContentsVO ( );

					healingContentsVO.setHcnt_id ( jsonObject.has ( "hcnt_id" ) ? jsonObject.getString ( "hcnt_id" ) : null );
					healingContentsVO.setHcnt_type ( jsonObject.has ( "hcnt_type" ) ? jsonObject.getString ( "hcnt_type" ) : null );
					healingContentsVO.setHcnt_name ( jsonObject.has ( "hcnt_name" ) ? jsonObject.getString ( "hcnt_name" ) : null );
					healingContentsVO.setHcnt_intro ( jsonObject.has ( "hcnt_intro" ) ? jsonObject.getString ( "hcnt_intro" ) : null );
					healingContentsVO.setHcnt_addr ( jsonObject.has ( "hcnt_addr" ) ? jsonObject.getString ( "hcnt_addr" ) : null );
					healingContentsVO.setHcnt_tel ( jsonObject.has ( "hcnt_tel" ) ? jsonObject.getString ( "hcnt_tel" ) : null );
					healingContentsVO.setHcnt_op_time ( jsonObject.has ( "hcnt_op_time" ) ? jsonObject.getString ( "hcnt_op_time" ) : null );
					healingContentsVO.setHcnt_duration ( jsonObject.has ( "hcnt_duration" ) ? jsonObject.getString ( "hcnt_duration" ) : null );
					healingContentsVO.setHcnt_guide ( jsonObject.has ( "hcnt_guide" ) ? jsonObject.getString ( "hcnt_guide" ) : null );
					healingContentsVO.setHcnt_guide_name ( jsonObject.has ( "hcnt_guide_name" ) ? jsonObject.getString ( "hcnt_guide_name" ) : null );
					healingContentsVO.setHcnt_guide_tel ( jsonObject.has ( "hcnt_guide_tel" ) ? jsonObject.getString ( "hcnt_guide_tel" ) : null );
					healingContentsVO.setHcnt_site ( jsonObject.has ( "hcnt_site" ) ? jsonObject.getString ( "hcnt_site" ) : null );
					healingContentsVO.setHcnt_sub_img ( jsonObject.has ( "hcnt_sub_img" ) ? jsonObject.getString ( "hcnt_sub_img" ) : null );
					healingContentsVO.setHcnt_rcmd ( jsonObject.has ( "hcnt_rcmd" ) ? jsonObject.getString ( "hcnt_rcmd" ) : null );
					healingContentsVO.setHcnt_rcmd_comt ( jsonObject.has ( "hcnt_rcmd_comt" ) ? jsonObject.getString ( "hcnt_rcmd_comt" ) : null );
					healingContentsVO.setHcnt_rcmd_comt_sec ( jsonObject.has ( "hcnt_rcmd_comt_sec" ) ? jsonObject.getString ( "hcnt_rcmd_comt_sec" ) : null );
					healingContentsVO.setHcnt_rcmd_comt_thd ( jsonObject.has ( "hcnt_rcmd_comt_thd" ) ? jsonObject.getString ( "hcnt_rcmd_comt_thd" ) : null );
					healingContentsVO.setHcnt_rcmd_comt_fth ( jsonObject.has ( "hcnt_rcmd_comt_fth" ) ? jsonObject.getString ( "hcnt_rcmd_comt_fth" ) : null );
					healingContentsVO.setHcnt_rcmd_comt_fiv ( jsonObject.has ( "hcnt_rcmd_comt_fiv" ) ? jsonObject.getString ( "hcnt_rcmd_comt_fiv" ) : null );
					healingContentsVO.setHcnt_hcd ( jsonObject.has ( "hcnt_hcd" ) ? jsonObject.getString ( "hcnt_hcd" ) : null );
					healingContentsVO.setHcnt_scd ( jsonObject.has ( "hcnt_scd" ) ? jsonObject.getString ( "hcnt_scd" ) : null );
					healingContentsVO.setHcnt_tab( jsonObject.has ( "hcnt_tab" ) ? jsonObject.getString ( "hcnt_tab" ) : null);

					hcdKeywordArray = jsonObject.has ( "hcnt_hcd_name" ) ? jsonObject.getJSONArray ( "hcnt_hcd_name" ) : null;
					scdKeywordArray = jsonObject.has ( "hcnt_scd_name" ) ? jsonObject.getJSONArray ( "hcnt_scd_name" ) : null;

					if ( hcdKeywordArray != null ){
						ArrayList < KeywordVO > list = new ArrayList<> ( );

						for ( int k = 0; k < hcdKeywordArray.length ( ); k++ ){
							JSONObject object;
							KeywordVO vo = new KeywordVO ( );
							object = hcdKeywordArray.getJSONObject ( k );
							vo.setCode_name ( object.has ( "code_name" ) ? object.getString ( "code_name" ) : null );
							vo.setClassL ( object.has ( "class" ) ? object.getString ( "class" ) : null );
							vo.setCode ( object.has ( "code" ) ? object.getString ( "code" ) : null );
							list.add ( vo );
						}
						healingContentsVO.setHcnt_hcd_name ( list );
					}

					if ( scdKeywordArray != null ){
						ArrayList < KeywordVO > list = new ArrayList < KeywordVO > ( );

						for ( int k = 0; k < scdKeywordArray.length ( ); k++ ){
							JSONObject object;
							KeywordVO vo = new KeywordVO ( );
							object = scdKeywordArray.getJSONObject ( k );
							vo.setCode_name ( object.has ( "code_name" ) ? object.getString ( "code_name" ) : null );
							vo.setClassL ( object.has ( "class" ) ? object.getString ( "class" ) : null );
							vo.setCode ( object.has ( "code" ) ? object.getString ( "code" ) : null );
							list.add ( vo );
						}
						healingContentsVO.setHcnt_scd_name ( list );
					}

					healingContentsVO.setHcnt_inv_hcnt ( jsonObject.has ( "hcnt_inv_hcnt" ) ? jsonObject.getString ( "hcnt_inv_hcnt" ) : null );
					healingContentsVO.setHt_id ( jsonObject.has ( "ht_id" ) ? jsonObject.getString ( "ht_id" ) : null );
					healingContentsVO.setNickname ( jsonObject.has ( "nickname" ) ? jsonObject.getString ( "nickname" ) : null );
					healingContentsVO.setHcnt_coord_x ( jsonObject.has ( "hcnt_coord_x" ) ? jsonObject.getString ( "hcnt_coord_x" ) : null );
					healingContentsVO.setHcnt_coord_y ( jsonObject.has ( "hcnt_coord_y" ) ? jsonObject.getString ( "hcnt_coord_y" ) : null );
					healingContentsVO.setHcnt_cost_adult ( jsonObject.has ( "hcnt_cost_adult" ) ? jsonObject.getString ( "hcnt_cost_adult" ) : " - " );
					healingContentsVO.setHcnt_cost_child ( jsonObject.has ( "hcnt_cost_child" ) ? jsonObject.getString ( "hcnt_cost_child" ) : " - " );
					healingContentsVO.setHcnt_cost_jeju ( jsonObject.has ( "hcnt_cost_jeju" ) ? jsonObject.getString ( "hcnt_cost_jeju" ) : " - " );
					healingContentsVO.setG_id ( jsonObject.has ( "g_id" ) ? jsonObject.getString ( "g_id" ) : null );

					healingContentsVO.setCart_count ( jsonObject.has ( "cart_count" ) ? jsonObject.getString ( "cart_count" ) : null );
					healingContentsVO.setComment_count ( jsonObject.has ( "comment_count" ) ? jsonObject.getString ( "comment_count" ) : null );
					healingContentsVO.setHcnt_kml ( jsonObject.has ( "hcnt_kml" ) ? jsonObject.getString ( "hcnt_kml" ) : null );

					scheduleVO.setHealingContentsVO( healingContentsVO );
				}
				else{
					SightSeeingContentsVO sightSeeingContentsVO = new SightSeeingContentsVO ( );
					cdKeywordArray = jsonObject.has ( "ccnt_cd_name" ) ? jsonObject.getJSONArray ( "ccnt_cd_name" ) : null;

					// 관광장소
					if ( cdKeywordArray != null ){
						ArrayList < KeywordVO > list = new ArrayList < KeywordVO > ( );

						for ( int k = 0; k < cdKeywordArray.length ( ); k++ ){
							JSONObject object;
							KeywordVO vo = new KeywordVO ( );
							object = cdKeywordArray.getJSONObject ( k );
							vo.setCode_name ( object.has ( "code_name" ) ? object.getString ( "code_name" ) : null );
							vo.setClassL ( object.has ( "class" ) ? object.getString ( "class" ) : null );
							vo.setCode ( object.has ( "code" ) ? object.getString ( "code" ) : null );
							list.add ( vo );
						}
						sightSeeingContentsVO.setCcnt_cd_name ( list );
					}

					sightSeeingContentsVO.setCcnt_name ( jsonObject.has ( "ccnt_name" ) ? jsonObject.getString ( "ccnt_name" ) : null );
					sightSeeingContentsVO.setCcnt_id ( jsonObject.has ( "ccnt_id" ) ? jsonObject.getString ( "ccnt_id" ) : null );
					sightSeeingContentsVO.setCcnt_cd ( jsonObject.has ( "ccnt_cd" ) ? jsonObject.getString ( "ccnt_cd" ) : null );

					sightSeeingContentsVO.setCcnt_coord_x ( jsonObject.has ( "ccnt_coord_x" ) ? jsonObject.getString ( "ccnt_coord_x" ) : null );
					sightSeeingContentsVO.setCcnt_coord_y ( jsonObject.has ( "ccnt_coord_y" ) ? jsonObject.getString ( "ccnt_coord_y" ) : null );

					sightSeeingContentsVO.setCart_count ( jsonObject.has ( "cart_count" ) ? jsonObject.getString ( "cart_count" ) : null );
					sightSeeingContentsVO.setComment_count ( jsonObject.has ( "comment_count" ) ? jsonObject.getString ( "comment_count" ) : null );
					scheduleVO.setSightSeeingContentsVO( sightSeeingContentsVO );
				}
			}
			catch ( JSONException | NullPointerException e ){
				Log.e ( TAG , e.toString ( ) );
			}

			arrayList.add ( scheduleVO );
		}
		return arrayList;
	}

	@Override
	protected void onPostExecute ( ArrayList < ScheduleVO > result ){
		progressDialog.dismiss ( );
		if(result != null){
			Log.e(TAG, "onPostExecute: " + result.size() );
		}
		jsonArrayResult.setJSONArrayResult ( result );

		super.onPostExecute ( result );
	}

	@Override
	protected void onCancelled() {
		progressDialog.dismiss ( );
		super.onCancelled();
	}
}
