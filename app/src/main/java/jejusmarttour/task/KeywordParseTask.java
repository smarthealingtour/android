package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.KeywordCategoryVO;

public class KeywordParseTask extends AsyncTask < String , Integer , ArrayList <KeywordCategoryVO> >
{
	private static final String TAG = KeywordParseTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;

	public KeywordParseTask ( Context context )
	{
		this.context = context;
	}

	@Override
	protected void onPreExecute ( )
	{
		//progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList < KeywordCategoryVO > doInBackground ( String ... params )
	{
		URL url;
		String category = params [ 0 ];
		String response;
		JSONArray responseJSON;
		ArrayList < KeywordCategoryVO > arrayList = null;

		try
		{
			String urlStr = CommonData.getKeywordCategory(category);

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 )
			{
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONArray ( response );

			arrayList = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		if ( arrayList == null || arrayList.size ( ) <= 0 )
		{
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;

	}

	// JSON 파싱
	private ArrayList < KeywordCategoryVO > getResultList ( JSONArray jsonArray )
	{
		if ( jsonArray == null )
			return null;

		ArrayList < KeywordCategoryVO > arrayList = new ArrayList < KeywordCategoryVO > ( );

		for ( int i = 0 ; i < jsonArray.length ( ) ; i++ )
		{
			JSONObject jsonObject;
			KeywordCategoryVO reviewVO = new KeywordCategoryVO ( );

			try
			{
				jsonObject = jsonArray.getJSONObject ( i );
				reviewVO.setCode_name ( jsonObject.has ( "code_name" ) ? jsonObject.getString ( "code_name" ) : null );
				reviewVO.setCode_id ( jsonObject.has ( "code_id" ) ? jsonObject.getString ( "code_id" ) : null );
				reviewVO.setL_class ( jsonObject.has ( "l_class" ) ? jsonObject.getString ( "l_class" ) : null );
				reviewVO.setM_class ( jsonObject.has ( "m_class" ) ? jsonObject.getString ( "m_class" ) : null );
				reviewVO.setS_class (  jsonObject.has ( "s_class" ) ? jsonObject.getString ( "s_class" ) : null );
				reviewVO.setCode ( jsonObject.has ( "code" ) ? SmartTourUtils.replaceSpaceToBlank(jsonObject.getString("code")) : null );
			}
			catch ( JSONException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}
			catch ( NullPointerException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}

			arrayList.add ( reviewVO );

			// System.out.print ( "reviewVO comment_subt : " +
			// reviewVO.getComment_subt ( ) + "\n" );
		}

		return arrayList;
	}

	@Override
	protected void onPostExecute ( ArrayList < KeywordCategoryVO > result )
	{
		//progressDialog.dismiss ( );
		for(int i = 0 ; i < result.size() ; i++){
			KeywordCategoryVO keyVo = result.get(i);
			if(keyVo.getCode().length() == 3){
				ArrayList<KeywordCategoryVO> depth = new ArrayList<KeywordCategoryVO>();
				for(int j = 0 ; j < result.size() ; j++){
					KeywordCategoryVO imsi = result.get(j);
					if(imsi.getCode().length() > 3 && imsi.getCode().contains(keyVo.getCode())){
						depth.add(imsi);
					}
				}
				keyVo.setArrayKeyword(depth);
			}
		}


		( ( JSONArrayResult ) context ).setJSONArrayResult ( result );
		super.onPostExecute ( result );
	}

}
