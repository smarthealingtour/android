package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.MyScheduleUpdateRequestVO;

public class MyScheduleUpdateTask extends AsyncTask < MyScheduleUpdateRequestVO , Integer , String >{
	private static final String TAG = MyScheduleUpdateTask.class.toString ( );
	private ProgressDialog progressDialog;
	private long totalSize = 0;
	private Context context;

	private ArrayList<String> images;

	public MyScheduleUpdateTask(Context context ){
		this.context = context;
	}

	@Override
	protected void onPreExecute ( ){
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		progressDialog.setProgressStyle ( android.R.style.Widget_Holo_Light_ProgressBar_Horizontal );
		// setting progress bar to zero
		progressDialog.setProgress ( 0 );
		super.onPreExecute ( );
	}

	@Override
	protected void onProgressUpdate ( Integer ... progress ){
		// updating progress bar value
		progressDialog.setProgress ( progress [ 0 ] );
	}

	@Override
	protected String doInBackground ( MyScheduleUpdateRequestVO ... params ){
		return uploadFile ( params [ 0 ] );
	}

	@SuppressWarnings ( "deprecation" )
	private String uploadFile ( MyScheduleUpdateRequestVO vo ){
		String responseString = null;

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

		try {
			JSONObject htour = vo.getUpdateJSONObject ( );

			builder.addPart("tour" , new StringBody( htour.toString ( ) , ContentType.APPLICATION_JSON) );

			InputStream inputStream;
			HttpClient httpclient = new DefaultHttpClient ( );
			HttpPost httppost = new HttpPost ( CommonData.getMTCUploadAddress ( ) );
			httppost.setEntity(builder.build());

			HttpResponse response = httpclient.execute ( httppost );
			HttpEntity httpEntity = response.getEntity();

			int statusCode = response.getStatusLine ( ).getStatusCode ( );

			if ( statusCode == 200 ){
				inputStream = httpEntity.getContent();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
				StringBuilder stringBuilder = new StringBuilder();
				String line;

				while ((line = bufferedReader.readLine()) != null){
					stringBuilder.append(line).append("\n");
				}
				inputStream.close();

				responseString = stringBuilder.toString();
			}
			else{
				responseString = "Error occurred! Http Status Code: " + statusCode;
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return responseString;
	}

	@Override
	protected void onPostExecute ( String result ){
		progressDialog.dismiss ( );
		Log.e ( TAG, "Response from server result: " + result );

		try{
			JSONObject temp = new JSONObject ( result );
			Log.e ( TAG , "Response from server: " + temp );
			if ( temp.getString ( "result" ).equals ( "success" ) )
				( ( JSONObjectResult ) context ).setJSONObjectResult ( "success" );
		}
		catch ( JSONException e ){
			Log.e ( TAG , e.toString () );
		}
		super.onPostExecute ( result );
	}
}