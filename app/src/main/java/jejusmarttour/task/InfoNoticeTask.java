package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import jejusmarttour.common.CommonData;
import jejusmarttour.util.CommonUtil;

/**
 * Created by A on 2015-10-26.
 */
public class InfoNoticeTask extends AsyncTask<Map, Void, String>{

    private ProgressDialog progressDialog;
    private Context context;

    public InfoNoticeTask(Context context){
        this.context = context;
    }

    @Override
    protected String doInBackground(Map... params) {
        String result = null;
        try
        {

            Map<String, String> paramMap = params[0];

            String urlStr = CommonData.getAddress() + paramMap.get("url") + "?";
            Iterator<String> keys = paramMap.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                urlStr += key + "=" + URLEncoder.encode(paramMap.get(key), "UTF-8") + "&";
            }

            URL url = new URL( urlStr.substring(0, urlStr.length() -1));
            // GET 방식
            URLConnection conn = url.openConnection ( );
            conn.setUseCaches ( false );
            InputStream is = conn.getInputStream ( );

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
            byte [ ] byteBuffer = new byte [ 1024 ];
            byte [ ] byteData = null;
            int nLength = 0;

            while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
            {
                byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
            }

            byteData = byteArrayOutputStream.toByteArray ( );

            if ( byteData.length <= 0 )
            {
                return null;
            }

            result = new String ( byteData );

        }
        catch ( MalformedURLException e )
        {
            e.printStackTrace();
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();

        Log.e("qtam result : ", result);
        ArrayList rstList = CommonUtil.jsonArrayParser(result);

        Log.e("qtam rstList : " , rstList.toString());

        ((JSONArrayResult) context).setJSONArrayResult(rstList);

        super.onPostExecute(result);
    }

}
