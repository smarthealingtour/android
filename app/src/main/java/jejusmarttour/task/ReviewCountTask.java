package jejusmarttour.task;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HTPReviewVO;
import jejusmarttour.vo.ReviewCountVO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

public class ReviewCountTask extends AsyncTask < String , Integer , ReviewCountVO >
{
	private static final String TAG = ReviewCountTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public ReviewCountTask ( Context context , Fragment fragment )
	{
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( )
	{
		// progressDialog = ProgressDialog.show ( context , "" , "Loading..." ,
		// true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ReviewCountVO doInBackground ( String ... params )
	{
		URL url;
		String type = params [ 0 ];
		String productId = params [ 1 ];
		String response;
		JSONObject responseJSON;
		ReviewCountVO result = null;

		try
		{
			String urlStr = CommonData.getHTPReviewCountAddress ( type , productId );

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 )
			{
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONObject ( response );

			result = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		return result;

	}

	// JSON 파싱
	private ReviewCountVO getResultList ( JSONObject jsonObject )
	{
		if ( jsonObject == null )
			return null;

		ReviewCountVO reviewCountVO = new ReviewCountVO();

		try
		{
			reviewCountVO.setCount(jsonObject.has("count") ? jsonObject.getString("count") : null);
			reviewCountVO.setHt_id(jsonObject.has("ht_id") ? jsonObject.getString("ht_id") : null);
			reviewCountVO.setU_ht_id(jsonObject.has("u_ht_id") ? jsonObject.getString("u_ht_id") : null );
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.getMessage ( ) );
		}

		return reviewCountVO;
	}

	@Override
	protected void onPostExecute ( ReviewCountVO result )
	{
		// progressDialog.dismiss ( );
		( ( ObjectResult ) fragment ).setObjectResult(result);
		super.onPostExecute ( result );
	}

}
