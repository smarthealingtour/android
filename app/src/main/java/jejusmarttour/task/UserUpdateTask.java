package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import jejusmarttour.common.CommonData;
import jejusmarttour.user.UserUpdateResultVO;

public class UserUpdateTask extends AsyncTask < String , Integer , UserUpdateResultVO>{
	private static final String TAG = UserUpdateTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;

	public UserUpdateTask ( Context context )
	{
		this.context = context;
	}

	@Override
	protected void onPreExecute ( ){
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected UserUpdateResultVO doInBackground ( String ... params ){
		if(isCancelled ( ))
			return null;
		
		URL url;
		String userID = params [ 0 ];
		String nickName = params [ 1 ];
		String response;
		JSONObject responseJSON = null;

		try{
			String urlStr = CommonData.getNickNameUpdateAddres(userID, nickName);

			Log.i("qtam", urlStr);

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 ){
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 ){
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONObject ( response );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( IOException | JSONException e ){
			Log.e ( TAG , e.toString ( ) );
		}

		return getResultList ( responseJSON );
	}

	// JSON 파싱
	private UserUpdateResultVO getResultList ( JSONObject jsonObject )
	{
		if ( jsonObject == null )
			return null;

		UserUpdateResultVO userVO = new UserUpdateResultVO ( );

		try{
			userVO.setEmail ( jsonObject.has ( "email" ) ? jsonObject.getString ( "email" ) : null );
			userVO.setResult ( jsonObject.has ( "result" ) ? jsonObject.getString ( "result" ) : null );
			userVO.setNickname ( jsonObject.has ( "nickname" ) ? jsonObject.getString ( "nickname" ) : null );
			userVO.setDescription ( jsonObject.has ( "description" ) ? jsonObject.getString ( "description" ) : null );
		}
		catch ( JSONException | NullPointerException e ){
			Log.e ( TAG , e.toString ( ) );
		}

		return userVO;
	}

	@Override
	protected void onPostExecute ( UserUpdateResultVO result ){
		progressDialog.dismiss ( );
		( ( JSONObjectResult < UserUpdateResultVO > ) context ).setJSONObjectResult ( result );
		super.onPostExecute ( result );
	}
}
