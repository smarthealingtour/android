package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.UpdateCartRequestVO;
import jejusmarttour.vo.UpdateResultVO;

public class HTPCartTask extends AsyncTask < UpdateCartRequestVO , Integer , UpdateResultVO > {
	private static final String TAG = HTPCartTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public HTPCartTask ( Context context , Fragment fragment ) {
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( ) {
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected UpdateResultVO doInBackground ( UpdateCartRequestVO ... params ){
		if ( isCancelled ( ) )
			return null;

		URL url;
		UpdateCartRequestVO vo = params [ 0 ];

		String response;
		JSONObject responseJSON = null;

		try{
			String urlStr = null;

			if (!vo.isContent()){
				urlStr = CommonData.getHTPCartAddress ( vo.getType ( ) , vo.getEmail ( ) , vo.getProductID ( ) , vo.isUser ( ) );
			}
			else{
				urlStr = CommonData.getContentCartAddress ( vo.getType ( ) , vo.getEmail ( ) , vo.getProductID ( ) , String.valueOf ( vo.isCcnt ( ) ) );
			}

			Log.e ( TAG , urlStr );

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 ) {
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 ) {
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONObject ( response );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		} catch ( IOException | JSONException e ){
			Log.e ( TAG , e.toString ( ) );
		}

		return getResultList ( responseJSON );
	}

	// JSON 파싱
	private UpdateResultVO getResultList ( JSONObject jsonObject ) {
		if ( jsonObject == null )
			return null;

		UpdateResultVO userVO = new UpdateResultVO ( );

		try {
			userVO.setResult ( jsonObject.has ( "result" ) ? jsonObject.getString ( "result" ) : null );
		}
		catch ( JSONException | NullPointerException e ) {
			Log.e ( TAG , e.toString ( ) );
		}

		return userVO;
	}

	@Override
	protected void onPostExecute ( UpdateResultVO result ) {
		progressDialog.dismiss ( );
		( ( JSONObjectResult < UpdateResultVO > ) fragment ).setJSONObjectResult ( result );
		super.onPostExecute ( result );
	}

}
