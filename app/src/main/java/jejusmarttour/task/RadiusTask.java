package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;

public class RadiusTask extends AsyncTask < String , Void , ArrayList<ScheduleVO> > {
    private Context context;
    private ProgressDialog progressDialog;

    public RadiusTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute ( ){
        super.onPreExecute ( );
        progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
    }

    @Override
    protected ArrayList<ScheduleVO> doInBackground(String... params) {
        URL url;
        String x = params [ 0 ];
        String y = params [ 1 ];
        String distance = params [ 2 ];
        String response;
        JSONObject responseJSON;
        ArrayList<ScheduleVO> arrayList = new ArrayList<>();

        try {
            String urlStr = CommonData.getradius(x, y, distance);

            Log.e(this.toString(), "반경검색 수행: " + urlStr);

            url = new URL ( urlStr );
            // GET 방식
            URLConnection conn = url.openConnection ( );
            conn.setUseCaches ( false );
            InputStream is = conn.getInputStream ( );

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
            byte [ ] byteBuffer = new byte [ 1024 ];
            byte [ ] byteData = null;
            int nLength = 0;

            while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
            {
                byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
            }

            byteData = byteArrayOutputStream.toByteArray ( );

            if ( byteData.length <= 0 ){
                return null;
            }

            response = new String ( byteData );
            responseJSON = new JSONObject ( response );

            arrayList = getResultList(responseJSON);

        } catch (JSONException | IOException e) {
            e.printStackTrace();
            Log.e(this.toString(), "doInBackground: 익셉션이다야" );
        }

        return arrayList;
    }

    private ArrayList<ScheduleVO> getResultList(JSONObject jsonObject){
        if ( jsonObject == null ) {
            Log.e(this.toString(), "getResultList: nulllllllll");
            return null;
        }

        JSONArray ccntJsonArray;
        JSONArray hcntJsonArray;
        ArrayList < ScheduleVO > arrayList = new ArrayList<> ( );

        try {
            if (jsonObject.getString("ccnt") != null){
                if (!jsonObject.getString("ccnt").equals("")){
                    ccntJsonArray = new JSONArray(jsonObject.getString("ccnt"));

                    for ( int i = 0; i < ccntJsonArray.length ( ); i++ ){
                        JSONObject ccntJsonObject;
                        ScheduleVO scheduleVO = new ScheduleVO ( );

                        scheduleVO.setHealingContentsVO( null );
                        scheduleVO.setSightSeeingContentsVO( null );

                        ccntJsonObject = ccntJsonArray.getJSONObject ( i );

                        SightSeeingContentsVO sightSeeingContentsVO = new SightSeeingContentsVO ( );
                        sightSeeingContentsVO.setCcnt_name ( ccntJsonObject.has ( "ccnt_name" ) ? ccntJsonObject.getString ( "ccnt_name" ) : null );
                        sightSeeingContentsVO.setCcnt_coord_x ( ccntJsonObject.has ( "ccnt_coord_x" ) ? ccntJsonObject.getString ( "ccnt_coord_x" ) : null );
                        sightSeeingContentsVO.setCcnt_coord_y ( ccntJsonObject.has ( "ccnt_coord_y" ) ? ccntJsonObject.getString ( "ccnt_coord_y" ) : null );

                        scheduleVO.setSightSeeingContentsVO( sightSeeingContentsVO );
                        arrayList.add(scheduleVO);
                    }
                }
            }

            if (jsonObject.getString("hcnt") != null){
                if (!jsonObject.getString("hcnt").equals("")){
                    hcntJsonArray = new JSONArray(jsonObject.getString("hcnt"));

                    for ( int i = 0; i < hcntJsonArray.length ( ); i++ ){
                        JSONObject hcntJsonObject;
                        ScheduleVO scheduleVO = new ScheduleVO ( );

                        scheduleVO.setHealingContentsVO( null );
                        scheduleVO.setSightSeeingContentsVO( null );

                        hcntJsonObject = hcntJsonArray.getJSONObject ( i );

                        HealingContentsVO healingContentsVO = new HealingContentsVO ( );
                        healingContentsVO.setHcnt_name ( hcntJsonObject.has ( "hcnt_name" ) ? hcntJsonObject.getString ( "hcnt_name" ) : null );
                        healingContentsVO.setHcnt_coord_x ( hcntJsonObject.has ( "hcnt_coord_x" ) ? hcntJsonObject.getString ( "hcnt_coord_x" ) : null );
                        healingContentsVO.setHcnt_coord_y ( hcntJsonObject.has ( "hcnt_coord_y" ) ? hcntJsonObject.getString ( "hcnt_coord_y" ) : null );

                        scheduleVO.setHealingContentsVO( healingContentsVO );
                        arrayList.add(scheduleVO);
                    }
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
            Log.e(this.toString(), "getResultList: 익셉션이다2" );
        }

        return arrayList;
    }

    @Override
    protected void onPostExecute (ArrayList<ScheduleVO> result){
        progressDialog.dismiss ( );

        if(result != null)
            ((JSONObjectResult<ArrayList<ScheduleVO>>)context).setJSONObjectResult(result);
        else
            Toast.makeText(context, "반경검색 결과가 없습니다.", Toast.LENGTH_SHORT).show();
        super.onPostExecute(result);
    }

    @Override
    protected void onCancelled(ArrayList<ScheduleVO> result) {
        super.onCancelled(result);
        progressDialog.dismiss ( );
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        progressDialog.dismiss ( );
    }
}