package jejusmarttour.task;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HTPReviewVO;

public class ReviewParseTask extends AsyncTask < String , Integer , ArrayList <HTPReviewVO> >
{
	private static final String TAG = ReviewParseTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public ReviewParseTask ( Context context , Fragment fragment )
	{
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( )
	{
		//progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList < HTPReviewVO > doInBackground ( String ... params )
	{
		URL url;
		String htpId = params [ 0 ];
		String page = params [ 1 ];
		String response;
		JSONArray responseJSON;
		ArrayList < HTPReviewVO > arrayList = null;

		try
		{
			String urlStr = CommonData.getHealingTourProductReviewAddress(htpId, page);

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 )
			{
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONArray ( response );

			arrayList = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		if ( arrayList.size ( ) <= 0 || arrayList == null )
		{
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;

	}

	// JSON 파싱
	private ArrayList < HTPReviewVO > getResultList ( JSONArray jsonArray )
	{
		if ( jsonArray == null )
			return null;

		ArrayList < HTPReviewVO > arrayList = new ArrayList < HTPReviewVO > ( );

		for ( int i = 0 ; i < jsonArray.length ( ) ; i++ )
		{
			JSONObject jsonObject;
			HTPReviewVO reviewVO = new HTPReviewVO ( );

			try
			{
				jsonObject = jsonArray.getJSONObject ( i );
				reviewVO.setComment_subt ( jsonObject.has ( "comment_subt" ) ? jsonObject.getString ( "comment_subt" ) : null );
				reviewVO.setComment_id ( jsonObject.has ( "comment_subt" ) ? jsonObject.getString ( "comment_id" ) : null );
				reviewVO.setComment_date ( jsonObject.has ( "comment_subt" ) ? jsonObject.getString ( "comment_date" ) : null );
				reviewVO.setNickname ( jsonObject.has ( "comment_subt" ) ? jsonObject.getString ( "nickname" ) : null );
				reviewVO.setHt_id ( jsonObject.has ( "comment_subt" ) ? jsonObject.getString ( "ht_id" ) : null );
				reviewVO.setHt_id ( jsonObject.has ( "comment_subt" ) ? jsonObject.getString ( "ht_id" ) : null );
			}
			catch ( JSONException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}
			catch ( NullPointerException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}

			arrayList.add ( reviewVO );

			// System.out.print ( "reviewVO comment_subt : " +
			// reviewVO.getComment_subt ( ) + "\n" );
		}

		return arrayList;
	}

	@Override
	protected void onPostExecute ( ArrayList < HTPReviewVO > result )
	{
		//progressDialog.dismiss ( );
		( ( JSONArrayResult ) fragment ).setJSONArrayResult ( result );
		super.onPostExecute ( result );
	}

}
