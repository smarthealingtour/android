package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.KeywordVO;
import jejusmarttour.vo.MyTourCourseRequestVO;
import jejusmarttour.vo.TourProductVO;

public class MyTourCourseTask extends AsyncTask <MyTourCourseRequestVO, Integer , ArrayList <TourProductVO> >
{
	private static final String TAG = MyTourCourseTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public MyTourCourseTask ( Context context )
	{
		this.context = context;
	}
	
	public MyTourCourseTask ( Context context , Fragment fragment )
	{
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( )
	{
		progressDialog = ProgressDialog.show( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList <TourProductVO> doInBackground ( MyTourCourseRequestVO ... params )
	{
		URL url;
		MyTourCourseRequestVO vo = params [ 0 ];
		String response;
		JSONArray responseJSON;
		ArrayList < TourProductVO > arrayList = null;

		try
		{
			String urlStr = CommonData.getMyTourCourseAddress(vo);

			Log.d ( TAG , urlStr );

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );
			
			if( byteData.length <= 0 )
			{
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONArray ( response );

			arrayList = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		if (arrayList == null || arrayList.size ( ) <= 0   )
		{
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;
	}

	// JSON 파싱
	private ArrayList < TourProductVO > getResultList ( JSONArray jsonArray )
	{
		if ( jsonArray == null )
			return null;

		ArrayList < TourProductVO > arrayList = new ArrayList < TourProductVO > ( );

		for ( int i = 0 ; i < jsonArray.length ( ) ; i++ )
		{
			JSONArray hcdKeywordArray = new JSONArray ( );
			JSONArray scdKeywordArray = new JSONArray ( );
			JSONObject jsonObject;
			TourProductVO tourProductVO = new TourProductVO ( );

			try
			{
				jsonObject = jsonArray.getJSONObject ( i );
				tourProductVO.setHt_type(jsonObject.has("ht_type") ? jsonObject.getString("ht_type") : null);
				tourProductVO.setHt_id ( jsonObject.has ( "ht_id" ) ? jsonObject.getString ( "ht_id" ) : null );
				tourProductVO.setHt_name ( jsonObject.has ( "ht_name" ) ? jsonObject.getString ( "ht_name" ) : null );
				tourProductVO.setHt_intro ( jsonObject.has ( "ht_intro" ) ? jsonObject.getString ( "ht_intro" ) : null );
				tourProductVO.setHt_status ( jsonObject.has ( "ht_status" ) ? jsonObject.getString ( "ht_status" ) : null );
				tourProductVO.setHt_rcmd_org ( jsonObject.has ( "ht_rcmd_org" ) ? jsonObject.getString ( "ht_rcmd_org" ) : null );
				tourProductVO.setHcnt_rcmd_comt( jsonObject.has ( "ht_rcmd_comt" ) ? jsonObject.getString ( "ht_rcmd_comt" ) : null );
				tourProductVO.setHt_img ( jsonObject.has ( "ht_img" ) ? jsonObject.getString ( "ht_img" ) : null );
				tourProductVO.setHt_hcd ( jsonObject.has ( "ht_hcd" ) ? jsonObject.getString ( "ht_hcd" ) : null );
				tourProductVO.setHt_scd ( jsonObject.has ( "ht_scd" ) ? jsonObject.getString ( "ht_scd" ) : null );
				tourProductVO.setHt_inv_hcnt ( jsonObject.has ( "ht_inv_hcnt" ) ? jsonObject.getString ( "ht_inv_hcnt" ) : null );
				tourProductVO.setHt_inv_memo ( jsonObject.has ( "ht_inv_memo" ) ? jsonObject.getString ( "ht_inv_memo" ) : null );
				tourProductVO.setROWNUM ( jsonObject.has ( "ROWNUM" ) ? jsonObject.getString ( "ROWNUM" ) : null );
				tourProductVO.setHt_inv_route ( jsonObject.has ( "ht_inv_route" ) ? jsonObject.getString ( "ht_inv_route" ) : null );
				tourProductVO.setCart_count ( jsonObject.has ( "cart_count" ) ? jsonObject.getString ( "cart_count" ) : null );
				tourProductVO.setComment_count ( jsonObject.has ( "comment_count" ) ? jsonObject.getString ( "comment_count" ) : null );
				tourProductVO.setCost ( jsonObject.has ( "cost" ) ? jsonObject.getString ( "cost" ) : null );
				tourProductVO.setDistance ( jsonObject.has ( "distance" ) ? jsonObject.getString ( "distance" ) : null );
				tourProductVO.setDays ( jsonObject.has ( "days" ) ? jsonObject.getString ( "days" ) : null );
				tourProductVO.setDuraion ( jsonObject.has ( "duration" ) ? jsonObject.getString ( "duration" ) : null );
				tourProductVO.setNickname ( jsonObject.has ( "nickname" ) ? jsonObject.getString ( "nickname" ) : null );
                tourProductVO.setHt_author ( jsonObject.has ( "ht_author" ) ? jsonObject.getString ( "ht_author" ) : null );
				tourProductVO.setHt_create_time ( jsonObject.has ( "ht_create_time" ) ? jsonObject.getString ( "ht_create_time" ) : null);

				hcdKeywordArray = jsonObject.has ( "hcnt_hcd_name" ) ? jsonObject.getJSONArray ( "hcnt_hcd_name" ) : null;
				scdKeywordArray = jsonObject.has ( "hcnt_scd_name" ) ? jsonObject.getJSONArray ( "hcnt_scd_name" ) : null;

				tourProductVO.setHcnt_rcmd_comt_sec( jsonObject.has ( "hcnt_rcmd_comt_sec" ) ? jsonObject.getString ( "hcnt_rcmd_comt_sec" ) : null );
				tourProductVO.setHcnt_rcmd_comt_thd( jsonObject.has ( "hcnt_rcmd_comt_thd" ) ? jsonObject.getString ( "hcnt_rcmd_comt_thd" ) : null );
				tourProductVO.setHcnt_rcmd_comt_fth( jsonObject.has ( "hcnt_rcmd_comt_fth" ) ? jsonObject.getString ( "hcnt_rcmd_comt_fth" ) : null );
				tourProductVO.setHcnt_rcmd_comt_fiv( jsonObject.has ( "hcnt_rcmd_comt_fiv" ) ? jsonObject.getString ( "hcnt_rcmd_comt_fiv" ) : null );

				tourProductVO.setHcnt_tab( jsonObject.has ( "hcnt_tab" ) ? jsonObject.getString ( "hcnt_tab" ) : null );

				if ( hcdKeywordArray != null )
				{
					ArrayList <KeywordVO> list = new ArrayList < KeywordVO > ( );

					for ( int k = 0 ; k < hcdKeywordArray.length ( ) ; k++ )
					{
						JSONObject object = new JSONObject ( );
						KeywordVO vo = new KeywordVO ( );
						object = hcdKeywordArray.getJSONObject ( k );
						vo.setCode_name(object.has("code_name") ? object.getString("code_name") : null);
						vo.setClassL(object.has("class") ? object.getString("class") : null);
						vo.setCode(object.has("code") ? object.getString("code") : null);
						list.add(vo);
					}
					tourProductVO.setHt_hcd_name ( list );
				}

				if ( scdKeywordArray != null )
				{
					ArrayList < KeywordVO > list = new ArrayList < KeywordVO > ( );

					for ( int k = 0 ; k < hcdKeywordArray.length ( ) ; k++ )
					{
						JSONObject object = new JSONObject ( );
						KeywordVO vo = new KeywordVO ( );
						object = scdKeywordArray.getJSONObject ( k );
						vo.setCode_name(object.has("code_name") ? object.getString("code_name") : null);
						vo.setClassL(object.has("class") ? object.getString("class") : null);
						vo.setCode(object.has("code") ? object.getString("code") : null);
						list.add(vo);
					}
					tourProductVO.setHt_scd_name ( list );
				}
			}
			catch ( JSONException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}
			catch ( NullPointerException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}

			arrayList.add ( tourProductVO );
		}

		return arrayList;
	}

	@Override
	protected void onPostExecute ( ArrayList < TourProductVO > result )
	{
		progressDialog.dismiss ( );
		( ( JSONArrayResult ) context ).setJSONArrayResult ( result );
		super.onPostExecute ( result );
	}

}
