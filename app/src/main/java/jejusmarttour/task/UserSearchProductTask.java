package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.KeywordVO;
import jejusmarttour.vo.SearchProductRequestVO;
import jejusmarttour.vo.UserTourProductVO;

public class UserSearchProductTask extends AsyncTask <SearchProductRequestVO, Integer , ArrayList <UserTourProductVO> >
{
	private static final String TAG = UserSearchProductTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public UserSearchProductTask ( Context context , Fragment fragment )
	{
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( )
	{
		progressDialog = ProgressDialog.show( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList < UserTourProductVO > doInBackground ( SearchProductRequestVO ... params )
	{
		URL url;
		SearchProductRequestVO vo = params [ 0 ];
		String response;
		JSONArray responseJSON;
		ArrayList < UserTourProductVO > arrayList = null;

		try
		{
			String urlStr = CommonData.searchHTPAddres(vo);

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );
			
			if( byteData.length <= 0 )
			{
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONArray ( response );

			arrayList = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		if ( arrayList.size ( ) <= 0 || arrayList == null )
		{
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;
	}

	// JSON 파싱
	private ArrayList < UserTourProductVO > getResultList ( JSONArray jsonArray )
	{
		if ( jsonArray == null )
			return null;

		ArrayList < UserTourProductVO > arrayList = new ArrayList < UserTourProductVO > ( );

		for ( int i = 0 ; i < jsonArray.length ( ) ; i++ )
		{
			JSONArray hcdKeywordArray = new JSONArray ( );
			JSONArray scdKeywordArray = new JSONArray ( );
			JSONObject jsonObject;
			UserTourProductVO userTourProductVO = new UserTourProductVO ( );

			try
			{
				jsonObject = jsonArray.getJSONObject ( i );
				userTourProductVO.setHt_author ( jsonObject.has ( "u_ht_author" ) ? jsonObject.getString ( "u_ht_author" ) : null );
				userTourProductVO.setHt_name ( jsonObject.has ( "u_ht_name" ) ? jsonObject.getString ( "u_ht_name" ) : null );
				userTourProductVO.setHt_inv_hcnt(jsonObject.has("u_ht_inv_hcnt") ? jsonObject.getString("u_ht_inv_hcnt") : null);
				userTourProductVO.setNickname(jsonObject.has("nickname") ? jsonObject.getString("nickname") : null);
				userTourProductVO.setHt_intro(jsonObject.has("u_ht_intro") ? jsonObject.getString("u_ht_intro") : null);
				userTourProductVO.setHt_inv_memo(jsonObject.has("u_ht_inv_memo") ? jsonObject.getString("u_ht_inv_memo") : null);
				userTourProductVO.setHt_status(jsonObject.has("u_ht_status") ? jsonObject.getString("u_ht_status") : null);
				userTourProductVO.setHt_create_time(jsonObject.has("u_ht_create_time") ? jsonObject.getString("u_ht_create_time") : null);
				userTourProductVO.setHt_img(jsonObject.has("u_ht_img") ? jsonObject.getString("u_ht_img") : null);
				userTourProductVO.setHt_id(jsonObject.has("u_ht_id") ? jsonObject.getString("u_ht_id") : null);
				userTourProductVO.setHt_scd(jsonObject.has("u_ht_scd") ? jsonObject.getString("u_ht_scd") : null);
				userTourProductVO.setHt_hcd(jsonObject.has("u_ht_hcd") ? jsonObject.getString("u_ht_hcd") : null);
				userTourProductVO.setROWNUM(jsonObject.has("ROWNUM") ? jsonObject.getString("ROWNUM") : null);
				userTourProductVO.setCart_count(jsonObject.has("cart_count") ? jsonObject.getString("cart_count") : null);
				userTourProductVO.setComment_count(jsonObject.has("comment_count") ? jsonObject.getString("comment_count") : null);
				
				hcdKeywordArray = jsonObject.has ( "ht_hcd_name" ) ? jsonObject.getJSONArray ( "ht_hcd_name" ) : null;
				scdKeywordArray = jsonObject.has ( "ht_scd_name" ) ? jsonObject.getJSONArray ( "ht_scd_name" ) : null;
				
				if ( hcdKeywordArray != null )
				{
					ArrayList <KeywordVO> list = new ArrayList < KeywordVO > ( );
					
					for ( int k = 0 ; k < hcdKeywordArray.length ( ) ; k++ )
					{
						JSONObject object = new JSONObject ( );
						KeywordVO vo = new KeywordVO ( );
						object = hcdKeywordArray.getJSONObject ( k );
						vo.setCode_name (  object.has ( "code_name" ) ? object.getString ( "code_name" ) : null );
						vo.setClassL ( object.has ( "class" ) ? object.getString ( "class" ) : null );
						vo.setCode ( object.has ( "code" ) ? object.getString ( "code" ) : null );
						list.add ( vo );
						userTourProductVO.setHt_hcd_name ( list );
					}
				}
				
				if ( scdKeywordArray != null )
				{
					ArrayList < KeywordVO > list = new ArrayList < KeywordVO > ( );
					
					for ( int k = 0 ; k < hcdKeywordArray.length ( ) ; k++ )
					{
						JSONObject object = new JSONObject ( );
						KeywordVO vo = new KeywordVO ( );
						object = scdKeywordArray.getJSONObject ( k );
						vo.setCode_name (  object.has ( "code_name" ) ? object.getString ( "code_name" ) : null );
						vo.setClassL ( object.has ( "class" ) ? object.getString ( "class" ) : null );
						vo.setCode ( object.has ( "code" ) ? object.getString ( "code" ) : null );
						list.add ( vo );
						userTourProductVO.setHt_scd_name ( list );
					}
				}
			}
			catch ( JSONException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}
			catch ( NullPointerException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}

			arrayList.add ( userTourProductVO );

			// System.out.print ( "reviewVO comment_subt : " +
			// reviewVO.getComment_subt ( ) + "\n" );
		}

		return arrayList;
	}

	@Override
	protected void onPostExecute ( ArrayList < UserTourProductVO > result )
	{
		progressDialog.dismiss ( );
		( ( JSONArrayResult ) fragment ).setJSONArrayResult ( result );
		super.onPostExecute ( result );
	}

}
