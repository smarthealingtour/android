package jejusmarttour.task;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.UpdateResultVO;

public class MTCShareDeleteTask extends AsyncTask < String , Integer , UpdateResultVO> {
	private static final String TAG = MTCShareDeleteTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;

	public MTCShareDeleteTask ( Context context ) {
		this.context = context;
	}

	@Override
	protected void onPreExecute ( ) {
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@SuppressLint ( "NewApi" )
	@Override
	protected UpdateResultVO doInBackground ( String ... params ) {
		URL url;
		String userId = params [ 0 ];
		String productId = params [ 1 ];
		String response;
		JSONObject responseJSON = null;

		try {
			String urlStr = CommonData.getMTCShareDeleteAddress ( userId , productId  );

			Log.e(TAG, "url: " + urlStr);

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData;
			int nLength;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 ) {
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 ) return null;
			response = new String ( byteData );

			responseJSON = new JSONObject ( response );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( IOException | JSONException e ) {
			Log.e ( TAG , e.toString ( ) );
		}

		return getResultList ( responseJSON );

	}

	// JSON 파싱
	private UpdateResultVO getResultList ( JSONObject jsonObject ) {
		if ( jsonObject == null ) return null;

		UpdateResultVO userVO = new UpdateResultVO ( );

		try {
			userVO.setResult ( jsonObject.has ( "result" ) ? jsonObject.getString ( "result" ) : null );
		}
		catch ( JSONException | NullPointerException e ) {
			Log.e ( TAG , e.toString ( ) );
		}

		return userVO;
	}

	@Override
	protected void onPostExecute ( UpdateResultVO result ) {
		progressDialog.dismiss ( );
		( ( JSONObjectResult < UpdateResultVO > ) context ).setJSONObjectResult ( result );
		super.onPostExecute ( result );
	}
}
