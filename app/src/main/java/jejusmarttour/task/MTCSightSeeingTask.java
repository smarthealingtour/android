package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.KeywordVO;
import jejusmarttour.vo.SightSeeingContentsVO;

public class MTCSightSeeingTask extends AsyncTask < String , Integer , ArrayList < SightSeeingContentsVO > >{
	private static final String TAG = MTCSightSeeingTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public MTCSightSeeingTask ( Context context , Fragment fragment ){
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( ){
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList < SightSeeingContentsVO > doInBackground ( String ... params ){
		URL url;
		String type = params [ 0 ];
		String page = params [ 1 ];
		String response;
		JSONArray responseJSON;
		ArrayList < SightSeeingContentsVO > arrayList = null;

		try{
			String urlStr = CommonData.getMyListAddress ( type , page );

			url = new URL ( urlStr );

			Log.e ( TAG , "url param: " + urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 )
			{
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONArray ( response );

			arrayList = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( IOException | JSONException e ) {
			Log.e ( TAG , e.toString ( ) );
		}

		if ( arrayList == null || arrayList.size ( ) <= 0  ) {
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;
	}

	// JSON 파싱
	private ArrayList < SightSeeingContentsVO > getResultList ( JSONArray jsonArray ) {
		if ( jsonArray == null )
			return null;

		ArrayList < SightSeeingContentsVO > arrayList = new ArrayList < SightSeeingContentsVO > ( );

		for ( int i = 0; i < jsonArray.length ( ); i++ ) {
			JSONArray cdKeywordArray ;
			JSONObject jsonObject;
			SightSeeingContentsVO sightSeeingContentsVO = new SightSeeingContentsVO ( );

			try {
				jsonObject = jsonArray.getJSONObject ( i );

				cdKeywordArray = jsonObject.has ( "ccnt_cd_name" ) ? jsonObject.getJSONArray ( "ccnt_cd_name" ) : null;

				// 관광장소
				if ( cdKeywordArray != null )
				{
					ArrayList < KeywordVO > list = new ArrayList < KeywordVO > ( );

					for ( int k = 0; k < cdKeywordArray.length ( ); k++ )
					{
						JSONObject object01 = new JSONObject ( );
						KeywordVO vo = new KeywordVO ( );
						object01 = cdKeywordArray.getJSONObject ( k );
						vo.setCode_name ( object01.has ( "code_name" ) ? object01.getString ( "code_name" ) : null );
						vo.setClassL ( object01.has ( "class" ) ? object01.getString ( "class" ) : null );
						vo.setCode ( object01.has ( "code" ) ? object01.getString ( "code" ) : null );
						list.add ( vo );
					}
					sightSeeingContentsVO.setCcnt_cd_name ( list );
				}

				sightSeeingContentsVO.setCcnt_name ( jsonObject.has ( "ccnt_name" ) ? jsonObject.getString ( "ccnt_name" ) : null );
				sightSeeingContentsVO.setCcnt_id ( jsonObject.has ( "ccnt_id" ) ? jsonObject.getString ( "ccnt_id" ) : null );
				sightSeeingContentsVO.setCcnt_img ( jsonObject.has ( "ccnt_img" ) ? jsonObject.getString ( "ccnt_img" ) : null );
				sightSeeingContentsVO.setCcnt_coord_x ( jsonObject.has ( "ccnt_coord_x" ) ? jsonObject.getString ( "ccnt_coord_x" ) : null );
				sightSeeingContentsVO.setCcnt_coord_y ( jsonObject.has ( "ccnt_coord_y" ) ? jsonObject.getString ( "ccnt_coord_y" ) : null );
				sightSeeingContentsVO.setCart_count ( jsonObject.has ( "cart_count" ) ? jsonObject.getString ( "cart_count" ) : null );
				sightSeeingContentsVO.setComment_count ( jsonObject.has ( "comment_count" ) ? jsonObject.getString ( "comment_count" ) : null );
				sightSeeingContentsVO.setCcnt_cd ( jsonObject.has ( "ccnt_cd" ) ? jsonObject.getString ( "ccnt_cd" ) : null );

				sightSeeingContentsVO.setCcnt_intro ( jsonObject.has ( "ccnt_intro" ) ? jsonObject.getString ( "ccnt_intro" ) : null );
				sightSeeingContentsVO.setCcnt_addr ( jsonObject.has ( "ccnt_addr" ) ? jsonObject.getString ( "ccnt_addr" ) : null );
				sightSeeingContentsVO.setCcnt_tel ( jsonObject.has ( "ccnt_tel" ) ? jsonObject.getString ( "ccnt_tel" ) : null );
				sightSeeingContentsVO.setCcnt_op_time ( jsonObject.has ( "ccnt_op_time" ) ? jsonObject.getString ( "ccnt_op_time" ) : null );
				sightSeeingContentsVO.setCcnt_duration ( jsonObject.has ( "ccnt_duration" ) ? jsonObject.getString ( "ccnt_duration" ) : null );
				sightSeeingContentsVO.setCcnt_site ( jsonObject.has ( "ccnt_site" ) ? jsonObject.getString ( "ccnt_site" ) : null );
				sightSeeingContentsVO.setCcnt_rcmd ( jsonObject.has ( "ccnt_rcmd" ) ? jsonObject.getString ( "ccnt_rcmd" ) : null );
				sightSeeingContentsVO.setCcnt_rcmd_comt ( jsonObject.has ( "ccnt_rcmd_comt" ) ? jsonObject.getString ( "ccnt_rcmd_comt" ) : null );
				sightSeeingContentsVO.setCcnt_hcd ( jsonObject.has ( "ccnt_hcd" ) ? jsonObject.getString ( "ccnt_hcd" ) : null );
				sightSeeingContentsVO.setNickname ( jsonObject.has ( "nickname" ) ? jsonObject.getString ( "nickname" ) : null );
				sightSeeingContentsVO.setCcnt_best ( jsonObject.has ( "ccnt_best" ) ? jsonObject.getString ( "ccnt_best" ) : null );
				sightSeeingContentsVO.setCcnt_park ( jsonObject.has ( "ccnt_park" ) ? jsonObject.getString ( "ccnt_park" ) : null );
				sightSeeingContentsVO.setCcnt_conv ( jsonObject.has ( "ccnt_conv" ) ? jsonObject.getString ( "ccnt_conv" ) : null );
				sightSeeingContentsVO.setG_id ( jsonObject.has ( "g_id" ) ? jsonObject.getString ( "g_id" ) : null );
				sightSeeingContentsVO.setTag ( jsonObject.has ( "tag" ) ? jsonObject.getString ( "tag" ) : null );
				sightSeeingContentsVO.setCcnt_bed ( jsonObject.has ( "ccnt_bed" ) ? jsonObject.getString ( "ccnt_bed" ) : null );
				sightSeeingContentsVO.setCcnt_cook ( jsonObject.has ( "ccnt_cook" ) ? jsonObject.getString ( "ccnt_cook" ) : null );
				sightSeeingContentsVO.setCcnt_meal ( jsonObject.has ( "ccnt_meal" ) ? jsonObject.getString ( "ccnt_meal" ) : null );
				sightSeeingContentsVO.setCcnt_rent ( jsonObject.has ( "ccnt_rent" ) ? jsonObject.getString ( "ccnt_rent" ) : null );
				sightSeeingContentsVO.setCcnt_code ( jsonObject.has ( "ccnt_code" ) ? jsonObject.getString ( "ccnt_code" ) : null );
				sightSeeingContentsVO.setCcnt_m_imgs( jsonObject.has ( "ccnt_m_img" ) ? jsonObject.getString ( "ccnt_m_img" ).split(",") : null);
				if (jsonObject.has ( "ccnt_m_img" )) {
					String str = jsonObject.getString ( "ccnt_m_img" );
					String[] m_imgs = str.split(",");

					for (String s :
							m_imgs) {
						Log.e("m_imgs", " str : " + s);
					}
				}
				else{
					sightSeeingContentsVO.setCcnt_m_imgs( null );
				}
			}
			catch ( JSONException | NullPointerException e ){
				Log.e ( TAG , e.toString ( ) );
			}

			arrayList.add ( sightSeeingContentsVO );

			// System.out.print ( "reviewVO comment_subt : " +
			// reviewVO.getComment_subt ( ) + "\n" );
		}

		return arrayList;
	}

	@Override
	protected void onPostExecute ( ArrayList < SightSeeingContentsVO > result )
	{
		progressDialog.dismiss ( );
		( ( JSONArrayResult ) fragment ).setJSONArrayResult ( result );
		super.onPostExecute ( result );
	}

}
