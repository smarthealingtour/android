package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.KeywordVO;

public class MTCHealingContentsTask extends AsyncTask < String , Integer , ArrayList < HealingContentsVO > >{
	private static final String TAG = MTCHealingContentsTask.class.toString ( );

	ProgressDialog progressDialog;
	private Context context;
	private JSONArrayResult fragment;

	public MTCHealingContentsTask ( Context context , JSONArrayResult fragment ){
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( ){
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList < HealingContentsVO > doInBackground ( String ... params ){
		URL url;
		String type = params [ 0 ];
		String page = params [ 1 ];
		Log.e(TAG, "size : " + params.length +"type: "+ type + " page: "+ page  );
		String response;
		JSONArray responseJSON;
		ArrayList < HealingContentsVO > arrayList = null;

		try{
			String urlStr = CommonData.getMyListAddress ( type , page );
			url = new URL ( urlStr );

			Log.e(TAG, "doInBackground: " + urlStr);

			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 ){
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 ){
				return null;
			}

			response = new String ( byteData );
			responseJSON = new JSONArray ( response );
			arrayList = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( JSONException | IOException e ){
			Log.e ( TAG , e.toString ( ) );
		}

		if ( arrayList == null || arrayList.size ( ) <= 0  )
		{
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;
	}

	// JSON 파싱
	private ArrayList < HealingContentsVO > getResultList ( JSONArray jsonArray ){
		if ( jsonArray == null )
			return null;

		ArrayList < HealingContentsVO > arrayList = new ArrayList < HealingContentsVO > ( );

		for ( int i = 0; i < jsonArray.length ( ); i++ ){
			JSONArray hcdKeywordArray = new JSONArray ( );
			JSONArray scdKeywordArray = new JSONArray ( );
			JSONObject object;
			HealingContentsVO healingContentsVO = new HealingContentsVO ( );

			try{
				object = jsonArray.getJSONObject ( i );

				healingContentsVO.setHcnt_id ( object.has ( "hcnt_id" ) ? object.getString ( "hcnt_id" ) : null );
				healingContentsVO.setHcnt_type ( object.has ( "hcnt_type" ) ? object.getString ( "hcnt_type" ) : null );
				healingContentsVO.setHcnt_name ( object.has ( "hcnt_name" ) ? object.getString ( "hcnt_name" ) : null );
				healingContentsVO.setHcnt_intro ( object.has ( "hcnt_intro" ) ? object.getString ( "hcnt_intro" ) : null );
				healingContentsVO.setHcnt_addr ( object.has ( "hcnt_addr" ) ? object.getString ( "hcnt_addr" ) : null );
				healingContentsVO.setHcnt_tel ( object.has ( "hcnt_tel" ) ? object.getString ( "hcnt_tel" ) : null );
				healingContentsVO.setHcnt_op_time ( object.has ( "hcnt_op_time" ) ? object.getString ( "hcnt_op_time" ) : null );
				healingContentsVO.setHcnt_duration ( object.has ( "hcnt_duration" ) ? object.getString ( "hcnt_duration" ) : null );
				healingContentsVO.setHcnt_guide ( object.has ( "hcnt_guide" ) ? object.getString ( "hcnt_guide" ) : null );
				healingContentsVO.setHcnt_guide_name ( object.has ( "hcnt_guide_name" ) ? object.getString ( "hcnt_guide_name" ) : null );
				healingContentsVO.setHcnt_guide_tel ( object.has ( "hcnt_guide_tel" ) ? object.getString ( "hcnt_guide_tel" ) : null );
				healingContentsVO.setHcnt_site ( object.has ( "hcnt_site" ) ? object.getString ( "hcnt_site" ) : null );
				healingContentsVO.setHcnt_sub_img ( object.has ( "hcnt_sub_img" ) ? object.getString ( "hcnt_sub_img" ) : null );
				healingContentsVO.setHcnt_rcmd ( object.has ( "hcnt_rcmd" ) ? object.getString ( "hcnt_rcmd" ) : null );
				healingContentsVO.setHcnt_rcmd_comt ( object.has ( "hcnt_rcmd_comt" ) ? object.getString ( "hcnt_rcmd_comt" ) : null );
				healingContentsVO.setHcnt_hcd ( object.has ( "hcnt_hcd" ) ? object.getString ( "hcnt_hcd" ) : null );
				healingContentsVO.setHcnt_scd ( object.has ( "hcnt_scd" ) ? object.getString ( "hcnt_scd" ) : null );

				hcdKeywordArray = object.has ( "hcnt_hcd_name" ) ? object.getJSONArray ( "hcnt_hcd_name" ) : null;
				scdKeywordArray = object.has ( "hcnt_scd_name" ) ? object.getJSONArray ( "hcnt_scd_name" ) : null;

				if ( hcdKeywordArray != null ){
					ArrayList < KeywordVO > list = new ArrayList < KeywordVO > ( );

					for ( int k = 0; k < hcdKeywordArray.length ( ); k++ ){
						JSONObject object01 = new JSONObject ( );
						KeywordVO vo = new KeywordVO ( );
						object01 = hcdKeywordArray.getJSONObject ( k );
						vo.setCode_name ( object01.has ( "code_name" ) ? object01.getString ( "code_name" ) : null );
						vo.setClassL ( object01.has ( "class" ) ? object01.getString ( "class" ) : null );
						vo.setCode ( object01.has ( "code" ) ? object01.getString ( "code" ) : null );
						list.add ( vo );
					}
					healingContentsVO.setHcnt_hcd_name ( list );
				}

				if ( scdKeywordArray != null ){
					ArrayList < KeywordVO > list = new ArrayList < KeywordVO > ( );

					for ( int k = 0; k < hcdKeywordArray.length ( ); k++ ){
						JSONObject object02 = new JSONObject ( );
						KeywordVO vo = new KeywordVO ( );
						object02 = scdKeywordArray.getJSONObject ( k );
						vo.setCode_name ( object02.has ( "code_name" ) ? object02.getString ( "code_name" ) : null );
						vo.setClassL ( object02.has ( "class" ) ? object02.getString ( "class" ) : null );
						vo.setCode ( object02.has ( "code" ) ? object02.getString ( "code" ) : null );
						list.add ( vo );
					}
					healingContentsVO.setHcnt_scd_name ( list );
				}

				healingContentsVO.setHcnt_inv_hcnt ( object.has ( "hcnt_inv_hcnt" ) ? object.getString ( "hcnt_inv_hcnt" ) : null );
				healingContentsVO.setHt_id ( object.has ( "ht_id" ) ? object.getString ( "ht_id" ) : null );
				healingContentsVO.setNickname ( object.has ( "nickname" ) ? object.getString ( "nickname" ) : null );
				healingContentsVO.setHcnt_coord_x ( object.has ( "hcnt_coord_x" ) ? object.getString ( "hcnt_coord_x" ) : null );
				healingContentsVO.setHcnt_coord_y ( object.has ( "hcnt_coord_y" ) ? object.getString ( "hcnt_coord_y" ) : null );
				healingContentsVO.setHcnt_cost_adult ( object.has ( "hcnt_cost_adult" ) ? object.getString ( "hcnt_cost_adult" ) : " - " );
				healingContentsVO.setHcnt_cost_child ( object.has ( "hcnt_cost_child" ) ? object.getString ( "hcnt_cost_child" ) : " - " );
				healingContentsVO.setHcnt_cost_jeju ( object.has ( "hcnt_cost_jeju" ) ? object.getString ( "hcnt_cost_jeju" ) : " - " );
				healingContentsVO.setG_id ( object.has ( "g_id" ) ? object.getString ( "g_id" ) : null );

				healingContentsVO.setCart_count ( object.has ( "cart_count" ) ? object.getString ( "cart_count" ) : null );
				healingContentsVO.setComment_count ( object.has ( "comment_count" ) ? object.getString ( "comment_count" ) : null );
				healingContentsVO.setHcnt_kml ( object.has ( "hcnt_kml" ) ? object.getString ( "hcnt_kml" ) : null );

			}
			catch ( JSONException | NullPointerException e ){
				e.printStackTrace ( );
			}

			arrayList.add ( healingContentsVO );

			// System.out.print ( "reviewVO comment_subt : " +
			// reviewVO.getComment_subt ( ) + "\n" );
		}

		return arrayList;
	}

	@Override
	protected void onPostExecute ( ArrayList < HealingContentsVO > result ){
		progressDialog.dismiss ( );
		fragment.setJSONArrayResult ( result );
		super.onPostExecute ( result );
	}

}
