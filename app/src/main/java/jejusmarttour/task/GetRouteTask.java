package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.RouteVO;

public class GetRouteTask extends AsyncTask < String , Integer , RouteVO >
{
	private static final String TAG = GetRouteTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public GetRouteTask ( Context context , Fragment fragment )
	{
		this.context = context;
		this.fragment = fragment;
	}

	public GetRouteTask ( Context context )
	{
		this.context = context;
	}

	@Override
	protected void onPreExecute ( )
	{
		// progressDialog = ProgressDialog.show ( context , "" , "Loading..." ,
		// true , false );
		super.onPreExecute ( );
	}

	@Override
	protected RouteVO doInBackground ( String ... params )
	{
		if ( isCancelled ( ) )
			return null;

		URL url;
		String start_id = params [ 0 ];
		String end_id = params [ 1 ];
		String position = params [ 2 ];
		String contentID = params [ 3 ];


		String response;
		RouteVO routeVO = null;

		try{
			String urlStr = null;

			urlStr = CommonData.getRouteAddress ( start_id , end_id);

			Log.d ( TAG , urlStr );

			System.out.println(urlStr);

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			routeVO = new RouteVO ( );
			routeVO.setStart(start_id);
			routeVO.setEnd(end_id);
			routeVO.setPosition ( position );
			routeVO.setContentID ( contentID );

			if ( byteData.length <= 0 )
			{
				return routeVO;
			}

			response = new String ( byteData );
			JSONObject object = new JSONObject ( response );
			JSONObject result = new JSONObject ( object.getString("route"));

			routeVO.setLead_time(String.valueOf(result.getInt("time")));
			routeVO.setDistance (String.valueOf(result.getInt("length" ) ) );
			routeVO.setGeometry ( result.getString ( "geometry" ) );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e )
		{
			Log.e ( TAG, e.toString());
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		return routeVO;
	}

	@Override
	protected void onPostExecute ( RouteVO result )
	{
		// progressDialog.dismiss ( );
		if ( fragment != null )
			( ( JSONObjectResult < RouteVO > ) fragment ).setJSONObjectResult ( result );
//      else
//         try
//         {
//            ( ( MapActivity ) context ).setRouteResult ( result );
//         }
//         catch ( JSONException e )
//         {
//            e.printStackTrace ( );
//         }
		super.onPostExecute ( result );
	}

}