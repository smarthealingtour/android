package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;

public class TourSpotTask extends AsyncTask <SearchProductRequestVO, Integer , ArrayList < ScheduleVO > >
{
	private static final String TAG = TourSpotTask.class.toString ( );

	private ProgressDialog progressDialog;
	private JSONArrayResult jsonArrayResult;
	private String scd;
	private String hcd;

	public TourSpotTask(Context context, JSONArrayResult jsonArrayResult, String scd, String hcd){
		this.jsonArrayResult = jsonArrayResult;
		this.scd = scd;
		this.hcd = hcd;
		progressDialog = ProgressDialog.show( context , "" , "Loading..." , true , true );
	}

	@Override
	protected void onPreExecute ( )	{
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList <ScheduleVO> doInBackground ( SearchProductRequestVO ... params ){
		URL url;
		SearchProductRequestVO vo = params [ 0 ];
		if ( !scd.equals("")){
			scd = "A" + scd;
			vo.setScd(scd);
		}
		if ( !hcd.equals("")){
			hcd = "A" + hcd;
			vo.setHcd(hcd);
		}
		String response;
		JSONArray responseJSON;
		ArrayList < ScheduleVO > arrayList = null;

		try{
			String urlStr = CommonData.searchSpotAddress(vo);

			url = new URL ( urlStr );

			Log.e("tag", "hs url parammm : " + urlStr);
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData;
			int nLength;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 ){
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );
			
			if( byteData.length <= 0 ){
				return null;
			}

			response = new String ( byteData );
			responseJSON = new JSONArray ( response );

			arrayList = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		} catch ( IOException | JSONException e ){
			Log.e ( TAG , e.toString ( ) );
		}

		if ( arrayList.size() <= 0 ){
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;
	}

	// JSON 파싱
	private ArrayList <ScheduleVO> getResultList ( JSONArray jsonArray ){
		if ( jsonArray == null )
			return null;

		ArrayList < ScheduleVO > arrayList = new ArrayList < ScheduleVO > ( );

		for ( int i = 0 ; i < jsonArray.length ( ) ; i++ ){
			JSONArray hcdKeywordArray = new JSONArray ( );
			JSONArray scdKeywordArray = new JSONArray ( );
			JSONObject jsonObject;
			ScheduleVO scheduleVO = new ScheduleVO ( );

			scheduleVO.setHealingContentsVO( null );
			scheduleVO.setSightSeeingContentsVO( null );

			try{
				jsonObject = jsonArray.getJSONObject ( i );

				Log.i("qtam_2", "json2" + jsonObject.toString());

				scheduleVO.setHt_id(jsonObject.has("hcnt_id") ? jsonObject.getString("hcnt_id") : null);

				HealingContentsVO healingContentsVO = new HealingContentsVO ( );

				healingContentsVO.setHcnt_id ( jsonObject.has ( "hcnt_id" ) ? jsonObject.getString ( "hcnt_id" ) : null );
				healingContentsVO.setHcnt_type ( jsonObject.has ( "hcnt_type" ) ? jsonObject.getString ( "hcnt_type" ) : null );
				healingContentsVO.setHcnt_name ( jsonObject.has ( "hcnt_name" ) ? jsonObject.getString ( "hcnt_name" ) : null );
				healingContentsVO.setHcnt_intro ( jsonObject.has ( "hcnt_intro" ) ? jsonObject.getString ( "hcnt_intro" ) : null );
				healingContentsVO.setHcnt_addr ( jsonObject.has ( "hcnt_addr" ) ? jsonObject.getString ( "hcnt_addr" ) : null );
				healingContentsVO.setHcnt_tel ( jsonObject.has ( "hcnt_tel" ) ? jsonObject.getString ( "hcnt_tel" ) : null );
				healingContentsVO.setHcnt_op_time ( jsonObject.has ( "hcnt_op_time" ) ? jsonObject.getString ( "hcnt_op_time" ) : null );
				healingContentsVO.setHcnt_duration ( jsonObject.has ( "hcnt_duration" ) ? jsonObject.getInt ( "hcnt_duration" ) + "" : null );
				healingContentsVO.setHcnt_guide ( jsonObject.has ( "hcnt_guide" ) ? jsonObject.getString ( "hcnt_guide" ) : null );
				healingContentsVO.setHcnt_guide_name ( jsonObject.has ( "hcnt_guide_name" ) ? jsonObject.getString ( "hcnt_guide_name" ) : null );
				healingContentsVO.setHcnt_guide_tel ( jsonObject.has ( "hcnt_guide_tel" ) ? jsonObject.getString ( "hcnt_guide_tel" ) : null );
				healingContentsVO.setHcnt_site ( jsonObject.has ( "hcnt_site" ) ? jsonObject.getString ( "hcnt_site" ) : null );
				healingContentsVO.setHcnt_sub_img ( jsonObject.has ( "hcnt_sub_img" ) ? jsonObject.getString ( "hcnt_sub_img" ) : null );
				healingContentsVO.setHcnt_rcmd ( jsonObject.has ( "hcnt_rcmd" ) ? jsonObject.getString ( "hcnt_rcmd" ) : null );
				healingContentsVO.setHcnt_rcmd_comt ( jsonObject.has ( "hcnt_rcmd_comt" ) ? jsonObject.getString ( "hcnt_rcmd_comt" ) : null );
				healingContentsVO.setHcnt_hcd ( jsonObject.has ( "hcnt_hcd" ) ? jsonObject.getString ( "hcnt_hcd" ) : null );
				healingContentsVO.setHcnt_scd ( jsonObject.has ( "hcnt_scd" ) ? jsonObject.getString ( "hcnt_scd" ) : null );

				hcdKeywordArray = jsonObject.has ( "hcnt_hcd_name" ) ? jsonObject.getJSONArray ( "hcnt_hcd_name" ) : null;
				scdKeywordArray = jsonObject.has ( "hcnt_scd_name" ) ? jsonObject.getJSONArray ( "hcnt_scd_name" ) : null;

				healingContentsVO.setHcnt_inv_hcnt ( jsonObject.has ( "hcnt_inv_hcnt" ) ? jsonObject.getString ( "hcnt_inv_hcnt" ) : null );
				healingContentsVO.setHt_id ( jsonObject.has ( "ht_id" ) ? jsonObject.getString ( "ht_id" ) : null );
				healingContentsVO.setNickname ( jsonObject.has ( "nickname" ) ? jsonObject.getString ( "nickname" ) : null );
				healingContentsVO.setHcnt_coord_x ( jsonObject.has ( "hcnt_coord_x" ) ? jsonObject.getString ( "hcnt_coord_x" ) : null );
				healingContentsVO.setHcnt_coord_y ( jsonObject.has ( "hcnt_coord_y" ) ? jsonObject.getString ( "hcnt_coord_y" ) : null );
				healingContentsVO.setHcnt_cost_adult ( jsonObject.has ( "hcnt_cost_adult" ) ? jsonObject.getString ( "hcnt_cost_adult" ) : " - " );
				healingContentsVO.setHcnt_cost_child ( jsonObject.has ( "hcnt_cost_child" ) ? jsonObject.getString ( "hcnt_cost_child" ) : " - " );
				healingContentsVO.setHcnt_cost_jeju ( jsonObject.has ( "hcnt_cost_jeju" ) ? jsonObject.getString ( "hcnt_cost_jeju" ) : " - " );
				healingContentsVO.setG_id ( jsonObject.has ( "g_id" ) ? jsonObject.getString ( "g_id" ) : null );

				healingContentsVO.setCart_count ( jsonObject.has ( "cart_count" ) ? jsonObject.getString ( "cart_count" ) : null );
				healingContentsVO.setComment_count ( jsonObject.has ( "comment_count" ) ? jsonObject.getString ( "comment_count" ) : null );

				healingContentsVO.setHcnt_rcmd_comt_sec( jsonObject.has ( "hcnt_rcmd_comt_sec" ) ? jsonObject.getString ( "hcnt_rcmd_comt_sec" ) : null );
				healingContentsVO.setHcnt_rcmd_comt_thd( jsonObject.has ( "hcnt_rcmd_comt_thd" ) ? jsonObject.getString ( "hcnt_rcmd_comt_thd" ) : null );
				healingContentsVO.setHcnt_rcmd_comt_fth( jsonObject.has ( "hcnt_rcmd_comt_fth" ) ? jsonObject.getString ( "hcnt_rcmd_comt_fth" ) : null );
				healingContentsVO.setHcnt_rcmd_comt_fiv( jsonObject.has ( "hcnt_rcmd_comt_fiv" ) ? jsonObject.getString ( "hcnt_rcmd_comt_fiv" ) : null );

				healingContentsVO.setHcnt_tab( jsonObject.has ( "hcnt_tab" ) ? jsonObject.getString ( "hcnt_tab" ) : null );
				healingContentsVO.setHcnt_kml ( jsonObject.has ( "hcnt_kml" ) ? jsonObject.getString ( "hcnt_kml" ) : null );

				scheduleVO.setHealingContentsVO( healingContentsVO );
			}
			catch ( JSONException | NullPointerException e )
			{
				e.printStackTrace();
			}

			arrayList.add ( scheduleVO );

			// System.out.print ( "reviewVO comment_subt : " +
			// reviewVO.getComment_subt ( ) + "\n" );
		}

		return arrayList;
	}

	@Override
	protected void onCancelled() {
		progressDialog.dismiss ( );
		super.onCancelled();
	}

	@Override
	protected void onPostExecute ( ArrayList < ScheduleVO > result ){
		progressDialog.dismiss ( );
		( ( JSONArrayResult ) jsonArrayResult).setJSONArrayResult ( result );
		super.onPostExecute ( result );
	}

}
