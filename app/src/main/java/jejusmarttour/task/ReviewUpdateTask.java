package jejusmarttour.task;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HTPReviewVO;
import jejusmarttour.vo.UpdateResultVO;

public class ReviewUpdateTask extends AsyncTask < String , Integer , UpdateResultVO>
{
	private static final String TAG = ReviewUpdateTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public ReviewUpdateTask ( Context context , Fragment fragment )
	{
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( )
	{
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@SuppressLint ( "NewApi" )
	@Override
	protected UpdateResultVO doInBackground ( String ... params )
	{
		URL url;
		String userId = params [ 0 ];
		String productId = params [ 1 ];
		String reviewStr = params [ 2 ];
		boolean isPro = params [ 3 ].equals ( "pro" ) ? true : false;
		String response = null;
		JSONObject responseJSON = null;
		ArrayList <HTPReviewVO> arrayList = null;

		try
		{
			String urlStr = CommonData.updateReviewAddress(userId, productId, reviewStr, isPro);
			//String urlParameters = "email=" + URLEncoder.encode ( "56023209" , "UTF-8" ) + "&ht_id=" + URLEncoder.encode ( "HILLING NO.02" , "UTF-8" ) + "&comment_subt=" + URLEncoder.encode ( "안녕하세요 TEST입니다." , "UTF-8" );
			//String urlStr = "http://ubist.iptime.org:8084/Jsmth/restful/add_review?" + urlParameters;

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			//conn.setDoOutput ( true );
			//conn.setDoInput ( true );
			//conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			InputStream is = conn.getInputStream ( );
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 )
			{
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONObject ( response );

			//br.close ( );
			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		return getResultList ( responseJSON );

	}

	// JSON 파싱
	private UpdateResultVO getResultList ( JSONObject jsonObject )
	{
		if ( jsonObject == null )
			return null;

		UpdateResultVO userVO = new UpdateResultVO ( );

		try
		{
			userVO.setResult ( jsonObject.has ( "result" ) ? jsonObject.getString ( "result" ) : null );
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( NullPointerException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		return userVO;
	}

	@Override
	protected void onPostExecute ( UpdateResultVO result )
	{
		progressDialog.dismiss ( );
		( ( JSONObjectResult < UpdateResultVO > ) fragment ).setJSONObjectResult ( result );
		super.onPostExecute ( result );
	}
}
