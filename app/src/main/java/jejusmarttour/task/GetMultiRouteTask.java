package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.RouteVO;

public class GetMultiRouteTask extends AsyncTask < String , Integer , List<RouteVO> >
{
	private static final String TAG = GetMultiRouteTask.class.toString ( );

	private ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public GetMultiRouteTask(Context context, Fragment fragment)
	{
		this.context = context;
		this.fragment = fragment;
	}

	public GetMultiRouteTask(Context context)
	{
		this.context = context;
	}

	@Override
	protected void onPreExecute ( )
	{
		// progressDialog = ProgressDialog.show ( context , "" , "Loading..." ,
		// true , false );
		super.onPreExecute ( );
	}

	@Override
	protected List<RouteVO> doInBackground ( String ... params )
	{
		if ( isCancelled ( ) )
			return null;

		URL url;
		String start_id = params [ 0 ];
		String end_id = params [ 1 ];
		String way_points_id = params [ 2 ];


		String response;
		List<RouteVO> re = null;

		try
		{
			String urlStr = null;

			urlStr = CommonData.getMultiRouteAddress ( start_id , end_id, way_points_id);

			Log.d ( TAG , urlStr );

//            System.out.println(urlStr);

			url = new URL ( urlStr );
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
			{
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );

			if ( byteData.length <= 0 )
			{
				return null;
			}

			re = new ArrayList<>();

			response = new String ( byteData );
			JSONArray mRouteArray = new JSONArray( response );

			for(int i = 0; i < mRouteArray.length(); i++) {
				JSONObject result = new JSONObject ( mRouteArray.getString(i) );

				RouteVO routeVO = new RouteVO ( );
				routeVO.setLead_time(String.valueOf(result.getInt("time")));
				routeVO.setDistance (String.valueOf(result.getInt("length" ) ) );
				routeVO.setGeometry ( result.getString ( "geometry" ) );
				re.add ( routeVO);
			}

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e )
		{
			Log.e ( TAG, e.toString());
		}
		catch ( JSONException e )
		{
			Log.e ( TAG , e.toString ( ) );
		}

		return re;
	}

	@Override
	protected void onPostExecute ( List<RouteVO> result )
	{
//        try
//    {
//        ( ( MapActivity ) context ).setMultiRouteResult ( result );
//        System.out.println(result);
//    }
//    catch ( JSONException e )
//    {
//        e.printStackTrace ( );
//    }
		super.onPostExecute ( result );
	}

}