package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.KeywordVO;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;

public class ScheduleTask extends AsyncTask<String, Integer, ArrayList<ScheduleVO>> {
    private static final String TAG = ScheduleTask.class.toString();

    ProgressDialog progressDialog;
    private Context context;

    public ScheduleTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = ProgressDialog.show(context, "", "Loading...", true, false);
        super.onPreExecute();
    }

    @Override
    protected ArrayList<ScheduleVO> doInBackground(String... params) {
        URL url;
        String ht_id = params[0];
        String response;
        JSONArray responseJSON;
        ArrayList<ScheduleVO> arrayList = null;

        try {
            String urlStr = CommonData.getSearchHealingTourProductCourseAddres(ht_id);

            Log.e("qtam", "url: " + urlStr);

            url = new URL(urlStr);
            // GET 방식
            URLConnection conn = url.openConnection();
            conn.setUseCaches(false);
            InputStream is = conn.getInputStream();

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] byteBuffer = new byte[1024];
            byte[] byteData = null;
            int nLength = 0;

            while ((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
                byteArrayOutputStream.write(byteBuffer, 0, nLength);
            }

            byteData = byteArrayOutputStream.toByteArray();

            if (byteData.length <= 0) {
                return null;
            }

            response = new String(byteData);

            responseJSON = new JSONArray(response);

            arrayList = getResultList(responseJSON);

            byteArrayOutputStream.close();
            is.close();
            conn = null;

        } catch (IOException | JSONException e) {
            Log.e(TAG, e.toString());
        }

        if (arrayList == null || arrayList.size() <= 0) {
            Log.e(TAG, "데이터가 존재하지 않습니다.");
            return null;
        }

        return arrayList;
    }

    // JSON 파싱
    private ArrayList<ScheduleVO> getResultList(JSONArray jsonArray) {
        if (jsonArray == null)
            return null;

        ArrayList<ScheduleVO> arrayList = new ArrayList<ScheduleVO>();
        final int numberOfItemsInResp = jsonArray.length();

        for (int i = 0; i < numberOfItemsInResp; i++) {
            JSONArray hcdKeywordArray;
            JSONArray scdKeywordArray;
            JSONArray cdKeywordArray;
            JSONArray memoArray;
            JSONArray memoWriterArray;
            JSONObject jsonObject;
            ScheduleVO scheduleVO = new ScheduleVO();

            try {
                jsonObject = jsonArray.getJSONObject(i);
                scheduleVO.setHt_schd_id(jsonObject.has("ht_schd_id") ? jsonObject.getString("ht_schd_id") : null);
                scheduleVO.setDays(jsonObject.has("days") ? jsonObject.getString("days") : null);
                scheduleVO.setSeq(jsonObject.has("seq") ? jsonObject.getString("seq") : null);
                scheduleVO.setDay(jsonObject.has("day") ? jsonObject.getString("day") : null);
                scheduleVO.setHt_id(jsonObject.has("ht_id") ? jsonObject.getString("ht_id") : null);
                String[] routePoint = jsonObject.has("route_point") ? jsonObject.getString("route_point").split(" ") : null;
                scheduleVO.setRoute_point(routePoint);

                memoArray = jsonObject.has("memo") ? jsonObject.getJSONArray("memo") : null;
                memoWriterArray = jsonObject.has("memo_writer") ? jsonObject.getJSONArray("memo_writer") : null;

                if (memoArray != null) {
                    final int memoLenth = memoArray.length();
                    ArrayList<String> list = new ArrayList<String>();

                    for (int k = 0; k < memoLenth; k++) {
                        list.add(memoArray.get(k).toString());
                    }

                    scheduleVO.setMemo(list);
                }

                if (memoWriterArray != null) {
                    final int memoLenth = memoWriterArray.length();
                    ArrayList<String> list = new ArrayList<String>();

                    for (int k = 0; k < memoLenth; k++) {
                        list.add(memoWriterArray.get(k).toString());
                    }

                    scheduleVO.setMemoWriter(list);
                }

                if (jsonObject.has("route")) {
                    JSONObject object = jsonObject.getJSONObject("route");
                    RouteVO routeVO = new RouteVO();
                    routeVO.setDistance(object.has("distance") ? object.getString("distance") : null);
                    routeVO.setRoute_id(object.has("route_id") ? object.getString("route_id") : null);
                    routeVO.setLead_time(object.has("lead_time") ? object.getString("lead_time") : null);
                    routeVO.setHt_id(object.has("ht_id") ? object.getString("ht_id") : null);
                    routeVO.setStart(object.has("start") ? object.getString("start") : null);
                    routeVO.setEnd(object.has("end") ? object.getString("end") : null);
                    routeVO.setGeometry(object.has("geometry") ? object.getString("geometry") : null);
                    scheduleVO.setRoute(routeVO);
                } else {
                    scheduleVO.setRoute(null);
                }

                if (jsonObject.has("hcnt_id")) {
                    JSONObject object = jsonObject.getJSONObject("hcnt_id");
                    HealingContentsVO healingContentsVO = new HealingContentsVO();

                    healingContentsVO.setHcnt_id(object.has("hcnt_id") ? object.getString("hcnt_id") : null);
                    healingContentsVO.setHcnt_type(object.has("hcnt_type") ? object.getString("hcnt_type") : null);
                    healingContentsVO.setHcnt_name(object.has("hcnt_name") ? object.getString("hcnt_name") : null);
                    healingContentsVO.setHcnt_intro(object.has("hcnt_intro") ? object.getString("hcnt_intro") : null);
                    healingContentsVO.setHcnt_addr(object.has("hcnt_addr") ? object.getString("hcnt_addr") : null);
                    healingContentsVO.setHcnt_tel(object.has("hcnt_tel") ? object.getString("hcnt_tel") : null);
                    healingContentsVO.setHcnt_op_time(object.has("hcnt_op_time") ? object.getString("hcnt_op_time") : null);
                    healingContentsVO.setHcnt_duration(object.has("hcnt_duration") ? object.getString("hcnt_duration") : null);
                    healingContentsVO.setHcnt_guide(object.has("hcnt_guide") ? object.getString("hcnt_guide") : null);
                    healingContentsVO.setHcnt_guide_name(object.has("hcnt_guide_name") ? object.getString("hcnt_guide_name") : null);
                    healingContentsVO.setHcnt_guide_tel(object.has("hcnt_guide_tel") ? object.getString("hcnt_guide_tel") : null);
                    healingContentsVO.setHcnt_site(object.has("hcnt_site") ? object.getString("hcnt_site") : null);
                    healingContentsVO.setHcnt_sub_img(object.has("hcnt_sub_img") ? object.getString("hcnt_sub_img") : null);
                    healingContentsVO.setHcnt_kml(object.has("hcnt_kml") ? object.getString("hcnt_kml") : null);
                    healingContentsVO.setHcnt_rcmd(object.has("hcnt_rcmd") ? object.getString("hcnt_rcmd") : null);
                    healingContentsVO.setHcnt_rcmd_comt(object.has("hcnt_rcmd_comt") ? object.getString("hcnt_rcmd_comt") : null);
                    healingContentsVO.setHcnt_hcd(object.has("hcnt_hcd") ? object.getString("hcnt_hcd") : null);
                    healingContentsVO.setHcnt_scd(object.has("hcnt_scd") ? object.getString("hcnt_scd") : null);

                    hcdKeywordArray = object.has("hcnt_hcd_name") ? object.getJSONArray("hcnt_hcd_name") : null;
                    scdKeywordArray = object.has("hcnt_scd_name") ? object.getJSONArray("hcnt_scd_name") : null;

                    healingContentsVO.setHcnt_rcmd_comt_sec(object.has("hcnt_rcmd_comt_sec") ? object.getString("hcnt_rcmd_comt_sec") : null);
                    healingContentsVO.setHcnt_rcmd_comt_thd(object.has("hcnt_rcmd_comt_thd") ? object.getString("hcnt_rcmd_comt_thd") : null);
                    healingContentsVO.setHcnt_rcmd_comt_fth(object.has("hcnt_rcmd_comt_fth") ? object.getString("hcnt_rcmd_comt_fth") : null);
                    healingContentsVO.setHcnt_rcmd_comt_fiv(object.has("hcnt_rcmd_comt_fiv") ? object.getString("hcnt_rcmd_comt_fiv") : null);

                    healingContentsVO.setHcnt_tab(object.has("hcnt_tab") ? object.getString("hcnt_tab") : null);

                    if (hcdKeywordArray != null) {
                        ArrayList<KeywordVO> list = new ArrayList<KeywordVO>();

                        for (int k = 0; k < hcdKeywordArray.length(); k++) {
                            JSONObject object01;
                            KeywordVO vo = new KeywordVO();
                            object01 = hcdKeywordArray.getJSONObject(k);
                            vo.setCode_name(object01.has("code_name") ? object01.getString("code_name") : null);
                            vo.setClassL(object01.has("class") ? object01.getString("class") : null);
                            vo.setCode(object01.has("code") ? object01.getString("code") : null);
                            list.add(vo);
                        }
                        healingContentsVO.setHcnt_hcd_name(list);
                    }

                    if (scdKeywordArray != null) {
                        ArrayList<KeywordVO> list = new ArrayList<KeywordVO>();

                        for (int k = 0; k < hcdKeywordArray.length(); k++) {
                            JSONObject object02;
                            KeywordVO vo = new KeywordVO();
                            object02 = scdKeywordArray.getJSONObject(k);
                            vo.setCode_name(object02.has("code_name") ? object02.getString("code_name") : null);
                            vo.setClassL(object02.has("class") ? object02.getString("class") : null);
                            vo.setCode(object02.has("code") ? object02.getString("code") : null);
                            list.add(vo);
                        }
                        healingContentsVO.setHcnt_scd_name(list);
                    }

                    healingContentsVO.setHcnt_inv_hcnt(object.has("hcnt_inv_hcnt") ? object.getString("hcnt_inv_hcnt") : null);
                    healingContentsVO.setHt_id(object.has("ht_id") ? object.getString("ht_id") : null);
                    healingContentsVO.setNickname(object.has("nickname") ? object.getString("nickname") : null);
                    healingContentsVO.setHcnt_coord_x(object.has("hcnt_coord_x") ? object.getString("hcnt_coord_x") : null);
                    healingContentsVO.setHcnt_coord_y(object.has("hcnt_coord_y") ? object.getString("hcnt_coord_y") : null);
                    healingContentsVO.setHcnt_cost_adult(object.has("hcnt_cost_adult") ? object.getString("hcnt_cost_adult") : " - ");
                    healingContentsVO.setHcnt_cost_child(object.has("hcnt_cost_child") ? object.getString("hcnt_cost_child") : " - ");
                    healingContentsVO.setHcnt_cost_jeju(object.has("hcnt_cost_jeju") ? object.getString("hcnt_cost_jeju") : " - ");
                    healingContentsVO.setG_id(object.has("g_id") ? object.getString("g_id") : null);

                    healingContentsVO.setCart_count(object.has("cart_count") ? object.getString("cart_count") : null);
                    healingContentsVO.setComment_count(object.has("comment_count") ? object.getString("comment_count") : null);
                    scheduleVO.setHealingContentsVO(healingContentsVO);
                } else {
                    scheduleVO.setHealingContentsVO(null);
                }

                if (jsonObject.has("ccnt_id")) {
                    JSONObject object = jsonObject.getJSONObject("ccnt_id");
                    SightSeeingContentsVO sightSeeingContentsVO = new SightSeeingContentsVO();

                    cdKeywordArray = object.has("ccnt_cd_name") ? object.getJSONArray("ccnt_cd_name") : null;

                    // 관광장소
                    if (cdKeywordArray != null) {
                        ArrayList<KeywordVO> list = new ArrayList<KeywordVO>();

                        for (int k = 0; k < cdKeywordArray.length(); k++) {
                            JSONObject object01;
                            KeywordVO vo = new KeywordVO();
                            object01 = cdKeywordArray.getJSONObject(k);
                            vo.setCode_name(object01.has("code_name") ? object01.getString("code_name") : null);
                            vo.setClassL(object01.has("class") ? object01.getString("class") : null);
                            vo.setCode(object01.has("code") ? object01.getString("code") : null);
                            list.add(vo);
                        }
                        sightSeeingContentsVO.setCcnt_cd_name(list);
                    }

                    sightSeeingContentsVO.setCcnt_name(object.has("ccnt_name") ? object.getString("ccnt_name") : null);
                    sightSeeingContentsVO.setCcnt_id(object.has("ccnt_id") ? object.getString("ccnt_id") : null);
                    sightSeeingContentsVO.setCcnt_img(object.has("ccnt_img") ? object.getString("ccnt_img") : null);
                    sightSeeingContentsVO.setCcnt_coord_x(object.has("ccnt_coord_x") ? object.getString("ccnt_coord_x") : null);
                    sightSeeingContentsVO.setCcnt_coord_y(object.has("ccnt_coord_y") ? object.getString("ccnt_coord_y") : null);
                    sightSeeingContentsVO.setCart_count(object.has("cart_count") ? object.getString("cart_count") : null);
                    sightSeeingContentsVO.setComment_count(object.has("comment_count") ? object.getString("comment_count") : null);
                    sightSeeingContentsVO.setCcnt_cd(object.has("ccnt_cd") ? object.getString("ccnt_cd") : null);

                    sightSeeingContentsVO.setCcnt_intro(object.has("ccnt_intro") ? object.getString("ccnt_intro") : null);
                    sightSeeingContentsVO.setCcnt_addr(object.has("ccnt_addr") ? object.getString("ccnt_addr") : null);
                    sightSeeingContentsVO.setCcnt_tel(object.has("ccnt_tel") ? object.getString("ccnt_tel") : null);
                    sightSeeingContentsVO.setCcnt_op_time(object.has("ccnt_op_time") ? object.getString("ccnt_op_time") : null);
                    sightSeeingContentsVO.setCcnt_duration(object.has("ccnt_duration") ? object.getString("ccnt_duration") : null);
                    sightSeeingContentsVO.setCcnt_site(object.has("ccnt_site") ? object.getString("ccnt_site") : null);
                    sightSeeingContentsVO.setCcnt_rcmd(object.has("ccnt_rcmd") ? object.getString("ccnt_rcmd") : null);
                    sightSeeingContentsVO.setCcnt_rcmd_comt(object.has("ccnt_rcmd_comt") ? object.getString("ccnt_rcmd_comt") : null);
                    sightSeeingContentsVO.setCcnt_hcd(object.has("ccnt_hcd") ? object.getString("ccnt_hcd") : null);
                    sightSeeingContentsVO.setNickname(object.has("nickname") ? object.getString("nickname") : null);
                    sightSeeingContentsVO.setCcnt_best(object.has("ccnt_best") ? object.getString("ccnt_best") : null);
                    sightSeeingContentsVO.setCcnt_park(object.has("ccnt_park") ? object.getString("ccnt_park") : null);
                    sightSeeingContentsVO.setCcnt_conv(object.has("ccnt_conv") ? object.getString("ccnt_conv") : null);
                    sightSeeingContentsVO.setG_id(object.has("g_id") ? object.getString("g_id") : null);
                    sightSeeingContentsVO.setTag(object.has("tag") ? object.getString("tag") : null);
                    sightSeeingContentsVO.setCcnt_bed(object.has("ccnt_bed") ? object.getString("ccnt_bed") : null);
                    sightSeeingContentsVO.setCcnt_cook(object.has("ccnt_cook") ? object.getString("ccnt_cook") : null);
                    sightSeeingContentsVO.setCcnt_meal(object.has("ccnt_meal") ? object.getString("ccnt_meal") : null);
                    sightSeeingContentsVO.setCcnt_rent(object.has("ccnt_rent") ? object.getString("ccnt_rent") : null);
                    sightSeeingContentsVO.setCcnt_code(object.has("ccnt_code") ? object.getString("ccnt_code") : null);
                    sightSeeingContentsVO.setCcnt_m_imgs(object.has("ccnt_m_img") ? object.getString("ccnt_m_img").split(",") : null);
                    if (object.has("ccnt_m_img")) {
                        String str = object.getString("ccnt_m_img");
                        String[] m_imgs = str.split(",");

                        for (String s :
                                m_imgs) {
                            Log.e("m_imgs", " str : " + s);
                        }
                    }

                    scheduleVO.setSightSeeingContentsVO(sightSeeingContentsVO);
                } else {
                    scheduleVO.setSightSeeingContentsVO(null);
                }
            } catch (JSONException | NullPointerException e) {
                Log.e(TAG, e.toString());
            }
            arrayList.add(scheduleVO);
        }

        return arrayList;
    }

    @Override
    protected void onPostExecute(ArrayList<ScheduleVO> result) {
        progressDialog.dismiss();
        ((JSONArrayResult) context).setJSONArrayResult(result);
        super.onPostExecute(result);
    }
}
