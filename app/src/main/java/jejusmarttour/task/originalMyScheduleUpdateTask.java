/*
package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import jejusmarttour.common.CommonData;
import jejusmarttour.util.AndroidMultiPartEntity;
import jejusmarttour.vo.MyScheduleUpdateRequestVO;

public class MyScheduleUpdateTask extends AsyncTask < MyScheduleUpdateRequestVO , Integer , String >{
	private static final String TAG = MyScheduleUpdateTask.class.toString ( );
	private ProgressDialog progressDialog;
	private long totalSize = 0;
	private Context context;

	public MyScheduleUpdateTask ( Context context ){
		this.context = context;
	}

	@Override
	protected void onPreExecute ( ){
		progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
		progressDialog.setProgressStyle ( android.R.style.Widget_Holo_Light_ProgressBar_Horizontal );
		// setting progress bar to zero
		progressDialog.setProgress ( 0 );
		super.onPreExecute ( );
	}

	@Override
	protected void onProgressUpdate ( Integer ... progress ){
		// updating progress bar value
		progressDialog.setProgress ( progress [ 0 ] );
	}

	@Override
	protected String doInBackground ( MyScheduleUpdateRequestVO ... params ){
		return uploadFile ( params [ 0 ] );
	}

	@SuppressWarnings ( "deprecation" )
	private String uploadFile ( MyScheduleUpdateRequestVO vo ){
		String responseString = null;

		HttpClient httpclient = new DefaultHttpClient ( );
		HttpPost httppost = new HttpPost ( CommonData.getMTCUploadAddress ( ) );

		Log.e(TAG, "http?: " + CommonData.getMTCUploadAddress ( ) );

		try{
			AndroidMultiPartEntity entity = new AndroidMultiPartEntity ( num -> publishProgress ( ( int ) ( ( num / ( float ) totalSize ) * 100 ) ));

			// 이미지 추가
			for ( int i = 0 ; i < vo.getImagePathArrayList ( ).size ( ) ; i++ ){
				File sourceFile = new File ( vo.getImagePathArrayList ( ).get ( i ) );
				if ( vo.getImagePathArrayList ( ).get ( i ).contains ( CommonData.getImageAddress ( ) ) ){
//					Log.e(TAG, "uploadFile: 이건가ㅏㅏㅏㅏㅏㅏㅏ가가가각ㄱㄱㄱㄱㄱ가가ㅏㄱ가가가ㅏㄱ가ㅏ가가가각가가ㅏ가ㅏ");
//					URL url = new URL(vo.getImagePathArrayList ( ).get (i));
//					FileUtils.copyURLToFile ( url , sourceFile );
//					String url2 = vo.getImagePathArrayList ( ).get ( i ).substring ( vo.getImagePathArrayList ( ).get ( i ).lastIndexOf ( "/" ) + 1 );
//					Log.d ( TAG , "filename : " + url + " / url : " + vo.getImagePathArrayList ( ).get ( i ) );
//					entity.addPart ( url2 , new StringBody ( vo.getImagePathArrayList ( ).get ( i ) ) );
				}
				else{
					entity.addPart ( "image" + i , new FileBody ( sourceFile ) );
				}
			}
			// JSONObject 추가
			JSONObject htour = vo.getUpdateJSONObject ( );
			entity.addPart ( "tour" , new StringBody ( htour.toString ( ) , Charset.forName ( "UTF-8" ) ) );

			totalSize = entity.getContentLength ( );
			httppost.setEntity ( entity );

			// Making server call
			HttpResponse response = httpclient.execute ( httppost );
			HttpEntity r_entity = response.getEntity ( );

			int statusCode = response.getStatusLine ( ).getStatusCode ( );

			if ( statusCode == 200 ){
				// Server response
				responseString = EntityUtils.toString ( r_entity );
			}
			else{
				responseString = "Error occurred! Http Status Code: " + statusCode;
			}
		}
		catch ( ClientProtocolException e ){
			Log.e("ClientProtocolException", "MyScheduleUpdateTask uploadFile ");
			responseString = e.toString ( );
		}
		catch ( IOException e ){
			Log.e("IOException", "uploadFile Exception ");
			responseString = e.toString ( );
		}
		return responseString;
	}

	@Override
	protected void onPostExecute ( String result ){
		progressDialog.dismiss ( );
		Log.e ( TAG, "Response from server result: " + result );

		try{
			JSONObject temp = new JSONObject ( result );
			Log.e ( TAG , "Response from server: " + temp );
			if ( temp.getString ( "result" ).equals ( "success" ) )
				( ( JSONObjectResult ) context ).setJSONObjectResult ( "success" );
		}
		catch ( JSONException e ){
			Log.e ( TAG , e.toString () );
		}
		super.onPostExecute ( result );
	}
}*/
