package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_spot.detailinfo.DetailMapFragment;
import jejusmarttour.main_tourproduct.HTPMapFragment;

public class GeometryTask extends AsyncTask < String , Void , JSONArray > {
    private Context context;
    private Fragment fragment;
    private ProgressDialog progressDialog;
    private boolean isHtour = true;
    private boolean isHcnt = true;
    private int schedule = 0;

    public GeometryTask(Context context, Fragment fragment, int schedule) {
        this.context = context;
        this.fragment = fragment;
        this.schedule = schedule;
    }

    public GeometryTask(Context context, Fragment fragment, boolean isHcnt) {
        this.context = context;
        this.fragment = fragment;
        this.isHtour = false;
        this.isHcnt = isHcnt;
    }

    @Override
    protected void onPreExecute ( )
    {
        super.onPreExecute ( );
        progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
    }

    @Override
    protected JSONArray doInBackground(String... params) {

        URL url;
        String id = params [ 0 ];
        String response;
        JSONArray responseJSON = null;

        try {
            String urlStr = CommonData.getGeometry_(id, isHtour, isHcnt, schedule);

            System.out.println(urlStr);

            url = new URL ( urlStr );
            // GET 방식
            URLConnection conn = url.openConnection ( );
            conn.setUseCaches ( false );
            InputStream is = conn.getInputStream ( );

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
            byte [ ] byteBuffer = new byte [ 1024 ];
            byte [ ] byteData = null;
            int nLength = 0;

            while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
            {
                byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
            }

            byteData = byteArrayOutputStream.toByteArray ( );

            if ( byteData.length <= 0 )
            {
                return null;
            }

            response = new String ( byteData );
            responseJSON = new JSONArray ( response );

        } catch(Exception e) {

        } finally {

        }

        return responseJSON;
    }

    @Override
    protected void onPostExecute (JSONArray result)
    {
        progressDialog.dismiss ( );
        try {

            if(isHtour)
                ((HTPMapFragment) this.fragment).setJsonResult(result);
            else
                ((DetailMapFragment) this.fragment).setJsonResult(result);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        super.onPostExecute(result);
    }
}
