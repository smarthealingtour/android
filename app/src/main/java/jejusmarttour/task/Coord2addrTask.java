package jejusmarttour.task;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import jejusmarttour.common.CommonData;
import jejusmarttour.map.MyMapView;
import ktmap.android.map.Coord;

public class Coord2addrTask extends AsyncTask <Coord, Void , JSONObject > {
    private Context context;
    private MyMapView mMyMapView;

    public Coord2addrTask(Context context, MyMapView fragment) {
        this.context = context;
        this.mMyMapView = fragment;
    }

    @Override
    protected JSONObject doInBackground(Coord... params) {

        URL url;
        String x = String.valueOf(params[0].getX());
        String y = String.valueOf(params [0].getY());
        String response;
        JSONObject responseJSON = null;

        try {
            String urlStr = CommonData.getAdress(x, y);

            url = new URL ( urlStr );
            // GET 방식
            URLConnection conn = url.openConnection ( );
            conn.setUseCaches ( false );
            InputStream is = conn.getInputStream ( );

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
            byte [ ] byteBuffer = new byte [ 1024 ];
            byte [ ] byteData = null;
            int nLength = 0;

            while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
            {
                byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
            }

            byteData = byteArrayOutputStream.toByteArray ( );

            if ( byteData.length <= 0 )
            {
                return null;
            }

            response = new String ( byteData );
            responseJSON = new JSONObject ( response );

        } catch(Exception e) {

        } finally {

        }

        return responseJSON;
    }

    @Override
    protected void onPostExecute (JSONObject result)
    {
        if(result != null)
            mMyMapView.setAddrResult(result);
        super.onPostExecute(result);
    }
}
