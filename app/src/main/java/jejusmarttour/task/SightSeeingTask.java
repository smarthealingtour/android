package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.KeywordVO;
import jejusmarttour.vo.SearchProductRequestVO;
import jejusmarttour.vo.SightSeeingContentsVO;

public class SightSeeingTask extends AsyncTask <SearchProductRequestVO, Integer , ArrayList <SightSeeingContentsVO> >
{
	private static final String TAG = SightSeeingTask.class.toString ( );

	ProgressDialog progressDialog;
	private Context context;
	private Fragment fragment;

	public SightSeeingTask(Context context, Fragment fragment){
		this.context = context;
		this.fragment = fragment;
	}

	@Override
	protected void onPreExecute ( ){
		progressDialog = ProgressDialog.show( context , "" , "Loading..." , true , false );
		super.onPreExecute ( );
	}

	@Override
	protected ArrayList <SightSeeingContentsVO> doInBackground ( SearchProductRequestVO ... params ){
		URL url;
		SearchProductRequestVO vo = params [ 0 ];
		String response;
		JSONArray responseJSON;
		ArrayList < SightSeeingContentsVO > arrayList = null;

		try{
			String urlStr = CommonData.searchSpotAddress(vo);

			url = new URL ( urlStr );

			Log.i("tag", "url param" + urlStr);
			// GET 방식
			URLConnection conn = url.openConnection ( );
			conn.setUseCaches ( false );
			InputStream is = conn.getInputStream ( );

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
			byte [ ] byteBuffer = new byte [ 1024 ];
			byte [ ] byteData = null;
			int nLength = 0;

			while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 ){
				byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
			}

			byteData = byteArrayOutputStream.toByteArray ( );
			
			if( byteData.length <= 0 ){
				return null;
			}

			response = new String ( byteData );

			responseJSON = new JSONArray ( response );

			arrayList = getResultList ( responseJSON );

			byteArrayOutputStream.close ( );
			is.close ( );
			conn = null;

		}
		catch ( MalformedURLException e ){
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( IOException e ){
			Log.e ( TAG , e.toString ( ) );
		}
		catch ( JSONException e ){
			Log.e ( TAG , e.toString ( ) );
		}

		if ( arrayList.size ( ) <= 0 || arrayList == null ){
			Log.e ( TAG , "데이터가 존재하지 않습니다." );
			return null;
		}

		return arrayList;
	}

	// JSON 파싱
	private ArrayList < SightSeeingContentsVO > getResultList ( JSONArray jsonArray )
	{
		if ( jsonArray == null )
			return null;

		ArrayList < SightSeeingContentsVO > arrayList = new ArrayList < SightSeeingContentsVO > ( );

		for ( int i = 0 ; i < jsonArray.length ( ) ; i++ )
		{
			JSONArray cdKeywordArray = new JSONArray ( );
			JSONObject object;
			SightSeeingContentsVO sightSeeingContentsVO = new SightSeeingContentsVO ( );

			try
			{
				object = jsonArray.getJSONObject ( i );

				cdKeywordArray = object.has ( "ccnt_cd_name" ) ? object.getJSONArray ( "ccnt_cd_name" ) : null;

				// 관광장소
				if ( cdKeywordArray != null )
				{
					ArrayList < KeywordVO > list = new ArrayList < KeywordVO > ( );

					for ( int k = 0; k < cdKeywordArray.length ( ); k++ )
					{
						JSONObject object01 = new JSONObject ( );
						KeywordVO vo = new KeywordVO ( );
						object01 = cdKeywordArray.getJSONObject ( k );
						vo.setCode_name ( object01.has ( "code_name" ) ? object01.getString ( "code_name" ) : null );
						vo.setClassL ( object01.has ( "class" ) ? object01.getString ( "class" ) : null );
						vo.setCode ( object01.has ( "code" ) ? object01.getString ( "code" ) : null );
						list.add ( vo );
					}
					sightSeeingContentsVO.setCcnt_cd_name ( list );
				}

				sightSeeingContentsVO.setCcnt_name(object.has("ccnt_name") ? object.getString("ccnt_name") : null);
				sightSeeingContentsVO.setCcnt_id(object.has("ccnt_id") ? object.getString("ccnt_id") : null);
				sightSeeingContentsVO.setCcnt_cd(object.has("ccnt_cd") ? object.getString("ccnt_cd") : null);
				sightSeeingContentsVO.setCcnt_coord_x(object.has("ccnt_coord_x") ? object.getString("ccnt_coord_x") : null);
				sightSeeingContentsVO.setCcnt_coord_y(object.has("ccnt_coord_y") ? object.getString("ccnt_coord_y") : null);

				sightSeeingContentsVO.setCart_count ( object.has("cart_count") ? object.getString("cart_count") : null);
				sightSeeingContentsVO.setComment_count ( object.has("comment_count") ? object.getString("comment_count") : null);
			}
			catch ( JSONException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}
			catch ( NullPointerException e )
			{
				Log.e ( TAG , e.toString ( ) );
			}

			arrayList.add ( sightSeeingContentsVO );

			// System.out.print ( "reviewVO comment_subt : " +
			// reviewVO.getComment_subt ( ) + "\n" );
		}

		return arrayList;
	}

	@Override
	protected void onPostExecute ( ArrayList < SightSeeingContentsVO > result )
	{
		progressDialog.dismiss ( );
		( ( JSONArrayResult ) fragment ).setJSONArrayResult ( result );
		super.onPostExecute ( result );
	}

}
