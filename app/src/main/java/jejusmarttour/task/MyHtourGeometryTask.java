package jejusmarttour.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_mytourproduct.ScheduleMapFragment;

public class MyHtourGeometryTask extends AsyncTask < String , Void , JSONArray > {
    private Context context;
    private Fragment fragment;
    private ProgressDialog progressDialog;

    public MyHtourGeometryTask(Context context, Fragment fragment) {
        this.context = context;
        this.fragment = fragment;
    }


    @Override
    protected void onPreExecute ( )
    {
        super.onPreExecute ( );
        progressDialog = ProgressDialog.show ( context , "" , "Loading..." , true , false );
    }

    @Override
    protected JSONArray doInBackground(String... params) {

        URL url;
        String ids = params [ 0 ];
        String response;
        JSONArray responseJSON = null;

        try {
            String urlStr = CommonData.getMyHtourGeomtry(ids);

            System.out.println(urlStr);

            url = new URL ( urlStr );
            // GET 방식
            URLConnection conn = url.openConnection ( );
            conn.setUseCaches ( false );
            InputStream is = conn.getInputStream ( );

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
            byte [ ] byteBuffer = new byte [ 1024 ];
            byte [ ] byteData = null;
            int nLength = 0;

            while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
            {
                byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
            }

            byteData = byteArrayOutputStream.toByteArray ( );

            if ( byteData.length <= 0 )
            {
                return null;
            }

            response = new String ( byteData );
            responseJSON = new JSONArray ( response );

        } catch(Exception e) {

        } finally {

        }

        return responseJSON;
    }

    @Override
    protected void onPostExecute (JSONArray result)
    {
        progressDialog.dismiss ( );
        try {
            ((ScheduleMapFragment) this.fragment).setJsonResult(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        super.onPostExecute(result);
    }
}
