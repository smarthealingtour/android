package jejusmarttour.task;

import java.util.ArrayList;

public interface JSONArrayResult<E>
{
	public void setJSONArrayResult(ArrayList<E> resultList);
}
