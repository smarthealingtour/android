package jejusmarttour.practices;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class DurationOfStayListAdapter extends BaseAdapter {

    private Activity activity;
    private List<DurationOfStay> datas;

    DurationOfStayListAdapter(Activity activity) {
        this.activity = activity;
        datas = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) datas.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemView itemView;

        if (convertView == null) {
            itemView = new ItemView(activity.getApplicationContext());
        } else {
            itemView = (ItemView) convertView;
        }

        itemView.setData(datas.get(position));

        return itemView;
    }

    public void updateItem(DurationOfStay durationOfStay) {
        int location = datas.indexOf(durationOfStay);

        if (location < 0) {
            datas.add(durationOfStay);
        } else {
            datas.set(location, durationOfStay);
        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

}
