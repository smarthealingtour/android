package jejusmarttour.practices;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import jejusmarttour.supports.DateUtils;
import syl.com.jejusmarttour.R;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class ItemView extends LinearLayout {

    private TextView spotNameView;
    private TextView enteredAtView;
    private TextView exitedAtView;
    private TextView duration;

    public ItemView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_duration_of_stay, this, true);

        spotNameView = (TextView) findViewById(R.id.spotName);
        enteredAtView = (TextView) findViewById(R.id.enteredAt);
        exitedAtView = (TextView) findViewById(R.id.exitedAt);
        duration = (TextView) findViewById(R.id.duration);
    }

    public void setData(DurationOfStay data) {
        spotNameView.setText(data.spotName());
        enteredAtView.setText(DateUtils.defaultFormat(data.enteredAt()));
        exitedAtView.setText(DateUtils.defaultFormat(data.exitedAt()));
        duration.setText(String.valueOf(data.duration()/60) + "분");
    }

}
