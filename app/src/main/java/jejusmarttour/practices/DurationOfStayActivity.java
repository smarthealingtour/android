package jejusmarttour.practices;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.tamra.Tamra;
import com.kakao.oreum.tamra.base.Config;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.base.TamraObserver;
import com.kakao.oreum.tamra.base.TamraObserverAdapter;
import com.kakao.oreum.tamra.error.TamraError;

import java.util.Date;

import jejusmarttour.annotation.SampleInfo;
import jejusmarttour.util.Constants;
import syl.com.jejusmarttour.R;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@SampleInfo(
        index = 1,
        title = "특정지점 체류시간 측정",
        description = "")
public class DurationOfStayActivity extends AppCompatActivity {

    private DurationOfStayListAdapter adapter;

    private Spot currentSpot;
    private Date enteredAt;

    private TamraObserver tamraObserver = new TamraObserverAdapter() {
        @Override
        public void ranged(final NearbySpots nearbySpots) {
            nearbySpots
                    .orderBy(Spot.ACCURACY_ORDER)
                    .first()
                    .ifPresent(new Consumer<Spot>() {
                        @Override
                        public void accept(Spot spot) {
                            if (currentSpot != null && spot.id() == currentSpot.id()) {
                                return;
                            }

                            Date now = new Date();

                            if (currentSpot != null) {
                                DurationOfStay current = new DurationOfStay(currentSpot.description(), enteredAt, now);
                                adapter.updateItem(current);
                            }

                            DurationOfStay durationOfStay = new DurationOfStay(spot.description(), now);
                            adapter.updateItem(durationOfStay);

                            currentSpot = spot;
                            enteredAt = now;
                        }
                    });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duration_of_stay);

        initView();
        initTamra();

        Tamra.addObserver(tamraObserver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Tamra.removeObserver(tamraObserver);
    }

    private void initView() {
        ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new DurationOfStayListAdapter(this);
        assert (listView != null);
        listView.setAdapter(adapter);
    }

    private void initTamra() {
        String appKey = "c91108b168194c47b636387da7c7ab56";
        Config config = Config.forTesting(
                getApplicationContext(),
                Constants.appKey)
                .onSimulation("My Device");
        Tamra.init(config);

        Tamra.startMonitoring(Regions.카카오스페이스닷투);

        for (TamraError error : Tamra.errors()) {
            error.exception().printStackTrace();
        }
    }

}