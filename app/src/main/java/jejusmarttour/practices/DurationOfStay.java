package jejusmarttour.practices;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class DurationOfStay {

    private String spotName;
    private Date enteredAt;
    private Date exitedAt;

    public DurationOfStay(String spotName, Date enteredAt) {
        this.spotName = spotName;
        this.enteredAt = enteredAt;
    }

    public DurationOfStay(String spotName, Date enteredAt, Date exitedAt) {
        this(spotName, enteredAt);
        this.exitedAt = exitedAt;
    }

    public String spotName() {
        return spotName;
    }

    public Date enteredAt() {
        return enteredAt;
    }

    public Date exitedAt() {
        return exitedAt;
    }

    public Long duration() {
        if (exitedAt == null) {
            return -1L;
        }

        return (exitedAt.getTime() - enteredAt.getTime())/1000;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DurationOfStay that = (DurationOfStay) o;

        if (!spotName.equals(that.spotName)) return false;
        return enteredAt.equals(that.enteredAt);

    }

    @Override
    public int hashCode() {
        int result = spotName.hashCode();
        result = 31 * result + enteredAt.hashCode();
        return result;
    }

}