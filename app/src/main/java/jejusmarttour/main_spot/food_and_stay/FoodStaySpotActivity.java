package jejusmarttour.main_spot.food_and_stay;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import jejusmarttour.FontAppCompatActivity;
import jejusmarttour.common.CommonData;
import jejusmarttour.search.SearchProductActivity;
import jejusmarttour.main_spot.detailinfo.FoodStayDetailInfoActivity;
import jejusmarttour.util.MyPagerSlidingTabStrip;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

public class FoodStaySpotActivity extends FontAppCompatActivity implements  SearchView.OnQueryTextListener{
	private final int MAX_PAGE = 2;
	private Fragment cur_fragment;
	private FoodFragment foodFragment;
    private StayFragment stayFragment;

	private ViewPager viewPager;
	private MyPagerSlidingTabStrip tab;

	private ActionBar actionBar;

	private SearchView searchView;
	private MenuItem mSearchItem;
	private MenuItem search_result_delete = null;

	private View restaurantTab;
	private View stayTab;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_food_stay);

		initFragment();
		initCustomTab();

		viewPager = ( ViewPager ) findViewById ( R.id.viewpager );
		viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
		viewPager.setCurrentItem(0);
		tab = ( MyPagerSlidingTabStrip ) findViewById ( R.id.htp_tabs );
		tab.setShouldExpand(true);
		setTabStyle();
		tab.setTabChangeListener(position -> {
            if (position == 0)	clickRestaurant();
            else 			  	clickStay();
        });
		tab.setViewPager(viewPager);
	}

	private void initFragment(){
		foodFragment = new FoodFragment();
        stayFragment = new StayFragment();
	}
	//요거 바꾸면 위의 소개, 지도탭 바뀜
	public void initCustomTab(){
		restaurantTab = getLayoutInflater().inflate(R.layout.view_tab,null);
		restaurantTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_restaurant_clicked));
		TextView tv1 = restaurantTab.findViewById(R.id.text);
		tv1.setText("음식점");
		tv1.setTextColor( getResources().getColor(R.color.statusbar) );

		stayTab = getLayoutInflater().inflate(R.layout.view_tab,null);
		stayTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_stay));
		TextView tv2 = stayTab.findViewById(R.id.text);
		tv2.setText("숙박장소");
		tv2.setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );
	}

	// TAB 스타일 바꾸기
	@SuppressWarnings ( "deprecation" )
	private void setTabStyle(){
		tab.setDividerColor(getResources().getColor(R.color.white));
		tab.setBackgroundColor ( getResources ( ).getColor ( R.color.white ) );
		tab.setIndicatorColor(getResources().getColor(R.color.statusbar));
		tab.setTextColor(getResources().getColor(R.color.white));
//		tab.setTabPaddingLeftRight(tab.getTabPaddingLeftRight());
	}

	public void clickRestaurant(){
		restaurantTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_restaurant_clicked));
		((TextView) restaurantTab.findViewById(R.id.text)).setTextColor( getResources().getColor(R.color.statusbar) );

		stayTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_stay));
		((TextView)stayTab.findViewById(R.id.text)).setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );
	}
	public void clickStay(){
		restaurantTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_restaurant));
		((TextView) restaurantTab.findViewById(R.id.text)).setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );

		stayTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_stay_clicked));
		((TextView)stayTab.findViewById(R.id.text)).setTextColor( getResources().getColor(R.color.statusbar) );
	}

	public void onProItemClick( ScheduleVO vo){
		CommonData.setScheduleVO(vo);
		Intent intent = new Intent ( this , FoodStayDetailInfoActivity.class );
		startActivity(intent);
	}

	@Override
	protected void onDestroy (){
		super.onDestroy();
	}

	// 액션바 메뉴
	@Override
	public boolean onCreateOptionsMenu ( Menu menu ){
		actionBar = getSupportActionBar ( );
		actionBar.setElevation(0);					// 액션바 그림자 지우기

		LayoutInflater mInflater = LayoutInflater.from ( this );
		View customView = mInflater.inflate(R.layout.actionbar_listview, null);

		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( R.string.food_stay_title);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.search, menu);

		LinearLayout backBtn = customView.findViewById ( R.id.back_btn );

		backBtn.setOnClickListener (view -> finish ( ));

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled(true);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

		searchView.setOnQueryTextListener(this);
		mSearchItem = menu.getItem(0);

		search_result_delete = menu.findItem(R.id.search_result_delete);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item ){
		int id = item.getItemId ( );

		if ( id == R.id.action_settings ){
			CommonData.gotoMyInfo ( this );
			return true;
		}
		else if (id  == android.R.id.home ) {
			finish();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		mSearchItem.collapseActionView();
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setTitle(query);
		searchProduct (query );

		return false;
	}

	//키워드 검색 수행
	private void searchProduct(String query) {
		Intent intent = new Intent( this , SearchProductActivity.class );
		intent.putExtra("data", query);
		startActivity(intent);
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}

	// 뷰페이지 어뎁터 탭 선택
	class ViewPagerAdapter extends FragmentPagerAdapter implements MyPagerSlidingTabStrip.CustomTabProvider {
		private int position = 0;

		public ViewPagerAdapter ( FragmentManager fm )
		{
			super ( fm );
		}

		@Override
		public Fragment getItem ( int position ){
			if ( position < 0 || MAX_PAGE <= position )
				return null;

			this.position = position;

			switch ( position ){
			case 0 :
				cur_fragment = foodFragment;
				break;
			case 1 :
				cur_fragment = stayFragment;
				break;
			}
			return cur_fragment;
		}

		public int getCurrentPosition ( )
		{
			return position;
		}

		@Override
		public int getCount ( )
		{
			return MAX_PAGE;
		}

		@Override
		public CharSequence getPageTitle ( int position ){
			if ( position == 0 ){
				return "음식점";
			}
			else{
				return "숙박장소";
			}
		}

		@Override
		public View getCustomTab(int position) {
			if ( position == 0 ){
				return restaurantTab;
			}
			else {
				return stayTab;
			}
		}
	}
}