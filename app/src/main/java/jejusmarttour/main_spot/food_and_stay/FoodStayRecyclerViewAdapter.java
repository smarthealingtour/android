package jejusmarttour.main_spot.food_and_stay;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.util.CommonUtil;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SightSeeingContentsVO;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2018-02-22.
 */

class FoodStayRecyclerViewAdapter extends RecyclerView.Adapter<FoodStayRecyclerViewAdapter.MyViewHolder> {
    private ArrayList<ScheduleVO> provider;
    private Context context;

    FoodStayRecyclerViewAdapter(Context context, ArrayList<ScheduleVO> provider){
        this.context = context;
        this.provider = provider;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext());

        View view = inflater.inflate(R.layout.item_staggered_list, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (provider.get(position).getSightSeeingContentsVO() != null){
            SightSeeingContentsVO vo = provider.get(position).getSightSeeingContentsVO();

            if (vo.getCcnt_imgArray() != null){
                Glide.with ( context ).load ( CommonData.getImageAddress () + vo.getCcnt_imgArray()[0] ).into ( holder.imageView );
            }
            else {
                Glide.with ( context ).load ( CommonData.getImageAddress () + vo.getCcnt_img() ).into ( holder.imageView );
            }

            holder.titleText.setText( vo.getCcnt_name());
            holder.introText.setText( vo.getCcnt_addr());

            if( vo.getCcnt_duration() == null || vo.getCcnt_duration().equals("")) {
                holder.timeText.setText("정보없음");
            }else{
                try{
                    int min = Integer.parseInt( vo.getCcnt_duration());
                    holder.timeText.setText( CommonUtil.changeHourMin(min) );
                }
                catch (NumberFormatException e){
                    holder.timeText.setText( vo.getCcnt_duration() );
                }

            }
        }

        holder.view.setOnClickListener(v ->
                ( (FoodStaySpotActivity) context ).onProItemClick ( provider.get ( position ))
        );
    }


    @Override
    public int getItemCount() {
        return provider.size();
    }

    void addData(ArrayList<ScheduleVO> scheduleVOS){
        provider.addAll(scheduleVOS);

        notifyDataSetChanged();
    }

    void removeAllData(){
        provider.clear();

        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView imageView;
        TextView titleText;
        TextView introText;
        TextView timeText;

        MyViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            imageView = itemView.findViewById( R.id.place_img );
            titleText = itemView.findViewById( R.id.title );
            introText = itemView.findViewById( R.id.intro );
            timeText = itemView.findViewById( R.id.average_time_text);
        }
    }
}
