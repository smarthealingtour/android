package jejusmarttour.main_spot.detailinfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.TextUtils.TruncateAt;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.common.KeywordData;
import jejusmarttour.main_mytourproduct.osy.interest_activity.InterestActivity;
import jejusmarttour.main_tourproduct.ImageViewPagerAdapter;
import jejusmarttour.task.GetContentsTask;
import jejusmarttour.task.HTPCartTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.TourProductSequenceVO;
import jejusmarttour.vo.UpdateCartRequestVO;
import jejusmarttour.vo.UpdateResultVO;
import syl.com.jejusmarttour.R;

public class FoodStayDetailInfoFragment extends Fragment implements OnClickListener , JSONArrayResult < ScheduleVO > , JSONObjectResult < UpdateResultVO >{
    private static final String TAG = FoodStayDetailInfoFragment.class.toString ( );
    private static final int [ ] ATTRS = new int [ ] { android.R.attr.textSize , android.R.attr.textColor };

    private ArrayList < ScheduleVO > scheduleArrayList;

    private HTPCartTask htpCartTask;
    private ImageButton cartBtn;

    private int MAX_PAGE = 0;
    private int prevPosition; // 이전에 선택되었던 포지션 값
    private int type;         // 음식인지 숙박인지

    private LinearLayout pageMark; // 현재 몇 페이지 인지 나타내는 뷰

    private GetContentsTask getContentsTask;

    private ScheduleVO scheduleVO;

    private ViewGroup keywordDetailContainer;

    private Context context = null;

    public static FoodStayDetailInfoFragment newInstance (ScheduleVO scheduleVO , int type ){
        FoodStayDetailInfoFragment fragment = new FoodStayDetailInfoFragment( );
        fragment.scheduleVO = scheduleVO;
        fragment.type = type;
        return fragment;
    }
    @Override
    public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
        View view = null;
        this.context = getActivity ( );

        try{
            view = inflater.inflate ( R.layout.fragment_detailinfo_food_stay, container , false );
        }
        catch ( InflateException e ){
            e.printStackTrace ( );
        }

        ( (FoodStayDetailInfoActivity) getActivity ( ) ).swipeOnOff ( false );

        initFragment();
        initViewPager(view);

        //담기 버튼
        cartBtn = ( ImageButton ) view.findViewById ( R.id.cart_btn );
        cartBtn.setOnClickListener ( this );

        getContentsTask = new GetContentsTask ( context , this );

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView titleText = view.findViewById(R.id.htp_title_text);
        titleText.setText ( scheduleVO.getSightSeeingContentsVO().getCcnt_name () );

        ImageButton callBtn = view.findViewById(R.id.call_btn);
        callBtn.setOnClickListener(view1 -> {
            if (Build.VERSION.SDK_INT >= 23) {
                //전화걸기 퍼미션 확인 후 허락되있는경우 전화걸기실행
                if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData( Uri.parse( "tel:"+scheduleVO.getSightSeeingContentsVO().getCcnt_tel()));

                    startActivity(intent);
                    return;
                }
                //전화걸기 퍼미션 확인 후 거절되있는 퍼미션얻기위한 다이얼로그 띄움
                else{
                    Log.v(TAG,"Permission 거절되있음");
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                }
                //퍼미션 다시 확인후 허락되있으면 전화걸기 실행
                if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData( Uri.parse( "tel:"+scheduleVO.getSightSeeingContentsVO().getCcnt_tel()));

                    startActivity(intent);
                }
            }
            /*else{
                Toast.makeText(getActivity(), "설정창에서 전화기능 권한을 설정해주세요!", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "설정창에서 전화기능 권한을 설정해주세요!");
            }*/
        });
    }

    private void initFragment(){
        Fragment fragment;
        if (type == 1){
            fragment = FoodDetailInfoFragment.newInstance(scheduleVO);
        }
        else {
            fragment = StayDetailInfoFragment.newInstance(scheduleVO);
        }
        FragmentManager manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(R.id.food_stay_fragment, fragment);
        fragmentTransaction.commit();
    }

    private void initViewPager(View view){
        // 이미지 뷰 페이저
        ImageView noImageView = view.findViewById(R.id.viewpager_imageview);

        pageMark = view.findViewById ( R.id.page_mark );
        ViewPager pager = view.findViewById(R.id.viewpager);

        keywordDetailContainer = view.findViewById ( R.id.keyword_detail_layout );

        if ( scheduleVO.getSightSeeingContentsVO( ) != null ){
            if ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_img ( ) != null ) {
                pageMark.setVisibility ( View.VISIBLE );
                pager.setVisibility ( View.VISIBLE );
                noImageView.setVisibility ( View.GONE );
                MAX_PAGE = scheduleVO.getSightSeeingContentsVO( ).getCcnt_imgArray ( ).length;
                pager.setAdapter ( new ImageViewPagerAdapter( context , scheduleVO.getSightSeeingContentsVO( ).getCcnt_imgArray ( ) , MAX_PAGE ) );
            }
            else {
                pageMark.setVisibility ( View.GONE );
                pager.setVisibility ( View.GONE );
                noImageView.setVisibility ( View.VISIBLE );
                pager.setAdapter ( new ImageViewPagerAdapter ( context , null , MAX_PAGE ) );
            }
        }

        pager.setOffscreenPageLimit ( MAX_PAGE - 1 );
        pager.addOnPageChangeListener (new ViewPager.OnPageChangeListener( ) {
            @Override
            public void onPageSelected ( int position ) {
                pageMark.getChildAt ( prevPosition ).setBackgroundResource ( R.drawable.page_not ); // 이전
                pageMark.getChildAt ( position ).setBackgroundResource ( R.drawable.page_select ); // 현재
                prevPosition = position; // 이전 포지션 값을 현재로 변경
            }

            @Override
            public void onPageScrolled ( int position , float positionOffest , int positionOffsetPixels ) {
            }

            @Override
            public void onPageScrollStateChanged ( int state ) {
            }
        } );

        initPageMark ( ); // 현재 페이지 표시하는 뷰 초기화
    }

    // 이미지 뷰페이저 하단의 현재 페이지 표시하는 뷰 초기화
    private void initPageMark ( ) {
        for ( int i = 0; i < MAX_PAGE; i++ ){
            ImageView iv = new ImageView ( context ); // 페이지
            iv.setLayoutParams ( new LayoutParams ( LayoutParams.WRAP_CONTENT , LayoutParams.WRAP_CONTENT ) );

            // 첫 페이지 표시 이미지 이면 선택된 이미지로
            if ( i == 0 )
                iv.setBackgroundResource ( R.drawable.page_select );
            else // 나머지는 선택안된 이미지로
                iv.setBackgroundResource ( R.drawable.page_not );

            // LinearLayout에 추가
            pageMark.addView ( iv );
        }
        prevPosition = 0; // 이전 포지션 값 초기화
    }

    private void initSequenceLayout ( ) {
        ArrayList <TourProductSequenceVO> sequenceArr = getSequenceData ( );
        getSequenceLayout ( sequenceArr );
    }



    // 상품 구성 및 여행순서 아이템 만들기
    @SuppressLint ( "Recycle" )
    private void getSequenceLayout ( ArrayList <TourProductSequenceVO> sequenceArr ){
        TourProductSequenceVO vo;

        DisplayMetrics dm = getResources ( ).getDisplayMetrics ( );

        int containerHeight = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 34 , dm );
        int iconWidth = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 15 , dm );

        TypedArray a = this.context.obtainStyledAttributes ( null , ATTRS );

        int textSize = 15;
        int textColor = getResources ( ).getColor ( R.color.font_color );

        textSize = a.getDimensionPixelSize ( 0 , textSize );
        textColor = a.getColor ( 1 , textColor );

        for ( int i = 0; i < sequenceArr.size ( ); i++ )
        {
            vo = sequenceArr.get ( i );
            LinearLayout container = new LinearLayout ( this.context );
            container.setOrientation ( LinearLayout.VERTICAL );
            container.setLayoutParams ( new LinearLayout.LayoutParams ( LinearLayout.LayoutParams.MATCH_PARENT , containerHeight ) );

            LinearLayout linearLayout = new LinearLayout ( this.context );
            linearLayout.setOrientation ( LinearLayout.HORIZONTAL );
            linearLayout.setLayoutParams ( new LayoutParams ( LayoutParams.MATCH_PARENT , LayoutParams.MATCH_PARENT ) );
            linearLayout.setGravity ( Gravity.CENTER_VERTICAL );

            TextView numText = new TextView ( context );
            numText.setLayoutParams ( new LayoutParams ( LayoutParams.WRAP_CONTENT , LayoutParams.MATCH_PARENT ) );
            numText.setGravity ( Gravity.CENTER_VERTICAL );
            TextView titleText = new TextView ( context );
            titleText.setLayoutParams ( new LinearLayout.LayoutParams ( 0 , LayoutParams.MATCH_PARENT , 1 ) );
            titleText.setSingleLine ( );
            titleText.setEllipsize ( TruncateAt.END );
            titleText.setGravity ( Gravity.CENTER_VERTICAL );
            TextView categoryText = new TextView ( context );
            categoryText.setLayoutParams ( new LayoutParams ( LayoutParams.WRAP_CONTENT , LayoutParams.MATCH_PARENT ) );
            categoryText.setGravity ( Gravity.CENTER_VERTICAL );

            ImageView iconImage = new ImageView ( context );
            iconImage.setLayoutParams ( new LayoutParams ( iconWidth , iconWidth ) );
            iconImage.setScaleType ( ScaleType.FIT_XY );

            Glide.with ( context ).load ( vo.getIconId ( ) ).thumbnail ( 0.1f ).into ( iconImage );

            numText.setTextSize ( textSize );
            titleText.setTextSize ( textSize );
            categoryText.setTextSize ( textSize );

            numText.setTextColor ( textColor );
            titleText.setTextColor ( textColor );
            categoryText.setTextColor ( textColor );

            numText.setText ( vo.getSeq( ) );
            // titleText.setText(vo.getName());
            SpannableString content = new SpannableString ( vo.getTitle ( ) );
            content.setSpan ( new UnderlineSpan ( ) , 0 , content.length ( ) , 0 );
            titleText.setText ( content );
            categoryText.setText ( vo.getCategory ( ) );

            linearLayout.addView ( numText );
            linearLayout.addView ( iconImage );
            linearLayout.addView ( titleText );
            linearLayout.addView ( categoryText );
            container.addView ( linearLayout );

            container.setTag ( i );

            container.setOnClickListener (v -> {
                CommonData.setScheduleVO ( scheduleArrayList.get ( Integer.valueOf ( v.getTag ( ).toString ( ) ) ) );
                Intent intent = new Intent ( getActivity ( ) , DetailInfoActivity.class );
                startActivity ( intent );
                // Toast.makeText ( this.context , v.getTag ( ).toString ( )
                // , Toast.LENGTH_SHORT ).show ( );
            });

//            sequenceLayout.addView ( container );
        }
    }

    // 여행상품목록 데이터 가져오기
    private ArrayList <TourProductSequenceVO> getSequenceData (){
        ArrayList <TourProductSequenceVO> arItem = new ArrayList <TourProductSequenceVO> ( );

        for ( int i = 0; i < scheduleArrayList.size ( ); i++ ){
            TourProductSequenceVO vo = new TourProductSequenceVO( );
            vo.setSeq( Integer.parseInt(scheduleArrayList.get(i).getSeq()) );
            vo.setCategory ( KeywordData.getKeyword(scheduleArrayList.get(i)) );

            if ( scheduleArrayList.get ( i ).getHealingContentsVO( ) != null ){
                vo.setTitle ( scheduleArrayList.get ( i ).getHealingContentsVO( ).getHcnt_name ( ) );
                vo.setIconId ( CommonData.getLocationIconId ( scheduleArrayList.get ( i ).getHealingContentsVO( ).getHcnt_type ( ) ) );
            }
            else if ( scheduleArrayList.get ( i ).getSightSeeingContentsVO( ) != null ){
                vo.setTitle ( scheduleArrayList.get ( i ).getSightSeeingContentsVO( ).getCcnt_name ( ) );
                vo.setIconId ( CommonData.getLocationIconId ( "point" ) );
            }

            arItem.add ( vo );
        }

        return arItem;
    }

    @Override
    public void onClick ( View v ){
        if ( v.getId ( ) == cartBtn.getId ( ) ){
//			addCartPopUp ( );
            htpCartTask = new HTPCartTask ( this.context , this );

            UpdateCartRequestVO requestVO = new UpdateCartRequestVO ( );
            requestVO.setType ( "add" );
            requestVO.setEmail ( CommonData.getUserVO ( ).getEmail ( ) );
            requestVO.setContent ( true );

            if ( scheduleVO.getHealingContentsVO( ) != null ){
                requestVO.setProductID ( scheduleVO.getHealingContentsVO( ).getHcnt_id ( ) );
                requestVO.setIsCcnt ( false );
            }
            else if ( scheduleVO.getSightSeeingContentsVO( ) != null ){
                requestVO.setProductID ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_id ( ) );
                requestVO.setIsCcnt ( true );
            }
            requestVO.setContent ( true );

            htpCartTask.execute ( requestVO );
        }
    }



    @Override
    public void setJSONArrayResult ( ArrayList < ScheduleVO > resultList ){
        getContentsTask.cancel ( true );

        if ( resultList == null ){
            Log.d ( TAG , " ArrayList < ScheduleVO > 값이 존재하지 않습니다." );

            return;
        }
        this.scheduleArrayList = resultList;

        initSequenceLayout ( );
    }

    // 장바구니 담기 팝업
    private void addCartPopUp ( ){
        LinearLayout layout = ( LinearLayout ) View.inflate ( this.context , R.layout.popup_cart , null );
        TextView title_text = layout.findViewById ( R.id.title_text );

        if ( scheduleVO.getHealingContentsVO( ) != null )
            title_text.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_name ( ) + " 을(를)" );
        else if ( scheduleVO.getSightSeeingContentsVO( ) != null )
            title_text.setText ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( ) + " 을(를)" );

        AlertDialog.Builder alertBox = new AlertDialog.Builder ( getActivity ( ) );
        alertBox.setView ( layout );
        alertBox.create ( );
        final DialogInterface popup = alertBox.show ( );

        Button cartBtn = layout.findViewById ( R.id.goto_cart_btn );

        cartBtn.setOnClickListener (v -> {
            Intent intent = new Intent ( getActivity ( ) , InterestActivity.class );
            startActivity ( intent );
            getActivity ( ).finish ( );
        });

        Button cancelBtn = layout.findViewById ( R.id.cancel_btn );
        cancelBtn.setOnClickListener (v -> popup.dismiss ( ));

    }

    // keywordDetailContainer 에 담길 키워드
    private void addDetailKeyword ( ) {
        ArrayList < View > arrayList = new ArrayList < View > ( );

        // Display display = ( ( WindowManager ) this.context.getSystemService
        // ( Context.WINDOW_SERVICE ) ).getDefaultDisplay ( );
        keywordDetailContainer.removeAllViews ( );

        for ( int i = 0; i < arrayList.size ( ); i++ )
        {
            keywordDetailContainer.addView ( arrayList.get ( i ) );
        }

    }

    private boolean isContainCategory ( int position , String [ ] strings ){
        if ( strings == null )
            return true;

        if ( position == 0 )
            return false;

        if ( position >= 1 ){
            if ( SmartTourUtils.replaceSpaceToBlank ( strings [ position - 1 ] ).contains ( SmartTourUtils.replaceSpaceToBlank ( strings [ position ] ).substring ( 0 , 3 ) ) == false )
                return false;
        }

        return true;
    }

    private ImageView getImageView ( ){
        DisplayMetrics dm = this.context.getResources ( ).getDisplayMetrics ( );
        int size = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 30 , dm );

        ImageView imageView = new ImageView ( this.context );
        imageView.setLayoutParams ( new LayoutParams ( size , size ) );
        imageView.setScaleType ( ScaleType.FIT_XY );

        return imageView;
    }

    private LinearLayout getLinearLayout ( ){
        DisplayMetrics dm = this.context.getResources ( ).getDisplayMetrics ( );
        int size = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 30 , dm );

        LinearLayout layout = new LinearLayout ( this.context );
        layout.setLayoutParams ( new LayoutParams ( LinearLayout.LayoutParams.WRAP_CONTENT , size ) );
        layout.setGravity ( Gravity.CENTER_VERTICAL );

        return layout;
    }

    @Override
    public void setJSONObjectResult ( UpdateResultVO result ){
        htpCartTask.cancel ( true );

        if ( result == null ){
            Log.d ( TAG , "결과값이 존재하지 않습니다." );
            return;
        }

        if ( result.getResult ( ).equals ( "success" ) ){
            addCartPopUp ( );
        }
        else if ( result.getResult ( ).equals ( "duplicate" ) ){
            Toast.makeText ( this.context , "이미 보유하신 장소입니다." , Toast.LENGTH_SHORT ).show ( );
        }
    }
}
