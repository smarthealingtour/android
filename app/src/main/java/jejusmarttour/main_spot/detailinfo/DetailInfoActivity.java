package jejusmarttour.main_spot.detailinfo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import jejusmarttour.FontAppCompatActivity;
import jejusmarttour.common.CommonData;
import jejusmarttour.util.CustomViewPager;
import jejusmarttour.util.MyPagerSlidingTabStrip;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

public class DetailInfoActivity extends FontAppCompatActivity{
	private static final String TAG = DetailInfoActivity.class.toString ( );

	private final int MAX_PAGE = 2;
	private Fragment cur_fragment;

	private CustomViewPager viewPager;
	private MyPagerSlidingTabStrip tab;

	private ScheduleVO scheduleVO;
	private boolean fromMap = false;

	private DetailInfoFragment infoFragment;
	private DetailMapFragment mapFragment;
	private Fragment DisplayedFragment;

	private View introduceTab;
	private View mapTab;


	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_detail_info );

		Intent intent = getIntent ( );
		if ( intent.getBooleanExtra ( "fromMap" , false ) )
			this.fromMap = true;

		this.scheduleVO = new ScheduleVO ( );
		this.scheduleVO = CommonData.getScheduleVO ( );
		CommonData.setScheduleVO ( null );

		infoFragment = DetailInfoFragment.newInstance(scheduleVO);
		mapFragment = DetailMapFragment.newInstance(scheduleVO);
		DisplayedFragment = infoFragment;

		initActionBar ( );
		initCustomTab();
		initView ( );

	}

	// 커스텀 액션바 적용
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_close , null );
		TextView titleTextView = customView.findViewById ( R.id.title_text );

		if ( scheduleVO.getHealingContentsVO( ) != null )
			titleTextView.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_name ( ) );
		else if ( scheduleVO.getSightSeeingContentsVO( ) != null )
			titleTextView.setText ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( ) );

		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	// 스와이프 ON OFF
	public void swipeOnOff ( boolean useSwipe )
	{
		viewPager.setSwipeable ( useSwipe );
	}

	//요거 바꾸면 위의 소개, 지도탭 바뀜
	public void initCustomTab(){
		introduceTab = getLayoutInflater().inflate(R.layout.view_tab,null);
		introduceTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_introduce_clicked));
		TextView tv1 = introduceTab.findViewById(R.id.text);
		tv1.setText("소개");
		tv1.setTextColor( getResources().getColor(R.color.statusbar) );

		mapTab = getLayoutInflater().inflate(R.layout.view_tab,null);
		mapTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_map));
		TextView tv2 = mapTab.findViewById(R.id.text);
		tv2.setText("지도");
		tv2.setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );
	}

	private void initView ( ){
		viewPager = ( CustomViewPager ) findViewById ( R.id.htp_intorduce_viewpager );
		viewPager.setAdapter ( new ViewPagerAdapter ( getSupportFragmentManager ( ) ) );
		viewPager.setOffscreenPageLimit ( 1 );
		tab = ( MyPagerSlidingTabStrip ) findViewById ( R.id.htp_intorduce_tabs );
		tab.setShouldExpand(true);
		tab.setTabChangeListener(position -> {
            if (position == 0)	clickIntroduce();
            else 			  	clickMap();
        });

		setTabStyle ( );
		tab.setViewPager ( viewPager );

		/*** map 초기화가 안됨 ***/
		// if(this.fromMap)
		// viewPager.setCurrentItem(1);

	}

	public void clickIntroduce(){
		DisplayedFragment = infoFragment;

		introduceTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_introduce_clicked));
		((TextView) introduceTab.findViewById(R.id.text)).setTextColor( getResources().getColor(R.color.statusbar) );

		mapTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_map));
		((TextView)mapTab.findViewById(R.id.text)).setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );
	}
	public void clickMap(){
		DisplayedFragment = mapFragment;

		introduceTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.icon_introduce));
		((TextView) introduceTab.findViewById(R.id.text)).setTextColor( ((CommonData)getApplication()).getTabTextColorStateList() );

		mapTab.findViewById(R.id.first_img).setBackground(getResources().getDrawable(R.drawable.ic_map_clicked));
		((TextView)mapTab.findViewById(R.id.text)).setTextColor( getResources().getColor(R.color.statusbar) );
	}

	// TAB 스타일 바꾸기
	@SuppressLint ( "NewApi" )
	@SuppressWarnings ( "deprecation" )
	private void setTabStyle ( ){
		tab.setDividerColor ( getResources ( ).getColor ( R.color.white ) );
		tab.setBackgroundColor ( getResources ( ).getColor ( R.color.white ) );
		tab.setIndicatorColor ( getResources ( ).getColor ( R.color.statusbar ) );
	}

	// 액션바 메뉴
	@Override
	public boolean onCreateOptionsMenu ( Menu menu ){
		getMenuInflater ( ).inflate ( R.menu.close , menu );
		return true;
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item ){
		int id = item.getItemId ( );

		if ( id == R.id.action_close ){
			finish ( );
			return true;
		}

		return super.onOptionsItemSelected ( item );
	}

	// 뷰페이지 어뎁터
	class ViewPagerAdapter extends FragmentPagerAdapter implements MyPagerSlidingTabStrip.CustomTabProvider {
		public ViewPagerAdapter ( FragmentManager fm ){
			super ( fm );
		}

		@Override
		public Fragment getItem ( int position ){
			if ( position < 0 || MAX_PAGE <= position )
				return null;

			switch ( position )	{
			case 0 :
				cur_fragment = infoFragment;
				break;
			case 1 :
				cur_fragment = mapFragment;
				break;
			}
			return cur_fragment;
		}

		@Override
		public int getCount ( )
		{
			return MAX_PAGE;
		}

		@Override
		public CharSequence getPageTitle ( int position ){
			if (scheduleVO.isHealingContents())
				return getResources ( ).getStringArray ( R.array.product_detail_titles_01 ) [ position ];
            else
                return getResources ( ).getStringArray ( R.array.product_detail_titles_02 ) [ position ];
		}

		@Override
		public View getCustomTab(int position) {
			if ( position == 0){
				return introduceTab;
			}
			else if (position == 1){
				return mapTab;
			}
			return null;
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		DisplayedFragment.onRequestPermissionsResult( requestCode, permissions, grantResults);
	}
}
