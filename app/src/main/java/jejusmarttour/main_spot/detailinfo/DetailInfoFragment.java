package jejusmarttour.main_spot.detailinfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.common.KeywordData;
import jejusmarttour.main_mytourproduct.osy.interest_activity.InterestActivity;
import jejusmarttour.main_tourproduct.HTPInfoTabFragment;
import jejusmarttour.main_tourproduct.ImageViewPagerAdapter;
import jejusmarttour.main_tourproduct.test.SequenceAdapter;
import jejusmarttour.task.GetContentsTask;
import jejusmarttour.task.HTPCartTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.TourProductSequenceVO;
import jejusmarttour.vo.UpdateCartRequestVO;
import jejusmarttour.vo.UpdateResultVO;
import syl.com.jejusmarttour.R;

public class DetailInfoFragment extends Fragment implements OnClickListener , JSONArrayResult < ScheduleVO > , JSONObjectResult < UpdateResultVO >{
	private static final String TAG = DetailInfoFragment.class.toString ( );

	private boolean isCloseTextView = true;
	private LinearLayout sequenceLayoutContainer;
	private ArrayList < ScheduleVO > scheduleArrayList;
	private HTPCartTask htpCartTask;
	private ImageButton cartBtn;
	private ImageButton callBtn;
	private String telNumber;

	private static final int [ ] ATTRS = new int [ ] { android.R.attr.textSize , android.R.attr.textColor };
	private int MAX_PAGE = 0;
	private int prevPosition; // 이전에 선택되었던 포지션 값

	private ViewPager pager; // 뷰 페이저
	private LinearLayout pageMark; // 현재 몇 페이지 인지 나타내는 뷰
	private GetContentsTask getContentsTask;
	private ScheduleVO scheduleVO;
	private ViewGroup keywordDetailContainer;

	private final static String TYPE_HCNT = "hcnt";
	private final static String TYPE_CCNT = "ccnt";

	private RecyclerView sequenceRecyclerView;
	private SequenceAdapter sequenceAdapter;

	private Context context = null;

	public static DetailInfoFragment newInstance (ScheduleVO scheduleVO ){
		DetailInfoFragment fragment = new DetailInfoFragment( );
		fragment.scheduleVO = scheduleVO;
		return fragment;
	}

	public View onCreateView ( LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState ){
		View view = null;
		this.context = getActivity ( );

		try {
			view = inflater.inflate ( R.layout.fragment_detailinfo , container , false );
		}
		catch ( InflateException e ) {
			e.printStackTrace ( );
		}

		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		( ( DetailInfoActivity ) getActivity ( ) ).swipeOnOff ( false );

		initInfoTabFragment();
		sequenceRecyclerView = view.findViewById(R.id.sequence_list);

		// 이미지 뷰 페이저
		pageMark =  view.findViewById ( R.id.page_mark );
		pager = view.findViewById ( R.id.viewpager );

		ImageView noImageView = view.findViewById ( R.id.viewpager_imageview );
		TextView titleText =  view.findViewById ( R.id.htp_title_text );
		TextView sessionText =  view.findViewById ( R.id.htp_session_time );
		TextView coastText =  view.findViewById ( R.id.htp_price );
		TextView addressText =  view.findViewById ( R.id.address_text );
		TextView openTimeText =  view.findViewById ( R.id.open_time_text );
		ImageButton copy = view.findViewById(R.id.food_copy);
		TextView sequence_list_text =  view.findViewById ( R.id.sequence_list_text );

		keywordDetailContainer =  view.findViewById ( R.id.keyword_detail_layout );

//		ArrayList < Integer > arrayList = null;
//		arrayList = KeywordData.getKeywordIconImage ( scheduleVO.getHealingContentsVO( ).getHcnt_hcdArray ( ) , scheduleVO.getHealingContentsVO( ).getHcnt_scdArray ( ) );

		// addImageData ( );
		if ( scheduleVO.getHealingContentsVO( ) != null ){
			titleText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_name ( ) );
			sessionText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_duration ( ) == null ? "- 분" : scheduleVO.getHealingContentsVO( ).getHcnt_duration ( ) + " 분" );
			coastText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_cost_adult ( ) == null ? "- 원" : scheduleVO.getHealingContentsVO( ).getHcnt_cost_adult ( ) + " 원" );
			addressText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_addr ( ) );
			openTimeText.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_op_time ( ) );
			telNumber =  scheduleVO.getHealingContentsVO( ).getHcnt_tel ( ) ;
			sequence_list_text.setText ( "관광지 구성" );

			if ( scheduleVO.getHealingContentsVO( ).getHcnt_sub_img ( ) != null ){
				pageMark.setVisibility ( View.VISIBLE );
				pager.setVisibility ( View.VISIBLE );
				noImageView.setVisibility ( View.GONE );
				MAX_PAGE = scheduleVO.getHealingContentsVO( ).getHcnt_sub_imgArray ( ).length;
				pager.setAdapter ( new ImageViewPagerAdapter ( this.context , scheduleVO.getHealingContentsVO( ).getHcnt_sub_imgArray ( ) , MAX_PAGE ) );
			}
			else{
				pageMark.setVisibility ( View.GONE );
				pager.setVisibility ( View.GONE );
				noImageView.setVisibility ( View.VISIBLE );
				//pager.setAdapter ( new ImageViewPagerAdapter ( this.context , null , MAX_PAGE ) );
			}
		}
		else if ( scheduleVO.getSightSeeingContentsVO( ) != null ){
			coastText.setText ( "- 원" );
			sessionText.setText("- 분");

			titleText.setText ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( ) );
			addressText.setText ( "-" );
			openTimeText.setText ( "-" );
			telNumber = null;

			if ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_img ( ) != null ){
				pageMark.setVisibility ( View.VISIBLE );
				pager.setVisibility ( View.VISIBLE );
				noImageView.setVisibility ( View.VISIBLE );
				MAX_PAGE = scheduleVO.getSightSeeingContentsVO( ).getCcnt_imgArray ( ).length;
				pager.setAdapter ( new ImageViewPagerAdapter ( this.context , scheduleVO.getSightSeeingContentsVO( ).getCcnt_imgArray ( ) , MAX_PAGE ) );
			}
			else{
				pageMark.setVisibility ( View.GONE );
				pager.setVisibility ( View.GONE );
				noImageView.setVisibility ( View.GONE );
				//pager.setAdapter ( new ImageViewPagerAdapter ( this.context , null , MAX_PAGE ) );
			}
		}

		pager.setOffscreenPageLimit ( MAX_PAGE - 1 );
		pager.addOnPageChangeListener ( new OnPageChangeListener (){
			@Override
			public void onPageSelected ( int position ){
				pageMark.getChildAt ( prevPosition ).setBackgroundResource ( R.drawable.page_not ); // 이전
				pageMark.getChildAt ( position ).setBackgroundResource ( R.drawable.page_select ); // 현재
				prevPosition = position; // 이전 포지션 값을 현재로 변경

			}
			@Override
			public void onPageScrolled ( int position , float positionOffset , int positionOffsetPixels ){}
			@Override
			public void onPageScrollStateChanged ( int state ){}
		} );

		initPageMark(); // 현재 페이지 표시하는 뷰 초기화

		//담기 버튼
		cartBtn = view.findViewById ( R.id.cart_btn );
		cartBtn.setOnClickListener ( this );

		callBtn = view.findViewById( R.id.call_btn);
		if ( telNumber != null){
			callBtn.setOnClickListener(v -> {
                if (Build.VERSION.SDK_INT >= 23) {
                    //전화걸기 퍼미션 확인 후 허락되있는경우 전화걸기실행
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData( Uri.parse( "tel:"+ telNumber ));

                        startActivity(intent);
                        return;
                    }
                    //전화걸기 퍼미션 확인 후 거절되있는 퍼미션얻기위한 다이얼로그 띄움
                    else{
                        Log.e(TAG,"Permission 거절되있음");
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                    }
                }
            });
		}
		else {
			callBtn.setVisibility(View.GONE);
		}

		copy.setOnClickListener(v -> {
			ClipboardManager clipManager = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

			ClipData clipData = ClipData.newPlainText("주소", scheduleVO.getHealingContentsVO().getHcnt_addr());
			clipManager.setPrimaryClip(clipData);

			Toast.makeText(getActivity(), "주소가 클립보드에 복사되었습니다.", Toast.LENGTH_SHORT).show();
		});

		sequenceLayoutContainer = view.findViewById ( R.id.product_schedule_layout );

		getContentsTask = new GetContentsTask ( this.context , this );

		if ( scheduleVO.getHealingContentsVO( ) != null ){		 //관광장소 상품목록 불러오기
			getContentsTask.execute ( TYPE_HCNT , scheduleVO.getHealingContentsVO( ).getHcnt_id ( ) );
		}
		else if ( scheduleVO.getSightSeeingContentsVO( ) != null ){	 //음식점일 경우 안불러옴
			sequenceLayoutContainer.setVisibility ( View.GONE );
		}
	}

	private void initInfoTabFragment(){
		Fragment infoTabFragment = HTPInfoTabFragment.newInstance(getContext(), scheduleVO);
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		fragmentTransaction.add(R.id.info_fragment, infoTabFragment);
		fragmentTransaction.commit();
	}

	private void initSequenceLayout ( ){
		ArrayList <TourProductSequenceVO> sequenceArr = getSequenceData ( );
		initSequenceLayout( sequenceArr );
	}

	// 이미지 뷰페이저 하단의 현재 페이지 표시하는 뷰 초기화
	private void initPageMark ( ){
		for ( int i = 0; i < MAX_PAGE; i++ ){
			ImageView iv = new ImageView ( this.context ); // 페이지
			iv.setLayoutParams ( new LayoutParams ( LayoutParams.WRAP_CONTENT , LayoutParams.WRAP_CONTENT ) );

			// 첫 페이지 표시 이미지 이면 선택된 이미지로
			if ( i == 0 )
				iv.setBackgroundResource ( R.drawable.page_select );
			else // 나머지는 선택안된 이미지로
				iv.setBackgroundResource ( R.drawable.page_not );

			// LinearLayout에 추가
			pageMark.addView ( iv );
		}
		prevPosition = 0; // 이전 포지션 값 초기화
	}

	// 상품 구성 및 여행순서 아이템 만들기
	@SuppressLint ( "Recycle" )
	private void initSequenceLayout(ArrayList <TourProductSequenceVO> sequenceArr ){
		sequenceLayoutContainer.setVisibility ( View.VISIBLE );

		LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
		sequenceRecyclerView.setLayoutManager(layoutManager);

		sequenceAdapter = new SequenceAdapter(sequenceArr);
		sequenceAdapter.setItemClickListener(v -> {
            CommonData.setScheduleVO ( scheduleArrayList.get((Integer)v.getTag()) );
            Intent intent = new Intent ( getActivity ( ) , DetailInfoActivity.class );
            startActivity ( intent );
        });

		sequenceRecyclerView.setAdapter( sequenceAdapter );
	}

	// 여행상품목록 데이터 가져오기
	private ArrayList<TourProductSequenceVO> getSequenceData ( ){
		ArrayList<TourProductSequenceVO> arItem = new ArrayList <> ( );

		for ( int i = 0; i < scheduleArrayList.size ( ); i++ ){
			TourProductSequenceVO vo = new TourProductSequenceVO( );
			vo.setSeq( i + 1 );

			if ( scheduleArrayList.get ( i ).getHealingContentsVO( ) != null ){
				vo.setTitle ( scheduleArrayList.get ( i ).getHealingContentsVO( ).getHcnt_name ( ) );
				vo.setIconId ( CommonData.getLocationIconId ( scheduleArrayList.get ( i ).getHealingContentsVO( ).getHcnt_type ( ) ) );
			}
			else if ( scheduleArrayList.get ( i ).getSightSeeingContentsVO( ) != null ){
				vo.setTitle ( scheduleArrayList.get ( i ).getSightSeeingContentsVO( ).getCcnt_name ( ) );
				vo.setIconId ( CommonData.getLocationIconId ( "point" ) );
			}

			arItem.add ( vo );
		}

		return arItem;
	}

	@Override
	public void onClick ( View v ){
		if ( v.getId ( ) == cartBtn.getId ( ) )	{
//			addCartPopUp ( );
            htpCartTask = new HTPCartTask ( this.context , this );

            UpdateCartRequestVO requestVO = new UpdateCartRequestVO ( );
            requestVO.setType ( "add" );
            requestVO.setEmail ( CommonData.getUserVO ( ).getEmail ( ) );
            requestVO.setContent ( true );

            if ( scheduleVO.getHealingContentsVO( ) != null ){
                requestVO.setProductID ( scheduleVO.getHealingContentsVO( ).getHcnt_id ( ) );
                requestVO.setIsCcnt ( false );
            }
            else if ( scheduleVO.getSightSeeingContentsVO( ) != null ){
                requestVO.setProductID ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_id ( ) );
                requestVO.setIsCcnt ( true );
            }
            requestVO.setContent ( true );

            htpCartTask.execute ( requestVO );
		}
	}

	@Override
	public void setJSONObjectResult ( UpdateResultVO result ){
		htpCartTask.cancel ( true );

		if ( result == null ){
			Log.d ( TAG , "결과값이 존재하지 않습니다." );
			return;
		}

		if ( result.getResult ( ).equals ( "success" ) ){
//			Toast.makeText ( this.context , "저장되었습니다." , Toast.LENGTH_SHORT ).show ( );
			addCartPopUp ( );
		}
		else if ( result.getResult ( ).equals ( "duplicate" ) ){
			Toast.makeText ( this.context , "이미 보유하신 여행지입니다." , Toast.LENGTH_SHORT ).show ( );
		}
	}

	@Override
	public void setJSONArrayResult ( ArrayList < ScheduleVO > resultList ){
		getContentsTask.cancel ( true );

		if ( resultList == null ){
			Log.d ( TAG , " ArrayList < ScheduleVO > 값이 존재하지 않습니다." );
			sequenceLayoutContainer.setVisibility ( View.GONE );

			return;
		}
		this.scheduleArrayList = resultList;

		initSequenceLayout ( );
	}

	// 장바구니 담기 팝업
	private void addCartPopUp ( ){
		LinearLayout layout = ( LinearLayout ) View.inflate ( this.context , R.layout.popup_cart , null );
		TextView title_text = layout.findViewById ( R.id.title_text );

		if ( scheduleVO.getHealingContentsVO( ) != null )
			title_text.setText ( scheduleVO.getHealingContentsVO( ).getHcnt_name ( ) + " 을(를)" );
		else if ( scheduleVO.getSightSeeingContentsVO( ) != null )
			title_text.setText ( scheduleVO.getSightSeeingContentsVO( ).getCcnt_name ( ) + " 을(를)" );

		AlertDialog.Builder alertbox = new AlertDialog.Builder ( getActivity ( ) );
		alertbox.setView ( layout );
		alertbox.create ( );
		final DialogInterface popup = alertbox.show ( );

		Button cartBtn = layout.findViewById ( R.id.goto_cart_btn );

		cartBtn.setOnClickListener (v -> {
            Intent intent = new Intent ( getActivity ( ) , InterestActivity.class );
            startActivity ( intent );
            getActivity ( ).finish ( );
        });

		Button cancelBtn = layout.findViewById ( R.id.cancel_btn );
		cancelBtn.setOnClickListener (v -> popup.dismiss ( ));

//		htpCartTask = new HTPCartTask ( this.context , this );
//
//		UpdateCartRequestVO requestVO = new UpdateCartRequestVO ( );
//		requestVO.setType ( "add" );
//		requestVO.setEmail ( CommonData.getUserVO ( ).getEmail ( ) );
//		requestVO.setContent ( true );
//
//		if ( scheduleVO.getHealingContentsVO ( ) != null )
//		{
//			requestVO.setProductID ( scheduleVO.getHealingContentsVO ( ).getHealingContentsVO ( ) );
//			requestVO.setIsCcnt ( false );
//		}
//		else if ( scheduleVO.getSightSeeingContentsVO ( ) != null )
//		{
//			requestVO.setProductID ( scheduleVO.getSightSeeingContentsVO ( ).getSightSeeingContentsVO ( ) );
//			requestVO.setIsCcnt ( true );
//		}
//		requestVO.setContent ( true );
//
//		htpCartTask.execute ( requestVO );
	}

	// keywordDetailContainer 에 담길 키워드
	private void addDetailKeyword ( ){
		ArrayList < View > arrayList = new ArrayList <> ( );

		TextView textView;

		if ( scheduleVO.getHealingContentsVO( ).getHcnt_hcdArray ( ) != null ){
			for (int i = 0; i < scheduleVO.getHealingContentsVO( ).getHcnt_hcdArray ( ).length; i++ ){
				ImageView imageView = getImageView ( );
				Glide.with ( this.context ).load ( KeywordData.getKeywordIconImage ( scheduleVO.getHealingContentsVO( ).getHcnt_hcdArray ( ) [ i ] ) ).thumbnail ( 0.1f ).into ( imageView );
				textView = KeywordData.getKeywordDetailTextView ( this.context , scheduleVO.getHealingContentsVO( ).getHcnt_hcdArray ( ) [ i ] , CommonData.getKeywordArrayList ( ) );
				LinearLayout layout = getLinearLayout ( );
				layout.addView ( textView );

				if (!isContainCategory(i, scheduleVO.getHealingContentsVO().getHcnt_hcdArray()))
					arrayList.add ( imageView );

				arrayList.add ( layout );
			}
		}

		if ( scheduleVO.getHealingContentsVO( ).getHcnt_scdArray ( ) != null ){
			for (int i = 0; i < scheduleVO.getHealingContentsVO( ).getHcnt_scdArray ( ).length; i++ ){
				ImageView imageView = getImageView ( );
				Glide.with ( this.context ).load ( KeywordData.getKeywordIconImage ( scheduleVO.getHealingContentsVO( ).getHcnt_scdArray ( ) [ i ] ) ).thumbnail ( 0.1f ).into ( imageView );
				textView = KeywordData.getKeywordDetailTextView ( this.context , scheduleVO.getHealingContentsVO( ).getHcnt_scdArray ( ) [ i ] , CommonData.getKeywordArrayList ( ) );
				LinearLayout layout = getLinearLayout ( );
				layout.addView ( textView );

				if (!isContainCategory(i, scheduleVO.getHealingContentsVO().getHcnt_scdArray()))
					arrayList.add ( imageView );

				arrayList.add ( layout );
			}
		}

		// Display display = ( ( WindowManager ) this.context.getSystemService
		// ( Context.WINDOW_SERVICE ) ).getDefaultDisplay ( );
		keywordDetailContainer.removeAllViews ( );

		for ( int i = 0; i < arrayList.size ( ); i++ ){
			keywordDetailContainer.addView ( arrayList.get ( i ) );
		}

	}

	private boolean isContainCategory ( int position , String [ ] strings ){
		if ( strings == null )
			return true;

		if ( position == 0 )
			return false;

		if ( position >= 1 ){
			if (!SmartTourUtils.replaceSpaceToBlank(strings[position - 1]).contains(SmartTourUtils.replaceSpaceToBlank(strings[position]).substring(0, 3)))
				return false;
		}

		return true;
	}

	private ImageView getImageView ( ){
		DisplayMetrics dm = this.context.getResources ( ).getDisplayMetrics ( );
		int size = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 30 , dm );

		ImageView imageView = new ImageView ( this.context );
		imageView.setLayoutParams ( new LayoutParams ( size , size ) );
		imageView.setScaleType ( ScaleType.FIT_XY );

		return imageView;
	}

	private LinearLayout getLinearLayout ( ){
		DisplayMetrics dm = this.context.getResources ( ).getDisplayMetrics ( );
		int size = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 30 , dm );

		LinearLayout layout = new LinearLayout ( this.context );
		layout.setLayoutParams ( new LayoutParams ( LinearLayout.LayoutParams.WRAP_CONTENT , size ) );
		layout.setGravity ( Gravity.CENTER_VERTICAL );

		return layout;
	}



	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		Log.e(TAG, "onRequestPermissionsResult: 뭐지뭔데뭘까왜안되니" );
		if (requestCode == 1){
			//퍼미션 다시 확인후 허락되있으면 전화걸기 실행
			if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData( Uri.parse( "tel:" + telNumber));

				startActivity(intent);
			} else {
				Toast.makeText(context, "전화걸기 권한이 필요합니다!", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
