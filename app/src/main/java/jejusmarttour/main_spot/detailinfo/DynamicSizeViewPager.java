package jejusmarttour.main_spot.detailinfo;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Osy on 2018-02-20.
 */

public class DynamicSizeViewPager extends ViewPager {
    private boolean swipeEnabled;

    public DynamicSizeViewPager(Context context) {
        super(context);
    }

    public DynamicSizeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if(this.swipeEnabled){
            return super.onTouchEvent(ev);
        }
        return false;
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.swipeEnabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setSwipeEnabled(boolean enabled) {
        this.swipeEnabled = enabled;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if ( MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST){
            int width = getMeasuredWidth();
            int height = getMeasuredHeight();

            if (getChildCount() > 0){
                View firstChild = getChildAt(0);

                firstChild.measure(MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST));

                width = firstChild.getMeasuredWidth();
                height = firstChild.getMeasuredHeight();
            }

            widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

            Log.e("pager크기", "width: " + width + " height: " + height );

            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
