package jejusmarttour.main_spot.detailinfo;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2017-11-09.
 */

public class StayDetailInfoFragment extends Fragment {
    private ScheduleVO scheduleVO;

    public static StayDetailInfoFragment newInstance(ScheduleVO scheduleVO){
        StayDetailInfoFragment INSTANCE = new StayDetailInfoFragment();
        INSTANCE.scheduleVO = scheduleVO;

        return INSTANCE;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detailinfo_stay,container,false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView shopDesc = view.findViewById(R.id.shop_desc);
        TextView address = view.findViewById(R.id.stay_address);
        ImageButton copy = view.findViewById(R.id.stay_copy);
        TextView time = view.findViewById(R.id.stay_time);
        TextView breakfast = view.findViewById(R.id.stay_breakfast);
        TextView bed = view.findViewById(R.id.stay_bed);
        TextView zzz = view.findViewById(R.id.stay_zzz);
        TextView cooking = view.findViewById(R.id.stay_cooking);
        TextView park = view.findViewById(R.id.stay_park);
        TextView conv = view.findViewById(R.id.stay_conv);
        TextView webPage = view.findViewById(R.id.stay_webpage);

        shopDesc.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_rcmd_comt() );
        address.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_addr());
        time.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_op_time());
        cooking.setText(scheduleVO.getSightSeeingContentsVO().getCcnt_cook() == null
                || scheduleVO.getSightSeeingContentsVO().getCcnt_cook().equals("0") ? "취사 불가" : "취사 가능");
        zzz.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_rent() == null
                || scheduleVO.getSightSeeingContentsVO().getCcnt_rent().equals("0") ? "대실 불가" : "대실 가능");
        breakfast.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_park() == null
                || scheduleVO.getSightSeeingContentsVO().getCcnt_park().equals("0") ? "주차 불가" : "주차 가능");
        bed.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_bed() == null
                || scheduleVO.getSightSeeingContentsVO().getCcnt_bed().equals("0") ? "침대 없음" : "침대 있음");
        park.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_park() == null
                || scheduleVO.getSightSeeingContentsVO().getCcnt_park().equals("0") ? "주차 불가" : "주차 가능");
        conv.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_conv());
        webPage.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_site());

        copy.setOnClickListener(v -> {
            ClipboardManager clipManager = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

            ClipData clipData = ClipData.newPlainText("주소", scheduleVO.getSightSeeingContentsVO().getCcnt_addr());
            clipManager.setPrimaryClip(clipData);

            Toast.makeText(getActivity(), "주소가 클립보드에 복사되었습니다.", Toast.LENGTH_SHORT).show();
        });
    }
}
