package jejusmarttour.main_spot.detailinfo;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import jejusmarttour.common.CommonData;
import jejusmarttour.vo.ScheduleVO;
import me.relex.circleindicator.CircleIndicator;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2017-11-09.
 */

public class FoodDetailInfoFragment extends Fragment {
    private ScheduleVO scheduleVO;
    private DynamicSizeViewPager viewPager;

    public static FoodDetailInfoFragment newInstance(ScheduleVO scheduleVO){
        FoodDetailInfoFragment INSTANCE = new FoodDetailInfoFragment();
        INSTANCE.scheduleVO = scheduleVO;

        return INSTANCE;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detailinfo_food,container,false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView shopDesc = view.findViewById(R.id.shop_desc);
        TextView address = view.findViewById(R.id.food_address);
        ImageButton copy = view.findViewById(R.id.food_copy);
        TextView time = view.findViewById(R.id.food_time);
        TextView timer = view.findViewById(R.id.food_timer);
        TextView mainMenu = view.findViewById(R.id.food_m_menu);
        ImageButton allMenu = view.findViewById(R.id.food_all_menu);
        TextView park = view.findViewById(R.id.food_park);
        TextView conv = view.findViewById(R.id.food_conv);
        TextView webPage = view.findViewById(R.id.food_webpage);

        shopDesc.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_rcmd_comt() );
        address.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_addr());
        time.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_op_time());
        if(scheduleVO.getSightSeeingContentsVO().getCcnt_duration() != null)
            timer.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_duration() + " 분");
        else
            timer.setText(" - ");
        mainMenu.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_best());
        park.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_park() == null
                || scheduleVO.getSightSeeingContentsVO().getCcnt_park().equals("0") ? "주차 불가" : "주차 가능");
        conv.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_conv());
        webPage.setText( scheduleVO.getSightSeeingContentsVO().getCcnt_site());

        allMenu.setOnClickListener(view12 -> {
            Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.dialog_menu);
            CircleIndicator circleIndicator = dialog.findViewById(R.id.pager_marker);

            final TextView noImageView = dialog.findViewById(R.id.no_image_text);

            viewPager = dialog.findViewById(R.id.menu_imgs);

            if( scheduleVO.getSightSeeingContentsVO().getCcnt_m_imgs() == null || scheduleVO.getSightSeeingContentsVO().getCcnt_m_imgs()[0].equals("")){
                viewPager.setVisibility(View.GONE);
                circleIndicator.setVisibility(View.GONE);
            }
            else{
                viewPager.setSwipeEnabled(true);
                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        pagerResize(position);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });

                viewPager.setAdapter(new ViewPagerAdapter(getActivity()));
                circleIndicator.setViewPager(viewPager);

                noImageView.setVisibility(View.GONE);
            }

            dialog.show();
        });

        copy.setOnClickListener(v -> {
            ClipboardManager clipManager = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

            ClipData clipData = ClipData.newPlainText("주소", scheduleVO.getSightSeeingContentsVO().getCcnt_addr());
            clipManager.setPrimaryClip(clipData);

            Toast.makeText(getActivity(), "주소가 클립보드에 복사되었습니다.", Toast.LENGTH_SHORT).show();
        });
    }

    // 이미지뷰 크기에따라 뷰페이지 크기 바꿈.
    private void pagerResize(int position){
        View view1 = viewPager.findViewWithTag(position);
        view1.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        int width = view1.getMeasuredWidth();
        int height = view1.getMeasuredHeight();

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        viewPager.setLayoutParams(params);
    }

    private class ViewPagerAdapter extends PagerAdapter{
        private LayoutInflater inflater;

        private ViewPagerAdapter(Context c){
            inflater = LayoutInflater.from(c);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v = inflater.inflate(R.layout.view_img, null);
            v.setTag( position );

            Glide.with ( getActivity() ).load ( CommonData.getImageAddress ( scheduleVO.getSightSeeingContentsVO().getCcnt_m_imgs()[position] ) )
                    .into ( (ImageView) v.findViewById(R.id.img) );
            container.addView(v);

            return v;
        }

        @Override
        public int getCount() {
            return scheduleVO.getSightSeeingContentsVO().getCcnt_m_imgs().length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }
    }
}
