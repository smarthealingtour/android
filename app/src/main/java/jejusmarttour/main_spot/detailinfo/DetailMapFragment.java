package jejusmarttour.main_spot.detailinfo;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.data.kml.KmlLayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.map.CommonData_;
import jejusmarttour.map.util;
import jejusmarttour.task.GeometryTask;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SmartTourProductsVO;
import jejusmarttour.vo.TourProductVO;
import syl.com.jejusmarttour.R;

public class DetailMapFragment extends Fragment implements OnMapReadyCallback {
    private ScheduleVO scheduleVO;
    private Boolean isCreateView = false;
    private View view = null;

    private SmartTourProductsVO smartTourProductsVO;

    private GoogleMap mMap;
    MapView gMapView;

    private TourProductVO tourProductVO;

    private ArrayList<RouteVO> geometry_data;
    private ArrayList<HealingContentsVO> geometry_data2;

    private String kml_raw;

    protected int getLayoutId() {
        return R.layout.fragment_detail_map_view;
    }

    private String cName = "content";

    public static DetailMapFragment newInstance(ScheduleVO scheduleVO) {
        DetailMapFragment fragment = new DetailMapFragment();
        fragment.scheduleVO = scheduleVO;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;

        try {
            view = inflater.inflate(R.layout.fragment_detail_map_view, container, false);
            gMapView = view.findViewById(R.id.htourMap);
            gMapView.onCreate(savedInstanceState);

            gMapView.getMapAsync(this);
        } catch (InflateException e) {
            e.printStackTrace();
        }
        if (scheduleVO.getSightSeeingContentsVO() == null) {
            if (scheduleVO.getHealingContentsVO().getHcnt_kml() != null) {
                kml_raw = scheduleVO.getHealingContentsVO().getHcnt_kml();
                Log.e("kml_raw", kml_raw);
            }
        }
//        FrameLayout mMainLayout = ( FrameLayout ) view.findViewById ( R.id.mapFrame ); // 지도
//        info = ( FrameLayout ) view.findViewById ( R.id.info );
//        px = ( int ) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, info.getLayoutParams().height, getResources().getDisplayMetrics());
//        info.setTranslationY( px );
//
//        lm = new LayerManager ( mMainLayout );
//        mMyMapView = new MyMapView( getActivity ( ) );
//        overlay = new overlay_( getActivity ( ) , mMyMapView);
//
//        lm.addLayer (mMyMapView, "baseMap" );
//        lm.addLayer ( overlay , "overlay_" );
//
//        mMyMapView.setLayerManager ( lm );
//        overlay.setLayerManager ( lm );
//
//        mMyMapView.setZoomLevel ( 4 );
//        mMyMapView.setMapResolution(1002);
//        mMyMapView.setMapCenter(util.mapCenter);
//        mMyMapView.setVisibility ( View.VISIBLE );
        CommonData.recentCname = cName;

        return view;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            String[] permissions = new String[]{ Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            requestPermissions(permissions, 1);
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setMinZoomPreference(8);
        mMap.setMaxZoomPreference(17);
    }

    private void retrieveFileFromUrl(String kml) {
        Log.e("이거 kml 주소", kml);
        new DownloadKmlFile(kml).execute();
    }

    private void moveCameraToKml() {
        if( geometry_data.size() == 0) {
            if(geometry_data2.size() == 0) {
                return;
            } else {
                for (int i = 0; geometry_data2.size() > i; i++ ){
                    String lat;
                    String lng;

                    lat = geometry_data2.get(i).getHcnt_coord_x();
                    lng = geometry_data2.get(i).getHcnt_coord_y();

                    mMap.addMarker( new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))) //coord x,y를 latlng 형태로, 좌표는 healingcontentsvo에서 받아와야하고, ScheduleVO에서 이 vo를 포함하고 있음, task는 scheduletaskvo를 이용
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                    if(i == 0) {
                        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)), 15));
                    }
                }
            }
        } else {
            for (int i = 0; geometry_data.size() > i; i++ ){
                String here = geometry_data.get(i).getGeometry();
                Log.e("이게 마커 데이터", here);
                int idx = here.indexOf(" ");
                String lat;
                String lng;

                lat = here.substring(0, idx);
                lng = here.substring(idx + 1);

                mMap.addMarker( new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))) //coord x,y를 latlng 형태로, 좌표는 healingcontentsvo에서 받아와야하고, ScheduleVO에서 이 vo를 포함하고 있음, task는 scheduletaskvo를 이용
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                if(i == 0) {
                    getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)), 15));
                }
            }
        }
    }

    protected GoogleMap getMap() {
        return mMap;
    }

    @Override
    public void setUserVisibleHint ( boolean isVisibleToUser ) {
        super.setUserVisibleHint ( isVisibleToUser );
        if ( isVisibleToUser ) {
            if ( !isCreateView && scheduleVO != null ) {
                executeTask ( );
                if(kml_raw != null) {
                    String url = null;
                    url = CommonData.getImageAddress();
                    url += kml_raw;
                    retrieveFileFromUrl(url);
                }
                isCreateView = true;
            }
            CommonData.recentCname = cName;
        }
    }

    private void executeTask ( ) {
        GeometryTask task = new GeometryTask( getActivity() , this,scheduleVO.isHealingContents() );
        task.execute ( scheduleVO.isHealingContents() ? scheduleVO.getHealingContentsVO().getHcnt_id() : scheduleVO.getSightSeeingContentsVO().getCcnt_id() );
    }


    public void setJsonResult ( JSONArray result ) throws JSONException {
        if(result == null) {
            Toast.makeText(getActivity(), "현재 지도에 표시할 위치정보 데이터가 없습니다. 빠른 시일 내에 마련하겠습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        final int numberOfItemsInResp = result.length ( );

        geometry_data = new ArrayList<>();
        geometry_data2 = new ArrayList<>();

        for ( int i = 0; i < numberOfItemsInResp; i++ ) {
            RouteVO routeVO = new RouteVO();
            HealingContentsVO healingContentsVO = new HealingContentsVO();
            JSONObject jsonObject = new JSONObject(result.getString(i));
            healingContentsVO.setHcnt_type( jsonObject.has ( "hcnt_type" ) ? jsonObject.getString ( "hcnt_type" ) : jsonObject.getString("type") );
            try {
                if( healingContentsVO.getHcnt_type().equals("line") || healingContentsVO.getHcnt_type().equals("zone") ) {
                    healingContentsVO.setHcnt_coord_x(jsonObject.getString("hcnt_coord_x"));
                    healingContentsVO.setHcnt_coord_y(jsonObject.getString("hcnt_coord_y"));
                    geometry_data2.add(healingContentsVO);
                } else {
                    routeVO.setGeometry(jsonObject.getString("geometry"));
                    geometry_data.add(routeVO);
                }
            } catch (NullPointerException e)
            {
                Log.e ( this.toString() , e.toString ( ) );
            }
            if( i == 0 ) {
                moveCameraToKml();
            }
        }
//        if ( overlay != null )
//        {
//            overlay.setHtourInGeometry ( result , info, px, false, null, -1);
//            overlay.setVisibility ( View.VISIBLE );
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isCreateView && !CommonData.recentCname.equals(cName)){
//            if(overlay != null) {
//                lm.removeLayer(overlay);
//                overlay = new overlay_( getActivity ( ) , mMyMapView);
//                lm.addLayer ( overlay , "overlay" );
//                overlay.setLayerManager ( lm );

//            }
            executeTask();
            if(kml_raw != null) {
                String url;
                url = CommonData.getImageAddress();
                url += kml_raw;
                retrieveFileFromUrl(url);
            }
            CommonData.recentCname = cName;
        }
        gMapView.onResume();
    }

    @Override
    public void onDestroy ( ) {
        super.onDestroy ( );
//        if ( mMyMapView != null )
//        {
//            mMyMapView.destroyDrawingCache ( );
//            if ( mMyMapView.commonUtils != null )
//                mMyMapView.commonUtils.clearData ( );
//        }
//
//        if( overlay != null )
//            util.recursiveRecycle(overlay);
        CommonData_ common = CommonData_.getInstance ( getActivity ( ) );
        if ( common != null )
            common.dispose ( );

        util.recursiveRecycle(getActivity().getWindow().getDecorView());

        System.gc ( );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1){
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.e(this.toString(), "onRequestPermissionsResult: 퍼미션 확인 완료" );
            } else {
                Toast.makeText(getActivity(), "지도보기 권한이 필요합니다!", Toast.LENGTH_SHORT).show();
                getActivity().fileList();
            }
        }
    }

    private class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
        private final String mUrl;

        DownloadKmlFile(String url) {
            mUrl = url;
        }

        protected byte[] doInBackground(String... params) {
            Log.e(this.toString(), "kml download url: " + mUrl );
            try {
                InputStream is =  new URL(mUrl).openStream();
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[16384];
                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                return buffer.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(byte[] byteArr) {
            try {
                KmlLayer kmlLayer = new KmlLayer(mMap, new ByteArrayInputStream(byteArr),
                        getContext());
                kmlLayer.addLayerToMap();
                moveCameraToKml();
            } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}