package jejusmarttour.main_spot.tourspot;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.FontAppCompatActivity;
import jejusmarttour.common.CommonData;
import jejusmarttour.main_spot.detailinfo.DetailInfoActivity;
import jejusmarttour.search.SearchProductActivity;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.util.code_data.SpotSpinnerTree;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

public class TourSpotActivity extends FontAppCompatActivity
		implements  SearchView.OnQueryTextListener, JSONArrayResult<ScheduleVO>{
	private TourSpotListFragment tourSpotListFragment;

	private MenuItem mSearchItem;
	private MenuItem search_result_delete;

	private Spinner spinner1;
	private Spinner spinner2;
	private Spinner spinner3;

	private String code = "";

	//스피너 전체가 선택될때 태스크 한번더 실행되니까 캐치해내려구 만듬
	private String excutedCode = "";

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sy_healing_tour_product);

		initSpinner();
		initFragment();
	}

	private void initSpinner ( ){
		spinner1 = (Spinner)findViewById(R.id.spinner1);
		spinner2 = (Spinner)findViewById(R.id.spinner2);
		spinner3 = (Spinner)findViewById(R.id.spinner3);

		SpotSpinnerTree spotSpinnerTree = SpotSpinnerTree.getInstance();
		ArrayAdapter<ArrayList<String>> adapter1 = new ArrayAdapter<ArrayList<String>>(this, R.layout.spinner_layout,
				spotSpinnerTree.getList(-1,-1));
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		ArrayAdapter< ArrayList<String>> adapter2 = new ArrayAdapter<ArrayList<String>>(this, R.layout.spinner_layout, new ArrayList());
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		ArrayAdapter< ArrayList<String>> adapter3 = new ArrayAdapter<ArrayList<String>>(this, R.layout.spinner_layout, new ArrayList());
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner1.setAdapter(adapter1);
		spinner2.setAdapter(adapter2);
		spinner3.setAdapter(adapter3);

		spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
				adapter2.clear();
				adapter3.clear();
				spinner2.setSelection(0);
				spinner3.setSelection(0);
				code = spotSpinnerTree.getCode( position,-1,-1);

				switch ( position ){
					case 0:
						setCode(code, false);
						adapter2.addAll( spotSpinnerTree.getList( position, -1) );
						break;
					case 1:
						setCode(code, false);
						adapter2.addAll( spotSpinnerTree.getList( position, -1) );
						break;
					case 2:
						setCode(code, false);
						adapter2.addAll( spotSpinnerTree.getList( position, -1) );
						break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
			}
		});

		spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				spinner3.setBackgroundResource(R.drawable.bg_spinner);
				adapter3.clear();
				spinner3.setSelection(0);

				code = spotSpinnerTree.getCode( Integer.parseInt(code.substring(0,1)) -1, position, -1);
				setCode(code, false);

				if(spotSpinnerTree.getList(Integer.parseInt(code.substring(0,1)) -1,position).size() != 0){
					adapter3.addAll( spotSpinnerTree.getList( Integer.parseInt(code.substring(0,1)) -1, position));
				}
				else {
					spinner3.setBackgroundResource(R.drawable.bg_inactive_spinner);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				boolean hasSubZone = spotSpinnerTree.hasSubZone(code,position);
				code = spotSpinnerTree.getCode(code, position);

				setCode(code, hasSubZone);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
	}

	private void initFragment(){
		tourSpotListFragment = TourSpotListFragment.newInstance();
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace( R.id.viewlist, tourSpotListFragment);
		fragmentTransaction.commit();
	}

	private void setCode(String code, boolean hasSubZone){
		//실행됬었던 코드가 실행하려는 코드와 같으면 태스크 실행 x

		if ( !excutedCode.equals(code) ){
			if (hasSubZone && code.substring(0,1).equals("1")) tourSpotListFragment.setScdAndGetSubZone(code);
			else if (hasSubZone && code.substring(0,1).equals("2")) tourSpotListFragment.setScdAndGetSubZone(code);
			else if ( !code.substring(0,1).equals("3") ) tourSpotListFragment.setScdAndTaskStart( code);
			else tourSpotListFragment.setHcdAndTaskStart( code);
		}

		excutedCode = code;
	}

	public void onItemClick(ScheduleVO vo ){
		CommonData.setScheduleVO(vo);
		Intent intent = new Intent ( this , DetailInfoActivity.class );
		startActivity(intent);
	}

	@Override
	protected void onDestroy ( )
	{
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected ( MenuItem item ){
		int id = item.getItemId ( );

		if ( id == R.id.action_settings ) {
			CommonData.gotoMyInfo ( this );
			return true;
		}
		else if(id  == android.R.id.home ) {
			finish();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	// 액션바 메뉴
	@Override
	public boolean onCreateOptionsMenu ( Menu menu ){
		ActionBar actionBar = getSupportActionBar();

		// 액션바 그림자 지우기
		actionBar.setElevation(0);

		LayoutInflater mInflater = LayoutInflater.from ( this );
		View customView = mInflater.inflate(R.layout.actionbar_listview, null);

		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( R.string.tour_spot_title);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.search, menu);

		LinearLayout backBtn = customView.findViewById ( R.id.back_btn );

		backBtn.setOnClickListener (view -> finish ( ));
		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled(true);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setOnQueryTextListener(this);
		mSearchItem = menu.getItem(0);

		search_result_delete = menu.findItem(R.id.search_result_delete);

//            if(searchResult != null){
//                if(searchResult.openShowResult.getVisibility() == View.VISIBLE)
//                    search_result_delete.setVisible(true);
//            }

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onQueryTextSubmit(String query){

		mSearchItem.collapseActionView();
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setTitle(query);
		searchProduct (query );

		return false;
	}

	//키워드 검색 수행
	private void searchProduct(String query) {
		Intent intent = new Intent( this , SearchProductActivity.class );
		intent.putExtra("data", query);
		startActivity(intent);
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}

	@Override
	public void setJSONArrayResult(ArrayList<ScheduleVO> resultList) { }
}