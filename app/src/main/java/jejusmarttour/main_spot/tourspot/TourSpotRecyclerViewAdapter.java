package jejusmarttour.main_spot.tourspot;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.util.CommonUtil;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

/**
 * Created by Osy on 2018-02-22.
 */

class TourSpotRecyclerViewAdapter extends RecyclerView.Adapter<TourSpotRecyclerViewAdapter.MyViewHolder> {
    private final DisplayImageOptions options;
    private ArrayList<ScheduleVO> provider;
    private Context context;

    TourSpotRecyclerViewAdapter(Context context, ArrayList<ScheduleVO> provider){
        this.context = context;
        this.provider = provider;

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.my_tour_course_no_img_icon)
                .showImageOnFail(R.drawable.my_tour_course_no_img_icon)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.NONE)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(100))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration
                .Builder(context)
                .defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext());

        View view = inflater.inflate(R.layout.item_staggered_list, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (provider.get(position).getHealingContentsVO() != null){
            HealingContentsVO vo = provider.get(position).getHealingContentsVO();

            if (vo.getHcnt_sub_imgArray() != null){
//                Glide.with ( context ).load ( CommonData.getImageAddress () + vo.getHcnt_sub_imgArray()[0] ).into ( holder.imageView );

                ImageLoader.getInstance().displayImage( CommonData.getImageAddress () + vo.getHcnt_sub_imgArray()[0], holder.imageView, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                        String message = null;
//                        switch (failReason.getType()) {
//                            case IO_ERROR:
//                                message = "Input/Output error";
//                                break;
//                            case DECODING_ERROR:
//                                message = "Image can't be decoded";
//                                break;
//                            case NETWORK_DENIED:
//                                message = "Downloads are denied";
//                                break;
//                            case OUT_OF_MEMORY:
//                                message = "Out Of Memory error";
//                                break;
//                            case UNKNOWN:
//                                message = "Unknown error";
//                                break;
//                        }
//                        Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    }
                });
            }
            else {
//                Glide.with ( context ).load ( CommonData.getImageAddress () + vo.getHcnt_sub_img() ).into ( holder.imageView );

                ImageLoader.getInstance().displayImage( CommonData.getImageAddress () + vo.getHcnt_sub_img(), holder.imageView, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                        String message = null;
//                        switch (failReason.getType()) {
//                            case IO_ERROR:
//                                message = "Input/Output error";
//                                break;
//                            case DECODING_ERROR:
//                                message = "Image can't be decoded";
//                                break;
//                            case NETWORK_DENIED:
//                                message = "Downloads are denied";
//                                break;
//                            case OUT_OF_MEMORY:
//                                message = "Out Of Memory error";
//                                break;
//                            case UNKNOWN:
//                                message = "Unknown error";
//                                break;
//                        }
//                        Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    }
                });
            }

            holder.titleText.setText( vo.getHcnt_name());
            holder.introText.setText( vo.getHcnt_addr());

            if( vo.getHcnt_duration() != null) {
                int min = Integer.parseInt( vo.getHcnt_duration());
                holder.timeText.setText( CommonUtil.changeHourMin(min) );
            }else{
                holder.timeText.setText("정보없음");
            }
        }

        holder.view.setOnClickListener(v ->
                ( (TourSpotActivity) context ).onItemClick ( provider.get(position))
        );
    }


    @Override
    public int getItemCount() {
        return provider.size();
    }

    void addData(ArrayList<ScheduleVO> scheduleVOS){
        provider.addAll(scheduleVOS);

        notifyDataSetChanged();
    }

    void removeAllData(){
        provider.clear();

        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView imageView;
        TextView titleText;
        TextView introText;
        TextView timeText;

        MyViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            imageView = itemView.findViewById( R.id.place_img );
            titleText = itemView.findViewById( R.id.title );
            introText = itemView.findViewById( R.id.intro );
            timeText = itemView.findViewById( R.id.average_time_text);
        }
    }
}
