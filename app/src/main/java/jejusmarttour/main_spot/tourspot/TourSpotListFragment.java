package jejusmarttour.main_spot.tourspot;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import jejusmarttour.task.GetContentsTask;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.TourSpotTask;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import syl.com.jejusmarttour.R;

public class TourSpotListFragment extends Fragment {
    private TourSpotRecyclerViewAdapter tourSpotRecyclerViewAdapter;
    private TourSpotTask searchProductTask;
    private SearchProductRequestVO vo;

    private int requestPageNumber = 1;

    private ArrayList<ScheduleVO> listData;

    private String scd = "";
    private String hcd = "";
    private boolean listViewClear = false;
    private boolean allDataGet = false; //설정한 카테고리(스피너)의 모든 데이터를 가져오면 태스크실행 x
    private boolean taskDoing = false;

    public static TourSpotListFragment newInstance() {
        return new TourSpotListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        try {
            view = inflater.inflate(R.layout.fragment_staggered_list, container, false);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listData = new ArrayList<>();

        initRecyclerView(view);
    }

    private void initRecyclerView(View view) {
        RecyclerView mRecyclerView = view.findViewById(R.id.list_view);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(!allDataGet && !taskDoing){
                    int[] visibleLastPositions = ((StaggeredGridLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPositions(null);

                    if (recyclerView.getAdapter().getItemCount() == visibleLastPositions[0] + 1
                            || recyclerView.getAdapter().getItemCount() == visibleLastPositions[1] + 1) {

                        requestPageNumber += 1;

                        executeTask();
                    }
                }
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        tourSpotRecyclerViewAdapter = new TourSpotRecyclerViewAdapter(getActivity(), listData);

        mRecyclerView.setAdapter(tourSpotRecyclerViewAdapter);
    }

    // 요청할 파라미터
    private SearchProductRequestVO getParam() {
        vo = new SearchProductRequestVO();
        vo.setPage(Integer.toString(requestPageNumber));
        vo.setType("0");

        return vo;
    }

	/*// 아이템 터치 이벤트
    private View.OnClickListener onClickListItem = new View.OnClickListener( ){
		@Override
		public void onClick ( View view ){
			( (TourSpotActivity) getActivity ( ) ).onItemClick ( ( ScheduleVO ) tourSpotRecyclerViewAdapter.getItem ( position ) );
		}
	};*/

    public void setScdAndTaskStart(String scd) {
        listViewClear = true;
        requestPageNumber = 1;
        allDataGet = false;

        this.hcd = "";
        this.scd = scd;

        Log.e(this.toString(), "setHcdAndTaskStart: 실행" + scd);
        tourSpotRecyclerViewAdapter.removeAllData();

        executeTask();
    }

    public void setHcdAndTaskStart(String hcd) {
        listViewClear = true;
        requestPageNumber = 1;
        allDataGet = false;

        this.scd = "";
        this.hcd = hcd;

        Log.e(this.toString(), "setScdAndTaskStart: 실행" + hcd);
        tourSpotRecyclerViewAdapter.removeAllData();

        executeTask();
    }

    public void setScdAndGetSubZone(String scd) {
        taskDoing = true;
        listViewClear = true;
        requestPageNumber = 1;
        allDataGet = false;

        this.hcd = "";
        this.scd = scd;

        Log.e(this.toString(), "setScdAndGetSubZone: 실행" + scd);
        tourSpotRecyclerViewAdapter.removeAllData();

        searchProductTask = new TourSpotTask(getActivity(), addResultAtQueue, scd, hcd);
        searchProductTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getParam());
    }
    public void setHcdAndGetSubZone(String hcd) {
        taskDoing = true;
        listViewClear = true;
        requestPageNumber = 1;

        this.hcd = hcd;
        this.scd = "";

        Log.e(this.toString(), "setHcdAndGetSubZone: 실행" + hcd);
        tourSpotRecyclerViewAdapter.removeAllData();

        searchProductTask = new TourSpotTask(getActivity(), addResultAtQueue, scd, hcd);
        searchProductTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getParam());
    }

    private void executeTask() {
        taskDoing = true;
        searchProductTask = new TourSpotTask(getActivity(), addResultAtList, scd, hcd);
        searchProductTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getParam());
    }

    JSONArrayResult<ScheduleVO> addResultAtList = new JSONArrayResult<ScheduleVO>() {
        @Override
        public void setJSONArrayResult(ArrayList<ScheduleVO> resultList) {
            searchProductTask.cancel(true);

            if (resultList == null) {
                if (listViewClear) {
                    tourSpotRecyclerViewAdapter.removeAllData();
                } else {
                    return;
                }
            } else {
                tourSpotRecyclerViewAdapter.addData(resultList);
            }
            listViewClear = false;
            taskDoing = false;
        }
    };

    private Queue<String> ht_ids = new LinkedList<>();

    JSONArrayResult<ScheduleVO> addResultAtQueue = new JSONArrayResult<ScheduleVO>() {
        @Override
        public void setJSONArrayResult(ArrayList<ScheduleVO> resultList) {
            if (resultList == null || resultList.size() == 0) {
                allDataGet = true;
            }
            else {
                Log.e(this.toString(), "setJSONArrayResult: " + resultList.size());
                for (ScheduleVO s :
                        resultList) {
                    ht_ids.offer(s.getHt_id());
                }
            }
            getLineZone();
        }
    };

    private void getLineZone() {
        String ht_id = ht_ids.poll();
        if (ht_id != null) {
            GetContentsTask getContentsTask = new GetContentsTask(getActivity(), jsonArrayResult3);
            getContentsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "hcnt", ht_id);

            Log.e(this.toString(), "getLineZone " );
        }
    }

    JSONArrayResult<ScheduleVO> jsonArrayResult3 = resultList -> {
        Log.e(this.toString(), "getLineZone 완료" );
        addResultAtList.setJSONArrayResult(resultList);
        if (ht_ids.peek() != null) {
            getLineZone();
        }
    };

	/*private void executeTask2 ( ){
        GetContentsTask getContentsTask = new GetContentsTask(getActivity(), this);
		searchProductTask = new TourSpotTask( getContext ( ) , this , scd, hcd);
		searchProductTask.executeOnExecutor ( AsyncTask.THREAD_POOL_EXECUTOR , getParam() );
	}*/
}
