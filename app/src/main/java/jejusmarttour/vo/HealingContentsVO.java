package jejusmarttour.vo;

import java.io.Serializable;
import java.util.ArrayList;

import jejusmarttour.util.SmartTourUtils;

public class HealingContentsVO implements Serializable{
    public final static int HCNT_TYPE = 1;
    private String hcnt_id;
    private String hcnt_type;
    private String hcnt_name;
    private String hcnt_intro;
    private String hcnt_addr;
    private String hcnt_tel;
    private String hcnt_op_time;
    //private String hcnt_cost;
    private String hcnt_duration;
    private String hcnt_guide;
    private String hcnt_guide_name;
    private String hcnt_guide_tel;
    private String hcnt_site;
    private String hcnt_sub_img = null;
    private String hcnt_rcmd;
    private String hcnt_rcmd_comt;
    private String hcnt_hcd;
    private String hcnt_scd;
    private ArrayList< KeywordVO > hcnt_hcd_name;
    private ArrayList < KeywordVO > hcnt_scd_name;
    private String hcnt_inv_hcnt;
    private String ht_id;
    private String nickname;
    private String hcnt_coord_x = null;
    private String hcnt_coord_y= null;
    private String hcnt_cost_adult;
    private String hcnt_cost_child;
    private String hcnt_cost_jeju;
    private String g_id;

    private String cart_count;
    private String comment_count;

    private String hcnt_rcmd_comt_sec;
    private String hcnt_rcmd_comt_thd;
    private String hcnt_rcmd_comt_fth;
    private String hcnt_rcmd_comt_fiv;

    private String hcnt_tab;

    private String hcnt_kml;


    public boolean isHcnt ( ){
        if ( getHcnt_name ( ) != null )
            return true;

        return false;
    }

    public String getHcnt_id ( )
    {
        return hcnt_id;
    }

    public void setHcnt_id ( String hcnt_id )
    {
        this.hcnt_id = hcnt_id;
    }

    public String getHcnt_type ( )
    {
        return hcnt_type;
    }

    public void setHcnt_type ( String hcnt_type )
    {
        this.hcnt_type = hcnt_type;
    }

    public String getHcnt_name ( )
    {
        return hcnt_name;
    }

    public void setHcnt_name ( String hcnt_name )
    {
        this.hcnt_name = hcnt_name;
    }

    public String getHcnt_intro ( )
    {
        return hcnt_intro;
    }

    public void setHcnt_intro ( String hcnt_intro )
    {
        this.hcnt_intro = hcnt_intro;
    }

    public String getHcnt_addr ( )
    {
        return hcnt_addr;
    }

    public void setHcnt_addr ( String hcnt_addr )
    {
        this.hcnt_addr = hcnt_addr;
    }

    public String getHcnt_tel ( )
    {
        return hcnt_tel;
    }

    public void setHcnt_tel ( String hcnt_tel )
    {
        this.hcnt_tel = hcnt_tel;
    }

    public String getHcnt_op_time ( )
    {
        return hcnt_op_time;
    }

    public void setHcnt_op_time ( String hcnt_op_time )
    {
        this.hcnt_op_time = hcnt_op_time;
    }

    public String getHcnt_duration ( )
    {
        return hcnt_duration;
    }

    public void setHcnt_duration ( String hcnt_duration )
    {
        this.hcnt_duration = hcnt_duration;
    }

    public String getHcnt_guide ( )
    {
        return hcnt_guide;
    }

    public void setHcnt_guide ( String hcnt_guide )  {
        this.hcnt_guide = hcnt_guide;
    }

    public String getHcnt_guide_name ( )
    {
        return hcnt_guide_name;
    }

    public void setHcnt_guide_name ( String hcnt_guide_name ) {
        this.hcnt_guide_name = hcnt_guide_name;
    }

    public String getHcnt_guide_tel ( ) {
        return hcnt_guide_tel;
    }

    public void setHcnt_guide_tel ( String hcnt_guide_tel )  {
        this.hcnt_guide_tel = hcnt_guide_tel;
    }

    public String getHcnt_site ( )
    {
        return hcnt_site;
    }

    public void setHcnt_site ( String hcnt_site )
    {
        this.hcnt_site = hcnt_site;
    }

    public String getHcnt_sub_img ( )
    {
        return hcnt_sub_img;
    }

    public void setHcnt_sub_img ( String hcnt_sub_img )
    {
        this.hcnt_sub_img = hcnt_sub_img;
    }

    public String getHcnt_rcmd ( )
    {
        return hcnt_rcmd;
    }

    public void setHcnt_rcmd ( String hcnt_rcmd )
    {
        this.hcnt_rcmd = hcnt_rcmd;
    }

    public String getHcnt_rcmd_comt ( )
    {
        return hcnt_rcmd_comt;
    }

    public void setHcnt_rcmd_comt ( String hcnt_rcmd_comt ) {
        this.hcnt_rcmd_comt = hcnt_rcmd_comt;
    }

    public String getHcnt_hcd ( )
    {
        return hcnt_hcd;
    }

    public void setHcnt_hcd ( String hcnt_hcd )
    {
        this.hcnt_hcd = hcnt_hcd;
    }

    public String getHcnt_scd ( )
    {
        return hcnt_scd;
    }

    public void setHcnt_scd ( String hcnt_scd )
    {
        this.hcnt_scd = hcnt_scd;
    }

    public ArrayList < KeywordVO > getHcnt_hcd_name ( )
    {
        return hcnt_hcd_name;
    }

    public String getCart_count ( )
    {
        return cart_count;
    }

    public void setCart_count ( String cart_count )
    {
        this.cart_count = cart_count;
    }

    public String getComment_count ( )
    {
        return comment_count;
    }

    public void setComment_count ( String comment_count )
    {
        this.comment_count = comment_count;
    }

    public void setHcnt_hcd_name ( ArrayList < KeywordVO > hcnt_hcd_name ){
        this.hcnt_hcd_name = hcnt_hcd_name;
    }

    public ArrayList < KeywordVO > getHcnt_scd_name ( )
    {
        return hcnt_scd_name;
    }

    public void setHcnt_scd_name ( ArrayList < KeywordVO > hcnt_scd_name ){
        this.hcnt_scd_name = hcnt_scd_name;
    }

    public String getHcnt_inv_hcnt ( )
    {
        return hcnt_inv_hcnt;
    }

    public void setHcnt_inv_hcnt ( String hcnt_inv_hcnt )
    {
        this.hcnt_inv_hcnt = hcnt_inv_hcnt;
    }

    public String getHt_id ( )
    {
        return ht_id;
    }

    public void setHt_id ( String ht_id )
    {
        this.ht_id = ht_id;
    }

    public String getNickname ( )
    {
        return nickname;
    }

    public void setNickname ( String nickname )
    {
        this.nickname = nickname;
    }

    public String getHcnt_coord_x ( )
    {
        return hcnt_coord_x;
    }

    public void setHcnt_coord_x ( String hcnt_coord_x )
    {
        this.hcnt_coord_x = hcnt_coord_x;
    }

    public String getHcnt_coord_y ( )
    {
        return hcnt_coord_y;
    }

    public void setHcnt_coord_y ( String hcnt_coord_y )
    {
        this.hcnt_coord_y = hcnt_coord_y;
    }

    public String getHcnt_cost_adult ( )
    {
        return hcnt_cost_adult;
    }

    public void setHcnt_cost_adult ( String hcnt_cost_adult ){
        this.hcnt_cost_adult = hcnt_cost_adult;
    }

    public String getHcnt_cost_child ( )
    {
        return hcnt_cost_child;
    }

    public void setHcnt_cost_child ( String hcnt_cost_child )
    {
        this.hcnt_cost_child = hcnt_cost_child;
    }

    public String getHcnt_cost_jeju ( )
    {
        return hcnt_cost_jeju;
    }

    public void setHcnt_cost_jeju ( String hcnt_cost_jeju )
    {
        this.hcnt_cost_jeju = hcnt_cost_jeju;
    }

    public String getG_id ( )
    {
        return g_id;
    }

    public void setG_id ( String g_id )
    {
        this.g_id = g_id;
    }

    public String [ ] getHcnt_sub_imgArray ( ){
        return SmartTourUtils.getValidateStringAraay(getHcnt_sub_img());
    }

    public String [ ] getHcnt_scdArray ( ){
        return SmartTourUtils.getValidateStringAraay ( getHcnt_scd ( ) );
    }

    public String [ ] getHcnt_hcdArray ( ){
        return SmartTourUtils.getValidateStringAraay ( getHcnt_hcd ( ) );
    }

    public String getHcnt_rcmd_comt_sec(){
        return hcnt_rcmd_comt_sec;
    }

    public void setHcnt_rcmd_comt_sec(String hcnt_rcmd_comt_sec) {
        this.hcnt_rcmd_comt_sec = hcnt_rcmd_comt_sec;
    }

    public String getHcnt_rcmd_comt_thd(){
        return hcnt_rcmd_comt_thd;
    }

    public void setHcnt_rcmd_comt_thd(String hcnt_rcmd_comt_thd) {
        this.hcnt_rcmd_comt_thd = hcnt_rcmd_comt_thd;
    }

    public String getHcnt_rcmd_comt_fth(){
        return hcnt_rcmd_comt_fth;
    }

    public void setHcnt_rcmd_comt_fth(String hcnt_rcmd_comt_fth) {
        this.hcnt_rcmd_comt_fth = hcnt_rcmd_comt_fth;
    }

    public String getHcnt_rcmd_comt_fiv() {
        return hcnt_rcmd_comt_fiv;
    }

    public void setHcnt_rcmd_comt_fiv(String hcnt_rcmd_comt_fiv) {
        this.hcnt_rcmd_comt_fiv = hcnt_rcmd_comt_fiv;
    }

    public String getHcnt_tab() {
        return hcnt_tab;
    }

    public void setHcnt_tab(String hcnt_tab) {
        this.hcnt_tab = hcnt_tab;
    }

    public String getHcnt_kml() {
        return hcnt_kml;
    }

    public void setHcnt_kml(String hcnt_kml) {
        this.hcnt_kml = hcnt_kml;
    }
}
