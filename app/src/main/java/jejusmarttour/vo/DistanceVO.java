package jejusmarttour.vo;

public class DistanceVO
{
	private String distance;
	private String start;
	private String route_id;
	private String lead_time;
	private String end;

	public String getDistance ( )
	{
		return distance;
	}

	public void setDistance ( String distance )
	{
		this.distance = distance;
	}

	public String getStart ( )
	{
		return start;
	}

	public void setStart ( String start )
	{
		this.start = start;
	}

	public String getRoute_id ( )
	{
		return route_id;
	}

	public void setRoute_id ( String route_id )
	{
		this.route_id = route_id;
	}

	public String getLead_time ( )
	{
		return lead_time;
	}

	public void setLead_time ( String lead_time )
	{
		this.lead_time = lead_time;
	}

	public String getEnd ( )
	{
		return end;
	}

	public void setEnd ( String end )
	{
		this.end = end;
	}

}
