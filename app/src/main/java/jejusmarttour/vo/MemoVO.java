package jejusmarttour.vo;

public class MemoVO
{
	private String memo_subt;
	private String memo_id;
	private String ht_id;

	public String getMemo_subt ( )
	{
		return memo_subt;
	}

	public void setMemo_subt ( String memo_subt )
	{
		this.memo_subt = memo_subt;
	}

	public String getMemo_id ( )
	{
		return memo_id;
	}

	public void setMemo_id ( String memo_id )
	{
		this.memo_id = memo_id;
	}

	public String getHt_id ( )
	{
		return ht_id;
	}

	public void setHt_id ( String ht_id )
	{
		this.ht_id = ht_id;
	}

}
