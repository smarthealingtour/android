package jejusmarttour.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class SmartTourProductsVO implements Serializable
{
	private TourProductVO tourProductVO;
	private ArrayList < ScheduleVO > scheduleArrayList = new ArrayList<ScheduleVO>();

	public TourProductVO getTourProductVO ( )
	{
		return tourProductVO;
	}

	public void setTourProductVO ( TourProductVO tourProductVO )
	{
		this.tourProductVO = tourProductVO;
	}

	public ArrayList < ScheduleVO > getScheduleArrayList ( )
	{
		return scheduleArrayList;
	}

	public void setScheduleArrayList ( ArrayList < ScheduleVO > scheduleArrayList )
	{
		this.scheduleArrayList = scheduleArrayList;
	}
}
