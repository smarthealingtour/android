package jejusmarttour.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import jejusmarttour.util.SmartTourUtils;

public class TourProductVO implements Serializable{
	// 힐링여행상품 타입 0:전문가 1:아마츄어
	private String ht_type;
	// 힐링여행상품아이디
	private String ht_id;
	// 상품명
	private String ht_name;
	// 간략소개
	private String ht_intro;
	// 추천기관
	private String ht_rcmd_org;
	// 추천기관 소견
	private String hcnt_rcmd_comt = null;
	// 이미지
	private String ht_img;
	// 힐링 코드
	private String ht_hcd;
	// 감성 코드
	private String ht_scd;
	// 관련 장소
	private String ht_inv_hcnt;
	// 관련 메모
	private String ht_inv_memo;
	// 담은 횟수
	private String cart_count;
	// 리뷰 횟수
	private String comment_count;

	private String nickname;
	private String ht_author;
	private String cost;
	private String distance;
	private String days;
	private String duraion;

	// 주소
	private String ht_address;
	// duration
	private int ht_duration;

	private String ht_status = null;

	private String ht_create_time;

	private String ROWNUM;

	// 관심 장소 때문에..
	private boolean isChecked = false;

	private String ht_inv_route;
	private ArrayList < KeywordVO > ht_hcd_name = new ArrayList<>();
	private ArrayList < KeywordVO > ht_scd_name = new ArrayList<>();

	private String hcnt_rcmd_comt_sec;
	private String hcnt_rcmd_comt_thd;
	private String hcnt_rcmd_comt_fth;
	private String hcnt_rcmd_comt_fiv;

	private String hcnt_tab;

	public boolean isChecked ( )
	{
		return isChecked;
	}

	public void setIsChecked ( boolean isChecked )
	{
		this.isChecked = isChecked;
	}

	public String getNickname ( )
	{
		return nickname;
	}

	public void setNickname ( String nickname )
	{
		this.nickname = nickname;
	}

	public String getHt_type ( )
	{
		return ht_type;
	}

	public void setHt_type ( String ht_type )
	{
		this.ht_type = ht_type;
	}

	public String getHt_id ( ){
		return ht_id;
	}

	public void setHt_id ( String ht_id )
	{
		this.ht_id = ht_id;
	}

	public String getHt_name ( )
	{
		return ht_name;
	}

	public void setHt_name ( String ht_name )
	{
		this.ht_name = ht_name;
	}

	public String getHt_intro ( )
	{
		return ht_intro;
	}

	public void setHt_intro ( String ht_intro )
	{
		this.ht_intro = ht_intro;
	}

	public String getHt_rcmd_org ( )
	{
		return ht_rcmd_org;
	}

	public void setHt_rcmd_org ( String ht_rcmd_org )
	{
		this.ht_rcmd_org = ht_rcmd_org;
	}

	public String getHcnt_rcmd_comt( )
	{
		return hcnt_rcmd_comt;
	}

	public void setHcnt_rcmd_comt(String hcnt_rcmd_comt)
	{
		this.hcnt_rcmd_comt = hcnt_rcmd_comt;
	}

	public String getHt_img ( )
	{
		return ht_img;
	}

	public void setHt_img ( String ht_img )
	{
		this.ht_img = ht_img;
	}

	public String getHt_hcd ( )
	{
		return ht_hcd;
	}

	public void setHt_hcd ( String ht_hcd )
	{
		this.ht_hcd = ht_hcd;
	}

	public String getHt_scd ( )
	{
		return ht_scd;
	}

	public void setHt_scd ( String ht_scd )
	{
		this.ht_scd = ht_scd;
	}

	public String getHt_inv_hcnt ( ){
		return ht_inv_hcnt;
	}

	public void setHt_inv_hcnt ( String ht_inv_hcnt ){
		this.ht_inv_hcnt = ht_inv_hcnt;
	}

	public String getHt_inv_memo ( )
	{
		return ht_inv_memo;
	}

	public void setHt_inv_memo ( String ht_inv_memo )
	{
		this.ht_inv_memo = ht_inv_memo;
	}

	public String getROWNUM ( )
	{
		return ROWNUM;
	}

	public void setROWNUM ( String rOWNUM )
	{
		ROWNUM = rOWNUM;
	}

	public String getHt_inv_route ( )
	{
		return ht_inv_route;
	}

	public void setHt_inv_route ( String ht_inv_route )
	{
		this.ht_inv_route = ht_inv_route;
	}

	public String getCart_count ( )
	{
		return cart_count;
	}

	public void setCart_count ( String cart_count )
	{
		this.cart_count = cart_count;
	}

	public String getComment_count ( )
	{
		return comment_count;
	}

	public void setComment_count ( String comment_count )
	{
		this.comment_count = comment_count;
	}

	public String getCost ( )
	{
		return cost;
	}

	public void setCost ( String cost )
	{
		this.cost = cost;
	}

	public String getDistance ( )
	{
		return distance;
	}

	public void setDistance ( String distance )
	{
		this.distance = distance;
	}

	public String getDays ( )
	{
		return days;
	}

	public void setDays ( String days )
	{
		this.days = days;
	}

	public String getDuraion ( )
	{
		return duraion;
	}

	public void setDuraion ( String duraion )
	{
		this.duraion = duraion;
	}

	public ArrayList < KeywordVO > getHt_hcd_name ( )
	{
		return ht_hcd_name;
	}

	public void setHt_hcd_name ( ArrayList < KeywordVO > ht_hcd_name ){
		this.ht_hcd_name = ht_hcd_name;
	}

	public ArrayList < KeywordVO > getHt_scd_name ( ){
		return ht_scd_name;
	}

	public void setHt_scd_name ( ArrayList < KeywordVO > ht_scd_name ){
		this.ht_scd_name = ht_scd_name;
	}

	public int getHt_duration ( )
	{
		return ht_duration;
	}

	public void setHt_duration ( int ht_duration )
	{
		this.ht_duration = ht_duration;
	}

	public String getHt_address ( )
	{
		return ht_address;
	}

	public void setHt_address ( String ht_address )
	{
		this.ht_address = ht_address;
	}

	public String getHt_status ( )
	{
		return ht_status;
	}

	public void setHt_status ( String ht_status )
	{
		this.ht_status = ht_status;
	}

	public String getHt_create_time ( )
	{
		return ht_create_time;
	}

	public void setHt_create_time ( String ht_create_time )
	{
		this.ht_create_time = ht_create_time;
	}

	public String getHt_author ( )
	{
		return ht_author;
	}

	public void setHt_author ( String ht_author )
	{
		this.ht_author = ht_author;
	}

	// Array값 리턴
	public String [ ] getHt_inv_routeArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getHt_inv_route ( ) );
	}

	public String [ ] getHt_inv_memoArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getHt_inv_memo ( ) );
	}

	public String [ ] getHt_imgArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getHt_img ( ) );
	}

	public String [ ] getHt_inv_hcntArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getHt_inv_hcnt ( ) );
	}

	public String [ ] getHt_scdArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getHt_scd ( ) );
	}

	public String [ ] getHt_hcdArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getHt_hcd ( ) );
	}

	// MAP
	public HashMap < String , String [ ] > getRelativeMemoMap ( ){
		return SmartTourUtils.getValidateRelativeMemoMap ( getHt_inv_memoArray ( ) );
	}

	public String getHcnt_rcmd_comt_sec() {
		return hcnt_rcmd_comt_sec;
	}

	public void setHcnt_rcmd_comt_sec(String hcnt_rcmd_comt_sec) {
		this.hcnt_rcmd_comt_sec = hcnt_rcmd_comt_sec;
	}

	public String getHcnt_rcmd_comt_thd() {
		return hcnt_rcmd_comt_thd;
	}

	public void setHcnt_rcmd_comt_thd(String hcnt_rcmd_comt_thd) {
		this.hcnt_rcmd_comt_thd = hcnt_rcmd_comt_thd;
	}

	public String getHcnt_rcmd_comt_fth() {
		return hcnt_rcmd_comt_fth;
	}

	public void setHcnt_rcmd_comt_fth(String hcnt_rcmd_comt_fth) {
		this.hcnt_rcmd_comt_fth = hcnt_rcmd_comt_fth;
	}

	public String getHcnt_rcmd_comt_fiv() {
		return hcnt_rcmd_comt_fiv;
	}

	public void setHcnt_rcmd_comt_fiv(String hcnt_rcmd_comt_fiv) {
		this.hcnt_rcmd_comt_fiv = hcnt_rcmd_comt_fiv;
	}

	public String getHcnt_tab() {
		return hcnt_tab;
	}

	public void setHcnt_tab(String hcnt_tab) {
		this.hcnt_tab = hcnt_tab;
	}
}
