package jejusmarttour.vo;

public class KeywordVO
{
	private String code_name;
	private String classL;
	private String code;

	public String getCode_name ( )
	{
		return code_name;
	}

	public void setCode_name ( String code_name )
	{
		this.code_name = code_name;
	}

	public String getClassL ( )
	{
		return classL;
	}

	public void setClassL ( String classL )
	{
		this.classL = classL;
	}

	public String getCode ( )
	{
		return code;
	}

	public void setCode ( String code )
	{
		this.code = code;
	}

}
