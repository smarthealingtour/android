package jejusmarttour.vo;

import java.util.ArrayList;

public class MTCSchedulePackageVO
{
	private String days;
	private String ht_id;
	private String ht_type;
	private String ht_name;
	private String nickname;
	private String email;
	private String ht_intro;
	private String ht_rcmd_comt;
	private ArrayList < MTCScheduleVO > mtcSchduleVOArrayList;

	public String getDays ( )
	{
		return days;
	}

	public void setDays ( String days )
	{
		this.days = days;
	}

	public String getHt_id ( )
	{
		return ht_id;
	}

	public void setHt_id ( String ht_id )
	{
		this.ht_id = ht_id;
	}

	public String getHt_type ( )
	{
		return ht_type;
	}

	public void setHt_type ( String ht_type )
	{
		this.ht_type = ht_type;
	}

	public String getHt_name ( )
	{
		return ht_name;
	}

	public void setHt_name ( String ht_name )
	{
		this.ht_name = ht_name;
	}

	public String getNickname ( )
	{
		return nickname;
	}

	public void setNickname ( String nickname )
	{
		this.nickname = nickname;
	}

	public String getEmail ( )
	{
		return email;
	}

	public void setEmail ( String email )
	{
		this.email = email;
	}

	public String getHt_intro ( )
	{
		return ht_intro;
	}

	public void setHt_intro ( String ht_intro )
	{
		this.ht_intro = ht_intro;
	}

	public String getHt_rcmd_comt ( )
	{
		return ht_rcmd_comt;
	}

	public void setHt_rcmd_comt ( String ht_rcmd_comt )
	{
		this.ht_rcmd_comt = ht_rcmd_comt;
	}

	public ArrayList < MTCScheduleVO > getMtcSchduleVOArrayList ( )
	{
		return mtcSchduleVOArrayList;
	}

	public void setMtcSchduleVOArrayList ( ArrayList < MTCScheduleVO > mtcSchduleVOArrayList )
	{
		this.mtcSchduleVOArrayList = mtcSchduleVOArrayList;
	}
}
