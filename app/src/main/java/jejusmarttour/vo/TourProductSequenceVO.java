package jejusmarttour.vo;

public class TourProductSequenceVO
{
	private int seq;
	private String title;
	private String category;
	private int iconId;
	
	public int getSeq( )
	{
		return seq;
	}
	public void setSeq(int seq)
	{
		this.seq = seq;
	}
	public String getTitle ( )
	{
		return title;
	}
	public void setTitle ( String title )
	{
		this.title = title;
	}
	public String getCategory ( )
	{
		return category;
	}
	public void setCategory ( String category )
	{
		this.category = category;
	}
	public int getIconId ( ){
		return iconId;
	}
	public void setIconId ( int iconId )
	{
		this.iconId = iconId;
	}
	
	
}
