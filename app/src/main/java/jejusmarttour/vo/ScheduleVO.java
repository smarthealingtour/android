package jejusmarttour.vo;

import java.util.ArrayList;

public class ScheduleVO{
	private String ht_schd_id;
	private String days;
	private String [ ] route_point = null;
	private ArrayList < String > memo = new ArrayList<>();
    private ArrayList < String > memoWriter = new ArrayList<>();
	private String seq;
	private String day;
	private HealingContentsVO healingContentsVO;
	private SightSeeingContentsVO sightSeeingContentsVO;
	private String ht_id;
	private RouteVO route;

	public boolean isHealingContents ( ){
		if ( getSightSeeingContentsVO( ) != null )
			return false;
		return true;
	}

	public String [ ] getRoute_point ( )
	{
		return route_point;
	}

	public void setRoute_point ( String [ ] route_point )
	{
		this.route_point = route_point;
	}

	public String getHt_schd_id ( )
	{
		return ht_schd_id;
	}

	public void setHt_schd_id ( String ht_schd_id )
	{
		this.ht_schd_id = ht_schd_id;
	}

	public String getDays ( )
	{
		return days;
	}

	public void setDays ( String days )
	{
		this.days = days;
	}

	public ArrayList < String > getMemo ( )
	{
		return memo;
	}

	public void setMemo ( ArrayList < String > memo ){
		this.memo = memo;
	}

    public ArrayList < String > getMemoWriter ( ){
        return memoWriter;
    }

    public void setMemoWriter ( ArrayList < String > memoWriter ){
        this.memoWriter = memoWriter;
    }
	
	public String getSeq ( ){
		return seq;
	}

	public void setSeq ( String seq ){
		this.seq = seq;
	}

	public String getDay ( ){
		return day;
	}

	public void setDay ( String day ){
		this.day = day;
	}

	public HealingContentsVO getHealingContentsVO( ){
		return healingContentsVO;
	}

	public void setHealingContentsVO(HealingContentsVO healingContentsVO){
		this.healingContentsVO = healingContentsVO;
	}

	public SightSeeingContentsVO getSightSeeingContentsVO( ){
		return sightSeeingContentsVO;
	}

	public void setSightSeeingContentsVO(SightSeeingContentsVO sightSeeingContentsVO){
		this.sightSeeingContentsVO = sightSeeingContentsVO;
	}

	public String getHt_id ( )
	{
		return ht_id;
	}

	public void setHt_id ( String ht_id )
	{
		this.ht_id = ht_id;
	}

	public RouteVO getRoute ( )
	{
		return route;
	}

	public void setRoute ( RouteVO route )
	{
		this.route = route;
	}

	public String getContentID ( ){
		if ( getHealingContentsVO( ) != null )
			return getHealingContentsVO( ).getHcnt_id ( );
		else
			return getSightSeeingContentsVO( ).getCcnt_id ( );
	}

	public String getContentName ( ){
		if ( getHealingContentsVO( ) != null )
			return getHealingContentsVO( ).getHcnt_name ( );
		else
			return getSightSeeingContentsVO( ).getCcnt_name ( );
	}

	public String getImage(){
		if ( getHealingContentsVO( ) != null )
			return getHealingContentsVO( ).getHcnt_sub_img( );
		else
			return getSightSeeingContentsVO( ).getCcnt_img( );
	}

	public String[] getImageArray(){
		if ( getHealingContentsVO( ) != null )
			return getHealingContentsVO( ).getHcnt_sub_imgArray ( );
		else
			return getSightSeeingContentsVO( ).getCcnt_imgArray ( );
	}

	public Object getContentVO(){
		if ( getHealingContentsVO( ) != null )
			return getHealingContentsVO( );
		else
			return getSightSeeingContentsVO( );
	}

	public boolean isSameContent(ScheduleVO vo ){
		return getContentID().equals(vo.getContentID());
	}
}
