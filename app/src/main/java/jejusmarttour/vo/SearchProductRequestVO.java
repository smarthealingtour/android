package jejusmarttour.vo;

public class SearchProductRequestVO
{
	// 힐링여행상품 검색어
	private String ht_name;
	// 힐링여행상품 타입(0:전문가, 1:사용자)
	private String type;
	// 정렬(name:이름, dist:거리)
	private String sort;
	// 페이지 넘버
	private String page;
	// 힐링코드
	private String hcd = null;
	// 감성코드
	private String scd = null;
	// 관광코드
	private String ccd = null;


	public String getHt_name ( )
	{
		return ht_name;
	}

	public void setHt_name ( String ht_name )
	{
		this.ht_name = ht_name;
	}

	public String getType ( )
	{
		return type;
	}

	public void setType ( String type )
	{
		this.type = type;
	}

	public String getSort ( )
	{
		return sort;
	}

	public void setSort ( String sort )
	{
		this.sort = sort;
	}

	public String getPage ( )
	{
		return page;
	}

	public void setPage ( String page )
	{
		this.page = page;
	}

	public String getHcd ( )
	{
		return hcd;
	}

	public void setHcd ( String hcd )
	{
		this.hcd = hcd;
	}

	public String getScd ( ) {return scd;}

	public void setScd ( String scd ) {this.scd = scd;}

	public String getCcd ( ) {return ccd;}

	public void setCcd ( String ccd ) {this.ccd = ccd;}

}
