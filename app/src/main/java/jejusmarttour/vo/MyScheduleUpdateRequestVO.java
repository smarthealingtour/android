package jejusmarttour.vo;

import org.json.JSONObject;

import java.util.ArrayList;

public class MyScheduleUpdateRequestVO
{
	private ArrayList < String > imagePathArrayList;
	private JSONObject updateJSONObject;

	public ArrayList < String > getImagePathArrayList ( )
	{
		return imagePathArrayList;
	}

	public void setImagePathArrayList ( ArrayList < String > imagePathArrayList )
	{
		this.imagePathArrayList = imagePathArrayList;
	}

	public JSONObject getUpdateJSONObject ( )
	{
		return updateJSONObject;
	}

	public void setUpdateJSONObject ( JSONObject updateJSONObject )
	{
		this.updateJSONObject = updateJSONObject;
	}
}
