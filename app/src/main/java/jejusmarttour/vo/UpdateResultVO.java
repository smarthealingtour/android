package jejusmarttour.vo;

public class UpdateResultVO
{
	private String result = null;
	private String ht_status = null;
	
	public boolean isMTCShare()
	{
		if(getHt_status() != null)
			return true;
					
		return false;
	}

	public String getResult ( )
	{
		return result;
	}

	public void setResult ( String result )
	{
		this.result = result;
	}

	public String getHt_status()
	{
		return ht_status;
	}

	public void setHt_status(String ht_status)
	{
		this.ht_status = ht_status;
	}
}
