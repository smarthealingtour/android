package jejusmarttour.vo;

import java.io.Serializable;
import java.util.ArrayList;

import jejusmarttour.util.SmartTourUtils;

public class SightSeeingContentsVO implements Serializable {
	public static int CCNT_TYPE = 2;
	private String ccnt_name;
	private String ccnt_id;
	private String ccnt_cd;
	private String ccnt_img = null;
	private ArrayList < KeywordVO > ccnt_cd_name;
	private String ccnt_coord_x = null;
	private String ccnt_coord_y = null;
	private String cart_count;
	private String comment_count;
	private String ccnt_intro;
	private String ccnt_addr;
	private String ccnt_tel;
	private String ccnt_op_time;
	private String[] ccnt_m_imgs;
	private String ccnt_duration;
	private String ccnt_site;
	private String nickname;
	private String ccnt_rcmd;
	private String ccnt_rcmd_comt;
	private String ccnt_hcd;
	private String ccnt_scd;
	private String ccnt_best;
	private String ccnt_park;
	private String ccnt_conv;
	private String g_id;
	private String tag;

	private String ccnt_bed;
	private String ccnt_rent;
	private String ccnt_cook;
	private String ccnt_meal;
	private String ccnt_code;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCcnt_code() {
		return ccnt_code;
	}

	public void setCcnt_code(String ccnt_code) {
		this.ccnt_code = ccnt_code;
	}

	public String getCcnt_bed() {
		return ccnt_bed;
	}

	public void setCcnt_bed(String ccnt_bed) {
		this.ccnt_bed = ccnt_bed;
	}

	public String getCcnt_rent() {
		return ccnt_rent;
	}

	public void setCcnt_rent(String ccnt_rent) {
		this.ccnt_rent = ccnt_rent;
	}

	public String getCcnt_cook() {
		return ccnt_cook;
	}

	public void setCcnt_cook(String ccnt_cook) {
		this.ccnt_cook = ccnt_cook;
	}

	public String getCcnt_meal() {
		return ccnt_meal;
	}

	public void setCcnt_meal(String ccnt_meal) {
		this.ccnt_meal = ccnt_meal;
	}

	public String getCcnt_intro() {
		return ccnt_intro;
	}

	public void setCcnt_intro(String ccnt_intro) {
		this.ccnt_intro = ccnt_intro;
	}

	public String getCcnt_addr() {
		return ccnt_addr;
	}

	public void setCcnt_addr(String ccnt_addr) {
		this.ccnt_addr = ccnt_addr;
	}

	public String getCcnt_tel() {
		return ccnt_tel;
	}

	public void setCcnt_tel(String ccnt_tel) {
		this.ccnt_tel = ccnt_tel;
	}

	public String getCcnt_op_time() {
		return ccnt_op_time;
	}

	public void setCcnt_op_time(String ccnt_op_time) {
		this.ccnt_op_time = ccnt_op_time;
	}

	public String[] getCcnt_m_imgs() {
		return ccnt_m_imgs;
	}

	public void setCcnt_m_imgs(String[] ccnt_m_img) {
		this.ccnt_m_imgs = ccnt_m_img;
	}

	public String getCcnt_duration() {
		return ccnt_duration;
	}

	public void setCcnt_duration(String ccnt_duration) {
		this.ccnt_duration = ccnt_duration;
	}

	public String getCcnt_site() {
		return ccnt_site;
	}

	public void setCcnt_site(String ccnt_site) {
		this.ccnt_site = ccnt_site;
	}

	public String getCcnt_rcmd() {
		return ccnt_rcmd;
	}

	public void setCcnt_rcmd(String ccnt_rcmd) {
		this.ccnt_rcmd = ccnt_rcmd;
	}

	public String getCcnt_rcmd_comt() {
		return ccnt_rcmd_comt;
	}

	public void setCcnt_rcmd_comt(String ccnt_rcmd_comt) {
		this.ccnt_rcmd_comt = ccnt_rcmd_comt;
	}

	public String getCcnt_hcd() {
		return ccnt_hcd;
	}

	public void setCcnt_hcd(String ccnt_hcd) {
		this.ccnt_hcd = ccnt_hcd;
	}

	public String getCcnt_scd() {
		return ccnt_scd;
	}

	public void setCcnt_scd(String ccnt_scd) {
		this.ccnt_scd = ccnt_scd;
	}

	public String getCcnt_best() {
		return ccnt_best;
	}

	public void setCcnt_best(String ccnt_best) {
		this.ccnt_best = ccnt_best;
	}

	public String getCcnt_park() {
		return ccnt_park;
	}

	public void setCcnt_park(String ccnt_park) {
		this.ccnt_park = ccnt_park;
	}

	public String getCcnt_conv() {
		return ccnt_conv;
	}

	public void setCcnt_conv(String ccnt_conv) {
		this.ccnt_conv = ccnt_conv;
	}

	public String getG_id() {
		return g_id;
	}

	public void setG_id(String g_id) {
		this.g_id = g_id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getCcnt_name ( )
	{
		return ccnt_name;
	}

	public void setCcnt_name ( String ccnt_name )
	{
		this.ccnt_name = ccnt_name;
	}

	public String getCcnt_id ( )
	{
		return ccnt_id;
	}

	public void setCcnt_id ( String ccnt_id )
	{
		this.ccnt_id = ccnt_id;
	}

	public String getCcnt_cd ( )
	{
		return ccnt_cd;
	}

	public void setCcnt_cd ( String ccnt_cd )
	{
		this.ccnt_cd = ccnt_cd;
	}

	public String getCcnt_img ( )
	{
		return ccnt_img;
	}

	public void setCcnt_img ( String ccnt_img )
	{
		this.ccnt_img = ccnt_img;
	}

	public ArrayList < KeywordVO > getCcnt_cd_name ( )
	{
		return ccnt_cd_name;
	}

	public void setCcnt_cd_name ( ArrayList < KeywordVO > ccnt_cd_name ){
		this.ccnt_cd_name = ccnt_cd_name;
	}

	public String getCcnt_coord_x ( )
	{
		return ccnt_coord_x;
	}

	public void setCcnt_coord_x ( String ccnt_coord_x )
	{
		this.ccnt_coord_x = ccnt_coord_x;
	}

	public String getCcnt_coord_y ( )
	{
		return ccnt_coord_y;
	}

	public void setCcnt_coord_y ( String ccnt_coord_y )
	{
		this.ccnt_coord_y = ccnt_coord_y;
	}

	public String getCart_count ( )
	{
		return cart_count;
	}

	public void setCart_count ( String cart_count )
	{
		this.cart_count = cart_count;
	}

	public String getComment_count ( )
	{
		return comment_count;
	}

	public void setComment_count ( String comment_count )
	{
		this.comment_count = comment_count;
	}

	// Array
	public String [ ] getCcnt_cdArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getCcnt_cd ( ) );
	}

	public String [ ] getCcnt_imgArray ( ){
		return SmartTourUtils.getValidateStringAraay ( getCcnt_img() );
	}
}
