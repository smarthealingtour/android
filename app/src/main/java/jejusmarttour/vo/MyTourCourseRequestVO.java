package jejusmarttour.vo;

public class MyTourCourseRequestVO
{
	private String email;
	private String type;
	private String page;

	public String getEmail ( )
	{
		return email;
	}

	public void setEmail ( String email )
	{
		this.email = email;
	}

	public String getType ( )
	{
		return type;
	}

	public void setType ( String type )
	{
		this.type = type;
	}

	public String getPage ( )
	{
		return page;
	}

	public void setPage ( String page )
	{
		this.page = page;
	}

}
