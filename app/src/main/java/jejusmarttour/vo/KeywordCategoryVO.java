package jejusmarttour.vo;

import java.util.ArrayList;

public class KeywordCategoryVO
{
	private String s_class;
	private String code_name;
	private String code_id;
	private String l_class;
	private String m_class;
	private String code;

	private ArrayList<KeywordCategoryVO> arrayKeyword;

	public String getS_class ( )
	{
		return s_class;
	}

	public void setS_class ( String s_class )
	{
		this.s_class = s_class;
	}

	public String getCode_name ( )
	{
		return code_name;
	}

	public void setCode_name ( String code_name )
	{
		this.code_name = code_name;
	}

	public String getCode_id ( )
	{
		return code_id;
	}

	public void setCode_id ( String code_id )
	{
		this.code_id = code_id;
	}

	public String getL_class ( )
	{
		return l_class;
	}

	public void setL_class ( String l_class )
	{
		this.l_class = l_class;
	}

	public String getM_class ( )
	{
		return m_class;
	}

	public void setM_class ( String m_class )
	{
		this.m_class = m_class;
	}

	public String getCode ( )
	{
		return code;
	}

	public void setCode ( String code )
	{
		this.code = code;
	}

	public ArrayList<KeywordCategoryVO> getArrayKeyword() {
		if(arrayKeyword == null)
			return null;
		else
			return arrayKeyword;
	}

	public void setArrayKeyword(ArrayList<KeywordCategoryVO> arrayKeyword) {
		this.arrayKeyword = arrayKeyword;
	}

}
