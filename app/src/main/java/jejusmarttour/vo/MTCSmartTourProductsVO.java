package jejusmarttour.vo;

import java.util.ArrayList;

public class MTCSmartTourProductsVO
{
	private TourProductVO tourProductVO;
	private ArrayList < ScheduleVO > scheduleArrayList = new ArrayList<ScheduleVO>();
	private ArrayList < MTCScheduleVO > mtcScheduleArrayList = new ArrayList<MTCScheduleVO>();

	public TourProductVO getTourProductVO ( )
	{
		return tourProductVO;
	}

	public void setTourProductVO ( TourProductVO tourProductVO )
	{
		this.tourProductVO = tourProductVO;
	}

	public ArrayList < ScheduleVO > getScheduleArrayList ( )
	{
		return scheduleArrayList;
	}

	public void setScheduleArrayList ( ArrayList < ScheduleVO > scheduleArrayList )
	{
		this.scheduleArrayList = scheduleArrayList;
	}

	public ArrayList < MTCScheduleVO > getMtcScheduleArrayList ( )
	{
		return mtcScheduleArrayList;
	}

	public void setMtcScheduleArrayList ( ArrayList < MTCScheduleVO > mtcScheduleArrayList )
	{
		this.mtcScheduleArrayList = mtcScheduleArrayList;
	}
}
