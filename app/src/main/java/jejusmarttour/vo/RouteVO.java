package jejusmarttour.vo;

import org.json.JSONObject;

public class RouteVO
{
	private String distance = null;
	private String start;
	private String end;
	private String route_id;
	private String lead_time = null;
	private String ht_id;
	private String geometry;
	private String position;
	private String contentID;

	public String getContentID ( )
	{
		return contentID;
	}

	public void setContentID ( String contentID )
	{
		this.contentID = contentID;
	}

	public String getPosition ( )
	{
		return position;
	}

	public void setPosition ( String position )
	{
		this.position = position;
	}

	public String getGeometry ( )
	{
		return geometry;
	}

	public void setGeometry ( String geometry )
	{
		this.geometry = geometry;
	}

	public String getDistance ( )
	{
		return distance;
	}

	public void setDistance ( String distance )
	{
		this.distance = distance;
	}

	public String getStart ( )
	{
		return start;
	}

	public void setStart ( String start )
	{
		this.start = start;
	}

	public String getEnd ( )
	{
		return end;
	}

	public void setEnd ( String end )
	{
		this.end = end;
	}

	public String getRoute_id ( )
	{
		return route_id;
	}

	public void setRoute_id ( String route_id )
	{
		this.route_id = route_id;
	}

	public String getLead_time ( )
	{
		return lead_time;
	}

	public void setLead_time ( String lead_time )
	{
		this.lead_time = lead_time;
	}

	public String getHt_id ( )
	{
		return ht_id;
	}

	public void setHt_id ( String ht_id )
	{
		this.ht_id = ht_id;
	}
}
