package jejusmarttour.vo;

public class HTPReviewVO
{
	// 코멘트 아이디
	private String comment_id;
	// 댓글 내용
	private String comment_subt;
	// 댓글 등록일
	private String comment_date;
	private String nickname;
	// 힐링여행상품 아이디
	private String ht_id;
	// 사용자 힐링여행상품 아이디
	private String u_ht_id;

	public String getComment_id ( )
	{
		return comment_id;
	}

	public void setComment_id ( String comment_id )
	{
		this.comment_id = comment_id;
	}

	public String getComment_subt ( )
	{
		return comment_subt;
	}

	public void setComment_subt ( String comment_subt )
	{
		this.comment_subt = comment_subt;
	}

	public String getComment_date ( )
	{
		return comment_date;
	}

	public void setComment_date ( String comment_date )
	{
		this.comment_date = comment_date;
	}

	public String getNickname ( )
	{
		return nickname;
	}

	public void setNickname ( String nickname )
	{
		this.nickname = nickname;
	}

	public String getHt_id ( )
	{
		return ht_id;
	}

	public void setHt_id ( String ht_id )
	{
		this.ht_id = ht_id;
	}

	public String getU_ht_id ( )
	{
		return u_ht_id;
	}

	public void setU_ht_id ( String u_ht_id )
	{
		this.u_ht_id = u_ht_id;
	}

}
