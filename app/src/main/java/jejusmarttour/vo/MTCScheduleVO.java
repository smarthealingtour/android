package jejusmarttour.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class MTCScheduleVO implements Serializable
{
	private String seq;
	private String hcnt_id;
	private String lead_time = null;
	private String distance = null;
	private String geomery;
	private ArrayList < String > memo = new ArrayList<String>();

	public String getSeq ( )
	{
		return seq;
	}

	public void setSeq ( String seq )
	{
		this.seq = seq;
	}

	public String getHcnt_id ( )
	{
		return hcnt_id;
	}

	public void setHcnt_id ( String hcnt_id )
	{
		this.hcnt_id = hcnt_id;
	}

	public String getLead_time ( )
	{
		return lead_time;
	}

	public void setLead_time ( String lead_time )
	{
		this.lead_time = lead_time;
	}

	public String getDistance ( )
	{
		return distance;
	}

	public void setDistance ( String distance )
	{
		this.distance = distance;
	}

	public String getGeomery ( )
	{
		return geomery;
	}

	public void setGeomery ( String geomery )
	{
		this.geomery = geomery;
	}

	public ArrayList < String > getMemo ( )
	{
		return memo;
	}

	public void setMemo ( ArrayList < String > memo )
	{
		this.memo = memo;
	}
}
