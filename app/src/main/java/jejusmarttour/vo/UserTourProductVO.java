package jejusmarttour.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import jejusmarttour.util.SmartTourUtils;


public class UserTourProductVO implements Serializable
{
	// 유저 힐링여행상품아이디
	private String u_ht_id;
	// 유저 상품명
	private String u_ht_name;
	// 닉네임
	private String nickname;
	// 유저 관련 장소
	private String u_ht_inv_hcnt;
	// 유저 간략소개
	private String u_ht_intro;
	// 유저 관련메모
	private String u_ht_inv_memo;
	// 유저 등록시간
	private String u_ht_create_time;
	// 유저 이미지
	private String u_ht_img;
	// 유저 감성코드
	private String u_ht_scd;
	// 유저 힐링코드
	private String u_ht_hcd;
	// ??
	private String u_ht_author;
	private String u_ht_status;
	//담은 횟수
	private String cart_count;
	//리뷰 횟수
	private String comment_count;

	private String ROWNUM;

	private String u_ht_inv_route;
	private ArrayList < KeywordVO > ht_hcd_name;
	private ArrayList < KeywordVO > ht_scd_name;
	
	

	public String getHt_create_time ( )
	{
		return u_ht_create_time;
	}

	public void setHt_create_time ( String u_ht_create_time )
	{
		this.u_ht_create_time = u_ht_create_time;
	}

	public String getHt_author ( )
	{
		return u_ht_author;
	}

	public void setHt_author ( String u_ht_author )
	{
		this.u_ht_author = u_ht_author;
	}

	public String getHt_status ( )
	{
		return u_ht_status;
	}

	public void setHt_status ( String u_ht_status )
	{
		this.u_ht_status = u_ht_status;
	}

	public String getNickname ( )
	{
		return nickname;
	}

	public void setNickname ( String nickname )
	{
		this.nickname = nickname;
	}

	public String getHt_id ( )
	{
		return u_ht_id;
	}

	public void setHt_id ( String ht_id )
	{
		this.u_ht_id = ht_id;
	}

	public String getHt_name ( )
	{
		return u_ht_name;
	}

	public void setHt_name ( String ht_name )
	{
		this.u_ht_name = ht_name;
	}

	public String getHt_intro ( )
	{
		return u_ht_intro;
	}

	public void setHt_intro ( String ht_intro )
	{
		this.u_ht_intro = ht_intro;
	}


	public String getHt_img ( )
	{
		return u_ht_img;
	}

	public void setHt_img ( String ht_img )
	{
		this.u_ht_img = ht_img;
	}

	public String getHt_hcd ( )
	{
		return u_ht_hcd;
	}

	public void setHt_hcd ( String ht_hcd )
	{
		this.u_ht_hcd = ht_hcd;
	}

	public String getHt_scd ( )
	{
		return u_ht_scd;
	}

	public void setHt_scd ( String ht_scd )
	{
		this.u_ht_scd = ht_scd;
	}

	public String getHt_inv_hcnt ( )
	{
		return u_ht_inv_hcnt;
	}

	public void setHt_inv_hcnt ( String ht_inv_hcnt )
	{
		this.u_ht_inv_hcnt = ht_inv_hcnt;
	}

	public String getHt_inv_memo ( )
	{
		return u_ht_inv_memo;
	}

	public void setHt_inv_memo ( String ht_inv_memo )
	{
		this.u_ht_inv_memo = ht_inv_memo;
	}

	public String getROWNUM ( )
	{
		return ROWNUM;
	}

	public void setROWNUM ( String rOWNUM )
	{
		ROWNUM = rOWNUM;
	}

	public String getHt_inv_route ( )
	{
		return u_ht_inv_route;
	}

	public void setHt_inv_route ( String ht_inv_route )
	{
		this.u_ht_inv_route = ht_inv_route;
	}

	public String getCart_count ( )
	{
		return cart_count;
	}

	public void setCart_count ( String cart_count )
	{
		this.cart_count = cart_count;
	}

	public String getComment_count ( )
	{
		return comment_count;
	}

	public void setComment_count ( String comment_count )
	{
		this.comment_count = comment_count;
	}

	public ArrayList < KeywordVO > getHt_hcd_name ( )
	{
		return ht_hcd_name;
	}

	public void setHt_hcd_name ( ArrayList < KeywordVO > ht_hcd_name )
	{
		this.ht_hcd_name = ht_hcd_name;
	}

	public ArrayList < KeywordVO > getHt_scd_name ( )
	{
		return ht_scd_name;
	}

	public void setHt_scd_name ( ArrayList < KeywordVO > ht_scd_name )
	{
		this.ht_scd_name = ht_scd_name;
	}

	// Array값 리턴
	public String [ ] getHt_inv_routeArray ( )
	{
		return SmartTourUtils.getValidateStringAraay(getHt_inv_route());
	}
	
	public String [ ] getHt_inv_memoArray ( )
	{
		return SmartTourUtils.getValidateStringAraay ( getHt_inv_memo ( ) );
	}

	public String [ ] getHt_imgArray ( )
	{
		return SmartTourUtils.getValidateStringAraay ( getHt_img ( ) );
	}
	
	public String [ ] getHt_inv_hcntArray ( )
	{
		return SmartTourUtils.getValidateStringAraay ( getHt_inv_hcnt ( ) );
	}
	
	public String [ ] getHt_scdArray ( )
	{
		return SmartTourUtils.getValidateStringAraay ( getHt_scd ( ) );
	}
	
	public String [ ] getHt_hcdArray ( )
	{
		return SmartTourUtils.getValidateStringAraay ( getHt_hcd ( ) );
	}
	
	//MAP
	public HashMap < String , String[] > getRelativeMemoMap()
	{
		return SmartTourUtils.getValidateRelativeMemoMap ( getHt_inv_memoArray() );
	}
}
