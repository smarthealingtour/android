package jejusmarttour.vo;

public class UpdateCartRequestVO
{
	private String type;
	private String email;
	private String productID;
	private boolean isUser = false;
	private boolean isContent = false;
	private boolean isCcnt = false;

	public String getType ( )
	{
		return type;
	}

	public void setType ( String type )
	{
		this.type = type;
	}

	public String getEmail ( )
	{
		return email;
	}

	public void setEmail ( String email )
	{
		this.email = email;
	}

	public String getProductID ( )
	{
		return productID;
	}

	public void setProductID ( String productID )
	{
		this.productID = productID;
	}

	public boolean isUser ( )
	{
		return isUser;
	}

	public void setUser ( boolean isUser )
	{
		this.isUser = isUser;
	}

	public boolean isContent ( )
	{
		return isContent;
	}

	public void setContent ( boolean isContent )
	{
		this.isContent = isContent;
	}

	public boolean isCcnt() {
		return isCcnt;
	}

	public void setIsCcnt(boolean isCcnt) {
		this.isCcnt = isCcnt;
	}
}
