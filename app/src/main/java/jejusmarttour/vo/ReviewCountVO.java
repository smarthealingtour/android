package jejusmarttour.vo;

/**
 * Created by HHS on 2015-10-15.
 */
public class ReviewCountVO
{
	private String count;
	private String ht_id;
	private String u_ht_id;

	public String getCount ( )
	{
		return count;
	}

	public void setCount ( String count )
	{
		this.count = count;
	}

	public String getHt_id ( )
	{
		return ht_id;
	}

	public void setHt_id ( String ht_id )
	{
		this.ht_id = ht_id;
	}

	public String getU_ht_id ( )
	{
		return u_ht_id;
	}

	public void setU_ht_id ( String u_ht_id )
	{
		this.u_ht_id = u_ht_id;
	}
}
