package jejusmarttour;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.tsengvn.typekit.TypekitContextWrapper;

/**
 * Created by Osy on 2017-12-26.
 */

//이 클래스를 상속받은 액티비티의 폰트가 바뀜
public class FontAppCompatActivity extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase){
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
