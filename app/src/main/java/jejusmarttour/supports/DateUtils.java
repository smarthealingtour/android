package jejusmarttour.supports;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class DateUtils {

    private DateUtils() {

    }

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.KOREA);

    public static String defaultFormat(Date date) {
        if (date == null) {
            return "null";
        }
        return dateFormat.format(date);
    }

}

