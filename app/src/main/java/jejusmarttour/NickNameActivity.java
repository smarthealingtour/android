package jejusmarttour;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.task.UserUpdateTask;
import jejusmarttour.user.UserUpdateResultVO;
import jejusmarttour.user.UserVO;
import syl.com.jejusmarttour.R;

public class NickNameActivity extends Activity implements OnClickListener , JSONObjectResult < UserUpdateResultVO >
{
	private static final String TAG = NickNameActivity.class.toString ( );

	private EditText editText;
	private Button nickBtn;
	private AlertDialog.Builder alert;
	private String nickName;
	private String userId;
	private String  modify;
	private UserUpdateTask userUpdateTask;
	private TextView nickNameDesc;

	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nick_name);

		userId = getIntent ( ).getStringExtra ( "userId" );
		modify = getIntent ( ).getStringExtra ( "mode" );


		editText = ( EditText ) findViewById ( R.id.nick_edit_text );
		nickBtn = ( Button ) findViewById ( R.id.nick_name_btn );
		nickNameDesc= (TextView) findViewById ( R.id.nick_name_desc );

		nickBtn.setOnClickListener ( this );

		if(modify.equals("modi")){
			nickNameDesc.setText(Html.fromHtml(CommonData.getUserProfile().getNickname() + "님" + "<br/>" + "변경할 닉네임을 입력해주세요"));
		}
	}

	@Override
	public void onClick ( View v )
	{
		if ( validateText ( ) )
		{
			nickName = editText.getText ( ).toString ( );
			// 서버에 닉네임 값보내고 성공하면 다음 액티비티로
			try
			{
				sendNickNameAndValidate ( );
			}
			catch ( UnsupportedEncodingException e )
			{
				Log.e(TAG , e.getMessage());
			}
		}
	}

	@Override
	public void setJSONObjectResult ( UserUpdateResultVO result )
	{
		if ( result == null )
		{
			Log.e ( TAG , "유저 정보 업데이트에 실패했습니다." );
			return;
		}

		if ( result.getResult ( ).equals ( "success" ) )
		{
			userUpdateTask.cancel ( true );
			gotoMainActivity ( );
            UserVO vo = new UserVO();
            vo.setNickname(result.getNickname());
            vo.setEmail(result.getEmail());
            CommonData.setUserVO(vo);
		}
		else if ( result.getResult ( ).equals ( "duplicate" ) )
		{
			removeText ( );
			Toast.makeText ( this , "이미 존재하는 닉네임입니다." , Toast.LENGTH_SHORT ).show ( );
		}
	}

	// 서버에 닉네임값 보냄
	private void sendNickNameAndValidate ( ) throws UnsupportedEncodingException
	{
		userUpdateTask = new UserUpdateTask ( this );
		String name = URLEncoder.encode ( nickName , "UTF-8" );
		String [ ] data = { userId , name };
		userUpdateTask.execute ( data );
	}

	// 다음 액티비티로 이동
	private void gotoMainActivity ( )
	{
		final Intent intent = new Intent ( NickNameActivity.this , MainActivity.class );
		startActivity ( intent );
		finish ( );
	}

	// EditText 텍스트 유효성 검사
	private boolean validateText ( )
	{
		if ( editText.getText ( ).toString ( ).equals ( "" ) )
		{
			openAlertDialog ( "닉네임을 입력해주십시요." );
			removeText ( );
			return false;
		}
		else if ( editText.getText ( ).length ( ) <= 1 || editText.getText ( ).length ( ) >= 11 )
		{
			openAlertDialog ( "닉네임은 2 - 10자로 정해주십시요." );
			removeText ( );
			return false;
		}

		return true;
	}

	// 팝업창 메세지
	private void openAlertDialog ( String message )
	{
		alert = new AlertDialog.Builder ( this );
		// alert.setTitle ( "NOTICE" );
		alert.setMessage ( message );
		alert.setNeutralButton ( "닫기" , new DialogInterface.OnClickListener ( )
		{
			@Override
			public void onClick ( DialogInterface dialog , int which )
			{
				removeText ( );
			}
		} );
		alert.show ( );
	}

	// EditText 텍스트 지우기
	private void removeText ( )
	{
		editText.setText ( "" );
	}
}