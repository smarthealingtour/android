package jejusmarttour.multitouchimageview;

import android.content.Intent;
import android.widget.RelativeLayout.LayoutParams;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import jejusmarttour.common.CommonData;
import syl.com.jejusmarttour.R;

public class MultiTouchImageViewActivity extends AppCompatActivity
{

	private static final String TAG = MultiTouchImageViewActivity.class.toString ( );
	private ImageView image;

	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate ( savedInstanceState );
		setContentView ( R.layout.activity_multi_touch_image_view );

		Intent intent = getIntent();
		String imageUrl = CommonData.getImageAddress(intent.getStringExtra("data"));

		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		image = ( ImageView ) findViewById ( R.id.multi_imageView );
		Glide.with(this).load ( imageUrl ).into(image);

	}
}
