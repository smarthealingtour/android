package jejusmarttour.adapter;

import android.support.v7.app.AppCompatActivity;

import jejusmarttour.annotation.SampleInfo;
import jejusmarttour.model.SampleItem;
import jejusmarttour.practices.DurationOfStayActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class PracticeListFragment extends SampleListFragment {

    private List<Class<? extends AppCompatActivity>> activityClasses =
            Arrays.<Class<? extends AppCompatActivity>>asList(
                    DurationOfStayActivity.class
            );

    @Override
    protected Iterable<SampleItem> items() {
        List<SampleItem> items = new ArrayList<>();

        for (Class<? extends AppCompatActivity> activityClass : activityClasses) {
            SampleInfo sampleInfo = activityClass.getAnnotation(SampleInfo.class);
            String title = sampleInfo.index() + " - " + sampleInfo.title();
            items.add(new SampleItem(title, activityClass));
        }

        return items;
    }

}

