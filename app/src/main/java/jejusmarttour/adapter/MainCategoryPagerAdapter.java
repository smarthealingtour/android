package jejusmarttour.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class MainCategoryPagerAdapter extends FragmentPagerAdapter {

    private enum Category {
        Examples(0, "기능", new ExampleListFragment()),
        Practices(1, "사례", new PracticeListFragment());

        private static final Category defaultCategory = Examples;

        Integer index;
        String title;
        Fragment fragment;

        Category(Integer index, String title, Fragment fragment) {
            this.index = index;
            this.title = title;
            this.fragment = fragment;
        }

        static Category findByIndex(Integer index) {
            for (Category category : values()) {
                if (category.index.equals(index)) {
                    return category;
                }
            }

            return defaultCategory;
        }
    }

    public MainCategoryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        return Category.findByIndex(position).fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Category.findByIndex(position).title;
    }

}