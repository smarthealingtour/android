package jejusmarttour.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import jejusmarttour.model.SampleItem;
import syl.com.jejusmarttour.R;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public abstract class SampleListFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_examples, null);

        final SampleListAdapter adapter = new SampleListAdapter(getContext());

        for (SampleItem item : items()) {
            adapter.addItem(item);
        }

        ListView listView = (ListView) view.findViewById(R.id.listView);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SampleItem item = (SampleItem) adapter.getItem(position);
                Intent intent = new Intent(getContext(), item.activityClass());
                startActivity(intent);
            }
        });

        return view;
    }

    abstract protected Iterable<SampleItem> items();

}
