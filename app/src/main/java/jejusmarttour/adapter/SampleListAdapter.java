package jejusmarttour.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import jejusmarttour.model.SampleItem;
import jejusmarttour.view.SampleItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class SampleListAdapter extends BaseAdapter {

    private Context context;
    private List<SampleItem> items;

    public SampleListAdapter(Context context) {
        this.context = context;
        items = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO 안 쓰는 메소드라서 일단 이렇게 해놨는데, 어쩌지?
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SampleItemView itemView;

        if (convertView == null) {
            itemView = new SampleItemView(context);
        } else {
            itemView = (SampleItemView) convertView;
        }

        itemView.setTitle(items.get(position).title());

        return itemView;
    }

    public void addItem(SampleItem item) {
        items.add(item);
        notifyDataSetChanged();
    }

}
