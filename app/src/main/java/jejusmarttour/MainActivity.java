package jejusmarttour;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.main_tourproduct.TourProductActivity;
import jejusmarttour.main_mytourproduct.MyTourProductActivity;
import jejusmarttour.main_spot.food_and_stay.FoodStaySpotActivity;
import jejusmarttour.main_spot.tourspot.TourSpotActivity;
import jejusmarttour.task.JSONArrayResult;
import jejusmarttour.task.KeywordParseTask;
import jejusmarttour.util.BackPressCloseHandler;
import jejusmarttour.vo.KeywordCategoryVO;
import syl.com.jejusmarttour.R;

public class MainActivity extends AppCompatActivity implements OnClickListener , JSONArrayResult < KeywordCategoryVO >{
	private static final String TAG = MainActivity.class.toString ( );

	private BackPressCloseHandler backPressCloseHandler;
	private ArrayList < KeywordCategoryVO > keywordArrayList;
	private KeywordParseTask keywordParseTask;

	private int canLoadView = 0;

	@Override
	protected void onCreate ( Bundle savedInstanceState ){
		super.onCreate ( savedInstanceState );

		initView ( );
		initActionBar ( );

		backPressCloseHandler = new BackPressCloseHandler ( this );

		keywordArrayList = new ArrayList <> ( );

		//키워드 얻어오는 쓰레드 이지만 사용하지 않아서 실행이 되어도 사용하지 않아서 얻어올 수 있는게 없음.
		keywordParseTask = new KeywordParseTask ( this );
		keywordParseTask.execute ( "A" ); // 분류목록(A: 힐링, P: 감성, D: 관광)
	}

	private void initView ( ){
		setContentView ( R.layout.activity_main2 );
		RelativeLayout healing_tour_product_btn = (RelativeLayout) findViewById(R.id.healing_tour_product_btn);
		RelativeLayout my_tour_course_btn = (RelativeLayout) findViewById(R.id.my_tour_course_btn);
		RelativeLayout healing_contents_layout = (RelativeLayout) findViewById(R.id.healing_contents_layout);
		RelativeLayout food_contents_layout = (RelativeLayout) findViewById(R.id.food_contents_layout);

		healing_tour_product_btn.setOnClickListener ( this );
		my_tour_course_btn.setOnClickListener ( this );
		healing_contents_layout.setOnClickListener ( this );
		food_contents_layout.setOnClickListener ( this );
	}

	// 커스텀 액션바 적용
	private void initActionBar ( ){
		ActionBar actionBar = getSupportActionBar ( );
		actionBar.setDisplayShowHomeEnabled ( false );
		actionBar.setDisplayShowTitleEnabled ( false );

		// 액션바 그림자 지우기
		actionBar.setElevation ( 0 );

		LayoutInflater mInflater = LayoutInflater.from ( this );

		View customView = mInflater.inflate ( R.layout.actionbar_default , null );
		TextView titleTextView = customView.findViewById ( R.id.title_text );
		titleTextView.setText ( R.string.app_name );

		LinearLayout keywordBtn = customView.findViewById ( R.id.keyword_btn );

		final Intent intent = new Intent ( this , SelectKeywordActivity.class ); // 맞춤키워드

		keywordBtn.setOnClickListener (view -> startActivity ( intent ));
		actionBar.setCustomView ( customView );
		actionBar.setDisplayShowCustomEnabled ( true );
	}

	@Override
	public void onClick ( View v ){
		if ( v.getId ( ) == R.id.healing_tour_product_btn ){	 // 여행상품
			Intent intent = new Intent ( this , TourProductActivity.class );
			startActivity ( intent );
		}
		else if ( v.getId ( ) == R.id.my_tour_course_btn ){		// 나의 여행 상품
			Intent intent = new Intent ( this , MyTourProductActivity.class );
			startActivity ( intent );
		}
		else if ( v.getId ( ) == R.id.healing_contents_layout ){	// 관광 장소
			Intent intent = new Intent ( this , TourSpotActivity.class );
			startActivity ( intent );
		}
		else if ( v.getId ( ) == R.id.food_contents_layout ){		 // 숙식 장소
			Intent intent = new Intent ( this , FoodStaySpotActivity.class );
			startActivity ( intent );
		}
	}

	// keywordParseTask.execute()가 실행완료된다음 실행되는 메서드
	@Override
	public void setJSONArrayResult ( ArrayList < KeywordCategoryVO > resultList ){
		keywordParseTask.cancel ( true );

		if ( resultList == null )
			return;

		keywordArrayList.addAll ( resultList );

		if ( canLoadView == 2 ){ 	  // 관광
			CommonData.setKeywordArrayList ( keywordArrayList );
		}
		else if ( canLoadView == 0 ){ // 힐링
			keywordParseTask = new KeywordParseTask ( this );
			keywordParseTask.execute ( "P" );
			canLoadView = 1;
		}
		else if ( canLoadView == 1 ){ // 감성
			keywordParseTask = new KeywordParseTask ( this );
			keywordParseTask.execute ( "D" );
			canLoadView = 2;
		}
	}

	// 뒤로가기 두번 종료
	@Override
	public void onBackPressed ( ){
		backPressCloseHandler.onBackPressed ( );
	}

	@Override
	public boolean onCreateOptionsMenu ( Menu menu ){
		getMenuInflater ( ).inflate ( R.menu.main , menu );
		return true;
	}

	//액션바의 옵션선택시
	@Override
	public boolean onOptionsItemSelected ( MenuItem item ){
		int id = item.getItemId ( );
		if ( id == R.id.action_settings ){
			CommonData.gotoMyInfo ( this );
			return true;
		}
		else if( id == R.id.vr_btn){
            Intent intent = new Intent(this, VrActivity.class);
            startActivity(intent);
		}

		return super.onOptionsItemSelected ( item );
	}
}
