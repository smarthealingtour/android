package jejusmarttour.map;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jejusmarttour.vo.MTCScheduleVO;
import jejusmarttour.vo.MTCSmartTourProductsVO;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import ktmap.android.map.Bounds;
import ktmap.android.map.Coord;
import ktmap.android.map.layer.OverlayLayer;
import ktmap.android.map.overlay.Image;
import ktmap.android.map.overlay.Marker;
import ktmap.android.map.overlay.Overlay;
import ktmap.android.map.overlay.OverlayEventListener;
import ktmap.android.map.vector.Circle;
import ktmap.android.map.vector.Geometry;
import ktmap.android.map.vector.Polygon;
import ktmap.android.map.vector.Polyline;
import syl.com.jejusmarttour.R;


/**
 * Created by User on 2014-07-23.
 */
public class overlay_ extends OverlayLayer{

    Context mContext;

    MarkerLayer markerlayer = new MarkerLayer();
    VectorLayer vectorLayer = new VectorLayer();
    public VectorLayer_ AreaCicle = new VectorLayer_();
    RoutePathVectorLayer pathVectorLayer = new RoutePathVectorLayer();

    List<JSONObject>  markerInfoList = null;

    List<JSONObject>  vectorInfoList = null;
    FrameLayout info = null;
    int px;

    //정보창
    TextView content_name;
    TextView content_addr;
    TextView content_type;

    RadiusMarkerLayer radiusMarkerLayer = new RadiusMarkerLayer();
    RadiusVectorLayer radiusVectorLayer = new RadiusVectorLayer();

    List<JSONObject>  radiusMarkerinfoList = null;
    List<JSONObject>  radiusVectorinfoList = null;

    List<JSONObject>  multiRouteinfoList = null;
    private JSONObject mResult;

    public boolean radiusOn = false;
    public boolean isRouteShow = false;

    OnImageLayer imagelayer = new OnImageLayer();

    public overlay_(Context context, MyMapView mMyMapView) {
        super(context, mMyMapView);
        this.mContext = context;
        oinit();
    }

    public overlay_(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    private void oinit() {
        this.getOverlays().add(markerlayer);
        this.getOverlays().add(radiusMarkerLayer);
        this.getVectors().add(vectorLayer);
        this.getVectors().add(radiusVectorLayer);
        this.getVectors().add(AreaCicle);
        this.getVectors().add(pathVectorLayer);
        this.getOverlays().add(imagelayer);
    }

    public void setInfo(FrameLayout info, int px) {
        this.info = info;
        this.px = px;

        content_name = (TextView)info.findViewById(R.id.content_name);
        content_addr = (TextView)info.findViewById(R.id.content_addr);
        content_type = (TextView)info.findViewById(R.id.content_type);
    }

    public void closeInfo() {
        if(info != null)
            info.setTranslationY(px);
    }

    // 힐링여행상품 > 일정보기 > 경로선택
    // 두 지점간 경로 보기
    public void showBtnSpotRoute(RouteVO vo, ArrayList<ScheduleVO> schedule) throws JSONException {
        allOverlayRemove();

        List<String[]> SEp = drawRoutePath(vo);

        Marker marker;
        Drawable drawable = null;

        markerInfoList = null;
        markerInfoList = new ArrayList<>();

        for(int i = 0; i < schedule.size(); i++) {
            if(schedule.get(i).getHt_schd_id().equals(vo.getStart())
                    ||  schedule.get(i).getHt_schd_id().equals(vo.getEnd())){

                String[] x_y = null;
                if(schedule.get(i).getHt_schd_id().equals(vo.getStart()))
                    x_y = SEp.get(0);
                if(schedule.get(i).getHt_schd_id().equals(vo.getEnd()))
                    x_y = SEp.get(1);

                Coord coord = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                marker = new Marker(coord);

                JSONObject data = new JSONObject();

                if(schedule.get(i).getHealingContentsVO() != null) {
                    drawable = getResources().getDrawable(R.drawable.hcnt_marker);
                    data.put("name", schedule.get(i).getHealingContentsVO().getHcnt_name());
                    data.put("addr", schedule.get(i).getHealingContentsVO().getHcnt_addr());
                    data.put("type", schedule.get(i).getHealingContentsVO().getHcnt_tel());

                }
                else {
                    drawable = getResources().getDrawable(R.drawable.ccnt_marker);
                    data.put("name", schedule.get(i).getSightSeeingContentsVO().getCcnt_name());
                }

                markerInfoList.add(data);

                marker.setIcon(drawable);
                marker.setTitle("");
                markerlayer.addItem(marker);
            }

            for (int index = 0; index < markerlayer.size(); index++) {
                markerlayer.getMarker(index).dispatchOverlayEvent(getMarkerLayerEventListener(index));
            }

            invalidate();
            isRouteShow = true;
        }
    }

    // 여행상품 경로 그리기
    private List<String[]> drawRoutePath(RouteVO vo) {

        List<String[]> SEp = new ArrayList<>();
        ArrayList<Float> xValue = new ArrayList<>();
        ArrayList<Float> yValue = new ArrayList<>();

        if (vo.getGeometry() != null && ! vo.equals("LINESTRING EMPTY")) {
            String[] array =vo.getGeometry().replace("LINESTRING", "").replace("(", "").replace(")", "").split(",");
            Coord[] line = new Coord[array.length];

            for (int pn = 0; pn < array.length; pn++) {
                String[] x_y = array[pn].toString().trim().split(" ");
                if (x_y.length > 1) {
                    if(pn == 0 || pn == array.length - 1 )
                        SEp.add(x_y);
                    line[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                    xValue.add(line[pn].getX());
                    yValue.add(line[pn].getY());
                }
            }

            Polyline pline = new Polyline(line, Color.rgb(255, 0, 0), 3);
            pline.setStrokeWidth(4);
            pline.setStrokeColor(Color.rgb(255, 0, 0));
            pathVectorLayer.addItem(pline.getVector());

            resultGeometryFitMap(xValue, yValue);
        }
        return  SEp;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void setHtourInGeometry(JSONArray resultt, FrameLayout info, int px, boolean isHtour, ArrayList<ScheduleVO> scheduleVOArrayList, int schedule) throws JSONException {
        if(resultt == null) {
            return;
        }

//        System.out.println("view name : " + parentView.getContext().getClass().getCanonicalName());
        imagelayer.removeAll();
        vectorLayer.removeAll();
        markerlayer.removeAll();
        radiusMarkerLayer.removeAll();
        radiusVectorLayer.removeAll();
        AreaCicle.removeAll(); // 반경검색 바운드 표시
        pathVectorLayer.removeAll();

        markerInfoList = null;
        vectorInfoList = null;

        Marker marker;
        Drawable drawable = null;

        this.info = info;
        this.px = px;

        content_name = (TextView)info.findViewById(R.id.content_name);
        content_addr = (TextView)info.findViewById(R.id.content_addr);
        content_type = (TextView)info.findViewById(R.id.content_type);

        markerInfoList = new ArrayList<>();
        vectorInfoList = new ArrayList<>();

        JSONArray result = new JSONArray();
        Map<String, ArrayList<Float>> map = null;

        // 콘텐츠 상세보기 일 경우
        if(resultt.length() > 0) {
            if(resultt.getJSONObject(0).has("g_id") && resultt.getJSONObject(0).has("hcnt_type")) {
                JSONObject temp = resultt.getJSONObject(0);
                JSONObject target = new JSONObject();
                target.put("name", temp.getString("name"));
                target.put("type", temp.getString("type"));
                if(temp.has("hcnt_addr") && !temp.isNull("hcnt_addr"))
                    target.put("address", temp.getString("hcnt_addr"));
                if(temp.has("hcnt_tel") && !temp.isNull("hcnt_tel"))
                    target.put("tel", temp.getString("hcnt_tel"));

                target.put("Center", temp.getString("Center"));
                target.put("geometry", temp.getJSONObject("geometry"));

                if(resultt.length() > 1)
                    result = resultt.getJSONArray(1);
                result.put(target);
            }
            else {
                result = resultt;
            }
        }

        ArrayList<Float> xValue = null;
        ArrayList<Float> yValue = null;

        if(isHtour && scheduleVOArrayList != null) {
            // 여행상품 경로 그리기
            map = drawRoutePath(scheduleVOArrayList, schedule, resultt);
            xValue = map.get("xValue");
            yValue = map.get("yValue");
        }
        else {
            xValue = new ArrayList<>();
            yValue = new ArrayList<>();
        }


        for (int i = 0; i < result.length(); i++) {

            if (result.getJSONObject(i).getString("type").equals("point")) { //점형
                markerInfoList.add(result.getJSONObject(i));
                if(result.getJSONObject(i).has("geometry") && !result.getJSONObject(i).isNull("geometry")){
                    String[] x_y = result.getJSONObject(i).get("geometry").toString().split(" ");

                    Coord coord = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
//					Coord trans_coord = mMapView.getProjection().transCoordination(1, 9, coord);

                    marker = new Marker(coord);

                    xValue.add(coord.getX());
                    yValue.add(coord.getY());

                    if(isHtour) // 여행상품일 경우 마커 순서 표시
                    {
                        String icon = "@drawable/location_icon_" + String.valueOf(i+1);
                        String packName = mContext.getPackageName();
                        int resID = getResources().getIdentifier(icon, "drawable", packName);
                        drawable = getResources().getDrawable(resID);
                    }else {
                        if(result.getJSONObject(i).has("hcnt_id")) {
                            drawable = getResources().getDrawable(R.drawable.hcnt_marker);
                        }
                        else if(result.getJSONObject(i).has("ccnt_id"))
                            drawable = getResources().getDrawable(R.drawable.ccnt_marker);
                        else
                            drawable = getResources().getDrawable(R.drawable.hcnt_marker);
                    }
//                    drawable.invalidateSelf();
                    marker.setIcon(drawable);
                    marker.setTitle("");
                    markerlayer.addItem(marker);
                }
            }
            else {

                if(result.getJSONObject(i).getString("type").equals("line") || result.getJSONObject(i).getString("type").equals("zone")) {
                    if(result.getJSONObject(i).getString("type").equals("line")) {
                        if(result.getJSONObject(i).has("geometry") && !result.getJSONObject(i).isNull("geometry")) {
                            vectorInfoList.add(result.getJSONObject(i));
                            JSONArray shape = result.getJSONObject(i).getJSONObject("geometry").getJSONArray("SHAPE");

                            for (int rn = 0; rn < shape.length(); rn++) {
                                JSONArray inr = shape.getJSONArray(rn);
                                Coord[] line = new Coord[inr.length()];

                                for (int pn = 0; pn < inr.length(); pn++) {
                                    String[] x_y = inr.getString(pn).split(" ");
                                    if (x_y.length > 1) {
                                        line[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                        if(!isHtour){
                                            xValue.add(line[pn].getX());
                                            yValue.add(line[pn].getY());
                                        }
                                    }
                                }
                                Polyline pline = new Polyline(line, Color.rgb(227, 113, 113), 8);
                                pline.setStrokeWidth(3);
                                pline.setStrokeColor(Color.rgb(227, 113, 113));
                                vectorLayer.addItem(pline.getVector());
                            }
                        }
                    }
                    if(result.getJSONObject(i).getString("type").equals("zone")) { // 면형
                        if(result.getJSONObject(i).has("geometry") && !result.getJSONObject(i).isNull("geometry")) {
                            vectorInfoList.add(result.getJSONObject(i));
                            JSONArray shape = result.getJSONObject(i).getJSONObject("geometry").getJSONArray("SHAPE");

                            for (int rn = 0; rn < shape.length(); rn++) {
                                JSONArray inr = shape.getJSONArray(rn);
                                Coord[] zone = new Coord[inr.length()];

                                for(int pn = 0; pn < inr.length(); pn++) {
                                    String[] x_y = inr.getString(pn).split(" ");
                                    if(x_y.length > 1) {
                                        zone[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                        if(!isHtour){
                                            xValue.add(zone[pn].getX());
                                            yValue.add(zone[pn].getY());
                                        }
                                    }
                                }
                                Polygon polygon = new Polygon(zone, Color.argb(7, 227, 113, 113), 100, Color.rgb(255, 192, 192), 50,5);
                                vectorLayer.addItem(polygon.getVector());
                            }
                        }
                    }
                    // 라인,존 센터 포인트 세팅
                    if(result.getJSONObject(i).has("Center") && !result.getJSONObject(i).isNull("Center")){
                        String[] x_y = result.getJSONObject(i).get("Center").toString().split(" ");

                        Coord coord = new Coord ( Float.parseFloat(x_y[0]), Float.parseFloat(x_y[1]) );
                        marker = new Marker(coord);


                        if(isHtour) // 여행상품일 경우 마커 순서 표시
                        {
                            String icon = "@drawable/location_icon_" + String.valueOf(i+1);
                            String packName = mContext.getPackageName();
                            int resID = getResources().getIdentifier(icon, "drawable", packName);
                            drawable = getResources().getDrawable(resID);
                        }
                        else {
                            drawable = getResources().getDrawable(R.drawable.hcnt_marker);
                        }

                        marker.setIcon(drawable);

                        marker.setTitle("");
                        markerlayer.addItem(marker);
                        markerInfoList.add(result.getJSONObject(i));
                    }
                    else // 라인, 면 (지오메트리 없는 거)
                    {
                        if(isHtour) {
                            String[] x_y = null;

                            if(result.getJSONObject(i).getInt("seq") == 1 && result.length() > 1) {
                                if(result.getJSONObject(i+1).has("route")) {

                                    RouteVO vo = util.setRouteVO(result.getJSONObject(i+1).getJSONObject("route"));
                                    if(util.isRouteExist(vo)) {
                                        String[] array = scheduleVOArrayList.get(i+1).getRoute().getGeometry().replace("LINESTRING", "").replace("(", "").replace(")", "").split(",");
                                        x_y =  array[0].toString().trim().split(" ");
                                    }
                                }
                            }
                            else
                            {
                                if(result.getJSONObject(i).has("route")) {
                                    RouteVO vo = util.setRouteVO(result.getJSONObject(i).getJSONObject("route"));

                                    if(util.isRouteExist(vo)) {
                                        String[] array = scheduleVOArrayList.get(i).getRoute().getGeometry().replace("LINESTRING", "").replace("(", "").replace(")", "").split(",");
                                        x_y =  array[array.length-1].toString().trim().split(" ");
                                    }
                                }

                            }

                            if(x_y != null) {
                                if (x_y.length > 1) {
                                    Coord coord = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                    marker = new Marker(coord);

                                    if(isHtour) // 여행상품일 경우 마커 순서 표시
                                    {
                                        String icon = "@drawable/location_icon_" + String.valueOf(i+1);
                                        String packName = mContext.getPackageName();
                                        int resID = getResources().getIdentifier(icon, "drawable", packName);
                                        drawable = getResources().getDrawable(resID);
                                    }
                                    else {
                                        drawable = getResources().getDrawable(R.drawable.hcnt_marker);
                                    }

                                    marker.setIcon(drawable);

                                    marker.setTitle("");
                                    markerlayer.addItem(marker);
                                    markerInfoList.add(result.getJSONObject(i));
                                }
                            }
                        }
                    }
                }
            }
        }

        for (int index = 0; index < markerlayer.size(); index++) {
            markerlayer.getMarker(index).dispatchOverlayEvent(getMarkerLayerEventListener(index));
        }

//        if(!isHtour)
        resultGeometryFitMap(xValue, yValue);
        invalidate();

        if(schedule > 0)
            isRouteShow = true;
    }

    // 여행상품 경로 그리기
    private Map<String, ArrayList<Float>> drawRoutePath(ArrayList<ScheduleVO> scheduleVOArrayList, int schedule,  JSONArray resultt) {
        ArrayList<Float> xValue = new ArrayList<>();
        ArrayList<Float> yValue = new ArrayList<>();

        Map<String, ArrayList<Float>> map = new HashMap<>();

        for(int m = 0; m < resultt.length(); m++) {
            for(int i = 0; i < scheduleVOArrayList.size(); i++) {

                try {
                    if(schedule > 0)
                        if(resultt.getJSONObject(m).getInt("day") != Integer.parseInt(scheduleVOArrayList.get(i).getDay())
                                || resultt.getJSONObject(m).getInt("seq") != Integer.parseInt(scheduleVOArrayList.get(i).getSeq()))//
                            continue;//

                    if (scheduleVOArrayList.get(i).getRoute() != null) {
                        if (scheduleVOArrayList.get(i).getRoute().getGeometry() != null
                                && !scheduleVOArrayList.get(i).getRoute().getGeometry().equals("LINESTRING EMPTY")
                                && !scheduleVOArrayList.get(i).getRoute().getGeometry().equals("")) {

                            String[] array = scheduleVOArrayList.get(i).getRoute().getGeometry().replace("LINESTRING", "").replace("(", "").replace(")", "").split(",");
                            Coord[] line = new Coord[array.length];

                            for (int pn = 0; pn < array.length; pn++) {
                                String[] x_y = array[pn].toString().trim().split(" ");
                                if (x_y.length > 1) {
                                    line[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                    xValue.add(line[pn].getX());
                                    yValue.add(line[pn].getY());
                                }
                            }

                            Polyline pline = new Polyline(line, Color.rgb(255, 0, 0), 3);
                            pline.setStrokeWidth(4);
                            pline.setStrokeColor(Color.rgb(255, 0, 0));
                            pathVectorLayer.addItem(pline.getVector());
                        }

                        map.put("xValue", xValue);
                        map.put("yValue", yValue);
    //                resultGeometryFitMap(xValue, yValue);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return map;
    }

    private Map<String, ArrayList<Float>> drawRoutePath(MTCSmartTourProductsVO tourProductsVO, JSONArray result, boolean isRouteShow) {

        ArrayList<MTCScheduleVO> scheduleVOArrayList = tourProductsVO.getMtcScheduleArrayList();

        ArrayList<Float> xValue = new ArrayList<>();
        ArrayList<Float> yValue = new ArrayList<>();

        Map<String, ArrayList<Float>> map = new HashMap<>();

        if(isRouteShow){
            if(result.length() > 1)
               result = util.RemoveJSONArray(result, 1);
        }

        for(int k = 0; k < result.length(); k++) {

            for(int i = 0; i < scheduleVOArrayList.size(); i++) {
                try {
                    String id = null;
                    if( result.getJSONObject(k).has("inv_hcnt_id") && !result.getJSONObject(k).isNull("inv_hcnt_id")) {
                        id = result.getJSONObject(k).getString("inv_hcnt_id");
                        if(id.equals(scheduleVOArrayList.get(i).getHcnt_id())) {
                            if(util.isRouteExist(scheduleVOArrayList.get(i))) {
                                String[] array = scheduleVOArrayList.get(i).getGeomery().replace("LINESTRING", "").replace("(", "").replace(")", "").split(",");
                                Coord[] line = new Coord[array.length];

                                for (int pn = 0; pn < array.length; pn++) {
                                    String[] x_y = array[pn].toString().trim().split(" ");
                                    if (x_y.length > 1) {
                                        line[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                        xValue.add(line[pn].getX());
                                        yValue.add(line[pn].getY());
                                    }
                                }

                                Polyline pline = new Polyline(line, Color.rgb(255, 0, 0), 3);
                                pline.setStrokeWidth(4);
                                pline.setStrokeColor(Color.rgb(255, 0, 0));
                                pathVectorLayer.addItem(pline.getVector());
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        map.put("xValue", xValue);
        map.put("yValue", yValue);
        return map;
    }

    public void setMyHtourScheduleDay(JSONArray result, MTCSmartTourProductsVO tourProductsVO, FrameLayout info, int px, boolean isRouteShow) throws JSONException {
        allOverlayRemove();

        Marker marker;
        Drawable drawable = null;

        this.info = info;
        this.px = px;

        content_name = (TextView)info.findViewById(R.id.content_name);
        content_addr = (TextView)info.findViewById(R.id.content_addr);
        content_type = (TextView)info.findViewById(R.id.content_type);

        markerInfoList = new ArrayList<>();
        vectorInfoList = new ArrayList<>();

        Map<String, ArrayList<Float>> map = null;
        ArrayList<Float> xValue;
        ArrayList<Float> yValue;


        if(tourProductsVO.getMtcScheduleArrayList() != null) {
            map = drawRoutePath(tourProductsVO, result, isRouteShow);
            xValue = map.get("xValue");
            yValue = map.get("yValue");
        }else {
            xValue = new ArrayList<>();
            yValue = new ArrayList<>();
        }


        for (int i = 0; i < result.length(); i++) {

            // center 포인트 세팅
            if(result.getJSONObject(i).has("Center") && !result.getJSONObject(i).isNull("Center")){
                String[] x_y = result.getJSONObject(i).get("Center").toString().split(" ");

                Coord coord = new Coord ( Float.parseFloat(x_y[0]), Float.parseFloat(x_y[1]) );
                marker = new Marker(coord);

                if(!isRouteShow) {
                    String icon = "@drawable/location_icon_" + String.valueOf(i+1);
                    String packName = mContext.getPackageName();
                    int resID = getResources().getIdentifier(icon, "drawable", packName);
                    drawable = getResources().getDrawable(resID);
                }
                else {
                    if(result.getJSONObject(i).has("inv_hcnt_id") && !result.getJSONObject(i).isNull("inv_hcnt_id"))
                        drawable = getResources().getDrawable(R.drawable.hcnt_marker);
                    if(result.getJSONObject(i).has("inv_ccnt_id") && !result.getJSONObject(i).isNull("inv_ccnt_id"))
                        drawable = getResources().getDrawable(R.drawable.ccnt_marker);
                }

                marker.setIcon(drawable);

                marker.setTitle("");
                markerlayer.addItem(marker);
                markerInfoList.add(result.getJSONObject(i));

                if(result.getJSONObject(i).getString("type").equals("point")){
                    xValue.add(coord.getX());
                    yValue.add(coord.getY());
                }
            }
            else // 지오메트리 없는 거( 라인,존)
            {

            }

            if(result.getJSONObject(i).getString("type").equals("line") || result.getJSONObject(i).getString("type").equals("zone")) {
                if(result.getJSONObject(i).getString("type").equals("line")) {
                    if(result.getJSONObject(i).has("geometry") && !result.getJSONObject(i).isNull("geometry")) {
                        vectorInfoList.add(result.getJSONObject(i));
                        JSONArray shape = result.getJSONObject(i).getJSONObject("geometry").getJSONArray("SHAPE");

                        for (int rn = 0; rn < shape.length(); rn++) {
                            JSONArray inr = shape.getJSONArray(rn);
                            Coord[] line = new Coord[inr.length()];

                            for (int pn = 0; pn < inr.length(); pn++) {
                                String[] x_y = inr.getString(pn).split(" ");
                                if (x_y.length > 1) {
                                    line[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                    xValue.add(line[pn].getX());
                                    yValue.add(line[pn].getY());
                                }
                            }
                            Polyline pline = new Polyline(line, Color.rgb(227, 113, 113), 8);
                            pline.setStrokeWidth(3);
                            pline.setStrokeColor(Color.rgb(227, 113, 113));
                            vectorLayer.addItem(pline.getVector());
                        }
                    }
                }
                if(result.getJSONObject(i).getString("type").equals("zone")) { // 면형
                    if(result.getJSONObject(i).has("geometry") && !result.getJSONObject(i).isNull("geometry")) {
                        vectorInfoList.add(result.getJSONObject(i));
                        JSONArray shape = result.getJSONObject(i).getJSONObject("geometry").getJSONArray("SHAPE");

                        for (int rn = 0; rn < shape.length(); rn++) {
                            JSONArray inr = shape.getJSONArray(rn);
                            Coord[] zone = new Coord[inr.length()];

                            for(int pn = 0; pn < inr.length(); pn++) {
                                String[] x_y = inr.getString(pn).split(" ");
                                if(x_y.length > 1) {
                                    zone[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                    xValue.add(zone[pn].getX());
                                    yValue.add(zone[pn].getY());
                                }
                            }
                            Polygon polygon = new Polygon(zone, Color.argb(7, 227, 113, 113), 100, Color.rgb(255, 192, 192), 50,5);
                            vectorLayer.addItem(polygon.getVector());
                        }
                    }
                }
            }
        }


        resultGeometryFitMap(xValue, yValue);

        for (int index = 0; index < markerlayer.size(); index++) {
            markerlayer.getMarker(index).dispatchOverlayEvent(getMarkerLayerEventListener(index));
        }

        invalidate();

    }


    public void allOverlayRemove() {
        setVisibility(View.INVISIBLE);
        imagelayer.removeAll();
        vectorLayer.removeAll();
        markerlayer.removeAll();
        radiusMarkerLayer.removeAll();
        radiusVectorLayer.removeAll();
        AreaCicle.removeAll(); // 반경검색 바운드 표시
        pathVectorLayer.removeAll();
        setVisibility(View.VISIBLE);
    }



    public void setRadiusResult(JSONObject result, String distance) throws JSONException {
        allOverlayRemove();

        radiusMarkerinfoList = null;
        radiusVectorinfoList = null;

        Marker marker;
        Drawable drawable = null;

        radiusMarkerinfoList = new ArrayList<>();
        radiusVectorinfoList = new ArrayList<>();

        // 영역 드로잉
        Coord mapCenter = mMapView.getMapCenter();
        Coord circle = new Coord(mapCenter.getX(), mapCenter.getY());
        Circle c1 = new Circle(circle, Color.RED, Float.parseFloat(distance),15, 5);

        AreaCicle.addItem(c1.getVector());

        Bounds circleBounds = new Bounds (
               (mapCenter.getX() - Float.parseFloat(distance)),
               (mapCenter.getY() - Float.parseFloat(distance)),
               (mapCenter.getX() + Float.parseFloat(distance)),
                (mapCenter.getY() + Float.parseFloat(distance)) );

        // 반경검색 결과 그리기
        if(!result.isNull("hcnt")) { // 힐링명소(포인트)
            for(int i = 0; i < result.getJSONArray("hcnt").length(); i++) {
                JSONObject temp = result.getJSONArray("hcnt").getJSONObject(i);
                if( ( temp.has("hcnt_coord_x") && !temp.isNull("hcnt_coord_x") )  && ( temp.has("hcnt_coord_y") && !temp.isNull("hcnt_coord_y") )){

                    Float x = ((Double) temp.get("hcnt_coord_x")).floatValue();
                    Float y = ((Double) temp.get("hcnt_coord_y")).floatValue();
                    Coord coord = new Coord ( x, y );

                    marker = new Marker(coord);
                    drawable = getResources().getDrawable(R.drawable.hcnt_marker);
                    marker.setIcon(drawable);

                    marker.setTitle("");
                    radiusMarkerLayer.addItem(marker);
                    radiusMarkerinfoList.add(temp);
                }
            }
        }
        else if(!result.isNull("ccnt")) {
            for(int i = 0; i < result.getJSONArray("ccnt").length(); i++) {
                JSONObject temp = result.getJSONArray("ccnt").getJSONObject(i);
                if( ( temp.has("ccnt_coord_x") && !temp.isNull("ccnt_coord_x") )  && ( temp.has("ccnt_coord_y") && !temp.isNull("ccnt_coord_y") )){

                    Float x = ((Double) temp.get("ccnt_coord_x")).floatValue();
                    Float y = ((Double) temp.get("ccnt_coord_y")).floatValue();

                    Coord coord = new Coord ( x, y );
                    marker = new Marker(coord);
                    drawable = getResources().getDrawable(R.drawable.ccnt_marker);
                    marker.setIcon(drawable);

                    marker.setTitle("");
                    radiusMarkerLayer.addItem(marker);
                    radiusMarkerinfoList.add(temp);
                }
            }
        }
        else if(!result.isNull("hcnt_linezone")) { // 힐링명소(라인,존)
            for(int i = 0; i < result.getJSONArray("hcnt_linezone").length(); i++) {
                JSONObject temp = result.getJSONArray("hcnt_linezone").getJSONObject(i);
                if(temp.get("hcnt_type").equals("line")) {
                    // 라인
                    if(temp.has("SHAPE") && !temp.isNull("SHAPE")) {
                        radiusVectorinfoList.add(temp);
                        JSONArray shape = temp.getJSONArray("SHAPE");

                        for (int rn = 0; rn < shape.length(); rn++) {
                            JSONArray inr = shape.getJSONArray(rn);
                            Coord[] line = new Coord[inr.length()];

                            for (int pn = 0; pn < inr.length(); pn++) {
                                String[] x_y = inr.getString(pn).split(" ");
                                if (x_y.length > 1) {
                                    line[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                }
                            }
                            Polyline pline = new Polyline(line, Color.rgb(227, 113, 113), 8);
                            pline.setStrokeWidth(3);
                            pline.setStrokeColor(Color.rgb(227, 113, 113));
                            radiusVectorLayer.addItem(pline.getVector());
                        }
                    }
                }
                if(temp.get("hcnt_type").equals("zone")) {
                    // 존
                    if(temp.has("SHAPE") && !temp.isNull("SHAPE")) {
                        radiusVectorinfoList.add(temp);
                        JSONArray shape = temp.getJSONArray("SHAPE");

                        for (int rn = 0; rn < shape.length(); rn++) {
                            JSONArray inr = shape.getJSONArray(rn);
                            Coord[] zone = new Coord[inr.length()];

                            for(int pn = 0; pn < inr.length(); pn++) {
                                String[] x_y = inr.getString(pn).split(" ");
                                if(x_y.length > 1) {
                                    zone[pn] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                                }

                                if(pn == inr.length()-1)
                                    break;
                            }
                            Polygon polygon = new Polygon(zone, Color.argb(7, 227, 113, 113), 100, Color.rgb(255, 192, 192), 70,5);
                            radiusVectorLayer.addItem(polygon.getVector());
                        }
                    }
                }

                // 라인,존 센터 포인트 세팅
                if(temp.has("Center") && !temp.isNull("Center")){
                    String[] x_y = temp.get("Center").toString().split(" ");

                    Coord coord = new Coord ( Float.parseFloat(x_y[0]), Float.parseFloat(x_y[1]) );
                    marker = new Marker(coord);
                    drawable = getResources().getDrawable(R.drawable.hcnt_marker);
                    marker.setIcon(drawable);

                    marker.setTitle("");
                    radiusMarkerLayer.addItem(marker);
                    radiusMarkerinfoList.add(temp);
                }
            }
        }

        for (int index = 0; index < radiusMarkerLayer.size(); index++) {
            radiusMarkerLayer.getMarker(index).dispatchOverlayEvent(getRadiusObjectEventListener(index));
        }

        mMapView.setZoomToExtent(circleBounds);
        invalidate();
    }

    public void cleanLayer() {
        if(markerlayer != null)
            markerlayer.removeAll();
        if(vectorLayer != null)
            vectorLayer.removeAll();
        invalidate();
    }

    private void resultGeometryFitMap(ArrayList<Float> xValue, ArrayList<Float> yValue) {
        if(xValue.size() > 0 && yValue.size() > 0){
            float xmin = Collections.min(xValue);
            float ymin = Collections.min(yValue);
            float xmax = Collections.max(xValue);
            float ymax = Collections.max(yValue);

            mMapView.setZoomToExtent(new Bounds(xmin, ymin, xmax, ymax));
        }
    }

    // 경로탐색 결과
    public void setRouteResult(RouteVO result, String start_name, String end_name) {
        allOverlayRemove();

        ArrayList<Float> xValue = new ArrayList<>();
        ArrayList<Float> yValue = new ArrayList<>();

        if(result.getGeometry().equals("")){
            Toast.makeText(mContext, "경로탐색 결과가 없습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(result.getGeometry().contains("EMPTY")) {
            Toast.makeText(mContext, "경로탐색 결과가 없습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        String[] routeString = result.getGeometry().replace("LINESTRING ", "").replace("(","").replace(")","").trim().split(",");

        Coord[] line = new Coord[routeString.length];
        for(int i = 0; i < routeString.length; i++) {
            String[] x_y = routeString[i].trim().split(" ");

            if(x_y.length > 1) {
                line[i] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                xValue.add(line[i].getX());
                yValue.add(line[i].getY());
            }

            if(i == 0){ // 시작점,끝점 마커 생성
                Coord coord = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                Marker marker = new Marker(coord);
                Drawable drawable = null;
                drawable = getResources().getDrawable(R.drawable.ico_route_map_start);
                marker.setIcon(drawable);
                marker.setTitle("");
                markerlayer.addItem(marker);
            }

            if(i == routeString.length - 1){ // 시작점,끝점 마커 생성
                Coord coord = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));

                Marker marker = new Marker(coord);
                Drawable drawable = null;
                drawable = getResources().getDrawable(R.drawable.ico_route_map_arrive);
                marker.setIcon(drawable);
                marker.setTitle("");
                markerlayer.addItem(marker);
            }
        }

        Polyline pline = new Polyline(line, Color.rgb(255, 0, 0), 5);
        pline.setStrokeWidth(4);
        pline.setStrokeColor(Color.rgb(255, 0, 0));
        vectorLayer.addItem(pline.getVector());

        resultGeometryFitMap(xValue, yValue);

        closeInfo();
        content_name.setTypeface(null, Typeface.BOLD);
        content_name.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
        content_name.setText("" + start_name + " -> " + "" + end_name);
        content_addr.setText("총거리  " + util.getKm(Double.parseDouble( result.getDistance() ) ) + " km");
        content_type.setText("소요시간  " + util.getHour(Double.parseDouble(result.getLead_time())) + " 시간");

        invalidate();

        ObjectAnimator.ofFloat(info, "translationY", 0).start();
    }

    // 경유지 포함 경로탐색
    public void setMultiRouteResult(List<RouteVO> result, String start_name, String end_name, String way_points_content) {
        allOverlayRemove();

        multiRouteinfoList = new ArrayList<>();

        ArrayList<Float> xValue = new ArrayList<>();
        ArrayList<Float> yValue = new ArrayList<>();
        boolean end = false;
        double distance = 0f;
        double time = 0f;

//        System.out.println(way_points_content);
        JSONArray way_points_ct = null;
        try {
            way_points_ct = new JSONArray(way_points_content);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int s = 0; s < result.size(); s++) {

            if(result.get(s).getGeometry().contains("EMPTY")) {
                Toast.makeText(mContext, "경로탐색 결과가 없습니다.", Toast.LENGTH_SHORT).show();
                return;
            }

            String[] routeString = result.get(s).getGeometry().replace("LINESTRING ", "").replace("(","").replace(")","").trim().split(",");
            Coord[] line = new Coord[routeString.length];
            for(int i = 0; i < routeString.length; i++) {
                String[] x_y = routeString[i].trim().split(" ");

                if(x_y.length > 1) {
                    line[i] = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));
                    xValue.add(line[i].getX());
                    yValue.add(line[i].getY());
                }

                if(s == 0 && i == 0){ // 출발
                    Coord coord = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));

                    Marker marker = new Marker(coord);
                    Drawable drawable = getResources().getDrawable(R.drawable.ico_route_map_start);
                    marker.setIcon(drawable);
                    marker.setTitle("");
                    markerlayer.addItem(marker);
                    multiRouteinfoList.add(null);
                }

                if(s == result.size()-1 && i == routeString.length - 1) { // 도착
                    Coord coord = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));

                    Marker marker = new Marker(coord);
                    Drawable drawable = getResources().getDrawable(R.drawable.ico_route_map_arrive);
                    marker.setIcon(drawable);
                    marker.setTitle("");
                    markerlayer.addItem(marker);
                    multiRouteinfoList.add(null);

                    end = true;
                }

                if(!end &&  i == routeString.length - 1 ) { // 경유지
                    Coord coord = new Coord(Float.valueOf(x_y[0]), Float.valueOf(x_y[1]));

                    Marker marker = new Marker(coord);

                    String icon = "@drawable/location_icon_" + String.valueOf(s+1);
                    String packName = mContext.getPackageName();
                    int resID = getResources().getIdentifier(icon, "drawable", packName);
                    Drawable drawable = getResources().getDrawable(resID);

                    marker.setIcon(drawable);
                    marker.setTitle("");
                    markerlayer.addItem(marker);
                    try {
                        multiRouteinfoList.add(way_points_ct.getJSONObject(s) != null ? way_points_ct.getJSONObject(s) : null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

            Polyline pline = new Polyline(line, Color.rgb(255, 0, 0), 5);
            pline.setStrokeWidth(4);
            pline.setStrokeColor(Color.rgb(255, 0, 0));
            vectorLayer.addItem(pline.getVector());

            distance += Double.parseDouble( result.get(s).getDistance() );
            time += Double.parseDouble( result.get(s).getLead_time() );
        }

        resultGeometryFitMap(xValue, yValue);

        closeInfo();
        content_name.setTypeface(null, Typeface.BOLD);
        content_name.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
        content_name.setText("" + start_name + " -> " + "" + end_name);
        content_addr.setText("총거리  " + util.getKm( distance ) + " km");
        content_type.setText("소요시간  " + util.getHour( time ) + " 시간");

        mResult = new JSONObject();
        try {
            mResult.put("result", "" + start_name + " -> " + "" + end_name);
            mResult.put("distance", "총거리  " + util.getKm( distance ) + " km");
            mResult.put("time", "소요시간  " + util.getHour( time ) + " 시간");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int index = 0; index < multiRouteinfoList.size(); index++) {
            markerlayer.getMarker(index).dispatchOverlayEvent(getMultiRouteEventListener(index));
        }

        invalidate();

        ObjectAnimator.ofFloat(info, "translationY", 0).start();

    }

    //내위치 지도 표시
    public void setUserLocation(Coord xy) {
        imagelayer.removeAll();

        Coord trans_coord =  mMapView.getProjection().transCoordination(1, 9, xy);

        Coord coord = new Coord(trans_coord.getX(), trans_coord.getY());

        Image user_location = new Image(coord, getResources().getDrawable(R.drawable.blue_circle) );
        user_location.setTitle("");


        mMapView.setMapCenter(coord);
        mMapView.setZoomLevel(11);
        imagelayer.addItem(user_location);

        invalidate();
    }

    //반경검색 결과 이벤트
    private OverlayEventListener getRadiusObjectEventListener(final int i) {
        return new OverlayEventListener() {
            @Override
            public boolean onTouch(Overlay overlay, float v, float v2) {

                String name = null;

                try {
                    if(radiusMarkerinfoList.get(i).has("hcnt_id")) {
                        name = util.getStringValue(radiusMarkerinfoList.get(i), "hcnt_name");
                        content_addr.setText(util.getStringValue(radiusMarkerinfoList.get(i), "hcnt_addr"));
                        content_type.setText(util.getStringValue(radiusMarkerinfoList.get(i), "hcnt_tel"));
                    }else
                        name = util.getStringValue(radiusMarkerinfoList.get(i), "ccnt_name");
                    content_name.setText(name);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                info.setTranslationY(px);

                if(overlay.getOverlayType() == markerlayer.OVERLAY_TYPE_MARKER) {
                    ObjectAnimator.ofFloat(info, "translationY", 0).start();
                }

                return false;
            }

            @Override
            public boolean onDoubleTouch(Overlay overlay, float v, float v2) {
                return false;
            }

            @Override
            public boolean onLongTouch(Overlay overlay, float v, float v2) {
                return false;
            }

            @Override
            public boolean onTouch(Geometry geometry, float v, float v2) {
                return false;
            }

            @Override
            public boolean onDoubleTouch(Geometry geometry, float v, float v2) {
                return false;
            }

            @Override
            public boolean onLongTouch(Geometry geometry, float v, float v2) {
                return false;
            }
        };
    }


    private OverlayEventListener getMultiRouteEventListener(final int i) {
        return new OverlayEventListener() {
            @Override
            public boolean onTouch(Overlay overlay, float v, float v2) {

                if(multiRouteinfoList.get(i) != null){ // 경유지 정보
                    try {
                        content_name.setText(util.getStringValue(multiRouteinfoList.get(i), "name"));
                        content_addr.setText(util.getStringValue(multiRouteinfoList.get(i), "address"));
                        content_type.setText(util.getStringValue(multiRouteinfoList.get(i), "tel"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    content_name.setTypeface(null, Typeface.BOLD);
                    content_name.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
                    try {
                        content_name.setText(mResult.getString("result"));
                        content_addr.setText(mResult.getString("distance"));
                        content_type.setText(mResult.getString("time"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                info.setTranslationY(px);

                if(overlay.getOverlayType() == markerlayer.OVERLAY_TYPE_MARKER) {
                    ObjectAnimator.ofFloat(info, "translationY", 0).start();
                }

                return false;
            }

            @Override
            public boolean onDoubleTouch(Overlay overlay, float v, float v2) {
                return false;
            }

            @Override
            public boolean onLongTouch(Overlay overlay, float v, float v2) {
                return false;
            }

            @Override
            public boolean onTouch(Geometry geometry, float v, float v2) {
                return false;
            }

            @Override
            public boolean onDoubleTouch(Geometry geometry, float v, float v2) {
                return false;
            }

            @Override
            public boolean onLongTouch(Geometry geometry, float v, float v2) {
                return false;
            }
        };
    }

    private OverlayEventListener getMarkerLayerEventListener(final int i) {
        return new OverlayEventListener() {

            @Override
            public boolean onTouch(Overlay overlay, float v, float v2) {

                try {
                    content_name.setText(util.getStringValue(markerInfoList.get(i), "name"));
                    content_addr.setText(util.getStringValue(markerInfoList.get(i), "address"));
                    content_type.setText(util.getStringValue(markerInfoList.get(i), "tel"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                info.setTranslationY(px);

                if(overlay.getOverlayType() == markerlayer.OVERLAY_TYPE_MARKER) {
                    ObjectAnimator.ofFloat(info, "translationY", 0).start();
                }

                return false;
            }

            @Override
            public boolean onDoubleTouch(Overlay overlay, float v, float v2) {
                return false;
            }

            @Override
            public boolean onLongTouch(Overlay overlay, float v, float v2) {
                return false;
            }

            @Override
            public boolean onTouch(Geometry geometry, float v, float v2) {
                return false;
            }

            @Override
            public boolean onDoubleTouch(Geometry geometry, float v, float v2) {
                return false;
            }

            @Override
            public boolean onLongTouch(Geometry geometry, float v, float v2) {
                return false;
            }
        };
    }
}
