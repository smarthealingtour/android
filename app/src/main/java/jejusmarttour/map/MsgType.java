package jejusmarttour.map;

/**
 * Created by User on 2014-06-30.
 */
public class MsgType {

    public static final int CHANGE_ZOOM_LEVEL = 0x100;
    public static final int CHANGE_MAP_MOVE = 0x101;
    public static final int CLICK_MY_LOCATION = 9;
}
