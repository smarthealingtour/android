package jejusmarttour.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.ToggleButton;

import org.json.JSONException;
import org.json.JSONObject;

import jejusmarttour.task.RadiusTask;
import syl.com.jejusmarttour.R;


/**
 * Created by User on 2014-09-25.
 */
public class MapController extends FrameLayout {

    private Context mContext;
    private View map_controller;
    private MyMapView mMyMapView = null;
    private overlay_ overlay = null;
    private Spinner distance;
    private Mylocation googleLocationService;
    private static final int REQUEST_CODE = 0;

    public MapController(Context context) {
        super(context);
        this.mContext = context;
    }

    public MapController(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void init(final MyMapView mMyMapView, final overlay_ overlay) {
        LayoutInflater inflater = ( LayoutInflater )getContext( ).getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        this.mMyMapView = mMyMapView;
        this.overlay = overlay;

        map_controller = inflater.inflate( R.layout.layout_map_control, this, true );

        //지도 확대
        ImageButton zoomIn = (ImageButton)map_controller.findViewById(R.id.mapZoomIn);
        zoomIn.setOnTouchListener((view, motionEvent) -> {
            mMyMapView.zoomAction(2.0F);
            overlay.zoomAction(2.0f);
            return false;
        });

        //지도 축소
        ImageButton zoomOut = (ImageButton)map_controller.findViewById(R.id.mapZoomOut);
        zoomOut.setOnTouchListener((view, motionEvent) -> {
            mMyMapView.zoomAction(-1.0F);
            overlay.zoomAction(-1.0F);
            return false;
        });

        // 반경검색 거리 설정
        distance = (Spinner) map_controller.findViewById(R.id.spinner_map);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.distance, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        distance.setAdapter(adapter);

        // 반경검색
        ImageButton radius = (ImageButton) map_controller.findViewById(R.id.radius_search);

        radius.setOnTouchListener((view, motionEvent) -> {
            if(MotionEvent.ACTION_UP == motionEvent.getAction())
                searchRadius();
            return false;
        });

        // 내위치
        ToggleButton myLocation = (ToggleButton) map_controller.findViewById(R.id.mylocation_btn);
        myLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if(checked){
                    googleLocationService = new Mylocation(mContext, overlay);
                    googleLocationService.startLocationUpdate();
                }
                else {
                    googleLocationService.stopLocationUpdate();
                }
            }
        });

        // 경로찾기
        ImageButton route = (ImageButton) map_controller.findViewById(R.id.route);
        route.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(mContext, RouteActivity.class);
                    intent.putExtra("x", mMyMapView.getMapCenter().getX());
                    intent.putExtra("y", mMyMapView.getMapCenter().getX());
                    ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE);
                }
                return false;
            }
        });

    }

    //반경검색 수행
    private void searchRadius() {
        overlay.closeInfo();

        RadiusTask task = new RadiusTask ( getContext ( ));
        String[] params = {
                String.valueOf(mMyMapView.getMapCenter().getX()),
                String.valueOf(mMyMapView.getMapCenter().getY()),
                distance.getSelectedItem().toString().replace("km","000")
        };
        task.execute ( params );
    }

    //반경검색 결과
    public void setJsonResult(JSONObject result) throws JSONException {
        overlay.setRadiusResult(result, distance.getSelectedItem().toString().replace("km","000"));
    }
}