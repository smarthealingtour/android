package jejusmarttour.map;

import java.util.ArrayList;
import java.util.List;

import ktmap.android.map.vector.Vector;


/**
 * Created by User on 2014-07-10.
 */
public class VectorLayer_ extends ktmap.android.map.vector.VectorLayer {

    public ArrayList<Vector> list = new ArrayList<Vector>();

    public VectorLayer_() {


    }

    @Override
    protected Vector getGeometry(int paramInt) {
        if(paramInt>=list.size()) return null;
        return list.get(paramInt);

    }


    @Override
    protected int size() {
        return list.size();
    }


    public void addItem(Vector vector) {


        list.add(vector);
    }

    @Override
    public List<Vector> getVectorList() {
        return this.list;
    }

    @Override
    public Vector getVectorType(int paramInt) {
        return list.get(paramInt);
    }

    public void removeAll(){
        list = null;
        list = new ArrayList();
    }
}
