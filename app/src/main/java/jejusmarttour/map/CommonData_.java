package jejusmarttour.map;



/**
 * Created by User on 2014-06-30.
 */
import android.content.Context;

import jejusmarttour.main_tourproduct.TourProductActivity;

/**
 * Created by User on 2014-06-30.
 */
public class CommonData_ {
    Context mContext;
    private static CommonData_ mSingleton;
    private long mClickTime; // 버튼클릭 시간 체크

    public CommonData_(Context context) {
        mContext = context;
    }

    public static CommonData_ getInstance(Context context) {
        if(context == null && mSingleton == null)
            return null;
        if(mSingleton == null)
            mSingleton = new CommonData_(context);
        if(!(mSingleton.getMainContext() instanceof TourProductActivity) && context instanceof TourProductActivity)
            mSingleton.setMainContext(context);
        return mSingleton;
    }

    public Context getMainContext() {
        return mContext;
    }

    public void setMainContext(Context context) {
        mContext = context;
    }

    // 클릭시간체크
    public static boolean isQuickClick() {
        if(mSingleton == null)
            return false;

        long currentClickTime = System.currentTimeMillis();
        long interval = currentClickTime - mSingleton.mClickTime;
        mSingleton.mClickTime = currentClickTime;

        if(interval < 500) {
            return true;
        }
        return false;
    }

    /**
     * 지도 해상도
     */
//    public int getMap_Resolution(){
//        return uGEonMap.HD_RESOLUTION;
//    }

    public void dispose() {
        mSingleton = null;
    }
}
