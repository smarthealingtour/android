package jejusmarttour.map;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jejusmarttour.search.HCNTSearchResultPageFragment;
import syl.com.jejusmarttour.R;

/**
 * Created by User on 2015-10-27.
 */
public class yoyoSearchForRouteActivity extends AppCompatActivity  implements  SearchView.OnQueryTextListener{

    private SearchView searchView;
    private MenuItem mSearchItem;
    MenuItem search_result_delete = null;
    MenuItem search_term_delete = null;
    private String title = "";
    private String searchStr;
    private boolean fromMap = false;
    private boolean fromRoute = true;
    LinearLayout routeSearchResultTab;
    MySQLiteHelper databaseTable;
    ListView listView = null;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchforroute);

        Intent i = getIntent();
        if(!i.getStringExtra("title").isEmpty())
            this.title = i.getStringExtra("title");

        LoadSearchDct();
        init();
    }

    private void LoadSearchDct()  {
        listView = (ListView) findViewById(R.id.search_term_list);

        databaseTable = new MySQLiteHelper(this);
        JSONArray values = null;
        try {
            values = databaseTable.getAllWords();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(values.length() > 0) {
//            ArrayAdapter<ArrayList> adapter = new ArrayAdapter<ArrayList>(this,
//                    android.R.layout.simple_list_item_activated_1, android.R.id.text1, values);
            SearchTermListViewAdapter viewAdapter =  new SearchTermListViewAdapter(this, values);
            listView.setAdapter(viewAdapter);
            listView.setVisibility(View.VISIBLE);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long ld) {

                    JSONObject  itemValue    = (JSONObject) listView.getItemAtPosition(position);
                    String word = "";
                    try {
                        word = itemValue.getString("word");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSearchItem.collapseActionView();
                    ActionBar actionBar = getSupportActionBar();
                    actionBar.setDisplayShowTitleEnabled(true);
                    actionBar.setTitle(word);
                    searchStr = word;

                    listView.setVisibility(View.GONE);
                    search_term_delete.setVisible(false);

                    initViewPager();
                    routeSearchResultTab.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void init() {
        ActionBar actionBar = getSupportActionBar ( );
        actionBar.setDisplayShowHomeEnabled ( false );
        routeSearchResultTab = (LinearLayout)findViewById(R.id.routeSearchResultTab);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        mSearchItem.collapseActionView();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(query);
        this.searchStr = query;
        searchProduct ( );

        if(!databaseTable.wordsExist(query)) // 기존 검색어 아니면 추가
            databaseTable.addWord(query, util.currentDate());

        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        return false;
    }

    private final int MAX_PAGE = 2;
    private Fragment cur_fragment = new Fragment ( );
    private ToggleButton tab2;
    private ToggleButton tab3;

    private ViewPager viewPager;
    private final PageListener pageListener = new PageListener ( );

    //키워드 검색 수행
    private void searchProduct() {

        if(listView != null)
            listView.setVisibility(View.GONE);

        if(viewPager != null){
            if (!(viewPager.getAdapter() == null))
                viewPager.getAdapter().notifyDataSetChanged();
        }
        else{
            initViewPager ( );
            routeSearchResultTab.setVisibility(View.VISIBLE);
        }
    }

    private void initViewPager ( )
    {
        tab2 = ( ToggleButton ) findViewById ( R.id.tab2 );
        tab3 = ( ToggleButton ) findViewById ( R.id.tab3 );

        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTabItem (0);
            }
        });

        tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTabItem ( 1 );
            }
        });

        tab2.setChecked ( true );

        viewPager = ( ViewPager ) findViewById ( R.id.viewpager );
        viewPager.setAdapter ( new ViewPagerAdapter ( getSupportFragmentManager ( ) ) );
        viewPager.setOffscreenPageLimit ( MAX_PAGE - 1 );
        viewPager.setOnPageChangeListener ( pageListener );
    }

    private ToggleButton [ ] getToggleButtons ( )
    {
        ToggleButton [ ] buttons = {  tab2 , tab3 };
        return buttons;
    }

    private void setTabItem ( int position )
    {
        if ( getToggleButtons ( ) [ position ].isChecked ( ) == true )
        {
            setCurrentItem ( position );
            validateToogleButton ( position );
        }
        else
        {
            getToggleButtons ( ) [ position ].setChecked ( true );
        }
    }

    private void setChangeTabItem ( int position )
    {
        getToggleButtons ( ) [ position ].setChecked ( true );
        validateToogleButton ( position );
    }

    private void validateToogleButton ( int position )
    {
        for ( int i = 0; i < getToggleButtons ( ).length; i++ )
        {
            if ( i != position )
                getToggleButtons ( ) [ i ].setChecked ( false );
        }
    }

    private void setCurrentItem ( int position )
    {
        viewPager.setCurrentItem ( position );
    }

    private class PageListener implements ViewPager.OnPageChangeListener
    {

        @Override
        public void onPageScrolled ( int position , float positionOffset , int positionOffsetPixels )
        {
        }

        @Override
        public void onPageScrollStateChanged ( int state )
        {
        }

        @Override
        public void onPageSelected ( int position )
        {
            setChangeTabItem ( position );
        }
    }

    // 뷰페이지 어뎁터
    class ViewPagerAdapter extends FragmentStatePagerAdapter
    {
        private int position = 0;

        public ViewPagerAdapter ( FragmentManager fm )
        {
            super ( fm );
        }

        @Override
        public Fragment getItem ( int position )
        {
            if ( position < 0 || MAX_PAGE <= position )
                return null;

            this.position = position;

            switch ( position )
            {
                case 0 :
                    cur_fragment = HCNTSearchResultPageFragment.newInstance (searchStr, fromMap, fromRoute );
                    break;
                case 1 :
//                    cur_fragment = new FoodStaySearchResultPageFragment( ).newInstance ( searchStr, fromMap, fromRoute );
                    break;
            }
            return cur_fragment;
        }

        public int getCurrentPosition ( )
        {
            return position;
        }

        @Override
        public int getCount ( )
        {
            return MAX_PAGE;
        }

        @Override
        public CharSequence getPageTitle ( int position )
        {
            return getResources ( ).getStringArray ( R.array.htp_main_titles ) [ position ];
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(this.title);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(this);
        mSearchItem = menu.getItem(0);

        search_result_delete = menu.findItem(R.id.search_result_delete);
        search_result_delete.setVisible(true);

        search_term_delete = menu.findItem(R.id.search_term_delete);
        if(listView.getVisibility() == View.VISIBLE) {
            search_term_delete.setVisible(true);

            search_term_delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                        if(databaseTable != null) {
                            databaseTable.deleteAllWords();
                            listView.setVisibility(View.GONE);
                            search_term_delete.setVisible(false);
                        }
                    return false;
                }
            });
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search_result_delete) {
                finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed ( )
    {
        super.onBackPressed ( );
        finish();
    }
}
