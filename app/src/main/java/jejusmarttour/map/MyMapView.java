package jejusmarttour.map;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.TextView;

import jejusmarttour.common.CommonData;
import jejusmarttour.task.Coord2addrTask;
import ktmap.android.map.Bounds;
import ktmap.android.map.Coord;
import ktmap.android.map.MapEventListener;
import ktmap.android.map.Pixel;
import ktmap.android.map.uGEonMap;
import ktmap.android.utils.Projection;

import com.cubex.tbcommon.BxMsgThread;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by User on 2014-07-04.
 */
public class MyMapView extends uGEonMap implements MapEventListener {

    public boolean isDoubleTouch = false; // Double Touch 체크 Flag
    private boolean isLongTabPOI = false; // 롱탭POI 체크 Flag
    private boolean isFirstDraw = true; // POI Draw시 맨처음만 SetZoomLevel(), SetMapCenter() 호출함
    private boolean isReDraw = true;
    private boolean isOptionPOIShowAni = true;
    private boolean isBalloonOptionTouching = false;
    private boolean isShowingAni = false;
    private boolean isStartLongTab = false;
    private boolean isTouchMove = false;
//    private boolean isIntentDraw = false;

    private static final int MSG_SET_ANGLE = 20;
    private Coord doubleTouchPoint;
    private OneTouchThread mOneTouchTread;

    Context mContext;
    private TextView addTextView;
    overlay_ overlay;
    private CommonData_ commonData;

    public MyMapView(Context context) {
        super(context, "OllehMapAPI0004", "vT1S4NqVdi");
        mContext = context;

        dispatchMapEvent(this);
//        setZoomLevel(6);
//        setSatellite(false);
        CommonData_.getInstance(mContext);

        mOneTouchTread = new OneTouchThread();
        mOneTouchTread.start();

        commonData = CommonData_.getInstance(mContext);
    }

    public MyMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        dispatchMapEvent(this);
    }

    public boolean isShowingAni() {
        return isShowingAni;
    }

    protected Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case MsgType.CHANGE_ZOOM_LEVEL:
//                    Log.i("CHANGE_ZOOM_LEVEL : ", msg.obj.toString());
                    setPOIByChangeZoomLevel();
                    break;
                case MSG_SET_ANGLE:
                    setangle();
                    break;
            }
        }
    };

    public void setangle(){
        if(getAngle()!=0){
            setAngle(0.0f);
            mHandler.sendEmptyMessageDelayed(MSG_SET_ANGLE, 10);
        }
    }

    private void setPOIByChangeZoomLevel() {
        if (!isReDraw)
            return;

        if (isDoubleTouch) {
            isDoubleTouch = false;
            this.setMapCenter(doubleTouchPoint);
        }
    }

    //주소표시 레이어
    public void setAddressLy(TextView addressTextView) {
        this.addTextView = addressTextView;
    }

    //*********** Inner Class *************
    // OneTouch 와 DoubleTouch 구분 Thread
    public class OneTouchThread extends BxMsgThread {
        private static final int interval = 300;
        private boolean DoubleTouch = false;
        private Runnable mOneTouchAniRunnable = new Runnable() {
            @Override
            public void run() {
                if(commonData.isQuickClick()) {
                    return;
                }
                if(mContext != null) {
//                    NotificationSubject notificationSubject = ((OllehMapMain)mContext).getObserverSubject();
//                    int currentMsgType = ((OllehMapMain)mContext).getCurrentMsgType();
//                    if(notificationSubject != null) {
                    isShowingAni =  true;
//                        notificationSubject.sendEmptyMessage(MsgType.ONE_TOUCH_MAP);
//                    }
                }
            }
        };

        public void setIsDoubleTouch(boolean isDoubleTouch) {
            DoubleTouch = isDoubleTouch;
            removeMessages(0);
            postMessageDelayed(0, 1);
        }

        @Override
        public void beginThread() {
            super.beginThread();
        }

        @Override
        public void endThread() {
            super.endThread();
        }

        @Override
        protected void onMessage(Message message) {
            try{
                sleep(interval);
                if(!DoubleTouch) {
                    MyMapView.this.mHandler.removeCallbacks(mOneTouchAniRunnable);
                    MyMapView.this.mHandler.post(mOneTouchAniRunnable);
                }
            }
            catch(Exception e) {
//                OllehLog.e("MapView", e.toString());
            }
        }
    }

    private void settingZoomLevel(int zoomLevel) {
        if(this.getMapResolution() != uGEonMap.NORMAL_SMALL_FONT)
            zoomLevel++;
        mHandler.sendMessage(mHandler.obtainMessage(MsgType.CHANGE_ZOOM_LEVEL, zoomLevel));
    }

    @Override
    public boolean onTouch(Pixel paramPixel) {
        if(isDoubleTouch) {
            if(this.getMaxZoomLevel() == getZoomLevel()) {
                isDoubleTouch = false;
                mOneTouchTread.setIsDoubleTouch(false);
            }
        }
        else {
            mOneTouchTread.setIsDoubleTouch(false);
        }
        return false;
    }

    @Override
    public boolean onDoubleTouch(Pixel pixel) {
        // 더블터치시 터치지점 가운데로
        if(!isDoubleTouch) {
            isDoubleTouch = true;
            mOneTouchTread.setIsDoubleTouch(isDoubleTouch);
            Projection projection = getProjection();
            doubleTouchPoint = projection.toCoord(pixel);
        }
        return false;
    }

    @Override
    public boolean onMultiTouch(Pixel[] paramArrayOfPixel) {
        return false;
    }

    @Override
    public boolean onLongTouch(Pixel paramPixel) {
        return false;
    }

    @Override
    public boolean onChangeZoomLevel(boolean flag, int zoomLevel) {
        if(isReDraw)
            settingZoomLevel(zoomLevel);
        return false;
    }

    @Override
    public boolean onMapInitializing(boolean paramBoolean) {
        return false;
    }

    @Override
    public boolean onBoundsChange(Bounds bounds) {
        if(CommonData.recentCname.equals("mainMap")) {
            if(util.isNetworkConnected(mContext)){
                Coord trans_coord = this.getProjection().transCoordination(9, 1, getMapCenter());
                Coord2addrTask task = new Coord2addrTask(mContext, this);
                task.execute(trans_coord);
            }
        }
        return false;
    }

    public void setAddrResult(JSONObject result) {
        if(result != null) {
            try {
                if(addTextView != null)
                    addTextView.setText(result.getString("adress"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
