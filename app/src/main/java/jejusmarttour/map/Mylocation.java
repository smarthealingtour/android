package jejusmarttour.map;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import jejusmarttour.util.GoogleLocationService;
import ktmap.android.map.Coord;

/**
 * Created by User on 2015-10-25.
 */
public class Mylocation extends GoogleLocationService {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public double latitude;
    public double longitude;
    private double altitude;


    Context context;
    overlay_ overlay = null;

    public Mylocation(Context context, overlay_ overlay) {
        super(context);
        this.context = context;
        this.overlay = overlay;

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }



    public void stopLocationUpdate() {
        mGoogleApiClient.disconnect();
    }

    public void startLocationUpdate() {
        mGoogleApiClient.connect();
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // Update location every second
//        mLocationRequest.setSmallestDisplacement(10);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(context, util.GooglePlayServiceErrorMessage(this.context, connectionResult.getErrorCode()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();

        overlay.setUserLocation(new Coord((float) longitude, (float) latitude));
    }
}
