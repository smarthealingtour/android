package jejusmarttour.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import jejusmarttour.vo.MTCScheduleVO;
import jejusmarttour.vo.RouteVO;
import ktmap.android.map.Coord;
import syl.com.jejusmarttour.R;

public class util {

    public static Coord mapCenter = new Coord(909837.7F, 1487886.6F);

    public util(Activity activity) {

    }
    public static int[] getCurrentDate() {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        int[] now = {year, month, day};

        return now;
    }

    public static String ImageSavePath() {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath() + "/NGII3/";
        File file = new File(root);

        String savePath;

        if(file.exists()) {
            File path = new File(root + "Image/");
            path.mkdir();
            savePath = path.getAbsolutePath();

            return savePath;
        }
        else
            return Environment.getExternalStorageDirectory().getAbsolutePath();
    }




    public static boolean isNetworkConnected(Context c){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static boolean isTablet(Context context)
    {
        boolean isTablet;

        int portrait_width_pixel = Math.min(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch = context.getResources().getDisplayMetrics().densityDpi;
        float virutal_width_inch = portrait_width_pixel / dots_per_virtual_inch;

        if (virutal_width_inch <= 2) {
            //is phone
            isTablet = false;
            return isTablet;
        } else {
            //is tablet
            isTablet = true;
            return isTablet;
        }
//        return (virutal_width_inch > 2);
    }

    public static String[] isLogin(Context context) {

        String[] params = null;

        SharedPreferences pref = context.getSharedPreferences("shareDate", Activity.MODE_PRIVATE); // 일반 사용자

        if(pref.getBoolean("loginStaus", false) && pref.contains("loginStaus")) {
            params = new String[]{pref.getString("user_id", "")};
            return params;
        }
        else
            return params;
    }

    public static String escapseTrans(String src) {

        String dst = src.replaceAll("'br'", "\n").replaceAll("amp;amp;", "&").replaceAll("amp;quot;", "\"");


        return dst;


    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String GooglePlayServiceErrorMessage(Context context, int errorCode) {
        String errorString;
        Resources mResources = context.getResources();

        switch (errorCode) {
            case ConnectionResult.DEVELOPER_ERROR:
                errorString = mResources.getString(R.string.connection_error_misconfigured);
                break;

            case ConnectionResult.INTERNAL_ERROR:
                errorString = mResources.getString(R.string.connection_error_internal);
                break;

            case ConnectionResult.INVALID_ACCOUNT:
                errorString = mResources.getString(R.string.connection_error_invalid_account);
                break;

            case ConnectionResult.LICENSE_CHECK_FAILED:
                errorString = mResources.getString(R.string.connection_error_license_check_failed);
                break;

            case ConnectionResult.NETWORK_ERROR:
                errorString = mResources.getString(R.string.connection_error_network);
                break;

            case ConnectionResult.RESOLUTION_REQUIRED:
                errorString = mResources.getString(R.string.connection_error_needs_resolution);
                break;

            case ConnectionResult.SERVICE_DISABLED:
                errorString = mResources.getString(R.string.connection_error_disabled);
                break;

            case ConnectionResult.SERVICE_INVALID:
                errorString = mResources.getString(R.string.connection_error_invalid);
                break;

            case ConnectionResult.SERVICE_MISSING:
                errorString = mResources.getString(R.string.connection_error_missing);
                break;

            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                errorString = mResources.getString(R.string.connection_error_outdated);
                break;

            case ConnectionResult.SIGN_IN_REQUIRED:
                errorString = mResources.getString(
                        R.string.connection_error_sign_in_required);
                break;

            default:
                errorString = mResources.getString(R.string.connection_error_unknown);
                break;
        }

        return errorString;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    @SuppressWarnings("deprecation")
    public static void turnGPSOn(Context context)
    {
        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
        context.sendBroadcast(intent);

        String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (! provider.contains("gps"))
        { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            context.sendBroadcast(poke);
        }
    }

    @SuppressWarnings("deprecation")
    public static void turnGPSOff(Context context)
    {
        String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps"))
        { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            context.sendBroadcast(poke);
        }
    }

    static String getKm(double meters) {

        double kilometers = meters * 0.001;
        String result = String.valueOf((int)(kilometers * 10) / 10.0);
        return result;
   }

   static String getHour(double mimuite) {
       double hours = Math.floor(mimuite / 60);
       String result = String.valueOf((int)(hours * 10) / 10.0);
       return result;
   }




  public static String getStringValue(JSONObject object, String key) throws JSONException {
      String result = "";

      if(object.has(key) && !object.isNull(key) ) {
          result = object.getString(key);
          if(result.equals("null"))
              return "";
      }
      return result;
  }

    public static void recursiveRecycle(View root) {
        if (root == null)
            return;
        root.setBackgroundDrawable(null);
        if (root instanceof ViewGroup) {
            ViewGroup group = (ViewGroup)root;
            int count = group.getChildCount();
            for (int i = 0; i < count; i++) {
                recursiveRecycle(group.getChildAt(i));
            }

            if (!(root instanceof AdapterView)) {
                group.removeAllViews();
            }
        }

        Drawable d = root.getBackground();
        if (d != null) {
            try {
                d.setCallback(null);
            } catch (Exception ignore) {
            }

        }

        if (root instanceof ImageView) {
            ImageView imageview = (ImageView) root;
            d = imageview.getDrawable();

            if (d != null) {
                d.setCallback(null);
            }

            if (d instanceof BitmapDrawable) {
                Bitmap bm = ((BitmapDrawable) d).getBitmap();

                if(bm != null && bm.isRecycled())
                    bm.recycle();
            }

            ((ImageView)root).setImageDrawable(null);

        }
//        else if (root instanceof WebView) {
//
//            ((WebView) root).destroyDrawingCache();
//
//            ((WebView) root).removeAllViews();
//            ((WebView) root).clearHistory();
//            ((WebView) root).clearCache(true);
//            ((WebView) root).freeMemory();  //new code
//            ((WebView) root).pauseTimers(); //new code
//            root = null;
//
////            ((WebView) root).destroy();
//
//        }

        try {
            root.setAnimation(null);
        } catch (Exception ignore) {

        }


        root = null;

        return;
    }

    public static String currentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String result = dateFormat.format(new Date());
        return  result;
    }

    public static int dpToPixel(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixel =  (int)(dp * scale + 0.5f);
        return pixel;
    }

    public static boolean isValueAble(String value) {
        if(value == null)
            return false;
        if(value.equals("") || value.equals("null"))
            return false;
        return true;
    }

    public static boolean isRouteExist(RouteVO vo) {
        if(vo == null)
            return false;
        if(vo.getGeometry() == null)
            return false;
        if(vo.getGeometry().equals("0") || vo.getGeometry().equals("") || vo.getGeometry().equals("null") || vo.getGeometry().equals("LINESTRING EMPTY"))
            return false;

        if(vo.getDistance() == null)
            return false;
        if(vo.getDistance().equals("0") || vo.getDistance().equals("") || vo.getDistance().equals("null"))
            return false;

        if(vo.getLead_time() == null)
            return false;
        if(vo.getLead_time().equals("0") || vo.getLead_time().equals("") || vo.getLead_time().equals("null"))
            return false;

        return true;
    }

    public static boolean isRouteExist(MTCScheduleVO vo) {
        if(vo == null)
            return false;
        if(vo.getGeomery() == null)
            return false;
        if(vo.getGeomery().equals("0") || vo.getGeomery().equals("") || vo.getGeomery().equals("null") || vo.getGeomery().equals("LINESTRING EMPTY"))
            return false;

        if(vo.getDistance() == null)
            return false;
        if(vo.getDistance().equals("0") || vo.getDistance().equals("") || vo.getDistance().equals("null"))
            return false;

        if(vo.getLead_time() == null)
            return false;
        if(vo.getLead_time().equals("0") || vo.getLead_time().equals("") || vo.getLead_time().equals("null"))
            return false;

        return true;
    }


    public static RouteVO setRouteVO(JSONObject obj) {
        RouteVO  vo = new RouteVO();

            try {
                if(obj.has("distance") && !obj.isNull("distance"))
                    vo.setDistance(obj.getString("distance"));
                else
                    vo.setDistance(null);

                if(obj.has("lead_time") && !obj.isNull("lead_time"))
                    vo.setLead_time(obj.getString("lead_time"));
                else
                    vo.setLead_time(null);

                if(obj.has("geometry") && !obj.isNull("geometry"))
                    vo.setGeometry(obj.getString("geometry"));
                else
                    vo.setGeometry(null);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        return vo;
    }

    public static JSONArray RemoveJSONArray( JSONArray jarray,int pos) {

        JSONArray Njarray = new JSONArray();
        try {
            for (int i = 0; i < jarray.length(); i++) {
                if (i != pos)
                    Njarray.put(jarray.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Njarray;
    }

    public static int ActionBarHeight(Context context, int res) {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(res, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }
}
