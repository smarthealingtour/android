package jejusmarttour.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by User on 2015-11-03.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String TABLE_WORDS = "words";
    private static final String KEY_ID = "id";
    private static final String KEY_TERM = "term";
    private static final String KEY_DATE = "date";

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "BookDB";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table
        String CREATE_BOOK_TABLE = "CREATE TABLE words ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "term TEXT," +
                "date TEXT )";

        // create books table
        db.execSQL(CREATE_BOOK_TABLE);
    }

    public void addWord(String word, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TERM, word);
        values.put(KEY_DATE, date);
        db.insert(TABLE_WORDS, null, values);
        db.close();
    }

    public JSONArray getAllWords() throws JSONException {
        JSONArray result = new JSONArray();
        String query = "SELECT  * FROM " + TABLE_WORDS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                JSONObject temp = new JSONObject();
                temp.put("word", cursor.getString(1));
                temp.put("date", cursor.getString(2));
                result.put(temp);
            } while (cursor.moveToNext());
        }
        db.close();
        return result;
    }

    public boolean wordsExist(String word) {
        String query = "SELECT  * FROM " + TABLE_WORDS + " where "+ KEY_TERM +" = \""+ word.trim() +"\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor result = db.rawQuery(query, null);
        if(result.moveToFirst())
            return true;
        else
            return false;
    }

    public void deleteAllWords() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WORDS, null, null);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS words");

        // create fresh books table
        this.onCreate(db);
    }
}