package jejusmarttour.map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jejusmarttour.vo.HealingContentsVO;
import syl.com.jejusmarttour.R;

public class SearchTermListViewAdapter extends BaseAdapter
{
	private static final String TAG = SearchTermListViewAdapter.class.toString ( );

	private JSONArray listData;
	private Context context;
	private LayoutInflater inflater;

	public SearchTermListViewAdapter(Context context, JSONArray listData)
	{
		this.context = context;
		this.inflater = ( LayoutInflater ) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
		this.listData = listData;
	}

	// 현재 아이템의 수를 리턴
	@Override
	public int getCount ( )
	{
		return listData.length ( );
	}

	// 현재 아이템의 오브젝트를 리턴, Object를 상황에 맞게 변경하거나 리턴받은 오브젝트를 캐스팅해서 사용
	@Override
	public Object getItem ( int position )
	{
        JSONObject result = null;
        try {
            result = (JSONObject) listData.get ( position );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
	}

	// 아이템 position의 ID 값 리턴
	@Override
	public long getItemId ( int position )
	{
		return position;
	}

	// 출력 될 아이템 관리
	@Override
	public View getView ( int position , View convertView , ViewGroup parent )
	{
        SearchTermViewHolder viewHolder;

		// 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
		if ( convertView == null )
		{
			// view가 null일 경우 커스텀 레이아웃을 얻어 옴
			convertView = inflater.inflate ( R.layout.search_term_listview_adapter , parent , false );

			viewHolder = new SearchTermViewHolder ( );
//			viewHolder.imageView = ( ImageView ) convertView.findViewById ( R.id.search_item_icon );
			viewHolder.search_term = ( TextView ) convertView.findViewById ( R.id.htp_title );
            viewHolder.seatch_term_date = ( TextView ) convertView.findViewById ( R.id.search_term_date );

			convertView.setTag ( viewHolder );
		}
		else
		{
			viewHolder = ( SearchTermViewHolder ) convertView.getTag ( );
		}
        String word = null;
        String date = null;
        try {
            word = ((JSONObject) listData.get(position)).getString("word");
            date = ((JSONObject) listData.get(position)).getString("date");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        viewHolder.search_term.setText (word);
        viewHolder.seatch_term_date.setText (date);

		return convertView;
	}

	@Override
	public void notifyDataSetChanged ( )
	{
		super.notifyDataSetChanged ( );
	}

	// 외부에서 아이템 추가 요청 시 사용
	public void add ( JSONObject vo )
	{
		listData.put(vo);
	}

	// 외부에서 아이템 삭제 요청 시 사용
	public void remove ( int position )
	{
		listData.remove ( position );
	}

	public void removeAll ( ) throws JSONException {
		listData = new JSONArray();
	}

    public class SearchTermViewHolder
    {
        ImageView imageView;
        TextView search_term;
        TextView seatch_term_date;
    }
}