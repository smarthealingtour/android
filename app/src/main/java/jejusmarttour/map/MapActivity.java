/*
package jejusmarttour.map;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.data.kml.KmlLayer;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import jejusmarttour.common.CommonData;
import jejusmarttour.search.SearchProductActivity;
import jejusmarttour.task.RadiusTask;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.RouteVO;
import ktmap.android.map.layer.LayerManager;
import syl.com.jejusmarttour.R;

*/
/**
 * Created by User on 2015-10-20.
 *//*

public class MapActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, OnMapReadyCallback {
    private FrameLayout mMainLayout;
    private LayerManager lm;

    private MyMapView mMyMapView = null;
    private overlay_ overlay = null;

    private SearchView searchView;
    private MenuItem mSearchItem;
    MenuItem search_result_delete = null;
    private MapController mapController;

    private FrameLayout info = null;
    private View view = null;
    int px = 0;

    private String start_name = null;
    private String end_name = null;
    private String way_points_content;

    private String cName = "mainMap";

    private static final int REQUEST_CODE = 0;

    private GoogleMap mMap;
    private SupportMapFragment gMapView;

    private Spinner distance;

    private double myLat;
    private double myLng;

    private ArrayList<RouteVO> geometry_data;
    private ArrayList<HealingContentsVO> geometry_data2;

    private String[] kml_raw;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        gMapView = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.htourMap);
        gMapView.getMapAsync(this);

        context = this;

//        mMainLayout = ( FrameLayout ) findViewById ( R.id.mapFrame ); // 지도
//
//        info = ( FrameLayout ) findViewById ( R.id.infoMainMap );
//        px = ( int ) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, info.getLayoutParams().height, getResources().getDisplayMetrics());
//        info.setTranslationY( px );
//
//        lm = new LayerManager( mMainLayout );
//        mMyMapView = new MyMapView(this);
//        overlay = new overlay_(this , mMyMapView);
//
//        lm.addLayer (mMyMapView, "baseMap" );
//        lm.addLayer ( overlay , "overlay" );
//        mMyMapView.setLayerManager ( lm );
//        overlay.setLayerManager ( lm );
//
//        mMyMapView.setZoomLevel ( 4 );
//        mMyMapView.setMapResolution(1002);
//        mMyMapView.setMapCenter(util.mapCenter);
//        mMyMapView.setVisibility ( View.VISIBLE );


        init(); //

        CommonData.recentCname = cName;
    }

    public void init() {
//        mapController = (MapController)findViewById(R.id.customView_map_controller);
//        mapController.init(mMyMapView, overlay);
//        TextView addressTextView  = (TextView)mapController.findViewById(R.id.address);
//
//        overlay.setInfo(info, px);
//
//        mMyMapView.setAddressLy(addressTextView);
//        overlay.setVisibility ( View.VISIBLE );

        // 반경검색 거리 설정
        distance = (Spinner) findViewById(R.id.spinner_map);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.distance, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        distance.setAdapter(adapter);

        // 반경검색
        ImageButton radius = (ImageButton) findViewById(R.id.radius_search);

        radius.setOnTouchListener((view, motionEvent) -> {
            if (MotionEvent.ACTION_UP == motionEvent.getAction())
                searchRadius();
            return false;
        });
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setMinZoomPreference(8);
        mMap.setMaxZoomPreference(17);
        getMap().moveCamera(CameraUpdateFactory.newLatLng(new LatLng(33.386600, 126.547384)));
    }

    private void retrieveFileFromUrl(String kml) {
        //new DownloadKmlFile("http://googlemaps.github.io/kml-samples/morekml/Polygons/Polygons.Google_Campus.kml").execute();
        //여기에 서버 주소랑 같이 kml 파일 열 수 있도록 수정
        Log.e("이거 kml 주소", kml);
        new DownloadKmlFile(kml).execute();
    }

    private void moveCameraToKml() {
        if( geometry_data.size() == 0 ) {
            return;
        } else {
            for (int i = 0; geometry_data.size() > i; i++ ){
                String here = geometry_data.get(i).getGeometry();
                Log.e("이게 마커 데이터", here);
                int idx = here.indexOf(" ");
                String lat;
                String lng;

                lat = here.substring(0, idx);
                lng = here.substring(idx + 1);

                mMap.addMarker( new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))) //coord x,y를 latlng 형태로, 좌표는 healingcontentsvo에서 받아와야하고, ScheduleVO에서 이 vo를 포함하고 있음, task는 scheduletaskvo를 이용
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                if(i == 0) {
                    getMap().moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                }
            }
        }
    }

    protected GoogleMap getMap() {
        return mMap;
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("지도");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(this);
        mSearchItem = menu.getItem(0);

        search_result_delete = menu.findItem(R.id.search_result_delete);

//            if(searchResult != null){
//                if(searchResult.openShowResult.getVisibility() == View.VISIBLE)
//                    search_result_delete.setVisible(true);
//            }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        mSearchItem.collapseActionView();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(query);
        searchProduct (query );

        return false;
    }

    public String point_x ( ) {
        myLat = getMap().getCameraPosition().target.latitude;
        String x_point = Double.toString(myLat);
        return x_point;
    }
    public String point_y ( ) {
        myLng = getMap().getCameraPosition().target.longitude;
        String x_point = Double.toString(myLng);
        return x_point;
    }

    //키워드 검색 수행
    private void searchProduct(String query) {
        Intent intent = new Intent( this , SearchProductActivity.class );
        intent.putExtra ( "data" , query );
        intent.putExtra("fromMap", true);
        startActivity(intent);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(data != null){
//            switch (requestCode){
//                case 0 :
//                    if(data.getExtras().containsKey("start_point_x")
//                            && data.getExtras().containsKey("start_point_y")
//                                && data.getExtras().containsKey("end_point_x")
//                                    && data.getExtras().containsKey("end_point_y")) {
//
//                        start_name =  data.getExtras().getString("start_name");
//                        end_name = data.getExtras().getString("end_name");
//                        way_points_content = data.getExtras().getString("way_points_content");
//                        routeSearch(
//                                data.getExtras().getString("start_id"),
//                                data.getExtras().getString("end_id"),
//                                data.getExtras().getString("way_points_id")
//
//                               );
//                    }
//                    else{
//                        Toast.makeText(this, "경로탐색 결과가 없습니다.", Toast.LENGTH_SHORT).show();
//                    }
//                    break;
//                default:
//                    break;
//            }
//        }
//    }

//    private void routeSearch(String start_id, String end_id, String way_points_id) {
//        String [] params = {
//                start_id,
//                end_id,
//                way_points_id,
//                null
//        };
//
//        if(way_points_id != null ) {
//            GetMultiRouteTask task = new GetMultiRouteTask(this);
//            task.execute(params);
//            if(kml_raw != null) {
//                for ( int i = 0; i < kml_raw.length; i++ ) {
//                    String url = null;
//                    if(kml_raw[i] !=null) {
//                        url = CommonData.getImageAddress();
//                        url += kml_raw[i];
//                        retrieveFileFromUrl(url);
//                    }
//                }
//            }
//        }
//        else {
//            GetRouteTask task2 = new GetRouteTask(this);
//            task2.execute(params);
//            if(kml_raw != null) {
//                for ( int i = 0; i < kml_raw.length; i++ ) {
//                    String url = null;
//                    if(kml_raw[i] !=null) {
//                        url = CommonData.getImageAddress();
//                        url += kml_raw[i];
//                        retrieveFileFromUrl(url);
//                    }
//                }
//            }
//        }
//    }

    // 경로탐색 결과
//    public void setRouteResult(RouteVO result) throws JSONException {
//        if(overlay != null)
//            overlay.setRouteResult(result, start_name, end_name);
//    }

//    public void setMultiRouteResult(List<RouteVO> result) throws JSONException {
//        if(overlay != null)
//            overlay.setMultiRouteResult(result, start_name, end_name, way_points_content);
//    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        gMapView.onResume();
        if(!CommonData.recentCname.equals(cName)){

            finish();
            startActivity(getIntent());
//            lm.removeLayer(overlay);
//            overlay = new overlay_( this , mMapView );
//            lm.addLayer ( overlay , "overlay" );
//            overlay.setLayerManager ( lm );



//            CommonData.recentCname = cName;
        }
    }

    @Override
    public void onDestroy ( )
    {
        super.onDestroy ( );
//        if ( mMyMapView != null )
//        {
//            mMyMapView.destroyDrawingCache ( );
//            if ( mMyMapView.commonUtils != null )
//                mMyMapView.commonUtils.clearData ( );
//        }
//
//        if( overlay != null )
//            overlay = null;
        CommonData_ common = CommonData_.getInstance(this);
        if ( common != null )
            common.dispose ( );

        System.gc ( );
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        gMapView.onLowMemory();
    }

    @Override
    public void onPause() {
        gMapView.onPause();
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        gMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        gMapView.onStop();
    }

    //반경검색 수행
    private void searchRadius() {
//        overlay.closeInfo();

        RadiusTask task = new RadiusTask ( getApplicationContext ( ) );

//        String x = map.point_x();
//        String y = map.point_y();
        Log.e("좌표", "x: "+point_x()+" y: "+point_y());
        String[] params = {
//                String.valueOf(mMyMapView.getMapCenter().getX()),
//                String.valueOf(mMyMapView.getMapCenter().getY()),
                point_x(), point_y(),
                distance.getSelectedItem().toString().replace("km","000")
        };
        task.execute ( params );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(this.toString(), "onRequestPermissionsResult: 뭐지뭔데뭘까왜안되니" );
        if (requestCode == 1){
            //퍼미션 다시 확인후 허락되있으면 전화걸기 실행
            if (ContextCompat.checkSelfPermission(context,Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(context, "지도보기 권한이 필요합니다!", Toast.LENGTH_SHORT).show();
                fileList();
            }
        }
    }

    //반경검색 결과
    public void setJsonResult(JSONObject result) throws JSONException {
//        overlay.setRadiusResult(result, distance.getSelectedItem().toString().replace("km","000"));
        if(result == null) {
            Toast.makeText(getApplicationContext(), "현재 지도에 표시할 위치정보 데이터가 없습니다. 빠른 시일 내에 마련하겠습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        final int numberOfItemsInResp = result.length ( );

        geometry_data = new ArrayList<RouteVO>();
        geometry_data2 = new ArrayList<HealingContentsVO>();

        for ( int i = 0; i < numberOfItemsInResp; i++ ) {
            RouteVO routeVO = new RouteVO();
            HealingContentsVO healingContentsVO = new HealingContentsVO();
            healingContentsVO.setHcnt_type( result.has ( "hcnt_type" ) ? result.getString ( "hcnt_type" ) : result.getString("type") );
            try {
                if( healingContentsVO.getHcnt_type().equals("line") || healingContentsVO.getHcnt_type().equals("zone") ) {
                    healingContentsVO.setHcnt_coord_x(result.getString("hcnt_coord_x"));
                    healingContentsVO.setHcnt_coord_y(result.getString("hcnt_coord_y"));
                    geometry_data2.add(healingContentsVO);
                } else {
                    routeVO.setGeometry(result.getString("geometry"));
                    geometry_data.add(routeVO);
                }
            } catch (NullPointerException e)
            {
                Log.e ( this.toString() , e.toString ( ) );
            }
            if( i == 0 ) {
                moveCameraToKml();
            }
        }
    }

    private class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
        private final String mUrl;

        public DownloadKmlFile(String url) {
            mUrl = url;
        }

        protected byte[] doInBackground(String... params) {
            try {
                InputStream is =  new URL(mUrl).openStream();
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[16384];
                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                return buffer.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(byte[] byteArr) {
            try {
                KmlLayer kmlLayer = new KmlLayer(mMap, new ByteArrayInputStream(byteArr), getApplicationContext());
                kmlLayer.addLayerToMap();
                moveCameraToKml();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}*/
