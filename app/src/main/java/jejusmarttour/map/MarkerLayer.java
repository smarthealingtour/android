package jejusmarttour.map;

import java.util.ArrayList;
import java.util.List;

import ktmap.android.map.overlay.Marker;
import ktmap.android.map.overlay.MarkersLayer;





/**
 * Created by User on 2014-10-10.
 */
public class MarkerLayer extends MarkersLayer {
    public ArrayList<Marker> list = new ArrayList<Marker>();

    @Override
    public Marker getMarker(int paramInt) {
        if(paramInt>=list.size()) return null;
        return (Marker) list.get(paramInt);
    }

    @Override
    protected int size() {
        return list.size();
    }


    @Override
    public List getOverlayList() {
        return this.list;
    }

    @Override
    public void setVisible(boolean on) {
        this.isVisible = on;
    }

    public void addItem( Marker mark){
        list.add(mark);
    }

    public void removeAll(){
        list = null;
        list = new ArrayList();
    }

}
