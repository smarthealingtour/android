package jejusmarttour.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.maps.android.data.kml.KmlLayer;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import jejusmarttour.FontAppCompatActivity;
import jejusmarttour.search.SearchProductActivity;
import jejusmarttour.task.JSONObjectResult;
import jejusmarttour.task.RadiusTask;
import jejusmarttour.vo.HealingContentsVO;
import jejusmarttour.vo.RouteVO;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

/**
 * Created by User on 2015-10-20.
 */
public class yoMapActivity extends FontAppCompatActivity implements SearchView.OnQueryTextListener, OnMapReadyCallback, JSONObjectResult<ArrayList<ScheduleVO>> {
    private SearchView searchView;
    private MenuItem mSearchItem;
    private MenuItem search_result_delete = null;

    private GoogleMap mMap;

    private Spinner distance;

    private ArrayList<RouteVO> geometry_data;
    private ArrayList<HealingContentsVO> geometry_data2;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Log.e(this.toString(), "onCreate: 설마 니가 ?" );

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        context = this;

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        init();
    }

    public void init() {
        // 반경검색 거리 설정
        distance = (Spinner) findViewById(R.id.spinner_map);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.distance, R.layout.spinner_layout);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        distance.setAdapter(adapter);

        // 반경검색
        Button radius = (Button) findViewById(R.id.radius_search);

        radius.setOnClickListener(view ->
            searchRadius()
        );
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = new String[]{ Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            requestPermissions(permissions, 1);
            return;
        }

        googleMap.setMyLocationEnabled(true);

        LatLng jejuCenter = new LatLng(33.386600, 126.547384);

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(jejuCenter));
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setRotateGesturesEnabled(false);
        uiSettings.setTiltGesturesEnabled(false);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo((float) 9.2);
        googleMap.animateCamera(zoom);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMinZoomPreference(8);
        googleMap.setMaxZoomPreference(23);
    }

    private Location mCurrentLocation;

    @SuppressLint("MissingPermission")
    private void getCurrentLocation(){
        OnCompleteListener<Location> mCompleteListener = task -> {
            if (task.isSuccessful() && task.getResult() != null){
                mCurrentLocation = task.getResult();
            }
        };
        mFusedLocationProviderClient.getLastLocation().addOnCompleteListener( this, mCompleteListener);
    }

    private void retrieveFileFromUrl(String kml) {
        //new DownloadKmlFile("http://googlemaps.github.io/kml-samples/morekml/Polygons/Polygons.Google_Campus.kml").execute();
        //여기에 서버 주소랑 같이 kml 파일 열 수 있도록 수정
        Log.e("이거 kml 주소", kml);
        new DownloadKmlFile(kml).execute();
    }

    private void moveCameraToKml() {
        if( geometry_data != null && geometry_data.size() != 0 ) {
            for (int i = 0; geometry_data.size() > i; i++ ){
                String here = geometry_data.get(i).getGeometry();
                Log.e("이게 마커 데이터", here);
                int idx = here.indexOf(" ");
                String lat;
                String lng;

                lat = here.substring(0, idx);
                lng = here.substring(idx + 1);

                mMap.addMarker( new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))) //coord x,y를 latlng 형태로, 좌표는 healingcontentsvo에서 받아와야하고, ScheduleVO에서 이 vo를 포함하고 있음, task는 scheduletaskvo를 이용
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                if(i == 0) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("지도");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(this);
        mSearchItem = menu.getItem(0);

        search_result_delete = menu.findItem(R.id.search_result_delete);

//            if(searchResult != null){
//                if(searchResult.openShowResult.getVisibility() == View.VISIBLE)
//                    search_result_delete.setVisible(true);
//            }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mSearchItem.collapseActionView();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(query);
        searchProduct (query );

        return false;
    }

    public String point_x ( ) {
        double myLat = mMap.getCameraPosition().target.latitude;
        return Double.toString(myLat);
    }
    public String point_y ( ) {
        double myLng = mMap.getCameraPosition().target.longitude;
        return Double.toString(myLng);
    }

    //키워드 검색 수행
    private void searchProduct(String query) {
        Intent intent = new Intent( this , SearchProductActivity.class );
        intent.putExtra ( "data" , query );
        intent.putExtra("fromMap", true);
        startActivity(intent);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(data != null){
//            switch (requestCode){
//                case 0 :
//                    if(data.getExtras().containsKey("start_point_x")
//                            && data.getExtras().containsKey("start_point_y")
//                                && data.getExtras().containsKey("end_point_x")
//                                    && data.getExtras().containsKey("end_point_y")) {
//
//                        start_name =  data.getExtras().getString("start_name");
//                        end_name = data.getExtras().getString("end_name");
//                        way_points_content = data.getExtras().getString("way_points_content");
//                        routeSearch(
//                                data.getExtras().getString("start_id"),
//                                data.getExtras().getString("end_id"),
//                                data.getExtras().getString("way_points_id")
//
//                               );
//                    }
//                    else{
//                        Toast.makeText(this, "경로탐색 결과가 없습니다.", Toast.LENGTH_SHORT).show();
//                    }
//                    break;
//                default:
//                    break;
//            }
//        }
//    }

//    private void routeSearch(String start_id, String end_id, String way_points_id) {
//        String [] params = {
//                start_id,
//                end_id,
//                way_points_id,
//                null
//        };
//
//        if(way_points_id != null ) {
//            GetMultiRouteTask task = new GetMultiRouteTask(this);
//            task.execute(params);
//            if(kml_raw != null) {
//                for ( int i = 0; i < kml_raw.length; i++ ) {
//                    String url = null;
//                    if(kml_raw[i] !=null) {
//                        url = CommonData.getImageAddress();
//                        url += kml_raw[i];
//                        retrieveFileFromUrl(url);
//                    }
//                }
//            }
//        }
//        else {
//            GetRouteTask task2 = new GetRouteTask(this);
//            task2.execute(params);
//            if(kml_raw != null) {
//                for ( int i = 0; i < kml_raw.length; i++ ) {
//                    String url = null;
//                    if(kml_raw[i] !=null) {
//                        url = CommonData.getImageAddress();
//                        url += kml_raw[i];
//                        retrieveFileFromUrl(url);
//                    }
//                }
//            }
//        }
//    }

    // 경로탐색 결과
//    public void setRouteResult(RouteVO result) throws JSONException {
//        if(overlay != null)
//            overlay.setRouteResult(result, start_name, end_name);
//    }

//    public void setMultiRouteResult(List<RouteVO> result) throws JSONException {
//        if(overlay != null)
//            overlay.setMultiRouteResult(result, start_name, end_name, way_points_content);
//    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onDestroy ( ) {
        super.onDestroy ( );
        CommonData_ common = CommonData_.getInstance(this);
        if ( common != null )
            common.dispose ( );

        System.gc ( );
    }

    //반경검색 수행
    private void searchRadius() {
        mMap.clear();
        RadiusTask task = new RadiusTask ( this );

        Log.e("좌표", "x: "+point_x()+" y: "+point_y());
        String s = distance.getSelectedItem().toString().replace("km","");
        double d = Double.parseDouble(s);
        double dd = d * 0.01;               //meter 변환

        String[] params = { point_x(), point_y(), String.valueOf(dd) };

        task.execute ( params );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(this.toString(), "onRequestPermissionsResult: 지도퍼미션상태가" );
        if (requestCode == 1){
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.e(this.toString(), "onRequestPermissionsResult: 퍼미션 확인 완료" );
            } else {
                Toast.makeText(context, "지도보기 권한이 필요합니다!", Toast.LENGTH_SHORT).show();
                fileList();
            }
        }
    }

    //반경검색 결과
    public void setJsonResult(JSONObject result) throws JSONException {
        if(result == null) {
            Toast.makeText(getApplicationContext(), "현재 지도에 표시할 위치정보 데이터가 없습니다. 빠른 시일 내에 마련하겠습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        final int numberOfItemsInResp = result.length ( );

        geometry_data = new ArrayList<>();
        geometry_data2 = new ArrayList<>();

        for ( int i = 0; i < numberOfItemsInResp; i++ ) {
            RouteVO routeVO = new RouteVO();
            HealingContentsVO healingContentsVO = new HealingContentsVO();
            healingContentsVO.setHcnt_type( result.has ( "hcnt_type" ) ? result.getString ( "hcnt_type" ) : result.getString("type") );
            try {
                if( healingContentsVO.getHcnt_type().equals("line") || healingContentsVO.getHcnt_type().equals("zone") ) {
                    healingContentsVO.setHcnt_coord_x(result.getString("hcnt_coord_x"));
                    healingContentsVO.setHcnt_coord_y(result.getString("hcnt_coord_y"));
                    geometry_data2.add(healingContentsVO);
                } else {
                    routeVO.setGeometry(result.getString("geometry"));
                    geometry_data.add(routeVO);
                }
            }
            catch (NullPointerException e){
                Log.e ( this.toString() , e.toString ( ) );
            }
            if( i == 0 ) {
                moveCameraToKml();
            }
        }
    }

    @Override
    public void setJSONObjectResult(ArrayList<ScheduleVO> result) {
        for (ScheduleVO vo:
                result) {
            MarkerOptions markerOptions = new MarkerOptions();

            if(vo.getHealingContentsVO() != null){
                LatLng latLng = new LatLng(Double.parseDouble(vo.getHealingContentsVO().getHcnt_coord_x())
                        ,Double.parseDouble(vo.getHealingContentsVO().getHcnt_coord_y()));
                markerOptions.position(latLng);
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
            else if (vo.getSightSeeingContentsVO() != null){
                LatLng latLng = new LatLng(Double.parseDouble(vo.getSightSeeingContentsVO().getCcnt_coord_x())
                        ,Double.parseDouble(vo.getSightSeeingContentsVO().getCcnt_coord_y()));
                markerOptions.position(latLng);
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
            markerOptions.title(vo.getContentName());
            mMap.addMarker(markerOptions);
        }
        Log.e(this.toString(), "result.size(): "+result.size() );
    }

    private class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
        private final String mUrl;

        DownloadKmlFile(String url) {
            mUrl = url;
        }

        protected byte[] doInBackground(String... params) {
            try {
                InputStream is =  new URL(mUrl).openStream();
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[16384];
                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                return buffer.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(byte[] byteArr) {
            try {
                KmlLayer kmlLayer = new KmlLayer(mMap, new ByteArrayInputStream(byteArr), getApplicationContext());
                kmlLayer.addLayerToMap();
                moveCameraToKml();
            } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}