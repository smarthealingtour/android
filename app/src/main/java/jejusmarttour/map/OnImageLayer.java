package jejusmarttour.map;

import java.util.ArrayList;
import java.util.List;

import ktmap.android.map.overlay.Image;
import ktmap.android.map.overlay.ImageLayer;


/**
 * Created by User on 2014-10-14.
 */
public class OnImageLayer extends ImageLayer {

    ArrayList list = new ArrayList();


    @Override
    protected Image getIcon(int paramInt) {
        if(paramInt >= list.size()) return null;
        return (Image)list.get(paramInt);
    }

    @Override
    protected int size() {
        return list.size();
    }

    public void addItem(Image image) {
        list.add(image);
    }

    @Override
    public List getOverlayList() {
        return this.list;
    }

    public void removeAll(){
        list = new ArrayList();
    }

}
