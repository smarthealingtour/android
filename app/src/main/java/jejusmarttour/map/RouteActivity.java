package jejusmarttour.map;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import syl.com.jejusmarttour.R;

/**
 * Created by User on 2015-10-27.
 */
public class RouteActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 0;
    private Context mContext;
    private View layout_route;
    private  EditText start;
    private  EditText end;
    private String start_point_x = null;
    private String start_point_y = null;
    private String end_point_x = null;
    private String end_point_y = null;

    private String start_id = null;
    private String end_id = null;
    private String start_id_type = null;
    private String end_id_type = null;

    private int focused_editText = -1;

    private LinearLayout input_frame;
    private int edCount = 0;
    private String focused_ed_tag = "";
    private String way_points_id = "";
    private JSONArray way_points_content = new JSONArray();

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        init();
        wayPointAdd();
    }

    private void wayPointAdd() {
        input_frame = (LinearLayout)findViewById(R.id.input_frame);
        LinearLayout way_point_add = (LinearLayout)findViewById(R.id.way_point_add);

        way_point_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edCount > 2) {
                    Toast.makeText(RouteActivity.this, "경유지는 최대 3개 입력 가능합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(input_frame.getChildCount() > 2){
                    for(int i = 1; i < edCount+1; i++) {
                        LinearLayout temp = (LinearLayout)input_frame.getChildAt(i);
                        EditText temp2 = (EditText)temp.getChildAt(1);

                        if(temp2.length() == 0) {
                            Toast.makeText(RouteActivity.this, "경유지를 입력해 주세요", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }

                addRouteLayout();
            }
        });
    }

    private void addRouteLayout() {

        final LinearLayout way_point = new LinearLayout(RouteActivity.this);
        way_point.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        param.setMargins(0, 15, 0, 0);
        way_point.setPadding(0, 15, 0, 0);
        way_point.setLayoutParams(param);


        TextView tx = new TextView(RouteActivity.this);
        LinearLayout.LayoutParams tParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT );
        int px = util.dpToPixel(this, 10);
        tParam.setMargins( px, px, px, px );

        ++ edCount;

        tx.setText( "경유");
        tx.setLayoutParams(tParam);


        final EditText ed = new EditText(RouteActivity.this);
        ed.setPadding( util.dpToPixel(this, 25), 0, 0, 0 );
//                ed.setGravity(Gravity.CENTER);
        ed.setInputType(InputType.TYPE_CLASS_TEXT);
        ed.setMaxLines(1);
        ed.setSingleLine(true);
        LinearLayout.LayoutParams eParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 98F);

        ed.setTag("ed" + edCount);
        ed.setLayoutParams(eParam);

        ed.setOnTouchListener(new View.OnTouchListener() { // 출발
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){

                    focused_ed_tag = (String)ed.getTag();

                    Intent intent = new Intent(getApplicationContext(), yoyoSearchForRouteActivity.class);
                    intent.putExtra("title", "경유지 검색");
                    startActivityForResult(intent, REQUEST_CODE);
                }
                return false;
            }
        });

        ImageView delete_view = new ImageView(RouteActivity.this);
        delete_view.setScaleType(ImageView.ScaleType.FIT_XY);
        delete_view.setImageResource(R.drawable.ic_action_remove);
        LinearLayout.LayoutParams dvParam = new LinearLayout.LayoutParams( util.dpToPixel(this, 30), util.dpToPixel(this, 30), 2F);
        delete_view.setLayoutParams(dvParam);

        way_point.addView(tx);
        way_point.addView(ed);
        way_point.addView(delete_view);

        input_frame.addView(way_point, edCount);

        delete_view.setOnClickListener(new View.OnClickListener() { // 경유지 삭제
            @Override
            public void onClick(View view) {
                way_point.setPadding(0, -15, 0, 0);
                way_point.removeAllViews();
                edCount--;
            }
        });
    }

    private void init() {
        ActionBar actionBar = getSupportActionBar ( );
        actionBar.setDisplayShowHomeEnabled ( false );
        actionBar.setDisplayShowTitleEnabled(false);

        View actionbar_route = getLayoutInflater().inflate(R.layout.actionbar_route, null);
        actionBar.setCustomView(actionbar_route);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // 액션바 (뒤로가기)
        ImageButton back  = (ImageButton)actionbar_route.findViewById(R.id.back);
        back.setOnTouchListener(new View.OnTouchListener() { // 뒤로가기
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP)
                    finish();

                return false;
            }
        });

        //액션바 (라우트 설정)
        ImageButton route_setting = (ImageButton) actionbar_route.findViewById(R.id.route_setting);
        route_setting.setOnTouchListener(new View.OnTouchListener() { // 뒤로가기
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    routeSettingPopUp();
                }

                return false;
            }
        });

        start = (EditText)findViewById(R.id.start);
        start.setOnTouchListener(new View.OnTouchListener() { // 출발
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    Intent intent = new Intent(getApplicationContext(), yoyoSearchForRouteActivity.class);
                    intent.putExtra("title", "출발지 검색");
                    startActivityForResult(intent, REQUEST_CODE);
                }
                return false;
            }
        });

        end = (EditText)findViewById(R.id.end);
        end.setOnTouchListener(new View.OnTouchListener() { // 도착
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(getApplicationContext(), yoyoSearchForRouteActivity.class);
                    intent.putExtra("title", "도착지 검색");
                    startActivityForResult(intent, REQUEST_CODE);
                }
                return false;
            }
        });

        Button route_search = (Button)findViewById(R.id.route_search);
        route_search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {


                    if(start.getText().length() > 0 && end.getText().length() > 0){
                        if(start_id == null || end_id == null){
                            Toast.makeText(RouteActivity.this, "장소 아이디가 없습니다.", Toast.LENGTH_SHORT).show();

                        }
                        if( ( start_id_type.equals("point") && end_id_type.equals("point") ) && start_id.equals(end_id)) {
                            Toast.makeText(RouteActivity.this, "출발지 목적지가 동일합니다.", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Intent intent = new Intent();
                            intent.putExtra("start_point_x", start_point_x);
                            intent.putExtra("start_point_y", start_point_y);
                            intent.putExtra("end_point_x", end_point_x);
                            intent.putExtra("end_point_y", end_point_y);
                            intent.putExtra("start_name", start.getText().toString());
                            intent.putExtra("end_name", end.getText().toString());
                            intent.putExtra("start_id", start_id);
                            intent.putExtra("end_id", end_id);
                            intent.putExtra("way_points_id", way_points_id != "" ?way_points_id.substring(1) : null);
                            intent.putExtra("way_points_content", way_points_content.toString());
                            setResult(0, intent);
                            finish();
                        }
                    }else {
                        Toast.makeText(RouteActivity.this, "출발지, 목적지를 입력해주세요", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });


        List<EditText> editTextList = new ArrayList();
        editTextList.add(start);
        editTextList.add(end);
        for(int i = 0; i < editTextList.size(); i++) {
            editTextList.get(i).setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean focus) {
                    if(focus)
                        focused_editText = view.getId();

                }
            });
        }
    }

    private void routeSettingPopUp() {
        LinearLayout layout = ( LinearLayout ) View.inflate ( this , R.layout.popup_route_setting , null );
        RadioButton opton_1 = (RadioButton)layout.findViewById(R.id.option1);
        RadioButton opton_2 = (RadioButton)layout.findViewById(R.id.option2);
        RadioButton opton_3 = (RadioButton)layout.findViewById(R.id.option3);

//        AlertDialog.Builder alertbox = new AlertDialog.Builder ( this );
        Dialog alertbox = new Dialog ( this );
        alertbox.setCanceledOnTouchOutside(true);
        alertbox.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertbox.setContentView(layout);



        opton_2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Toast.makeText(RouteActivity.this, "준비중입니다.", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        opton_3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Toast.makeText(RouteActivity.this, "준비중입니다.", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        alertbox.show ( );

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data != null){
            switch (requestCode){
                case 0 :
                    if(data.getExtras().containsKey("name") &&
                            data.getExtras().containsKey("x") && data.getExtras().containsKey("y")) {
                        if (focused_editText == start.getId()) {
                            start.setText("");
                            start.setText(data.getExtras().getString("name"));
                            start.clearFocus();
                            start_point_x = data.getExtras().getString("x");
                            start_point_y = data.getExtras().getString("y");
                            start_id = data.getExtras().getString("id");
                            start_id_type = data.getExtras().getString("type");

                            focused_editText = -1;
                        }
                        else if(focused_editText == end.getId()) {
                            end.setText("");
                            end.setText(data.getExtras().getString("name"));
                            end.clearFocus();
                            end_point_x = data.getExtras().getString("x");
                            end_point_y = data.getExtras().getString("y");
                            end_id = data.getExtras().getString("id");
                            end_id_type = data.getExtras().getString("type");

                            focused_editText = -1;
                        }
                        else {

                            if(way_points_id.contains(data.getExtras().getString("id"))) {
                                Toast.makeText(this, "이미 추가한 경유지 입니다.", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            for(int i = 1; i < edCount+1; i++) {
                                LinearLayout temp = (LinearLayout)input_frame.getChildAt(i);
                                EditText temp2 = (EditText)temp.getChildAt(1);
                                if(temp2.getTag() != null){
                                    if(temp2.getTag().equals(focused_ed_tag)) {
                                        temp2.setText("");
                                        temp2.setText(data.getExtras().getString("name"));
                                        way_points_id +="," + data.getExtras().getString("id");

                                        try {
                                            JSONObject obj = new JSONObject();
                                            obj.put("name", data.getExtras().getString("name"));
                                            obj.put("address", data.getExtras().getString("address"));
                                            obj.put("tel", data.getExtras().getString("tel"));
                                            way_points_content.put(obj);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed ( )
    {
        super.onBackPressed ( );
        finish();
    }
}
