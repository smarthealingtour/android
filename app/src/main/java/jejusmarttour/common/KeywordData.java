package jejusmarttour.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import java.util.ArrayList;

import jejusmarttour.util.SmartTourUtils;
import jejusmarttour.vo.KeywordCategoryVO;
import jejusmarttour.vo.ScheduleVO;
import syl.com.jejusmarttour.R;

public class KeywordData {
	private static final String TAG = KeywordData.class.toString ( );
	// 힐링분류체계 및 분류코드
	private String [ ] healing = { "A0101" , "A0102" , "A0103" , "A0201" , "A0202" , "A0203" , "A0204" };

	// 감성분류체계 및 분류코드
	private String [ ] sensitivity = { "P0101" , "P0102" , "P0103" , "P0104" , "P0201" , "P0202" , "P0203" , "P0301" , "P0302" , "P0303" , "P0304" , "P0401" , "P0402" , "P0403" , "P0404" , "P0501" , "P0502" , "P0503" , "P0504" , "P0505" };

	// 관광장소분류체계 및 분류코드
	private String [ ] sightseeing = { "D0101" , "D0102" , "D0201" , "D0202" , "D0203" , "D0301" , "D0302" , "D0401" , "D0402" };

	// 키워드별 이미지 아이디값 가져오기
	public static int getKeywordIconImage ( String keyword )
	{
		int drawable = 0;

		if ( keyword.contains ( "A" ) )
		{
			if ( keyword.contains ( "A01" ) )
			{
				drawable = R.drawable.healing_keyword_nature;
			}
			else if ( keyword.contains ( "A02" ) )
			{
				drawable = R.drawable.healing_keyword_culture;
			}
		}
		else if ( keyword.contains ( "P" ) )
		{
			if ( keyword.contains ( "P01" ) )
			{
				drawable = R.drawable.sensitivity_keyword_sight;
			}
			else if ( keyword.contains ( "P02" ) )
			{
				drawable = R.drawable.sensitivity_keyword_taste;
			}
			else if ( keyword.contains ( "P03" ) )
			{
				drawable = R.drawable.sensitivity_keyword_exercise;
			}
			else if ( keyword.contains ( "P04" ) )
			{
				drawable = R.drawable.sensitivity_keyword_emotion;
			}
			else if ( keyword.contains ( "P05" ) )
			{
				drawable = R.drawable.sensitivity_keyword_intelligent;
			}
		}
		else if ( keyword.contains ( "D" ) )
		{

		}

		return drawable;
	}

	// 키워드 아이콘 이미지 가져오기
	public static ArrayList < Integer > getKeywordIconImage ( String [ ] hcdArray , String [ ] scdArray )
	{
		ArrayList < Integer > keywordImageId = new ArrayList < Integer > ( );

		int hcdLenth = 0;
		int scdLenth = 0;

		try
		{
			hcdLenth = hcdArray.length;
			scdLenth = scdArray.length;
		}
		catch ( NullPointerException e )
		{
		}

		if ( ( hcdLenth + scdLenth ) <= 0 )
		{
			return null;
		}

		if ( hcdLenth > 0 )
		{
			for ( int i = 0; i < hcdLenth; i++ )
			{
				if ( keywordImageId.contains ( KeywordData.getKeywordIconImage ( hcdArray [ i ] ) ) == false )
					keywordImageId.add ( KeywordData.getKeywordIconImage ( hcdArray [ i ] ) );
			}
		}

		if ( scdLenth > 0 )
		{
			for ( int i = 0; i < scdLenth; i++ )
			{
				if ( keywordImageId.contains ( KeywordData.getKeywordIconImage ( scdArray [ i ] ) ) == false )
					keywordImageId.add ( KeywordData.getKeywordIconImage ( scdArray [ i ] ) );
			}
		}

		return keywordImageId;
	}

	// 맞춤키워드 선택된 키워드 텍스트뷰 가져오기
	@SuppressWarnings ( "deprecation" )
	public static TextView getSelectedKeywordTextView ( Context context , String keyword , ArrayList <KeywordCategoryVO> arrayList )
	{
		if ( arrayList == null )
		{
			Log.e ( TAG , "키워드 목록을 불러오지 못했습니다." );
			return null;
		}

		int wordLenth = 1;
		String textStr = null;

		for ( int i = 0; i < arrayList.size ( ); i++ )
		{
			if ( arrayList.get ( i ).getCode ( ).equals ( keyword ) )
			{
				textStr = SmartTourUtils.getValidateKeywordName(arrayList.get(i).getCode_name());
				wordLenth = textStr.length ( );
			}
		}

		DisplayMetrics dm = context.getResources ( ).getDisplayMetrics ( );

		int [ ] ATTRS = new int [ ] { android.R.attr.textSize , android.R.attr.textColor };

		TypedArray a = context.obtainStyledAttributes ( null , ATTRS );

		int textSize = 11;
		textSize = a.getDimensionPixelSize ( 0 , textSize );

		int widthSize = 20 + ( 10 * wordLenth );
		int bgWidth = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , widthSize , dm );
		int bgHeight = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 25 , dm );

		TextView textView = new TextView ( context );
		textView.setLayoutParams ( new LayoutParams ( bgWidth , bgHeight) );
		textView.setSingleLine ( );
		textView.setGravity ( Gravity.CENTER );
		textView.setTextSize ( textSize );
		textView.setTextColor ( context.getResources ( ).getColor ( R.color.white ) );

		if ( keyword.contains ( "A01" ) )
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_nature_selected ) );
		else if ( keyword.contains ( "A02" ) )
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_culture_selected ) );
		else if ( keyword.contains ( "P01" ) )
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_sight_selected ) );
		else if ( keyword.contains ( "P02" ) )
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_taste_selected ) );
		else if ( keyword.contains ( "P03" ) )
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_exercise_selected ) );
		else if ( keyword.contains ( "P04" ) )
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_emotion_selected ) );
		else if ( keyword.contains ( "P05" ) )
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_intelligent_selected ) );

		textView.setText ( textStr );

		return textView;
	}

	// 상품정보 하단 키워드 텍스트뷰 가져오기
	@SuppressWarnings ( "deprecation" )
	public static TextView getKeywordDetailTextView ( Context context , String keyword , ArrayList < KeywordCategoryVO > arrayList )
	{
		if ( arrayList == null )
		{
			Log.e ( TAG , "키워드 목록을 불러오지 못했습니다." );
			return null;
		}
		
		int wordLenth = 1;
		String textStr = null;
		
		keyword = SmartTourUtils.replaceSpaceToBlank ( keyword );
		
		for ( int i = 0 ; i < arrayList.size ( ) ; i++ )
		{
			//Log.d ( TAG , "arrayList.get ( i ).getCode ( ) : " + arrayList.get ( i ).getCode ( ) + " / keyword : " + keyword + "equal : " + arrayList.get ( i ).getCode ( ).equals (  keyword) );
			
			if(arrayList.get ( i ).getCode ( ).equals (  keyword ) == true ) 
			{
				textStr = SmartTourUtils.getValidateKeywordName ( arrayList.get ( i ).getCode_name ( ) );
				wordLenth = textStr.length ( );
			}
		}
		
		DisplayMetrics dm = context.getResources ( ).getDisplayMetrics ( );
		
		int [ ] ATTRS = new int [ ] { android.R.attr.textSize , android.R.attr.textColor };
		
		TypedArray a = context.obtainStyledAttributes ( null , ATTRS );
		
		int textSize = 11;
		textSize = a.getDimensionPixelSize ( 0 , textSize );
		
		int widthSize = 20 + ( 10 * wordLenth );
		int bgWidth = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , widthSize , dm );
		int bgHeight = ( int ) TypedValue.applyDimension ( TypedValue.COMPLEX_UNIT_DIP , 25 , dm );

		TextView textView = new TextView ( context );
		textView.setLayoutParams ( new LayoutParams ( bgWidth , bgHeight ) );
		textView.setSingleLine ( );
		textView.setGravity ( Gravity.CENTER );
		textView.setTextSize ( textSize );
		textView.setTextColor ( context.getResources ( ).getColor ( R.color.font_color ) );
		
		if ( keyword.contains ( "A01" ) )
		{
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_nature_not ) );
		}
		else if ( keyword.contains ( "A02" ) )
		{
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_culture_not ) );
		}
		else if ( keyword.contains ( "P01" ) )
		{
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_sight_not ) );
		}
		else if ( keyword.contains ( "P02" ) )
		{
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_taste_not ) );
		}
		else if ( keyword.contains ( "P03" ) )
		{
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_exercise_not ) );
		}
		else if ( keyword.contains ( "P04" ) )
		{
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_emotion_not ) );
		}
		else if ( keyword.contains ( "P05" ) )
		{
			textView.setBackground ( context.getResources ( ).getDrawable ( R.drawable.shape_intelligent_not ) );
		}
		
		textView.setText ( textStr );
		
		return textView;
	}

	//키워드 카테고리 가져오기
	public static String getKeyword ( ScheduleVO vo )
	{
		String str = null;

		if ( vo.getHealingContentsVO( ) != null )
		{
			if ( vo.getHealingContentsVO( ).getHcnt_hcd_name ( ) != null )
			{
				if ( vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).size ( ) > 0 )
					str = vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getHealingContentsVO( ).getHcnt_hcd_name ( ).get ( 0 ).getCode_name ( );
			}
			else if ( vo.getHealingContentsVO( ).getHcnt_scd_name ( ) != null )
			{
				if ( vo.getHealingContentsVO( ).getHcnt_scd_name ( ).size ( ) > 0 )
					str = vo.getHealingContentsVO( ).getHcnt_scd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getHealingContentsVO( ).getHcnt_scd_name ( ).get ( 0 ).getCode_name ( );
			}
		}
		else if ( vo.getSightSeeingContentsVO( ) != null )
		{
			if ( vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ) != null )
			{
				if ( vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).size ( ) > 0 )
					str = vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).get ( 0 ).getClassL ( ) + " > " + vo.getSightSeeingContentsVO( ).getCcnt_cd_name ( ).get ( 0 ).getCode_name ( );
			}
		}

		return str;
	}

}
