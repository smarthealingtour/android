package jejusmarttour.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.multidex.MultiDex;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.kakao.auth.KakaoSDK;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.helper.log.Logger;
import com.tsengvn.typekit.Typekit;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import jejusmarttour.KakaoSDKAdapter;
import jejusmarttour.mypage.MyPageActivity;
import jejusmarttour.user.UserVO;
import jejusmarttour.vo.KeywordCategoryVO;
import jejusmarttour.vo.MTCSmartTourProductsVO;
import jejusmarttour.vo.MyTourCourseRequestVO;
import jejusmarttour.vo.ScheduleVO;
import jejusmarttour.vo.SearchProductRequestVO;
import jejusmarttour.vo.SmartTourProductsVO;
import syl.com.jejusmarttour.R;

public class CommonData extends Application{
	// 유저 정보
	private static UserVO userVO;
	private static UserProfile userProfile;

	private static ScheduleVO scheduleVO;

	private static SmartTourProductsVO smartTourProductsVO;

	private MTCSmartTourProductsVO mtcSmartTourProductsVO;

	private static ArrayList < KeywordCategoryVO > keywordArrayList;

	private ArrayList < ScheduleVO > makeScheduleArrayList = new ArrayList < ScheduleVO > ( );

	public static String recentCname = "";

	private String sortType = "date";

	//탭클릭시 글자색 변하게 하기위해 만듬. onCreate()에서 설정
	private ColorStateList tabTextColorStateList;

	public ArrayList < ScheduleVO > getMakeScheduleArrayList ( ){
		return makeScheduleArrayList;
	}

	public static String getUserProfileImage ( Context context ){
		SharedPreferences pref = context.getSharedPreferences ( "JeJuSmartPref" , Activity.MODE_PRIVATE );
		String value = pref.getString ( "USER_IMAGE_PATH" , "" );
		return value;
	}

	public static void setUserProfileImage ( Context context , String imagePath ){
		SharedPreferences pref = context.getSharedPreferences ( "JeJuSmartPref" , Activity.MODE_PRIVATE );
		SharedPreferences.Editor editor = pref.edit ( );
		editor.putString ( "USER_IMAGE_PATH" , imagePath );
		editor.commit ( );
	}

	public MTCSmartTourProductsVO getMtcSmartTourProductsVO ( )
	{
		return mtcSmartTourProductsVO;
	}

	public void setMtcSmartTourProductsVO ( MTCSmartTourProductsVO mtcSmartTourProductsVO ){
		Log.e("CommonData", "setMtcSmartTourProductsVO ");
		this.mtcSmartTourProductsVO = mtcSmartTourProductsVO;
	}

	public void setMakeScheduleArrayList ( ArrayList < ScheduleVO > makeScheduleArrayList ){
		/*ArrayList<ScheduleVO> copyMakeScheduleArrayList = new ArrayList<>();

		for (int i = 0 ; i < makeScheduleArrayList.size(); i++){
			ScheduleVO copyVO;
			try {
				copyVO = makeScheduleArrayList.get(i).clone();
				copyMakeScheduleArrayList.add(copyVO);
			}catch (CloneNotSupportedException e){
				e.printStackTrace();
			}
		}
*/
		this.makeScheduleArrayList = makeScheduleArrayList;
	}

	public String getSortType ( ){
		return sortType;
	}

	public void setSortType ( String sortType ){
		this.sortType = sortType;
	}

	public static UserProfile getUserProfile ( ){
		return userProfile;
	}

	public static void setUserProfile ( UserProfile userProfile ){
		CommonData.userProfile = userProfile;
	}

	public static UserVO getUserVO ( ){
		return userVO;
	}

	public static void setUserVO ( UserVO userVO ){
		CommonData.userVO = userVO;
	}

	public static ScheduleVO getScheduleVO ( ){
		return scheduleVO;
	}

	public static void setScheduleVO ( ScheduleVO scheduleVO ){
		CommonData.scheduleVO = scheduleVO;
	}

	public static SmartTourProductsVO getSmartTourProductsVO ( ){
		return smartTourProductsVO;
	}

	public static void setSmartTourProductsVO ( SmartTourProductsVO smartTourProductsVO ){
		CommonData.smartTourProductsVO = smartTourProductsVO;
	}

	public static ArrayList < KeywordCategoryVO > getKeywordArrayList ( ){
		return keywordArrayList;
	}

	public static ArrayList < KeywordCategoryVO > getKeywordArrayList ( ArrayList < KeywordCategoryVO > returnList , String gubun ){
		for ( int i = 0; i < keywordArrayList.size ( ); i++ ){

			KeywordCategoryVO data = keywordArrayList.get ( i );
			Log.e ( "qtam" , "qtam" + data.getArrayKeyword ( ) );
			if ( data.getArrayKeyword ( ) != null ){

				if ( gubun.equals ( "D" ) ){
					if ( data.getL_class ( ).equals ( "D" ) )
					{ // 관광
						returnList.add ( data );
					}
				}
				else if ( gubun.equals ( "AP" ) ){
					if ( data.getL_class ( ).equals ( "A" ) || data.getL_class ( ).equals ( "P" ) )
					{
						returnList.add ( data );
					}
				}
				else{
					returnList.add ( data );
				}
			}
		}

		Log.e ( "qtam" , "qtam" + returnList.size ( ) );

		return returnList;
	}

	public static void setKeywordArrayList ( ArrayList < KeywordCategoryVO > keywordArrayList ){
		CommonData.keywordArrayList = keywordArrayList;
	}

//    private static final String ADDRESS = "http://192.168.0.42:8080/Jsmth/restful/"; // 개발서버
//    private static final String IMAGE_ADDRESS = "http://192.168.0.42:8080/Jsmth_image"; //개발서버

//	private static final String ADDRESS = "http://ubist.iptime.org:8084/Jsmth/restful/"; // 개발서버
//	private static final String IMAGE_ADDRESS = "http://ubist.iptime.org:8084/Jsmth_image"; //개발서버

    private static final String ADDRESS = "http://203.253.207.137:8080/Jsmth/restful/"; //실서버
    private static final String IMAGE_ADDRESS = "http://203.253.207.137:8080/Jsmth_image";// 실서버

	public static String getAddress ( )
	{
		return ADDRESS;
	}

	// 회원체크
	public static boolean isMemberAddres ( )
	{
		return true;
	}

	// 사용자 확인
	public static String getCheckUserIdAddress ( String userId ){
		return ADDRESS + "get_user?email=" + userId;
	}

	// 사용자 닉네임 업데이트
	public static String getNickNameUpdateAddres ( String userId , String nickName ){
		return ADDRESS + "add_user?email=" + userId + "&nickname=" + nickName;
	}

	// 분류 목록(힐링,감성,관광)
	public static String getKeywordCategory ( String category ){
		return ADDRESS + "classify?l_class=" + category;
	}

	// 힐링여행상품 전체보기 / 전문가 / 함께
	public static String searchHTPAddres ( SearchProductRequestVO vo ) throws UnsupportedEncodingException{
		String requestParam = "htour";

		if ( vo.getHt_name ( ) != null ){
			requestParam += "?ht_name=" + URLEncoder.encode ( vo.getHt_name ( ).toString ( ) , "UTF-8" );
		}

		if ( vo.getType ( ) != null ){
			if ( requestParam.contains ( "?" ) )
			{
				requestParam += "&type=" + vo.getType ( );
			}
			else{
				requestParam += "?type=" + vo.getType ( );
			}
		}

		if ( vo.getSort ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&sort=" + vo.getSort ( );
			}
			else{
				requestParam += "?sort=" + vo.getSort ( );
			}
		}

		if ( vo.getPage ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&page=" + vo.getPage ( );
			}
			else{
				requestParam += "?page=" + vo.getPage ( );
			}
		}

		if ( vo.getHcd ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&hcd=" + vo.getHcd ( );
			}
			else{
				requestParam += "?hcd=" + vo.getHcd ( );
			}
		}

		if ( vo.getScd ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&scd=" + vo.getScd ( );
			}
			else{
				requestParam += "?scd=" + vo.getScd ( );
			}
		}

		if ( vo.getCcd ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&ccd=" + vo.getCcd ( );
			}
			else{
				requestParam += "?ccd=" + vo.getCcd ( );
			}
		}

		return ADDRESS + requestParam;
	}

	// 힐링여행상품 전체보기 / 전문가 / 함께
	public static String searchSpotAddress(SearchProductRequestVO vo ) throws UnsupportedEncodingException{
		String requestParam = "content";

		if ( vo.getHt_name ( ) != null ){
			requestParam += "?hcnt_name=" + URLEncoder.encode ( vo.getHt_name ( ).toString ( ) , "UTF-8" );
		}

		if ( vo.getType ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&type=" + vo.getType ( );
			}
			else{
				requestParam += "?type=" + vo.getType ( );
			}
		}

		if ( vo.getSort ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&sort=" + vo.getSort ( );
			}
			else{
				requestParam += "?sort=" + vo.getSort ( );
			}
		}

		if ( vo.getPage ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&page=" + vo.getPage ( );
			}
			else{
				requestParam += "?page=" + vo.getPage ( );
			}
		}

		if ( vo.getHcd ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&hcd=" + vo.getHcd ( );
			}
			else{
				requestParam += "?hcd=" + vo.getHcd ( );
			}
		}

		if ( vo.getScd ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&scd=" + vo.getScd ( );
			}
			else{
				requestParam += "?scd=" + vo.getScd ( );
			}
		}

		if ( vo.getCcd ( ) != null ){
			if ( requestParam.contains ( "?" ) ){
				requestParam += "&ccd=" + vo.getCcd ( );
			}
			else {
				requestParam += "?ccd=" + vo.getCcd ( );
			}
		}
		return ADDRESS + requestParam;
	}

	public static String getImageAddress ( )
	{
		return IMAGE_ADDRESS;
	}

	// 이미지 경로
	public static String getImageAddress ( String imageArg )
	{
		return IMAGE_ADDRESS + imageArg;
	}

	// 힐링여행상품 상품검색
	public static String getSearchHealingTourProductCourseAddres ( String ht_Id ) throws UnsupportedEncodingException
	{
		return ADDRESS + "htour_schedule?ht_id=" + URLEncoder.encode ( ht_Id , "UTF-8" );
	}

	// 힐링여행상품 코스검색
	public static String getContentsAddres ( String type , String ht_Id ) throws UnsupportedEncodingException
	{
		if ( type.equals ( "hcnt" ) )
			return ADDRESS + "content_id?hcnt_id=" + URLEncoder.encode ( ht_Id , "UTF-8" );
		else
			return ADDRESS + "content_id?ccnt_id=" + URLEncoder.encode ( ht_Id , "UTF-8" );
	}

	// 분류 목록(힐링,감성,관광)
	public static String getKeywordListAddres ( )
	{
		return ADDRESS + "classify?l_class=P";
	}

	// 힐링여행상품 리뷰
	public static String getHealingTourProductReviewAddress ( String htpId , String page ) throws UnsupportedEncodingException
	{
		return ADDRESS + "review?ht_id=" + URLEncoder.encode ( htpId , "UTF-8" ) + "&page=" + page;
	}

	// 힐링여행상품 리뷰 카운트
	public static String getHTPReviewCountAddress ( String type , String productId ) throws UnsupportedEncodingException
	{
		if ( type.equals ( "ht" ) )
			return ADDRESS + "review_count?ht_id=" + URLEncoder.encode ( productId , "UTF-8" );
		else
			return ADDRESS + "review_count?ht_id=" + URLEncoder.encode ( productId , "UTF-8" );
	}

	// 리뷰 업데이트
	public static String updateReviewAddress ( String userId , String productId , String reviewStr , boolean isPro ) throws UnsupportedEncodingException
	{
		if ( isPro == true )
			return ADDRESS + "add_review?email=" + URLEncoder.encode ( userId , "UTF-8" ) + "&ht_id=" + URLEncoder.encode ( productId , "UTF-8" ) + "&comment_subt=" + URLEncoder.encode ( reviewStr , "UTF-8" );
		else
			return ADDRESS + "add_review?email=" + URLEncoder.encode ( userId , "UTF-8" ) + "&ht_id=" + URLEncoder.encode ( productId , "UTF-8" ) + "&comment_subt=" + URLEncoder.encode ( reviewStr , "UTF-8" );
	}

	// 힐링여행상품 담기/지우기
	public static String getHTPCartAddress ( String type , String email , String productId , boolean isUser ) throws UnsupportedEncodingException
	{
		return ADDRESS + "cart_htour?type=" + type + "&email=" + CommonData.getUserVO ( ).getEmail ( ) + "&ht_id=" + URLEncoder.encode ( productId , "UTF-8" );
	}

	// 힐링명소 담기/지우기
	public static String getContentCartAddress ( String type , String email , String productId , String isCcnt ) throws UnsupportedEncodingException
	{
		if ( isCcnt.equals ( "true" ) )
		{
			return ADDRESS + "cart_ccnt?type=" + type + "&email=" + CommonData.getUserVO ( ).getEmail ( ) + "&ccnt_id=" + URLEncoder.encode ( productId , "UTF-8" );
		}
		else
		{
			return ADDRESS + "cart_hcnt?type=" + type + "&email=" + CommonData.getUserVO ( ).getEmail ( ) + "&hcnt_id=" + URLEncoder.encode ( productId , "UTF-8" );
		}
	}

	public static String getGeometry_ ( String id , boolean isHtour , boolean isHcnt , int schedule ) throws UnsupportedEncodingException
	{
		if ( isHtour )
			return ADDRESS + "geometry?ht_id=" + URLEncoder.encode ( id , "UTF-8" ) + "&schedule=" + schedule;
		else
		{
			if ( isHcnt )
				return ADDRESS + "geometry_content?hcnt_id=" + URLEncoder.encode ( id , "UTF-8" );
			else
				return ADDRESS + "geometry_content?ccnt_id=" + URLEncoder.encode ( id , "UTF-8" );
		}
	}

	public static String getradius ( String x , String y , String distance ) throws UnsupportedEncodingException
	{
		return ADDRESS + "radius?x=" + x + "&y=" + y + "&bound=" + distance;
	}

    public static String getMyHtourGeomtry ( String ids ) throws UnsupportedEncodingException
    {
        return ADDRESS + "schedule_day_cnt?ids=" + URLEncoder.encode ( ids, "UTF-8" );
    }

	public static String getAdress ( String x , String y ) throws UnsupportedEncodingException
	{
		return ADDRESS + "coord2addr?x=" + x + "&y=" + y;
	}

	// 나의여행코스 가져오기
	public static String getMyTourCourseAddress ( MyTourCourseRequestVO vo )
	{
		return ADDRESS + "my_list?type=" + vo.getType ( ) + "&email=" + vo.getEmail ( ) + "&page=" + vo.getPage ( );
	}

	// 사용자 힐링여행상품 곻유
	public static String getMTCShareUpdateAddress ( String email , String productId ) throws UnsupportedEncodingException
	{
		return ADDRESS + "share?email=" + CommonData.getUserVO ( ).getEmail() + "&ht_id=" + URLEncoder.encode(productId, "UTF-8");
	}

	// 사용자 힐링여행상품 삭제
	public static String getMTCShareDeleteAddress ( String email , String productId ) throws UnsupportedEncodingException
	{
		return ADDRESS + "delete_htour?email=" + CommonData.getUserVO ( ).getEmail ( ) + "&ht_id=" + URLEncoder.encode ( productId , "UTF-8" );
	}

	// 마이리스트 힐링명소, 관광장소 가져오기
	public static String getMyListAddress ( String type , String page ) throws UnsupportedEncodingException
	{
		if ( page == null )
			return ADDRESS + "my_list?email=" + CommonData.getUserVO ( ).getEmail ( ) + "&type=" + type;
		else
			return ADDRESS + "my_list?email=" + CommonData.getUserVO ( ).getEmail ( ) + "&type=" + type + "&page=" + page;
	}
	//이미지 저장
	public static String getMTCUploadAddress()
	{
//		return ADDRESS + "add_tour_product";
		return ADDRESS + "add_htour";
	}

	// 위치 아이콘 아이디값 가져오기
	public static int getLocationIconId ( String type ){
		int id = 0;
		if (type == null){
			//요거요거
			id = R.drawable.location_icon;
		}
		else if ( type.equals ( "point" ) ){
			id = R.drawable.location_icon;
		}
		else if ( type.equals ( "line" ) ){
			id = R.drawable.location_2d_icon;
		}
		else if ( type.equals ( "zone" ) ){
			id = R.drawable.location_3d_icon;
		}

		return id;
	}

	// 거리 시간 정보 가져오기
	public static String getRouteAddress ( String start_id , String end_id ) throws UnsupportedEncodingException{
		return ADDRESS + "route?start_id=" + URLEncoder.encode ( start_id , "UTF-8" ) + "&end_id=" + URLEncoder.encode ( end_id , "UTF-8" );
	}

    public static String getMultiRouteAddress ( String start_id , String end_id, String way_points_id ) throws UnsupportedEncodingException{
        return ADDRESS + "route_waypoint?start_id=" + URLEncoder.encode ( start_id , "UTF-8" ) +"&end_id=" + URLEncoder.encode ( end_id , "UTF-8" ) + "&way_point_id=" + URLEncoder.encode ( way_points_id , "UTF-8" );
    }

	// 내정보 페이지 이동
	public static void gotoMyInfo ( Context context ){
		Intent intent = new Intent ( context , MyPageActivity.class );
		context.startActivity ( intent );
		// ( ( Activity ) context ).finish ( );
	}

	/**
	 * onConfigurationChanged() 컴포넌트가 실행되는 동안 단말의 화면이 바뀌면 시스템이 실행 한다.
	 */
	@Override
	public void onConfigurationChanged ( Configuration newConfig ){
		super.onConfigurationChanged ( newConfig );
	}

	@Override
	protected void attachBaseContext ( Context base ){
		super.attachBaseContext ( base );
		MultiDex.install ( this );
	}

    private static volatile CommonData instance = null;
    private static volatile Activity currentActivity = null;
    private ImageLoader imageLoader;

    public static Activity getCurrentActivity() {
        Logger.d("++ currentActivity : " + (currentActivity != null ? currentActivity.getClass().getSimpleName() : ""));
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        CommonData.currentActivity = currentActivity;
    }

    /**
     * singleton 애플리케이션 객체를 얻는다.
     * @return singleton 애플리케이션 객체
     */
    public static CommonData getGlobalApplicationContext() {
        if(instance == null)
            throw new IllegalStateException("this application does not inherit com.kakao.GlobalApplication");
        return instance;
    }

	public ColorStateList getTabTextColorStateList() {
		return tabTextColorStateList;
	}

	/**
     * 이미지 로더, 이미지 캐시, 요청 큐를 초기화한다.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        KakaoSDK.init(new KakaoSDKAdapter());

        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        ImageLoader.ImageCache imageCache = new ImageLoader.ImageCache() {
            final LruCache<String, Bitmap> imageCache = new LruCache<String, Bitmap>(30);

            @Override
            public void putBitmap(String key, Bitmap value) {
                imageCache.put(key, value);
            }

            @Override
            public Bitmap getBitmap(String key) {
                return imageCache.get(key);
            }
        };

        imageLoader = new ImageLoader(requestQueue, imageCache);

        Typekit.getInstance()
				.addNormal(Typekit.createFromAsset(this, "BMJUA_ttf.ttf"));
		int[][] states = new int[][]{new int[]{ android.R.attr.state_pressed},new int[]{} }; //첫번째는 클릭, 두번째꺼는 평상시
		int[] color = new int[]{Color.parseColor("#ED6148"),Color.parseColor("#575757")};

        tabTextColorStateList = new ColorStateList(states, color);

    }

    /**
     * 이미지 로더를 반환한다.
     * @return 이미지 로더
     */
    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    /**
     * 애플리케이션 종료시 singleton 어플리케이션 객체 초기화한다.
     */
    @Override
	public void onTerminate() {
		super.onTerminate();
		instance = null;
	}
}
